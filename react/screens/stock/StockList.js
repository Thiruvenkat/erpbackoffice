/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  FlatList,
  StyleSheet,
  View,
  Text,
  Image,
  StatusBar,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import {
  Card,
  CardItem,
  Body,
  Icon,
  Left,
  InputGroup,
  Input,
  Button,
} from 'native-base';
import MyIcon from 'react-native-vector-icons/FontAwesome';
import AppConstants from '../../constant/AppConstants';
import { Styles, STATUS_COLOR } from '../../style/Styles';
import Colors from '../../style/Colors';
import {
  RemoveSeparator,
  NumberFormat,
  DateFormat,
  DATE_TIME_UI,
} from 'react-native-erp-mobile-library/react/common/Common';
import { GetAllStockList } from 'react-native-erp-mobile-library/react/screens/stock/dbHelper/StockDBHelper';
import { getAllStockList } from 'react-native-erp-mobile-library/react/controller/SyncDataController';
import PreparingProgress from 'react-native-erp-mobile-library/react/component/PreparingProgress';
import { sessionInfo } from 'react-native-erp-mobile-library';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import { renderHeaderView } from 'react-native-erp-mobile-library/react/util/HeaderUtil';
import { messages } from 'react-native-erp-mobile-library/react/i18n/i18n';
import ProductFilter, {
  FilterTypes,
} from 'react-native-erp-mobile-library/react/screens/products/ProductFilter';
import { GetAllProductDetails } from 'react-native-erp-mobile-library/react/screens/products/dbHelper/ProductDB';
import Toast from 'react-native-simple-toast';

const blankImage = require('../../../assets/blank.png');

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

// create a component
export default class StockList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      stockLists: [],
      session: sessionInfo({ type: 'get', key: 'loginInfo' }),
      filterDate: DateFormat(new Date(), DATE_TIME_UI),
      loadMore: false,
      offset: 0,
      limit: 10,
      conditions: [
        {
          value: '0',
          column: 'quantity',
          operator: '>',
          dataType: 'String',
        },
      ],
      filterTypes: [
        {
          type: FilterTypes.ProductType,
          filters: [],
          selectedValues: {},
          column: 'product_type_id',
        },
        {
          type: FilterTypes.CategoryType,
          filters: [],
          selectedValues: {},
          column: 'category_id',
        },
        {
          type: FilterTypes.stockAccount,
          filters: [],
          selectedValues: {},
          column: 'stock_account_id',
        },
        {
          type: FilterTypes.status,
          filters: [],
          selectedValues: {},
          column: 'status_id',
        },
        {
          type: FilterTypes.brand,
          filters: [],
          selectedValues: {},
          column: 'brand_id',
        },
      ],
      searchQuery: '',
      productExponent: 2,
    };
  }

  componentDidMount() {
    this._initHeader();
    this._loadFiltersFromStorage();
    this._initProductExponentData();
  }

  componentWillUnmount() {
    AsyncStorage.setItem(AppConstants.keyStockScreenFilters, '');
    // this.backHandler.remove();
  }

  _initHeader() {
    this.props.navigation.setParams({
      headerTitle: this.props.route.params?.headerTitle ?? 'Stock',
      headerType:
        this.props.route.params?.headerType ?? HeaderTypes.HeaderHomeMenu,
    });
    this.props.navigation.setOptions(renderHeaderView(this));
  }

  _initProductExponentData() {
    var filter = {
      limit: 1,
      offset: 0,
      orderBy: 'last_updated desc',
    };
    GetAllProductDetails(datas => {
      let productExponent = 2;
      if (datas[0] && datas[0].exponent) {
        productExponent = datas[0].exponent;
        this.state.productExponent = productExponent;
      }
    }, filter);
  }

  onPressApplyButton = filterTypes => {
    this.state.filterTypes = filterTypes;
    this.state.offset = 0;
    this.state.isProductFilterEnabled = false;
    this._getAllStockList();
    AsyncStorage.setItem(AppConstants.keyStockScreenFilters, JSON.stringify(filterTypes));
  };

  _loadFiltersFromStorage() {
    AsyncStorage.getItem(AppConstants.keyStockScreenFilters).then(filters => {
      if (filters) {
        this.setState(
          { stockLists: [], offset: 0, filterTypes: JSON.parse(filters) },
          () => {
            this._getAllStockList();
          },
        );
      } else {
        this._getAllStockList();
      }
    });
  }

  _getAllStockList() {
    var filter = {
      limit: this.state.limit,
      offset: this.state.offset,
      orderBy: 'last_update_time desc',
      conditions: [],
    };

    if (this.state.searchQuery.length > 0) {
      filter.conditions = [
        {
          value: '%' + this.state.searchQuery + '%',
          column: 'product_name',
          operator: 'like',
        },
        ...filter.conditions,
      ];
    }

    this.filtersCount = 0;
    if (this.state.filterTypes.length > 0) {
      this.state.filterTypes.forEach(filterType => {
        const values = Object.keys(filterType.selectedValues);
        if (values.length > 0) {
          this.filtersCount += values.length;
          filter.conditions = [
            {
              value: values,
              column: filterType.column,
              operator: 'in',
            },
            ...filter.conditions,
          ];
        }
      });
    }
    GetAllStockList(datas => {
      let productMap = {};
      if (this.state.searchQuery.length === 0) {
        if (this.state.offset !== 0) {
          this.state.stockLists.forEach(product => {
            productMap[product.stock_id] = product;
          });
        }
      }
      datas.forEach(product => {
        productMap[product.stock_id] = product;
      });

      let valuess = Object.keys(productMap).length > 0 ? Object.values(productMap) : datas;
      if (datas.length > 0 || this.state.searchQuery.length > 0 || this.filtersCount > 0) {
        this.setState({
          stockLists: valuess,
          isLoading: false,
          loadMore: false,
          offset: this.state.searchQuery.length > 0 ? 0 : this.state.offset + datas.length,
          isServiceCalled: false,
        });
      } else if (!this.state.isServiceCalled) {
        this._getAllStockListFromServer();
      } else {
        this.setState({ isLoading: false, loadMore: false });
      }
    }, filter);
  }

  _getAllStockListFromServer() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        let syncValue = {
          limit: this.state.limit,
          offset: this.state.offset,
          staticConditions: [
            {
              column: 'lastUpdated',
              value: '1483228800000',
              dataType: 'Date',
              operator: '>',
            },
          ],
        };
        getAllStockList(this.state.session, syncValue, isSuccess => {
          if (isSuccess) {
            this._getAllStockList();
          }
        });
      } else {
        this.setState({ isLoading: false, loadMore: false });
        console.log('make internet connection..!');
      }
    });
  }

  onRefresh() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        this.setState({ offset: 0 }, () => {
          this._getAllStockListFromServer();
        });
      } else {
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    });
  }

  renderItem({ item }) {
    return (
      <StockItemView
        stock={item}
        session={this.state.session}
        navigation={this.props.navigation}
        parentProps={this.props}
        exponent={this.state.productExponent}
      />
    );
  }

  ListEmptyView = () => {
    return (
      <View
        style={{
          width: DEVICE_WIDTH,
          height: DEVICE_HEIGHT - 40,
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
        }}>
        <Icon
          name="cube"
          type="FontAwesome"
          style={{ color: Colors.headerBackground, fontSize: 40 }}
        />
        <Text style={{ textAlign: 'center', color: 'darkgrey', marginTop: 5 }}>
          No data found.
        </Text>
      </View>
    );
  };

  render() {
    if (this.state.isLoading) {
      return <PreparingProgress />;
    } else {
      return (
        <View
          padder
          style={{
            backgroundColor: Styles.appViewBackground.backgroundColor,
            ...StyleSheet.absoluteFillObject,
          }}>
          <StatusBar
            backgroundColor={Styles.statusBar.backgroundColor}
            barStyle="light-content"
          />
          <InputGroup borderType="rounded">
            <Input
              style={{ marginTop: 5 }}
              placeholder="Search Product"
              onChangeText={text => {
                this.setState(
                  { stockLists: [], searchQuery: text, offset: 0 },
                  () => {
                    this._getAllStockList();
                  },
                );
              }}
            />
            <View>
              <Button
                onPress={() => {
                  this.setState({ isProductFilterEnabled: true });
                }}
                style={{
                  backgroundColor: 'lightGrey',
                  flexDirection: 'column',
                  paddingHorizontal: 20,
                  borderRadius: 4,
                  marginTop: 5,
                  marginRight: 5,
                }}>
                <MyIcon
                  name="filter"
                  style={{
                    fontSize: 30,
                    color: Colors.headerBackground,
                  }}
                />
                {this.filtersCount > 0 && (
                  <View
                    style={{
                      backgroundColor: 'red',
                      top: -30,
                      right: -5,
                      width: 10,
                      height: 10,
                      borderRadius: 5,
                      alignSelf: 'flex-end',
                    }}
                  />
                )}
              </Button>
            </View>
          </InputGroup>
          <ProductFilter
            isVisible={this.state.isProductFilterEnabled}
            filterTypes={this.state.filterTypes}
            closeBtnClicked={() => {
              this.setState({ isProductFilterEnabled: false });
            }}
            onPressApplyButton={this.onPressApplyButton.bind(this)}
          />
          {this.state.stockLists.length > 0 && (
            <FlatList
              data={this.state.stockLists}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              renderItem={item => this.renderItem(item)}
              keyExtractor={item => item.stock_id.toString()}
              onEndReachedThreshold={0.1}
              onEndReached={() => {
                if (
                  !this.state.loadMore &&
                  this.state.offset >= this.state.limit
                ) {
                  this.setState({ loadMore: true }, () => {
                    this._getAllStockList();
                  });
                }
              }}
              onRefresh={() => this.onRefresh()}
              refreshing={this.state.isLoading}
              ListEmptyComponent={this.ListEmptyView}
              ListFooterComponent={
                this.state.loadMore && this.state.searchQuery.length === 0 ? (
                  <View
                    style={{
                      padding: 7,
                      flex: 1,
                      flexDirection: 'row',
                      borderRadius: 2,
                      backgroundColor: Colors.headerBackground,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <ActivityIndicator size="small" color="white" />
                    <Text style={{ color: 'white', fontWeight: 'bold' }}>
                      {' Loading..'}
                    </Text>
                  </View>
                ) : null
              }
            />
          )}
        </View>
      );
    }
  }
}

class StockItemView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stock: null,
      index: 0,
      session: {},
      exponent: 2,
      ...props,
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({ ...nextProps });
  }

  getProductImage() {
    const { stock, session } = this.state;
    if (stock && stock.document_location !== 'null') {
      return { uri: `${session.imageIp}${stock.document_location}` };
    }
    return blankImage;
  }

  render() {
    const stock = this.state.stock;
    const productExponent = this.state.exponent;
    const session = this.state.session;
    return (
      <Card>
        <CardItem style={{ backgroundColor: 'white' }}>
          <Left>
            <Image
              source={this.getProductImage()}
              style={{
                height: 60,
                width: 60,
                marginTop: 3,
                marginBottom: 3,
              }}
            />
            <Body>
              <Text style={{ fontSize: 15, fontWeight: 'bold' }}>
                {stock.product_name}
              </Text>
              {/* <Text style={{ marginTop: 1 }}>
                <Icon name={"key"} type="FontAwesome" style={styles.currency} />
                {" " + stock.product_code}
              </Text>
              <Text style={{ marginTop: 1 }}>
                <Icon name={"tag"} type="FontAwesome" style={styles.currency} />
                {" " + stock.stock_account_name}
              </Text> */}
              <Text style={{ marginTop: 1, fontSize: 13 }}>
                <Icon
                  name={'bookmark'}
                  type="FontAwesome"
                  style={styles.currency}
                />
                {' ' + stock.category_name}
              </Text>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <Text style={{ flex: 0.5, marginTop: 5 }}>
                  <Icon
                    name={'cubes'}
                    type="FontAwesome"
                    style={styles.currency}
                  />
                  {' ' +
                    NumberFormat(
                      RemoveSeparator(stock.quantity, stock.quantity_exponent),
                      session.info,
                    )}
                </Text>
                <Text style={{ marginTop: 3, flex: 0.5 }}>
                  <Icon
                    name={'balance-scale'}
                    type="FontAwesome"
                    style={styles.currency}
                  />
                  <Text
                    style={{ textAlign: 'right', justifyContent: 'flex-end' }}>
                    {stock.uom_name}
                  </Text>
                </Text>
              </View>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <Text style={{ flex: 0.5, marginTop: 8, color: 'green' }}>
                  <Icon
                    name={session.info[AppConstants.keyCurrency]}
                    type="FontAwesome"
                    style={styles.currency}
                  />
                  {' ' +
                    NumberFormat(
                      RemoveSeparator(stock.value1, productExponent),
                      session.info,
                    )}
                </Text>
                {!session.info.protectedViewEnabled && (
                  <Text style={{ marginTop: 8, flex: 0.5, color: 'red' }}>
                    <Icon
                      name={session.info[AppConstants.keyCurrency]}
                      type="FontAwesome"
                      style={styles.currency}
                    />
                    <Text
                      style={{ textAlign: 'right', justifyContent: 'flex-end' }}>
                      {' ' +
                        NumberFormat(
                          RemoveSeparator(stock.value2, productExponent),
                          session.info,
                        )}
                    </Text>
                  </Text>
                )}
              </View>
              <View
                style={{
                  marginTop: 5,
                  flexDirection: 'row',
                  flex: 1,
                  justifyContent: 'flex-end',
                }}>
                <View
                  style={[
                    styles.statusContainer,
                    STATUS_COLOR[stock.status_id] != null
                      ? { backgroundColor: STATUS_COLOR[stock.status_id] }
                      : { backgroundColor: '#d35400' },
                  ]}>
                  <Text note style={[styles.statusText]}>
                    {' '}
                    {stock.status_name}
                  </Text>
                </View>
              </View>
            </Body>
          </Left>
        </CardItem>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  currency: {
    fontSize: 14,
    fontWeight: 'normal',
  },
  statusText: {
    fontWeight: 'bold',
    borderRadius: 3,
    color: '#fff',
  },
  statusContainer: {
    paddingBottom: 2,
    paddingLeft: 3,
    paddingRight: 3,
    borderRadius: 3,
  },
});
