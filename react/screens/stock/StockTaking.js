import React from 'react';
import * as rn from 'react-native';
import * as nb from 'native-base';
import * as erpLibrary from 'react-native-erp-mobile-library';
import Colors from '../../style/Colors';
import { Styles } from '../../style/Styles';
import CustomSearchView, { SearchType } from 'react-native-erp-mobile-library/react/utilities/CustomSearchView';
import { GetAllProductDetails } from 'react-native-erp-mobile-library/react/screens/products/dbHelper/ProductDB';
import AppConstants from '../../constant/AppConstants';
import Toast from 'react-native-simple-toast';
import { GetAllStockList } from 'react-native-erp-mobile-library/react/screens/stock/dbHelper/StockDBHelper';
import { getAllStockList } from 'react-native-erp-mobile-library/react/controller/SyncDataController';
import { prepareOrderProductObject, prepareCreateOrderJson, sendPendingOrdersToServer } from 'react-native-erp-mobile-library/react/screens/orders/CreateOrderUtils';
import MasterConstants from '../../constant/MasterConstants';
import { GetAllAppConfigDetail } from 'react-native-erp-mobile-library/react/screens/orders/dbHelper/CreateOrderDBHelper';
import PreparingProgress from 'react-native-erp-mobile-library/react/component/PreparingProgress';
import { CreateMode } from 'react-native-erp-mobile-library/react/screens/orders/CreateOrder';
import { messages } from "react-native-erp-mobile-library/react/i18n/i18n";
import { DateFormat, DATE_TIME_SERVER, RemoveSeparator, AddSeparator } from 'react-native-erp-mobile-library/react/common/Common';
import { GetAllCustomers } from 'react-native-erp-mobile-library/react/screens/customers/dbHelper/CustomerDB';


const DEVICE_WIDTH = rn.Dimensions.get('window').width;
const DEVICE_HEIGHT = rn.Dimensions.get('window').height;

const styles = rn.StyleSheet.create({
  image: {
    height: 60,
    width: 60,
    marginTop: 3,
    marginBottom: 3
  },
  productName: {
    fontSize: 16,
    fontWeight: "bold",
    marginTop: 5,
    marginLeft: 10,
  },
  productCode: {
    marginTop: 5,
    fontSize: 14
  },
  productCategory: {
    marginTop: 5,
    fontSize: 14
  },
  headerSearchIcon: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 22,
    padding: 10,
    marginLeft: 5
  },
  productCardItem: {
    paddingTop: 0,
    paddingLeft: 0,
    backgroundColor: 'white'
  },
  productQuantityInput: {
    color: 'red',
    fontSize: 20
  },
  submitButton: {
    marginTop: 5,
    fontSize: 20,
    fontWeight: "bold",
    padding: 10,
    backgroundColor: "transparent",
    color: "white",
    textAlign: "center",
    flex: 1,
    width: DEVICE_WIDTH,
  },
  footerView: {
    backgroundColor: Colors.headerBackground,
  }
});
export default class StockTaking extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: 'Stock Taking',
      headerTintColor: Colors.headerTitleColor,
      headerStyle: {
        backgroundColor: Colors.headerBackground,
        fontWeight: "bold"
      },
      headerTitleStyle: {
        fontWeight: "bold",
        color: Colors.headerTitleColor
      },
      headerRight:
        <nb.Icon style={styles.headerSearchIcon} name="search" type="FontAwesome" onPress={() =>
          params.onClickSearchButton()
        } />
    };
  }
  constructor(props) {
    super(props);
    this.state = {
      isPreparing: false,
      isProductSearchEnabled: false,
      productMap: {},
      dbName: this.props.navigation.getParam('dbName'),
      fieldwork: this.props.navigation.state.params.fieldwork,
      session: null
    }
    this.updateStockTakingProducts = this.updateStockTakingProducts.bind(this);
    this.addSelectedProducts = this.addSelectedProducts.bind(this);
  }
  componentDidMount() {
    this.props.navigation.setParams({ onClickSearchButton: this.onClickSearchButton.bind(this) });
    this.setState({ session: erpLibrary.sessionInfo({ type: 'get', key: 'loginInfo' }) }, () => {
      this.getAllStockProductList();
      this.getAppConfigJsonForOrderType();
    });
  }
  onClickSearchButton = () => {
    this.setState({ isProductSearchEnabled: true });
  }
  onClickSubmitButton() {
    let orderProductList = [];
    Object.values(this.state.productMap).forEach(product => {
      if (product['isUpdated']) {
        product['price'] = product.value1;
        product['orderQuantity'] = AddSeparator(product.availableQuantity, product.quantity_exponent);
        product['discountAmount'] = 0;
        product['discountPercentage'] = 0;
        let orderProduct = prepareOrderProductObject(this.state, product, false, false)
        orderProduct['sequence'] = orderProductList.length + 1;
        orderProductList.push(orderProduct);
      }
    });
    let state = this.state;
    state['orderProductList'] = orderProductList;
    state['orderTypeId'] = MasterConstants.orderTypeStockTaking;
    state['createMode'] = CreateMode.submit;
    state['dbName'] = this.state.session.databaseName

    if (orderProductList.length > 0) {
      rn.Alert.alert(
        "Confirmation",
        "Are you sure, do you want to sumbit?",
        [
          {
            text: "No",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          {
            text: "Yes", onPress: () => {
              let generalInfo = {
                toParty: {
                  value: {
                    id: state.fieldwork.customerId
                  }
                },
                orderDate: {
                  value: DateFormat(new Date(), DATE_TIME_SERVER)
                }
              }
              prepareCreateOrderJson(state, generalInfo, null, null, (isSuccess) => {
                if (isSuccess) {
                  Toast.show(messages('requestSaved'), Toast.SHORT);
                  sendPendingOrdersToServer();
                  this.props.navigation.goBack();
                }
              });
            }
          }
        ],
        { cancelable: false }
      );
    } else {
      Toast.show('Please add product', Toast.SHORT);
    }
  }
  onChooseProduct = ({ value }) => {

  }
  getAppConfigJsonForOrderType() {
    let filter = {
      conditions: [{
        column: 'app_config_id',
        value: 'create_' + MasterConstants.orderTypeStockTaking,
        operator: '='
      }]
    }
    this.setState({ isPreparing: true }, () => {
      GetAllAppConfigDetail((appConfigDetails) => {
        if (appConfigDetails.length > 0) {
          const appConfig = JSON.parse(appConfigDetails[0].details);
          var orderJson = appConfig.orderJson;
          this.setState({ isPreparing: false, formJson: orderJson });
        } else {
          rn.Alert.alert(
            "Info",
            "Master data not found for make next process. Please sync master details(Menu->Sync data->Master)",
            [
              {
                text: "Ok", onPress: () => {
                  this.props.navigation.goBack();
                }
              }
            ],
            { cancelable: false }
          );
        }
      }, filter);
    })
  }
  getAllStockProductList() {
    let filter = {
      conditions: [{
        value: this.state.fieldwork.customerId,
        column: "customer_id",
        operator: "="
      }]
    }
    GetAllCustomers(AppConstants.affordeBackOfficeDB, (datas) => {
      let customer = datas[0];
      var filter = {
        orderBy: 'last_update_time desc',
        conditions: [{
          value: customer.cus_stock_account_id,
          column: "stock_account_id",
          operator: "="
        }, {
          value: MasterConstants.statusAvailable,
          column: "status_id",
          operator: "="
        }]
      }
      GetAllStockList((datas) => {
        if (datas.length > 0) {
          let stockProducts = {};
          datas.forEach(stockProduct => {
            stockProducts[stockProduct.product_id] = stockProduct;
          })
          this.getProducts(Object.keys(stockProducts), (products) => {
            let productMap = {};
            products.forEach(product => {
              if (typeof productMap[product.product_id] === 'undefined') {
                product['availableQuantity'] = stockProducts[product.product_id].quantity;
                product['isUpdated'] = false;
                productMap[product.product_id] = product;
              }
            })
            this.updateStockTakingProducts(Object.values(productMap));
          });
        }
      }, filter);
    }, filter);
  }
  getProducts(productIds, callback) {
    let filter = {
      conditions: [{
        value: productIds,
        column: 'product_id',
        operator: 'in'
      }]
    }
    GetAllProductDetails((products) => {
      callback(products);
    }, filter)
  }
  addSelectedProducts(productIds) {
    this.getProducts(productIds, (products) => {
      products.forEach(product => {
        if (typeof this.state.productMap[product.product_id] === 'undefined') {
          product['availableQuantity'] = 0;
          product['isUpdated'] = false;
          let productMap = {};
          products.forEach(product => {
            productMap[product.product_id] = product;
          });
          Object.values(this.state.productMap).forEach(product => {
            productMap[product.product_id] = product;
          });
          this.setState({ isProductSearchEnabled: false, productMap: productMap });
        } else {
          Toast.show('Product already exist');
        }
      });
    });
  }
  updateStockTakingProducts(products) {
    let productMap = {};
    products.forEach(product => {
      productMap[product.product_id] = product;
    });
    Object.values(this.state.productMap).forEach(product => {
      productMap[product.product_id] = product;
    });
    this.setState({ isProductSearchEnabled: false, productMap: productMap });
  }

  renderListEmptyView = () => {
    return (
      <nb.View
        style={{
          width: DEVICE_WIDTH,
          height: DEVICE_HEIGHT - 40,
          alignItems: "center",
          justifyContent: "center",
          flex: 1
        }}
      >
        <nb.Icon
          name="cube"
          type="FontAwesome"
          style={{ color: Colors.headerBackground, fontSize: 40 }}
        />
        <nb.Text style={{ textAlign: "center", color: "darkgrey", marginTop: 5 }}>
          No products found. Search and add products.
        </nb.Text>
      </nb.View>
    );
  };
  renderStockTakingItemView = ({ item, index }) => {
    return (
      <StockTakingItemView
        product={item}
        index={index}
      />
    );
  }
  renderProductListView = (state) => {
    return (
      <rn.FlatList
        extraData={state}
        data={Object.values(state.productMap)}
        ItemSeparatorComponent={this.FlatListItemSeparator}
        renderItem={this.renderStockTakingItemView.bind(this)}
        keyExtractor={product => product.product_id}
        ListEmptyComponent={this.renderListEmptyView}
      />
    )
  }
  renderProductSearchModal = (state) => {
    return (
      <CustomSearchView
        modalVisible={state.isProductSearchEnabled}
        searchType={SearchType.products}
        conditions={[]}
        session={this.state.session}
        hideModel={() => {
          this.setState({ isProductSearchEnabled: false });
        }}
        onChooseSearchValue={(value) => {
          this.addSelectedProducts([value.id]);
        }}
      />
    )
  }
  renderFooterView = () => {
    return (
      <nb.Footer style={styles.footerView}>
        <rn.TouchableOpacity
          onPress={this.onClickSubmitButton.bind(this)}>
          <nb.Text style={styles.submitButton}>{"Submit"}</nb.Text>
        </rn.TouchableOpacity>
      </nb.Footer>
    );
  }
  renderContentView = () => {
    const state = this.state;
    return (
      <nb.Content>
        {this.renderProductSearchModal(state)}
        {this.renderProductListView(state)}
      </nb.Content>
    );
  }
  render() {
    if (this.state.isPreparing) {
      return (
        <PreparingProgress />
      );
    } else {
      return (
        <nb.Container>
          {this.renderContentView()}
          {this.renderFooterView()}
        </nb.Container>);
    }
  }
}

export class StockTakingItemView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      product: null,
      isUserTyping: false,
      ...props,
    };
  }
  UNSAFE_componentWillReceiveProps(newProps) {
    this.setState({ ...newProps });
  }
  render() {
    const product = this.state.product;
    return (
      <nb.Card>
        <nb.Text style={styles.productName}>
          {product.product_name}
        </nb.Text>
        <nb.CardItem style={styles.productCardItem}>
          <nb.Left>
            <nb.Body>
              <nb.Text style={styles.productCode}>
                {product.product_code}
              </nb.Text>
              <nb.Text style={styles.productCategory}>
                {product.category_name}
              </nb.Text>
            </nb.Body>
          </nb.Left>
          <nb.Right>
            <nb.InputGroup borderType='rounded' >
              <nb.Input
                style={styles.productQuantityInput}
                placeholder='0.00'
                keyboardType='decimal-pad'
                value={this.state.isUserTyping ? product.availableQuantity.toString() : RemoveSeparator(product.availableQuantity, product.quantity_exponent)}
                onChangeText={(value) => {
                  product['availableQuantity'] = value;
                  product['isUpdated'] = true;
                  this.setState({ isUserTyping: true, product: product });
                }}
              />
            </nb.InputGroup>
          </nb.Right>
        </nb.CardItem>
      </nb.Card>
    );
  }
}
