/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable eqeqeq */
import React, {Component} from 'react';
import {
  StyleSheet,
  StatusBar,
  FlatList,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {
  Icon,
  Left,
  Body,
  Card,
  CardItem,
  Root,
  InputGroup,
  Input,
  Button,
} from 'native-base';
import NetInfo from '@react-native-community/netinfo';
import MyIcon from 'react-native-vector-icons/FontAwesome';
import {Styles, IconColor} from '../../style/Styles';
import Colors from '../../style/Colors';
import Loader from 'react-native-erp-mobile-library/react/component/Loader';
import Toast from 'react-native-simple-toast';
import {sessionInfo} from 'react-native-erp-mobile-library/index';
import PreparingProgress from 'react-native-erp-mobile-library/react/component/PreparingProgress';
import Route from 'react-native-erp-mobile-library/react/router/Route';
import Controller from 'react-native-erp-mobile-library/react/controller/Controller';
import AppConstants from '../../constant/AppConstants';
import {
  DateFormat,
  DATE_TIME_SERVER,
  Currency,
} from 'react-native-erp-mobile-library/react/common/Common';
import {InsertMenuDetail} from 'react-native-erp-mobile-library/react/navigation/dbHelper/MenuDBHelper';
import MasterConstants from '../../constant/MasterConstants';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import {renderHeaderView} from 'react-native-erp-mobile-library/react/util/HeaderUtil';
import {deleteMenuData, deletMenuOnly} from 'react-native-erp-mobile-library/react/database/DatabaseHandler';

export default class TenantDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tenantList: [],
      isPreparing: false,
      isLoading: false,
      searchQuery: '',
    };
  }

  componentDidMount() {
    this._initHeader();
    const session = sessionInfo({type: 'get', key: 'loginInfo'});
    const tenantMappingIp = sessionInfo({type: 'get', key: 'tenantMappingIp'});
    console.log(session);
    let tenantList =
      session.info.tenantMappings != undefined
        ? session.info.tenantMappings
        : [];
    this.setState({
      tenantList: tenantList,
      session: session,
      searchQuery: '',
      tenantMappingIp: tenantMappingIp,
    });
  }
  componentWillUnmount() {
    // this.backHandler.remove();
  }

  _initHeader() {
    this.props.navigation.setParams({
      headerTitle: this.props.route.params?.headerTitle ?? 'Tenant List',
      headerType: this.props.route.params?.headerType ?? HeaderTypes.HeaderBack,
    });
    this.props.navigation.setOptions(renderHeaderView(this));
  }
  _switchTenant(tenantId) {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        this.setState({isLoading: true});
        this._callLoginService(tenantId, () => {
          this.setState({isLoading: false});
          // this.props.navigation.dispatch(StackActions.reset({
          //   index: 0,
          //   actions: [
          //     NavigationActions.navigate({
          //       routeName: Route.SplashScreen
          //     }),
          //   ],
          // }))
          this.props.navigation.replace(Route.SplashScreen);
        });
      } else {
        Toast.show('Ur are in offline can not change tenant', Toast.SHORT);
      }
    });
  }

  _callLoginService(tenantId, callBack) {
    let loginJson = {
      request: {
        envelope: {
          sourceApplication: 'Afforde UI',
          requestDate: DateFormat(new Date(), DATE_TIME_SERVER),
        },
        content: {
          bo_data: {
            type: 'login',
            tenantId: tenantId,
            parentTenantId: this.state.session.info.tenantId,
            userId: this.state.session.info.userId,
            channelTypeId: 2,
          },
        },
      },
    };
    new Controller()
      .playPostApi(
        this.state.session.loginToken,
        this.state.session.publicIp + AppConstants.loginWithThirdParty,
        loginJson,
      )
      .then(response => {
        if (response.status === 200) {
          deletMenuOnly(AppConstants.affordeBackOfficeDB);
          AsyncStorage.setItem(AppConstants.keyLoggedInStatus, 'loggedIn');
          const responseBody = JSON.parse(response._bodyText);
          AsyncStorage.setItem(
            AppConstants.keyLoginInfo,
            JSON.stringify({
              [AppConstants.keyDatabaseName]: AppConstants.affordeBackOfficeDB,
              [AppConstants.keyLoginToken]:
                responseBody.response.header.taAuthToken,
              [AppConstants.keyPublicIp]: this.state.session.publicIp,
              [AppConstants.keyTenantId]: tenantId,
              [AppConstants.keySessionInfo]: JSON.stringify(
                responseBody.response.content.bo_data,
              ),
            }),
          );
          AsyncStorage.setItem(
            AppConstants.keyLoginToken,
            responseBody.response.header.taAuthToken,
          );
          AsyncStorage.setItem(
            AppConstants.keySessionInfo,
            JSON.stringify(responseBody.response.content.bo_data),
          );
          AsyncStorage.setItem(
            AppConstants.keyPublicIp,
            this.state.session.publicIp,
          );
          AsyncStorage.setItem(AppConstants.keyTenantId, tenantId);
          AsyncStorage.getItem(AppConstants.keyLoginInfo).then(loginInfo => {
            const data = JSON.parse(loginInfo);
            const publicIp = data[AppConstants.keyPublicIp].split(':');
            const imageIp =
              publicIp[0] +
              ':' +
              publicIp[1] +
              ':' +
              MasterConstants.tenantConfig.tenantId.nginxPort;
            const sessionInf = JSON.parse(data[AppConstants.keySessionInfo]);
            sessionInf[AppConstants.keyCurrency] = Currency(sessionInf);
            var session = {
              databaseName: AppConstants.affordeBackOfficeDB,
              loginToken: data[AppConstants.keyLoginToken],
              publicIp: data[AppConstants.keyPublicIp],
              imageIp,
              info: sessionInf,
            };
            sessionInfo({type: 'set', key: 'loginInfo', value: session});
          });
          console.log('Logging in');
          InsertMenuDetail(
            AppConstants.affordeBackOfficeDB,
            responseBody.response.content.bo_data.menus,
            () => {
              callBack();
            },
          );
        } else {
          this.setState({isLoading: false});
          Toast.show('Login failure', Toast.SHORT);
        }
      });
  }

  renderItem({item, index}) {
    if (
      this.state.searchQuery == '' ||
      item.tenant.name
        .toLowerCase()
        .includes(this.state.searchQuery.toLowerCase())
    ) {
      return (
        <Card style={styles.card}>
          <CardItem>
            <Left>
              <TouchableOpacity
                onPress={() => {
                  this._switchTenant(item.tenant.id);
                }}>
                <View
                  // eslint-disable-next-line react-native/no-inline-styles
                  style={{
                    width: 50,
                    height: 50,
                    borderRadius: 50 / 2,
                    backgroundColor: IconColor[index % IconColor.length],
                    justifyContent: 'center',
                  }}>
                  <Text style={[styles.text, {color: 'white', fontSize: 18}]}>
                    {item.tenant.name.substring(0, 2).toUpperCase()}
                  </Text>
                </View>
              </TouchableOpacity>
              <Body>
                <TouchableOpacity
                  onPress={() => {
                    this._switchTenant(item.tenant.id);
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styles.text}> {item.tenant.name} </Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styles.subText}>
                      {' '}
                      {this.state.session.info.userTenantName}{' '}
                    </Text>
                  </View>
                </TouchableOpacity>
              </Body>
            </Left>
          </CardItem>
        </Card>
      );
    }
  }

  ListEmptyView = () => {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
          marginTop: 200,
          backgroundColor: 'white',
        }}>
        <Icon
          name="shopping-cart"
          type="FontAwesome"
          style={{color: Colors.headerBackground, fontSize: 40}}
        />
        <Text style={{textAlign: 'center', color: 'darkgrey', marginTop: 10}}>
          You have did not have mapping Tenant Details.
        </Text>
      </View>
    );
  };

  render() {
    if (this.state.isPreparing) {
      return <PreparingProgress />;
    } else {
      return (
        <Root>
          <View
            padder
            style={{
              backgroundColor: Styles.appViewBackground.backgroundColor,
              ...StyleSheet.absoluteFillObject,
            }}>
            <StatusBar
              backgroundColor={Styles.statusBar.backgroundColor}
              barStyle="light-content"
            />
            <StatusBar
              backgroundColor={Styles.statusBar.backgroundColor}
              barStyle="light-content"
            />
            <InputGroup borderType="rounded">
              <Input
                style={{marginTop: 5}}
                placeholder="Search Tenant"
                onChangeText={text => {
                  this.setState({searchQuery: text});
                }}
              />
              <Button
                onPress={() => {
                  //this._filterData(this.state.query);
                }}
                style={{
                  backgroundColor: 'lightGrey',
                  paddingHorizontal: 20,
                  borderRadius: 4,
                  marginTop: 5,
                  marginRight: 5,
                }}>
                <MyIcon
                  name="search"
                  style={{fontSize: 20, color: Colors.headerBackground}}
                />
              </Button>
            </InputGroup>
            <Loader loading={this.state.isLoading} />
            <FlatList
              extraData={this.state}
              data={this.state.tenantList}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              renderItem={item => this.renderItem(item)}
              refreshing={this.state.isRefresh}
              ListEmptyComponent={this.ListEmptyView}
            />
          </View>
        </Root>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  createDate: {
    fontSize: 13,
    textAlign: 'right',
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  subText: {
    fontSize: 10,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
});
