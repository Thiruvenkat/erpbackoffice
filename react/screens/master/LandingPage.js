import React, { Component } from "react";
import { Root } from "native-base";
import AppConstants from "../../constant/AppConstants";
import MasterConstants from "../../constant/MasterConstants";
import { getAllCustomerDetails } from "../../controller/SyncDataController";
import AppNavigator from "../../navigation/AppNavigator";

export default class LandingPage extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      session: {}
    };
  }

  render() {
    return (
      <Root>
        <AppNavigator />
      </Root>
    );
  }
}
