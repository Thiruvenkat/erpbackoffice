import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Container, Header, Item, Input, Icon, Button, Text } from 'native-base';

// create a component
class SearchScreen extends React.Component {
    static navigationOptions = {
        header: null
    };

    constructor(props){
        super(props)
        this.state={
            showHeaderSearch : true
        }
    }

    showSearch(){
        this.setState({showHeaderSearch:true});
    }

    hideSearch(){
        this.setState({showHeaderSearch:false});
    }

    render() {
        return (
            <Container>
                <Header searchBar rounded>
                    
                        {(this.state.showHeaderSearch)?
                        <Item>
                        <Input placeholder="Search" />
                        <Icon name="ios-search" />
                        </Item>:<Icon name="ios-search" />
                    }
                        
                    
                    <Button transparent>
                        <Text>Search</Text>
                    </Button>
                </Header>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default SearchScreen;
