import React, { Component } from 'react';
import { View, Text, StyleSheet, Animated } from 'react-native';

class ProgressBarComponent extends React.Component {

    componentWillMount() {
        this.animation = new Animated.Value(this.props.progress);
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.progress != this.props.progress) {
            Animated.timing(this.animation, {
                toValue: this.props.progress,
                duration: this.props.duration
            }).start();
        }
    }

    render() {
        const {
            height,
            borderColor,
            borderWidth,
            borderRadius,
            barColor,
            fillColor,
            row
        } = this.props;

        const widthInterpolated = this.animation.interpolate({
            inputRange: [0, 1],
            outputRange: ["0%", "100%"],
            extrapolate: "clamp"
        })
        return (
            <View>
                <View style={[{ flexDirection: "row", height, width: 170 }, row ? { flex: 1 } : undefined]}>
                    <View style={{ flex: 1, borderColor, borderWidth, borderRadius }}>
                        <View style={[StyleSheet.absoluteFill, { backgroundColor: fillColor, borderRadius }]}>
                            <Animated.View
                                style={{
                                    position: "absolute",
                                    left: 0,
                                    top: 0,
                                    bottom: 0,
                                    borderRadius,
                                    width: widthInterpolated,
                                    backgroundColor: barColor
                                }}
                            />
                        </View>
                    </View>
                </View>
                <Text style={{ textAlign: "right", color: "#fff" }}>{(this.props.progress <= 1) ? Math.floor(this.props.progress * 100) : 100}%</Text>
            </View>
        )
    }
}

ProgressBarComponent.defaultProps = {
    height: 15,
    borderColor: "#000",
    borderWidth: 1,
    borderRadius: 6,
    barColor: "tomato",
    // fillColor: "rgba(0,0,0,.5)",
    fillColor: "#fff",
    duration: 100
}
// create a component
class ProgressBar extends React.Component {

    interval = null;

    constructor(props) {
        super(props)
        this.state = {
            progress: 0,
            barColor: (this.props.barColor == null) ? "#0ccc" : this.props.barColor,
        }
    }

    componentDidMount() {
        setInterval(() => {
            this.setState({
                progress: this.state.progress + 0.1,
            });
            // console.log(this.state.progress);
        }, 1000);
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.progressContainer}>
                    <ProgressBarComponent
                        progress={this.state.progress}
                        duration={500}
                        barColor={this.state.barColor}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }, progressContainer: {
        alignItems: "center"
    }
});

//make this component available to the app
export default ProgressBar;
