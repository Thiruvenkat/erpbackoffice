import React, { Component } from 'react';
import { FlatList,  Dimensions } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import Colors from '../../style/Colors';
import { sessionInfo } from "react-native-erp-mobile-library/index";
import { getAllBsPlanSummaryList } from 'react-native-erp-mobile-library/react/screens/finance/dbHelper/FinanceDBHelper';
import { RemoveSeparator, DateFormat, DATE_TIME_SERVER } from 'react-native-erp-mobile-library/react/common/Common';
import { getAllBsPlanAllocationSummaryList } from 'react-native-erp-mobile-library/react/controller/SyncDataController';
import { DrawerActions } from 'react-navigation';
import { Icon, View, Text, Toast } from 'native-base';
import { Styles } from '../../style/Styles';
import { ActivityIndicator } from 'react-native-paper';
import MasterConstants from '../../constant/MasterConstants';
import AppConstants from '../../constant/AppConstants';
import { PlanSummaryDetailItemView } from '../finance/PlanSummaryDetailItemView';
import Route from '../../router/Route';
import { messages } from 'react-native-erp-mobile-library/react/i18n/i18n';
import {heightPercentageToDP, widthPercentageToDP} from "react-native-erp-mobile-library/react/utilities/PixelRatioConverter";

const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;

class Dashboard extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: "Target",
      headerTitleStyle: { color: "white" },
      headerStyle: {
        backgroundColor: Colors.headerBackground,
        fontWeight: "bold"
      },
      headerLeft:
        <Icon style={Styles.homeBurgerMenu} name="bars" type="FontAwesome" onPress={() => {
          navigation.dispatch(DrawerActions.openDrawer())
        }} />
    };
  };

  constructor(props) {
    super(props)
    this.state = {
      dashboardData: [],
      isLoading: false,
      loadMore: false,
      isRefresh: false,
      apiCall: true,
      accountId: '',
      offset: 0,
      limit: 10,
    }
  }

  componentDidMount() {
    const sessions = sessionInfo({ type: 'get', key: 'loginInfo' });
    console.log(sessions)
    if (sessions.info.tenantCustomerId === sessions.info.loggedInCustomerId) {
      this.setState({
        session: sessions,
        accountId: "",
      }, () => {
        this.getSummaryDetail();
      });
    } else {
      AsyncStorage.getItem(AppConstants.accountId).then((value) => {
        var accountId = value != null ? value : "";
        this.setState({
          session: sessions,
          accountId: accountId,
        }, () => {
          value == null ? null : this.getSummaryDetail();
        });
      });
    }
  }

  getSummaryDetail() {
    this.getAllBsPlanSummary((datas) => {
      if (datas.length > 0) {
        let dashboardData = []
        for (let index = 0; index < datas.length; index++) {
          const summaryDetail = datas[index];
          console.log(summaryDetail);
          let chartData = {
            id: summaryDetail.bs_plan_allocation_id,
            name: summaryDetail.account_name + " (" + summaryDetail.bs_plan_allocation_summary_name + ")",
            planValue: summaryDetail.plan_allocation_value,
            planAchieveValue: summaryDetail.achieve_value,
            exponent: summaryDetail.exponent,
            vaildFrom: summaryDetail.start_date,
            vaildTo: summaryDetail.end_date
          };
          dashboardData.push(chartData);
        }
        console.log(dashboardData);
        this._updateLoadingState();
        this.setState({ dashboardData: dashboardData, isLoading: false, loadMore: false, offset: this.state.offset + datas.length });
      } else {
        this.getAllBsPlanSummaryFromServer();
      }
    });
  }

  getAllBsPlanSummary(callback) {
    var filter = {
      limit: this.state.limit,
      offset: this.state.offset,
      orderBy: 'start_date desc',
      conditions: []
    }
    if (!this.state.fetchAll) {
      filter.conditions.push({
        value: DateFormat(new Date(), DATE_TIME_SERVER),
        column: "start_date",
        operator: "<=",
        dataType: "String"
      });
      filter.conditions.push({
        value: DateFormat(new Date(), DATE_TIME_SERVER),
        column: "end_date",
        operator: ">=",
        dataType: "String"
      });
    }
    if (this.state.accountId != '') {
      filter.conditions.push({
        value: this.state.accountId,
        column: "account_id",
        operator: "=",
        dataType: "String"
      });
    }
    console.log(filter)
    getAllBsPlanSummaryList((datas) => {
      callback(datas);
    }, filter);
  }

  getAllBsPlanSummaryFromServer() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected && this.state.apiCall) {
        console.log('calling service..');
        let filter = {
          limit: this.state.limit,
          offset: this.state.offset,
          orderBy: 'bsas.last_updated',
          conditions: [
            {
              column: "bsas.start_date",
              value: new Date().getTime(),
              dataType: "Date",
              operator: "<="
            },
            {
              column: "bsas.end_date",
              value: new Date().getTime(),
              dataType: "Date",
              operator: ">="
            },
            {
              column: "s.status_id",
              value: MasterConstants.StatusActive,
              dataType: "String",
              operator: "="
            }]
        }
        if (this.state.accountId != '') {
          filter.conditions.push({
            value: this.state.accountId,
            column: "a.account_id",
            operator: "=",
            dataType: "String"
          });
        }
        getAllBsPlanAllocationSummaryList(this.state.session, filter, (isSuccess) => {
          this.getSummaryDetail();
        });
        this.setState({
          apiCall: false
        });
      } else {
        this._updateLoadingState();
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    })
  }

  _updateLoadingState() {
    this.setState({ isLoading: false, loadMore: false, isPreparing: false, isRefresh: false });
  }

  onRefresh() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        this.setState({ offset: 0, apiCall: true }, () => {
          this.getAllBsPlanSummaryFromServer('');
        });
      } else {
        this.setState({ offset: 0, apiCall: true }, () => {
          this.getSummaryDetail();
        });
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    });
  }

  showSummaryDetail(item) {
    this.props.navigation.navigate(Route.PlanSummaryDetail, item);
  }

  renderItem({ item }) {
    console.log(item)
    return (
      <PlanSummaryDetailItemView
        cardData={item}
        navigation={this.props.navigation}
        onPressSummary={this.showSummaryDetail.bind(this)}
      />
    )
  }

  ListEmptyView = () => {
    return (
      <View
        style={{
          alignItems: "center",
          justifyContent: "center",
          flex: 1,
          width: DEVICE_WIDTH,
          height: DEVICE_HEIGHT - 40,
          backgroundColor: 'white'
        }}
      >
        <Icon
          name="shopping-cart"
          type="FontAwesome"
          style={{ color: Colors.headerBackground, fontSize: 40 }}
        />
        <Text style={{ textAlign: "center", color: "darkgrey", marginTop: 10 }}>
          You have no Target's
        </Text>
      </View>
    );
  };

  render() {
    return (
      <FlatList
        extraData={this.state}
        data={this.state.dashboardData}
        renderItem={this.renderItem.bind(this)}
        keyExtractor={(item, index) => index.toString()}
        onRefresh={() => this.onRefresh()}
        refreshing={this.state.isRefresh}
        onEndReachedThreshold={0.1}
        onEndReached={({ distanceFromEnd }) => {
          if (!this.state.loadMore && this.state.offset >= this.state.limit) {
            this.setState({ loadMore: true, }, () => {
              this.getSummaryDetail();
            });
          }
        }}
        ListEmptyComponent={this.ListEmptyView}
        ListFooterComponent={
          (this.state.loadMore ?
            <View style={{ padding: 7, flex: 1, flexDirection: 'row', borderRadius: 2, backgroundColor: Colors.headerBackground, alignItems: 'center', justifyContent: 'center' }}>
              <ActivityIndicator size="small" color="white" />
              <Text style={{ color: 'white', fontWeight: 'bold' }}>{' Loading..'}</Text>
            </View>
            : null)
        }
      />
    );
  }
}

export default Dashboard;
