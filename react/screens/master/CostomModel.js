/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Label} from 'native-base';
import Colors from '../../style/Colors';
import Modal from 'react-native-modal';

export class CustomModel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: this.props.visibleModal,
      title: this.props.title,
      percentage: this.props.percentage,
      ...props,
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({...nextProps});
  }

  render() {
    return (
      <Modal isVisible={this.state.visibleModal}>
        <View style={styles.modalContent}>
          <View>
            <Text
              style={{
                fontSize: 20,
                color: 'black',
                fontWeight: 'bold',
                textAlign: 'left',
              }}>
              {this.state.title}
            </Text>
          </View>
          <View
            style={{
              height: 1,
              backgroundColor: '#d7d9dd',
              marginLeft: 5,
              marginTop: 5,
              marginRight: 5,
            }}
          />
          <View style={styles.itemView}>
            <Label style={{marginTop: 20, color: Colors.headerBackground}}>
              {'Downloading... ' + this.state.percentage + ' %'}
            </Label>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});

export default CustomModel;
