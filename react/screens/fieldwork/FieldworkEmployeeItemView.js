/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-undef */
/* eslint-disable prettier/prettier */
import React from 'react';
import { StyleSheet, Text, View, StatusBar, ActivityIndicator } from 'react-native';
import { DateFormat, DATE_TIME_SERVER } from 'react-native-erp-mobile-library/react/common/Common';
import MasterConstants from '../../constant/MasterConstants';
import Colors from '../../style/Colors';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import { backHandler, renderHeaderView } from 'react-native-erp-mobile-library/react/util/HeaderUtil';
import { messages } from 'react-native-erp-mobile-library/react/i18n/i18n';
import { Icon, Fab } from 'native-base';
import PreparingProgress from 'react-native-erp-mobile-library/react/component/PreparingProgress';
import { FlatList } from 'react-native-gesture-handler';

class FieldworkEmployeeItemView extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return renderHeaderView(navigation);
      };

    constructor(props) {
        if (props.screenProps) {
            props.navigation = props.screenProps.navigation;
          }
          props.navigation.state.params = { ...props.navigation.state.params, ...props.screenProps };
        super(props);
        this.state = {
            isPreparing: true,
            isLoading: true,
            activityList: [],
            session: {},
            loadMore: false,
            offset: 0,
            limit: 10,
            createActivityModalVisible: false,
            isDateTimePickerVisible: false,
            isServiceCalled: false,
            fieldActivityId: null,
            newActivity: {
                date: DateFormat(new Date(), DATE_TIME_SERVER),
                work: true,
                notes: '',
                reason: {
                    id: MasterConstants.reasonNone,
                },
            },
        };
    }
    componentDidMount(){
        this._initHeader();
    }
    componentWillUnmount() {
        this.backHandler.remove();
      }

    _initHeader() {
        this.props.navigation.setParams({
          headerTitle: this.props.navigation.getParam('title', messages('fieldworkActivities')),
          headerType: this.props.navigation.getParam('headerType', HeaderTypes.HeaderBack),
        });
        this.backHandler = backHandler(this.props.navigation);
      }

    ListEmptyView = () => {
        return (
            <View
                style={{
                    width: DEVICE_WIDTH,
                    height: DEVICE_HEIGHT - 40,
                    alignItems: 'center',
                    justifyContent: 'center',
                    flex: 1,
                }}
            >
                <Icon
                    name="briefcase"
                    type="FontAwesome"
                    style={{ color: Colors.headerBackground, fontSize: 40 }}
                />
                <Text style={{ textAlign: 'center', color: 'darkgrey', marginTop: 5 }}>
                    You have no activities.
                </Text>
            </View>
        );
    };

    render() {
        if (this.state.isPreparing) {
            return (
                <PreparingProgress />
            );
        } else {
            return (
                <View
                    padder
                    style={{
                        backgroundColor: Styles.appViewBackground.backgroundColor,
                    }}
                >
                    <StatusBar
                        backgroundColor={Styles.statusBar.backgroundColor}
                        barStyle="light-content"
                    />
                    <FlatList
                        extraData={this.state}
                        data={this.state.activityList}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={(item, index) => this.renderItem(item, index)}
                        keyExtractor={(item, index) => index.toString()}
                        onEndReachedThreshold={0.1}
                        onEndReached={({ distanceFromEnd }) => {
                            if (!this.state.loadMore && this.state.offset >= this.state.limit && this.state.isServiceCalled) {
                                this.setState({ loadMore: true }, () => {
                                    this._getAllActivityDetails();
                                });
                            }
                        }}
                        onRefresh={() => this.onRefresh()}
                        refreshing={this.state.isLoading}
                        ListEmptyComponent={this.ListEmptyView}
                        ListFooterComponent={
                            this.state.loadMore ?
                                <View style={{ padding: 7, flex: 1, flexDirection: 'row', borderRadius: 2, backgroundColor: Colors.headerBackground, alignItems: 'center', justifyContent: 'center' }}>
                                    <ActivityIndicator size="small" color="white" />
                                    <Text style={{ color: 'white', fontWeight: 'bold' }}>{' Loading..'}</Text>
                                </View>
                                : null
                        }
                    />

                    {this.props.navigation.getParam('fieldActivityVisit').statusId === (MasterConstants.workInProgress || MasterConstants.workNotStarted || MasterConstants.workClosed) ?
                          null :   <Fab
                            style={{ backgroundColor: Colors.headerBackground, size: 5 }}
                            direction="up"
                            position="bottomRight"
                            onPress={() => {
                                this.setState({ createActivityModalVisible: true });
                            }}                    >
                            <Icon type="FontAwesome" name="plus" style={styles.fabicon} />
                        </Fab>
                        }
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

export default FieldworkEmployeeItemView;
