/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable radix */
import { Body, Card, CardItem, Icon, Left, Thumbnail } from 'native-base';
import React from 'react';
import { Text, TouchableOpacity, View, Alert } from 'react-native';
import {
  DateFormat,
  DATE_TIME_SERVER,
} from 'react-native-erp-mobile-library/react/common/Common';
import AppConstants from 'react-native-erp-mobile-library/react/constant/AppConstants';
import { GetAllCustomers } from 'react-native-erp-mobile-library/react/screens/customers/dbHelper/CustomerDB';
import Toast from 'react-native-simple-toast';
import MasterConstants from '../../constant/MasterConstants';
import Route from '../../router/Route';
import Colors from 'react-native-erp-mobile-library/react/style/Colors';
import { STATUS_COLOR } from 'react-native-erp-mobile-library/react/style/Styles';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';

export default class FieldworkCustomerItemView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activity: null,
      parentActivityDetails: {},
      session: {},
      index: 0,
      userLocation: {},
      appConfig: {},
      ...props,
    };
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({ ...nextProps });
  }

  _updateActivity() {
    this.props.updateActivity(this.state.activity);
  }

  _getCustomerDetails(customer_id, callback) {
    var filter = {
      limit: 1,
      offset: 0,
      orderBy: 'last_updated desc',
      conditions: [
        {
          value: customer_id,
          column: 'customer_id',
          operator: '=',
        },
      ],
    };
    GetAllCustomers(
      AppConstants.affordeBackOfficeDB,
      datas => {
        callback(datas);
      },
      filter,
    );
  }

  updateActivityValues(activity, userLocation) {
    if (
      this.state.parentActivityDetails.statusId ===
      MasterConstants.statusWorkInProgressId
    ) {
      Alert.alert(
        'Confirmation',
        'Are you sure want to ' +
        (activity.statusId === MasterConstants.statusWorkNotStartedId
          ? ' start '
          : activity.statusId === MasterConstants.statusWorkInProgressId
            ? ' stop '
            : activity.statusId === MasterConstants.statusWorkClosedId
              ? ' restart '
              : ' update ') +
        ' work?',
        [
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: 'Yes',
            onPress: () => {
              activity.lastUpdated = DateFormat(new Date(), DATE_TIME_SERVER);
              if (activity.statusId === MasterConstants.statusWorkNotStartedId) {
                activity.startTime = DateFormat(new Date(), DATE_TIME_SERVER);
                activity.endTime = '';
                activity.statusId = MasterConstants.statusWorkInProgressId;
                activity.statusName = MasterConstants.statusWorkInProgressName;
                activity.startLocation =
                  userLocation !== 'unknown' ?
                    {
                      longitude: userLocation.longitude,
                      latitude: userLocation.latitude,
                      altitude: 0,
                      accuracy: 0,
                      bearing: 0,
                      verticalAccuracyMeters: 0,
                      provider: 0,
                    } : {
                      longitude: 1,
                      latitude: 0,
                      altitude: 0,
                      accuracy: 0,
                      bearing: 0,
                      verticalAccuracyMeters: 0,
                      provider: 0,
                    };
              } else if (activity.statusId === MasterConstants.statusWorkInProgressId) {
                activity.endTime = DateFormat(new Date(), DATE_TIME_SERVER);
                activity.statusId = MasterConstants.statusWorkClosedId;
                activity.statusName = MasterConstants.statusWorkClosedName;
                activity.endLocation =
                  userLocation !== 'unknown' ?
                    {
                      longitude: userLocation.longitude,
                      latitude: userLocation.latitude,
                      altitude: 0,
                      accuracy: 0,
                      bearing: 0,
                      verticalAccuracyMeters: 0,
                      provider: 0,
                    } : {
                      longitude: 1,
                      latitude: 0,
                      altitude: 0,
                      accuracy: 0,
                      bearing: 0,
                      verticalAccuracyMeters: 0,
                      provider: 0,
                    };
              } else if (activity.statusId === MasterConstants.statusWorkClosedId) {
                activity.endTime = '';
                activity.statusId = MasterConstants.statusWorkInProgressId;
                activity.statusName = MasterConstants.statusWorkInProgressName;
                activity.endLocation = {
                  longitude: 1,
                  latitude: 0,
                  altitude: 0,
                  accuracy: 0,
                  bearing: 0,
                  verticalAccuracyMeters: 0,
                  provider: 0,
                };
              }
              this.setState({ activity: activity }, () => {
                this._updateActivity();
              });
            },
          },
        ],
        { cancelable: false },
      );
    } else {
      Toast.show('(Re)start your work and try again!', Toast.SHORT);
    }
  }

  _parseDate(totalDuration) {
    var diff = '';
    diff +=
      parseInt(totalDuration / (1000 * 60 * 60 * 24)) > 0 ? parseInt(totalDuration / (1000 * 60 * 60 * 24)) + 'd ' : '';
    diff +=
      parseInt((totalDuration / (60 * 60 * 1000)) % 24) > 0 ? parseInt((totalDuration / (60 * 60 * 1000)) % 24) + 'h ' : '';
    diff +=
      parseInt((totalDuration / (60 * 1000)) % 60) > 0 ? parseInt((totalDuration / (60 * 1000)) % 60) + 'm ' : '';
    return (diff +=
      parseInt((totalDuration / 1000) % 60) > 0 ? parseInt((totalDuration / 1000) % 60) + 's' : '');
  }

  // Calculate distance in Kilometers
  getDistance(latitude1, longitude1, latitude2, longitude2) {
    const R = 6371; // Earth’s mean radius in meter
    const dLat = this.radius(latitude2 - latitude1);
    const dLong = this.radius(longitude2 - longitude1);
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.radius(latitude1)) *
      Math.cos(this.radius(latitude2)) *
      Math.sin(dLong / 2) *
      Math.sin(dLong / 2);
    const c = 2 * Math.asin(Math.sqrt(a));
    const kiloMeter = R * c;
    return kiloMeter;
  }

  radius(deg) {
    return (deg * Math.PI) / 180.0;
  }

  render() {
    const { navigation } = this.props;
    const activity = this.state.activity;
    let userLocation = this.state.userLocation;
    let parentActivityDetail = this.state.parentActivityDetails;
    var startTime = activity.startTime.length > 0 ? new Date(activity.startTime) : null;
    var endTime = activity.endTime.length > 0 ? new Date(activity.endTime) : null;
    var diff = '';
    if (startTime != null && endTime != null) {
      let totalDuration = endTime.getTime() - startTime.getTime();
      diff = this._parseDate(totalDuration);
    } else if (startTime != null) {
      let totalDuration = new Date().getTime() - startTime.getTime();
      diff = this._parseDate(totalDuration);
    }
    return (
      <Card>
        <CardItem style={{ backgroundColor: 'white' }}>
          <TouchableOpacity
            onPress={() => {
              let activitys = this.state.activity;
              this._getCustomerDetails(activitys.customerId, datas => {
                if (this.state.activity.statusId !== MasterConstants.statusWorkNotStartedId) {
                  navigation.navigate(Route.FieldworkEventList, {
                    fieldActivityVisit: this.state.activity,
                    customerTypeId: datas[0].customer_type_id,
                    screenType: this.state.parentProps.route.params.screenType,
                    parentActivity: parentActivityDetail,
                    headerTitle: 'Events',
                    headerType: HeaderTypes.HeaderBack,
                  });
                }
              });
            }}>
            <Thumbnail
              small
              source={{ uri: 'http://www.thecellartrust.org/wp-content/uploads/2017/04/Trustees.jpg' }}
            />
          </TouchableOpacity>
          <Left>
            <Body>
              <View style={{ flexDirection: 'row' }}>
                <TouchableOpacity
                  onPress={() => {
                    if (activity.statusId === MasterConstants.statusWorkNotStartedId) {
                      if (this.state.activityList.filter(node => node.statusId === MasterConstants.statusWorkInProgressId).length === 0) {
                        this.props.onPressEditCustomer(this.state.activity);
                      } else {
                        Toast.show("Please close the other visit's and then add", Toast.SHORT);
                      }
                    }
                  }}>
                  <Text style={{ fontWeight: 'bold', fontSize: 14, flex: 0.1 }}>
                    {activity.customerName + ' '}
                    {activity.statusId === MasterConstants.statusWorkNotStartedId && (
                      <Icon
                        name="edit"
                        type="FontAwesome"
                        style={{ color: Colors.headerBackground, fontSize: 15 }}
                      />
                    )}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{ flex: 1, flexDirection: 'row', marginTop: 2 }}>
                <TouchableOpacity
                  onPress={() => {
                    if (activity.startLocation.longitude !== undefined && activity.startLocation.longitude !== 1) {
                      this.props.navigation.navigate(Route.TravelLocationMapView, {
                        startLocation: activity.startLocation,
                      });
                    }
                  }}
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={{ textAlign: 'center' }}>
                    {'Start '}
                    {activity.startLocation !== undefined && activity.startLocation.longitude !== undefined &&
                      Number(activity.startLocation.longitude) !== 1 ? (
                        <Icon
                          name="map-marker"
                          type="FontAwesome"
                          style={{ color: 'green', fontSize: 15 }}
                        />
                      ) : null}
                  </Text>
                  <Text style={{ textAlign: 'center' }}>
                    {startTime != null ? DateFormat(activity.startTime, 'hh:mm a') : '00:00'}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    if (activity.endLocation.longitude !== undefined && activity.endLocation.longitude !== 1) {
                      this.props.navigation.navigate(Route.TravelLocationMapView, {
                        endLocation: activity.endLocation,
                        startLocation: activity.startLocation,
                      });
                    }
                  }}
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text style={{ textAlign: 'center' }}>
                    {'End '}{' '}
                    {activity.endLocation !== undefined && activity.endLocation.longitude !== undefined &&
                      Number(activity.endLocation.longitude) !== 1 ? (
                        <Icon
                          name="map-marker"
                          type="FontAwesome"
                          style={{ color: 'red', fontSize: 15 }}
                        />
                      ) : null}
                  </Text>
                  <Text style={{ textAlign: 'center' }}>
                    {endTime != null ? DateFormat(activity.endTime, 'hh:mm a') : '00:00'}
                  </Text>
                </TouchableOpacity>
                {activity.statusId !== MasterConstants.statusWorkNotStartedId ? (
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'column',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text
                      style={{
                        fontSize: 10,
                        marginRight: 5,
                        marginTop: 3,
                        padding: 2,
                        borderRadius: 3,
                        backgroundColor: '#f4f5f7',
                        color: 'red',
                      }}>
                      {"Event's : " + activity.eventCount}
                    </Text>
                    <Text style={{ fontSize: 12, textAlign: 'center' }}>
                      {diff}
                    </Text>
                  </View>
                ) : null}
                <TouchableOpacity
                  style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: 'transparent',
                    padding: 2,
                    borderRadius: 5,
                  }}
                  onPress={() => {
                    let activityss = this.state.activity;
                    let appConfig = this.state.appConfig;
                    this._getCustomerDetails(activityss.customerId, datas => {
                      if (this.state.parentProps.route.params.screenType !== undefined && this.state.parentProps.route.params.screenType === 'editable') {
                        if (datas.length > 0) {
                          if (this.state.activityList.filter(node =>
                            node.statusId === MasterConstants.statusWorkInProgressId).length === 0 ||
                            activityss.statusId === MasterConstants.statusWorkInProgressId) {
                            //
                            if (appConfig && appConfig.tracking.fieldActivity && appConfig.tracking.fieldActivity.locationMandatory === false) {
                              this.updateActivityValues(activityss, userLocation);
                            } else if (
                              datas[0].latitude == null ||
                              datas[0].longitude == null ||
                              datas[0].latitude === 'null' ||
                              datas[0].longitude === 'null' ||
                              datas[0].longitude === '1234') {
                              Toast.show('Sorry ' + activityss.customerName + ' Location is not found', Toast.SHORT);
                            } else {
                              var distance = this.getDistance(
                                datas[0].latitude,
                                datas[0].longitude,
                                userLocation.latitude,
                                userLocation.longitude,
                              );
                              if (distance < 0.2) {
                                this.updateActivityValues(activityss, userLocation);
                              } else {
                                Toast.show('Your Location is not matched to customer', Toast.SHORT);
                              }
                            }
                          } else {
                            Toast.show("Please Stop the other work's and then Start", Toast.SHORT);
                          }
                        } else {
                          Toast.show('Please sync customer data.', Toast.SHORT);
                        }
                      } else {
                        Toast.show('Edit not Allowed', Toast.SHORT);
                      }
                    });
                  }}>
                  {activity.statusId === MasterConstants.statusWorkNotStartedId ? (
                    <Icon
                      name="play"
                      type="FontAwesome"
                      style={{ color: 'green', fontSize: 25 }}
                    />
                  ) : activity.statusId === MasterConstants.statusWorkInProgressId ? (
                    <Icon
                      name="stop"
                      type="FontAwesome"
                      style={{ color: 'red', fontSize: 25 }}
                    />
                  ) : activity.statusId === MasterConstants.statusWorkClosedId ? (
                    <Icon
                      name="repeat"
                      type="FontAwesome"
                      style={{ color: '#028df2', fontSize: 25 }}
                    />
                  ) : null}
                </TouchableOpacity>
              </View>
            </Body>
          </Left>
        </CardItem>
        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
          {activity.statusId === MasterConstants.statusWorkInProgressId ? (
            <TouchableOpacity
              style={{ paddingStart: 10, alignItems: 'center' }}
              onPress={() => {
                this.props.onPressAddNotes(this.state.activity);
              }}>
              <Text
                style={{
                  backgroundColor: Colors.headerBackground,
                  borderRadius: 4,
                  height: 12,
                  padding: 2,
                  flex: 0.4,
                  color: 'white',
                  fontSize: 12,
                }}>
                {'Notes'}
              </Text>
            </TouchableOpacity>
          ) : activity.notes !== '' ? (
            <View style={{ paddingStart: 10, flex: 0.15 }}>
              <Text style={{ fontWeight: 'bold', flex: 0.4 }}>{'Notes:'}</Text>
            </View>
          ) : null}
          <View style={{ paddingStart: 10, flex: 0.6 }}>
            <Text style={{ borderRadius: 4, flex: 0.4 }}>
              {activity.notes + ' (Reason:' + activity.reasonName + ')'}
            </Text>
          </View>
          <View style={{ flex: 0.5, alignItems: 'flex-end', marginBottom: 5 }}>
            <View style={{ flex: 0.3, alignItems: 'flex-end', marginBottom: 5 }}>
              <Text
                style={{
                  marginRight: 5,
                  padding: 2,
                  borderRadius: 3,
                  backgroundColor: STATUS_COLOR[activity.statusId],
                  color:
                    activity.statusId === MasterConstants.statusWorkNotStartedId ? 'red'
                      : activity.statusId === MasterConstants.statusWorkInProgressId ||
                        activity.statusId === MasterConstants.statusWorkClosedId ? 'white'
                        : 'black',
                }}>
                {activity.statusName}
              </Text>
            </View>
          </View>
        </View>
      </Card>
    );
  }
}
