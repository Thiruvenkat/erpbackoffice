/* eslint-disable no-shadow */
/* eslint-disable dot-notation */
/* eslint-disable prettier/prettier */
import {
  DateFormat,
  DATE_TIME_SERVER,
} from 'react-native-erp-mobile-library/react/common/Common';
import { openDatabase } from 'react-native-erp-mobile-library/react/database/DatabaseHandler';
import { TABLE } from 'react-native-erp-mobile-library/react/database/DBConstants';
import { QueryBuilder } from 'react-native-erp-mobile-library/react/database/DBUtilities';
import AppConstants from '../../../constant/AppConstants';
import { sessionInfo } from 'react-native-erp-mobile-library';

let db;
var SQLite = require('react-native-sqlite-storage');

export function GetAllActivityDetails(callback, filter) {
  let DBInstance = openDatabase(AppConstants.affordeBackOfficeDB);
  DBInstance.transaction(dbs => {
    var query = QueryBuilder(
      'SELECT field_activity_id, date, user_id, start_time, end_time, status_id, status_name, start_location, end_location, reason_id, reason_name, notes, last_updated, tenant_id, opt_lock, visit_count, create_json, field_plan_id, seq_no, sync_status FROM ' +
      TABLE.FieldActivity,
      filter,
    );
    dbs.executeSql(
      query,
      [],
      (tx, results) => {
        var activities = [];
        for (let index = 0; index < results.rows.length; index++) {
          const row = results.rows.item(index);
          let activity = {
            fieldActivityId: row.field_activity_id,
            date: row.date,
            userId: row.user_id,
            startTime: row.start_time,
            endTime: row.end_time,
            statusId: row.status_id,
            statusName: row.status_name,
            startLocation: JSON.parse(row.start_location),
            endLocation: JSON.parse(row.end_location),
            reasonId: row.reason_id,
            reasonName: row.reason_name,
            notes: row.notes,
            lastUpdated: row.last_updated,
            tenantId: row.tenant_id,
            optLock: row.opt_lock,
            visitCount: row.visit_count,
            createJson: row.create_json,
            fieldPlanId: row.field_plan_id,
            seqNo: row.seq_no,
            syncStatus: row.sync_status,
          };
          activities[index] = activity;
        }
        callback(activities);
      },
      error => {
        callback([]);
      },
    );
  });
}

export function InsertFieldActivity(datas, callback) {
  let length = datas.length;
  if (length > 0) {
    let DBInstance = openDatabase(AppConstants.affordeBackOfficeDB);
    DBInstance.transaction((db) => {
      let tenantId = null;
      for (let index = 0; index < length; index++) {
        let values = datas[index];
        tenantId = values.tenantId;
        db.executeSql('INSERT OR REPLACE INTO ' + TABLE.FieldActivity + ' (' +
          'field_activity_id, date, user_id, start_time, end_time, status_id, status_name, start_location, end_location, reason_id, reason_name, notes, last_updated, tenant_id, opt_lock, visit_count, create_json, field_plan_id, seq_no, sync_status) ' +
          'VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
          values.fieldActivityId,
          values.date,
          values.userId,
          values.startTime,
          values.endTime,
          values.statusId,
          values.statusName,
          values.startLocation,
          values.endLocation,
          values.reasonId,
          values.reasonName,
          values.notes,
          values.lastUpdated,
          values.tenantId,
          values.optLock,
          values.visitCount,
          prepareFieldActivityRequest(values),
          values.fieldPlanId,
          values.seqNo,
          values.syncStatus,
        ],
          () => {
            // insert success
            if (index === length - 1) {
              if (tenantId != null) {
                db.executeSql('INSERT OR REPLACE INTO ' + TABLE.Sync + ' VALUES (?,?,?)', [
                  TABLE.FieldActivity,
                  DateFormat(new Date(), DATE_TIME_SERVER),
                  tenantId,
                ], () => {
                  console.log('Time updated field activity Successfully');
                }, (error) => {
                  console.log('Time update Error occured :' + JSON.stringify(error));
                });
              }
              callback(true);
            }
          },
          (error) => {
            // insert failure
            if (index === length - 1) { callback(false); }
            console.log('Insert Field activity Error occured :' + JSON.stringify(error));
          });
      }
    });
  } else {
    callback(false);
  }
}

export function GetAllActivityVisitDetails(callback, filter) {
  let DBInstance = openDatabase(AppConstants.affordeBackOfficeDB);
  DBInstance.transaction(database => {
    var query = QueryBuilder(
      'SELECT field_activity_customer_visit_id, field_activity_id, customer_id,customer_name,start_time,end_time,status_id,status_name,start_location,end_location,reason_id,reason_name,notes,create_json,start_end_location_matched, last_updated, tenant_id, opt_lock, sync_status, events_count, seq_no, order_mandatory FROM ' +
      TABLE.FieldActivityCustomerVisit,
      filter,
    );
    database.executeSql(
      query,
      [],
      (tx, results) => {
        var activities = [];
        for (let index = 0; index < results.rows.length; index++) {
          const row = results.rows.item(index);
          let activity = {
            fieldActivityCustomerVisitId: row.field_activity_customer_visit_id,
            fieldActivityId: row.field_activity_id,
            customerId: row.customer_id,
            customerName: row.customer_name,
            startTime: row.start_time,
            endTime: row.end_time,
            statusId: row.status_id,
            statusName: row.status_name,
            startLocation: JSON.parse(row.start_location),
            endLocation: JSON.parse(row.end_location),
            reasonId: row.reason_id,
            reasonName: row.reason_name,
            notes: row.notes,
            lastUpdated: row.last_updated,
            tenantId: row.tenant_id,
            optLock: row.opt_lock,
            eventCount: row.events_count,
            startEndLocationMatched: row.start_end_location_matched,
            syncStatus: row.sync_status,
            createJson: row.create_json,
            seqNo: row.seq_no,
            orderMandatory: row.order_mandatory,
          };
          activities[index] = activity;
        }
        callback(activities);
      },
      error => {
        callback([]);
      },
    );
  });
}

export function InsertFieldActivityVisits(datas, callback) {
  let length = datas.length;
  if (length > 0) {
    let DBInstance = openDatabase(AppConstants.affordeBackOfficeDB);
    DBInstance.transaction(databases => {
      let tenantId = null;
      for (let index = 0; index < length; index++) {
        let values = datas[index];
        tenantId = values.tenantId;
        databases.executeSql(
          'INSERT OR REPLACE INTO ' +
          TABLE.FieldActivityCustomerVisit +
          ' (' +
          'field_activity_customer_visit_id,field_activity_id,customer_id,customer_name,start_time,end_time,status_id,status_name,start_location,end_location,reason_id,reason_name,notes,last_updated,tenant_id,opt_lock,events_count,create_json,sync_status,start_end_location_matched, seq_no, order_mandatory) ' +
          'VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
          [
            values.fieldActivityCustomerVisitId,
            values.fieldActivityId,
            values.customerId,
            values.customerName,
            values.startTime,
            values.endTime,
            values.statusId,
            values.statusName,
            values.startLocation,
            values.endLocation,
            values.reasonId,
            values.reasonName,
            values.notes,
            values.lastUpdated,
            values.tenantId,
            values.optLock,
            values.eventCount,
            prepareFieldActivityVisitRequest(values),
            values.syncStatus,
            values.startEndLocationMatched,
            values.seqNo,
            values.orderMandatory,
          ],
          () => {
            // insert success
            if (index === length - 1) {
              if (tenantId != null) {
                databases.executeSql('INSERT OR REPLACE INTO ' + TABLE.Sync + ' VALUES (?,?,?)', [
                  TABLE.FieldActivityCustomerVisit,
                  DateFormat(new Date(), DATE_TIME_SERVER),
                  tenantId,
                ],
                  () => {
                    console.log('Time updated FieldActivity Customer Visit Successfully');
                  },
                  error => {
                    console.log('Time update Error occured :' + JSON.stringify(error));
                  },
                );
              }
            }
            callback(true);
          },
          error => {
            // insert failure
            if (index === length - 1) {
              //
            }
            console.log('Insert Field activity customer visit Error occured :' + JSON.stringify(error));
            callback(false);
          },
        );
      }
    });
  } else {
    callback(false);
  }
}

export function DeleteAllCustomerVisit(callback) {
  const session = sessionInfo({ type: 'get', key: 'loginInfo' });
  db = SQLite.openDatabase({
    name: session.databaseName + '.db',
    location: 'default',
  });
  db.transaction(
    txn => {
      txn.executeSql(
        'DELETE FROM ' + TABLE.FieldActivityCustomerVisit,
        [],
        () => {
          console.log(TABLE.FieldActivityCustomerVisit + ' deleted');
        },
      );
    },
    () => {
      callback(true);
    },
  );
}

export function GetAllFieldworkEvents(callback, filter) {
  let DBInstance = openDatabase(AppConstants.affordeBackOfficeDB);
  DBInstance.transaction(dbi => {
    var query = QueryBuilder(
      'SELECT field_activity_events_id, field_activity_customer_visit_id, event_date, event_reference_id, event_location, event_type_id, create_json, last_updated, tenant_id, opt_lock, sync_status FROM ' +
      TABLE.FieldActivityEvents,
      filter,
    );
    dbi.executeSql(
      query,
      [],
      (tx, results) => {
        var activities = [];
        for (let index = 0; index < results.rows.length; index++) {
          const row = results.rows.item(index);
          let activity = {
            fieldActivityEventsId: row.field_activity_events_id,
            fieldActivityCustomerVisitId: row.field_activity_customer_visit_id,
            eventDate: row.event_date,
            eventReferenceId: row.event_reference_id,
            eventLocation: JSON.parse(row.event_location),
            eventTypeId: row.event_type_id,
            createJson: row.create_json,
            lastUpdated: row.last_updated,
            tenantId: row.tenant_id,
            optLock: row.opt_lock,
            syncStatus: row.sync_status,
          };
          activities[index] = activity;
        }
        callback(activities);
      },
      error => {
        callback([]);
      },
    );
  });
}

export function InsertFieldworkEvents(datas, callback) {
  let length = datas.length;
  if (length > 0) {
    let DBInstance = openDatabase(AppConstants.affordeBackOfficeDB);
    DBInstance.transaction(dbis => {
      let tenantId = null;
      for (let index = 0; index < length; index++) {
        let values = datas[index];
        tenantId = values.tenantId;
        dbis.executeSql(
          'INSERT OR REPLACE INTO ' +
          TABLE.FieldActivityEvents +
          ' (' +
          'field_activity_events_id, field_activity_customer_visit_id, event_date, event_reference_id, event_location, event_type_id, create_json, last_updated, tenant_id, opt_lock, sync_status) ' +
          'VALUES (?,?,?,?,?,?,?,?,?,?,?)',
          [
            values.fieldActivityEventsId,
            values.fieldActivityCustomerVisitId,
            values.eventDate,
            values.eventReferenceId,
            values.eventLocation,
            values.eventTypeId,
            prepareFieldActivityEventRequest(values),
            values.lastUpdated,
            values.tenantId,
            values.optLock,
            values.syncStatus,
          ],
          () => {
            // insert success
            if (index === length - 1) {
              if (tenantId != null) {
                db.executeSql(
                  'INSERT OR REPLACE INTO ' + TABLE.Sync + ' VALUES (?,?,?)',
                  [
                    TABLE.FieldActivityEvents,
                    DateFormat(new Date(), DATE_TIME_SERVER),
                    tenantId,
                  ],
                  () => {
                    console.log(
                      'Time updated Field Activity Events Successfully',
                    );
                  },
                  error => {
                    console.log(
                      'Time update Error occured :' + JSON.stringify(error),
                    );
                  },
                );
              }
              callback(true);
            }
          },
          error => {
            // insert failure
            if (index === length - 1) {
              callback(false);
            }
            console.log(
              'Insert Field activity events Error occured :' +
              JSON.stringify(error),
            );
          },
        );
      }
    });
  } else {
    callback(false);
  }
}

function prepareFieldActivityRequest(activity) {
  let contentData = {
    type: 'createFieldActivityWithPlans_BOO',
    fieldActivity: {
      optLock: Number(activity.optLock),
      fieldActivityId: activity.fieldActivityId,
      date: activity.date.replace(' ', 'T'),
      user: {
        id: activity.userId,
      },
      tenant: {
        id: activity.tenantId,
      },
      startTime: activity.startTime.replace(' ', 'T'),
      endTime: activity.endTime.replace(' ', 'T'),
      status: {
        id: activity.statusId,
      },
      startLocation: activity.startLocation,
      endLocation: activity.endLocation,
      notes: activity.notes,
      seqNo: activity.seqNo,
      fieldPlanId: activity.fieldPlanId,
    },
  };
  if (activity.reasonId != null && activity.reasonId !== '') {
    contentData.fieldActivity['reason'] = {
      id: activity.reasonId,
      name: activity.reasonName,
    };
  }
  if (activity.fieldPlanId && activity.fieldPlanId.length > 0) {
    contentData.fieldActivity['fieldPlan'] = {
      id: activity.fieldPlanId,
    };
  }
  return JSON.stringify({
    request: {
      envelope: {
        sourceApplication: 'Afforde UI',
        requestDate: DateFormat(new Date(), DATE_TIME_SERVER),
        requestFromType: 'android',
      },
      content: {
        bo_data: {
          ...contentData,
        },
      },
    },
  });
}

function prepareFieldActivityVisitRequest(activity) {
  let contentData = {
    type: 'fieldActivityCustomerVisit',
    optLock: Number(activity.optLock),
    fieldActivityCustomerVisitId: activity.fieldActivityCustomerVisitId,
    fieldActivity: {
      id: activity.fieldActivityId,
    },
    customer: {
      id: activity.customerId,
    },
    tenant: {
      id: activity.tenantId,
    },
    startTime: activity.startTime.replace(' ', 'T'),
    endTime: activity.endTime.replace(' ', 'T'),
    status: {
      id: activity.statusId,
    },
    startLocation: activity.startLocation,
    endLocation: activity.endLocation,
    notes: activity.notes,
    seqNo: activity.seqNo,
  };
  if (activity.reasonId != null && activity.reasonId !== '') {
    contentData['reason'] = {
      id: activity.reasonId,
      name: activity.reasonName,
    };
  }
  if (
    activity.orderMandatory != null &&
    activity.orderMandatory !== undefined
  ) {
    contentData['orderMandatory'] = activity.orderMandatory;
  }
  return prepareRequest(contentData);
}

function prepareFieldActivityEventRequest(activity) {
  return '{}';
}

export function prepareFieldActivityTrackingRequest(activity, location) {
  let contentData = {
    type: 'fieldActivityAgentTracking',
    fieldActivity: {
      id: activity.fieldActivityId,
    },
    date: DateFormat(new Date(), DATE_TIME_SERVER),
    location: JSON.stringify(location),
    tenant: {
      id: activity.tenantId,
    },
    notes: activity.notes,
    status: {
      id: activity.trackingStatus,
    },
  };
  return prepareRequest(contentData);
}

function prepareRequest(contentData) {
  return JSON.stringify({
    request: {
      envelope: {
        sourceApplication: 'Afforde UI',
        requestDate: DateFormat(new Date(), DATE_TIME_SERVER),
        requestFromType: 'android',
      },
      content: {
        data: {
          ...contentData,
        },
      },
    },
  });
}
