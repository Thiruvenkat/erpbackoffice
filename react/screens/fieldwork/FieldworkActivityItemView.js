/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-alert */
/* eslint-disable radix */
import { Body, Card, CardItem, Icon, Left } from 'native-base';
import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import {
  DateFormat,
  DATE_TIME_UI,
  StringToDate,
} from 'react-native-erp-mobile-library/react/common/Common';
import NetInfo from '@react-native-community/netinfo';
import Toast from 'react-native-simple-toast';
import MasterConstants from '../../constant/MasterConstants';
import {
  getAllFieldActivity,
  updateFieldActivity,
} from '../../controller/FieldworkController';
import Loader from 'react-native-erp-mobile-library/react/component/Loader';
import {
  GetAllActivityDetails,
  InsertFieldActivity,
  GetAllActivityVisitDetails,
} from './dbHelper/FieldWorkDBHelper';
import BaseComponent from 'react-native-erp-mobile-library/react/screens/masters/BaseComponent';
import {
  IconColor,
  STATUS_COLOR,
} from 'react-native-erp-mobile-library/react/style/Styles';

export function updateActivity(activity, session) {
  return new Promise((resolve, reject) => {
    var fieldActivity = {
      fieldActivityId: activity.fieldActivityId,
      date: activity.date,
      userId: activity.userId,
      startTime: activity.startTime,
      endTime: activity.endTime,
      statusId: activity.statusId,
      statusName: activity.statusName,
      startLocation: JSON.stringify(activity.startLocation),
      endLocation: JSON.stringify(activity.endLocation),
      reasonId: activity.reasonId,
      reasonName: activity.reasonName,
      notes: activity.notes,
      lastUpdated: activity.lastUpdated,
      tenantId: activity.tenantId,
      optLock: activity.optLock,
      visitCount: activity.visitCount,
      fieldPlanId: activity.fieldPlanId,
      seqNo: activity.seqNo,
      syncStatus: 'Not Updated',
    };
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        let filter = {
          conditions: [
            {
              column: 'field_activity_id',
              value: activity.fieldActivityId,
              dataType: 'String',
              operator: '=',
            },
          ],
        };
        getAllFieldActivity(session, filter, isSuccess => {
          if (isSuccess) {
            GetAllActivityDetails(activities => {
              fieldActivity.optLock = activities[0].optLock;
              InsertFieldActivity([fieldActivity], isSuccesss => {
                if (isSuccesss) {
                  GetAllActivityDetails(activitiess => {
                    let object = JSON.parse(activitiess[0].createJson);
                    updateFieldActivity(session, object, isSuccessss => {
                      if (isSuccessss) {
                        resolve();
                        // Toast.show('Updated successfully', Toast.SHORT)
                      } else {
                        reject();
                        Toast.show('Update failure', Toast.SHORT);
                      }
                    });
                  }, filter);
                }
              });
            }, filter);
          }
        });
      } else {
        InsertFieldActivity([fieldActivity], isSuccess => {
          if (isSuccess) {
          }
        });
      }
    });
  });
}


export function calculateRadius(deg) {
  return deg * (Math.PI / 180);
}

export default class FieldworkActivityItemView extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      activity: null,
      activityList: null,
      fieldPlan: null,
      session: {},
      index: 0,
      isLoading: false,
      userLocation: {},
      ...props,
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({ ...nextProps });
  }

  _updateActivity() {
    let activity = this.state.activity;
    let session = this.state.session;
    updateActivity(activity, session).then(() => { });
  }

  _updatedMessage(isSuccess) {
    if (isSuccess) {
      //Toast.show('Updated successfully', Toast.SHORT)
    } else {
      Toast.show('Update failure', Toast.SHORT);
    }
  }

  _parseDate(totalDuration) {
    var diff = '';
    diff +=
      parseInt(totalDuration / (1000 * 60 * 60 * 24)) > 0
        ? parseInt(totalDuration / (1000 * 60 * 60 * 24)) + 'd '
        : '';
    diff +=
      parseInt((totalDuration / (60 * 60 * 1000)) % 24) > 0
        ? parseInt((totalDuration / (60 * 60 * 1000)) % 24) + 'h '
        : '';
    diff +=
      parseInt((totalDuration / (60 * 1000)) % 60) > 0
        ? parseInt((totalDuration / (60 * 1000)) % 60) + 'm '
        : '';
    return (diff +=
      parseInt((totalDuration / 1000) % 60) > 0
        ? parseInt((totalDuration / 1000) % 60) + 's'
        : '');
  }

  _isPastDateFieldWorkActivity() {

    let activity = this.state.activity;
    let currentDate = new Date();
    const currentDateStart = StringToDate(DateFormat(currentDate, DATE_TIME_UI).concat('T00:00:00')).getTime();
    const currentDateEnd = StringToDate(DateFormat(currentDate, DATE_TIME_UI).concat('T23:59:59')).getTime();
    const activityDateStart = StringToDate(activity.date).getTime();
    const activityDateEnd = StringToDate(activity.date).getTime();

    if (this.state.activityList.filter(
      node => node.statusId === MasterConstants.statusWorkInProgressId,
    ).length > 0
    ) {
      if (activity.statusId === MasterConstants.statusWorkNotStartedId) {
        Toast.show('Previous work not yet closed', Toast.SHORT);
      }
      return true;
    } else if (
      currentDateStart <= activityDateStart &&
      currentDateEnd >= activityDateEnd
    ) {
      return false;
    } else {
      Toast.show('This date not allowed', Toast.SHORT);
      return true;
    }
  }

  _allowChangeStatus() {
    if (!this._isPastDateFieldWorkActivity() || this.state.activity.statusId === MasterConstants.statusWorkInProgressId) {
      return true;
    }
    return false;
  }

  onPressActivityDate() {
    let activity = this.state.activity;
    if (this.validatePastDateActivity(activity.date) && this.state.activity.statusId !== MasterConstants.statusWorkNotStartedId) {
      if (this.state.parentProps.route.params.screenType !== 'attendence') {
        this.props.onPressActivityItemView(activity);
      }
    }
  }

  validatePastDateActivity(date) {
    const activityDate = DateFormat(date, DATE_TIME_UI);
    const currentDate = DateFormat(new Date(), DATE_TIME_UI);
    if (activityDate === currentDate) {
      return true;
    }
    return false;
  }

  onPressStartLocation() {
    const activity = this.state.activity;
    this.props.onPressStartLocationButton(activity);
  }

  onPressEndLocation() {
    const activity = this.state.activity;
    this.props.onPressEndLocationButton(activity);
  }

  async changeStatusValidation() {
    return new Promise((resolve, reject) => {
      if (!this.state.tenantAddressLocation) {
        alert('Location detail not found. Please contact your admin');
        this.setState({ isLoading: false });
      } else {
        this._fetchCurrentLocation().then(location => {
          NetInfo.fetch().then(isConnected => {
            if (isConnected) {
              let userLocation = location;
              if (this.calcDistance(JSON.parse(this.state.tenantAddressLocation), userLocation) < 5.0) {
                this.setState({ isLoading: false });
                resolve();
              } else {
                reject();
              }
            }
          });
        });
      }
    });
  }

  calcDistance = (locationA, locationB) => {
    const R = 6371; // Earth’s mean radius in meter
    const dLat = calculateRadius(locationA.latitude - locationB.latitude);
    const dLong = calculateRadius(locationA.longitude - locationB.longitude);
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(calculateRadius(locationB.latitude)) *
      Math.cos(calculateRadius(locationA.latitude)) *
      Math.sin(dLong / 2) *
      Math.sin(dLong / 2);
    const c = 2 * Math.asin(Math.sqrt(a));
    const kiloMeter = R * c;
    return kiloMeter;
  };

  onPressWorkStatus() {
    let activity = this.state.activity;
    this.props.onPressChangeStatus(activity, updatedActivity => {
      this.setState({ activity: updatedActivity }, () => {
        this._updateActivity();
      });
    });
  }

  _validateCustomerActivityCompletedOrNot(callback) {
    let filter = {
      conditions: [
        {
          column: 'status_id',
          value: MasterConstants.statusWorkInProgressId,
          operator: '=',
        },
      ],
    };
    GetAllActivityVisitDetails(data => {
      if (data.length > 0) {
        callback(false);
      } else {
        callback(true);
      }
    }, filter);
  }

  render() {
    const activity = this.state.activity;
    var startTime =
      activity.startTime.length > 0 ? new Date(activity.startTime) : null;
    var endTime =
      activity.endTime.length > 0 ? new Date(activity.endTime) : null;
    var diff = '';
    if (startTime != null && endTime != null) {
      let totalDuration = endTime.getTime() - startTime.getTime();
      diff = this._parseDate(totalDuration);
    } else if (startTime != null) {
      let totalDuration = new Date().getTime() - startTime.getTime();
      diff = this._parseDate(totalDuration);
    }
    return (
      <View>
        <Loader loading={this.state.isLoading} />
        <Card>
          <CardItem style={{ backgroundColor: 'white' }}>
            <TouchableOpacity
              onPress={() => {
                this.onPressActivityDate();
              }}>
              <Text
                style={{
                  padding: 10,
                  borderRadius: 10,
                  backgroundColor:
                    IconColor[this.state.index % IconColor.length],
                  color: 'white',
                  fontWeight: 'bold',
                }}>
                {DateFormat(activity.date, 'MMM DD')}
              </Text>
            </TouchableOpacity>
            {activity.statusId !== MasterConstants.statusLeaveId ? (
              <Left>
                <Body>
                  <View style={{ flex: 1, flexDirection: 'row' }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.state.screenType === 'attendence' ? null : this.onPressStartLocation();
                      }}
                      style={{
                        flex: 1,
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={{ textAlign: 'center' }}>
                        {'Start '}{' '}
                        {activity.startLocation !== undefined &&
                          activity.startLocation.longitude !== undefined &&
                          Number(activity.startLocation.longitude) !== 1 ? (
                            <Icon
                              name="map-marker"
                              type="FontAwesome"
                              style={{ color: 'green', fontSize: 15 }}
                            />
                          ) : null}
                      </Text>
                      <Text style={{ textAlign: 'center' }}>
                        {startTime != null
                          ? DateFormat(activity.startTime, 'hh:mm a')
                          : '00:00'}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        this.state.screenType === 'attendence' ? null : this.onPressEndLocation();
                      }}
                      style={{
                        flex: 1,
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={{ textAlign: 'center' }}>
                        {'End '}{' '}
                        {activity.endLocation !== undefined &&
                          activity.endLocation.longitude !== undefined &&
                          Number(activity.endLocation.longitude) !== 1 ? (
                            <Icon
                              name="map-marker"
                              type="FontAwesome"
                              style={{ color: 'red', fontSize: 15 }}
                            />
                          ) : null}
                      </Text>
                      <Text style={{ textAlign: 'center' }}>
                        {endTime != null
                          ? DateFormat(activity.endTime, 'hh:mm a')
                          : '00:00'}
                      </Text>
                    </TouchableOpacity>
                    {activity.statusId !==
                      MasterConstants.statusWorkNotStartedId &&
                      this.state.parentProps.route.params.trackingType !==
                      MasterConstants.attendence ? (
                        <View
                          style={{
                            flex: 1,
                            flexDirection: 'column',
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                          <Text
                            style={{
                              fontSize: 10,
                              marginRight: 5,
                              marginTop: 3,
                              padding: 2,
                              borderRadius: 3,
                              backgroundColor: '#f4f5f7',
                              color: 'red',
                            }}>
                            {'Visits : ' + activity.visitCount}
                          </Text>
                          <Text style={{ fontSize: 12, textAlign: 'center' }}>
                            {diff}
                          </Text>
                        </View>
                      ) : null}
                    <TouchableOpacity
                      style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: 'transparent',
                        padding: 2,
                        borderRadius: 5,
                      }}
                      onPress={() => {
                        if (this.state.parentProps.route.params.trackingType === MasterConstants.attendence && this._allowChangeStatus()) {

                          if (activity.statusId !== MasterConstants.statusWorkClosedId) {
                            this.setState({ isLoading: true }, () => {
                              this.changeStatusValidation()
                                .then(() => {
                                  this.onPressWorkStatus();
                                })
                                .catch(() => {
                                  this.setState({ isLoading: false });
                                  alert('You are outside the company!');
                                });
                            });
                          }

                        } else {
                          if (
                            this._allowChangeStatus() &&
                            this.state.parentProps.route.params.screenType !==
                            undefined &&
                            this.state.parentProps.route.params.screenType ===
                            'editable'
                          ) {
                            this._validateCustomerActivityCompletedOrNot(
                              isSuccess => {
                                if (isSuccess) {
                                  this.onPressWorkStatus();
                                } else {
                                  Toast.show(
                                    "Close your field visitor's work activity",
                                    Toast.SHORT,
                                  );
                                }
                              },
                            );
                          }
                        }
                      }}>
                      {activity.statusId ===
                        MasterConstants.statusWorkNotStartedId ? (
                          <Icon
                            name="play"
                            type="FontAwesome"
                            style={{ color: 'green', fontSize: 25 }}
                          />
                        ) : activity.statusId ===
                          MasterConstants.statusWorkInProgressId ? (
                            <Icon
                              name="stop"
                              type="FontAwesome"
                              style={{ color: 'red', fontSize: 25 }}
                            />
                          ) : activity.statusId ===
                            MasterConstants.statusWorkClosedId &&
                            this.state.parentProps.route.params.trackingType !==
                            MasterConstants.attendence ? (
                              <Icon
                                name="repeat"
                                type="FontAwesome"
                                style={{ color: '#028df2', fontSize: 25 }}
                              />
                            ) : null}
                    </TouchableOpacity>
                  </View>
                </Body>
              </Left>
            ) : (
                <Left>
                  <Body>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                      <Text style={{ flex: 0.7, paddingStart: 10 }}>
                        {activity.notes +
                          (activity.reasonId !== MasterConstants.reasonNone
                            ? ' (' + activity.reasonName + ') '
                            : '')}
                      </Text>
                    </View>
                  </Body>
                </Left>
              )}
          </CardItem>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <Text style={{ flex: 0.6, paddingStart: 10 }}>{activity.notes}</Text>
            <View style={{ flex: 0.5, alignItems: 'flex-end', marginBottom: 5 }}>
              <Text
                style={{
                  marginRight: 5,
                  padding: 2,
                  borderRadius: 3,
                  backgroundColor: STATUS_COLOR[activity.statusId],
                  color:
                    activity.statusId === MasterConstants.statusWorkNotStartedId
                      ? 'red'
                      : activity.statusId ===
                        MasterConstants.statusWorkInProgressId ||
                        activity.statusId === MasterConstants.statusWorkClosedId
                        ? 'white'
                        : 'white',
                }}>
                {activity.statusName}
              </Text>
            </View>
          </View>
        </Card>
      </View>
    );
  }
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//   },
//   horizontal: {
//     flexDirection: 'row',
//     justifyContent: 'space-around',
//     padding: 10,
//   },
//   currency: {
//     fontSize: 14,
//     fontWeight: 'normal',
//   },
//   fabicon: {
//     textAlign: 'center',
//     alignItems: 'center',
//     justifyContent: 'center',
//     color: 'white',
//     fontSize: 22,
//   },
//   leftContainer: {
//     paddingLeft: 10,
//     flex: 1,
//     flexDirection: 'row',
//     justifyContent: 'flex-start',
//     backgroundColor: 'transparent',
//   },
//   rightContainer: {
//     flex: 1,
//     flexDirection: 'row',
//     justifyContent: 'flex-end',
//     alignItems: 'center',
//     backgroundColor: 'transparent',
//   },
// });
