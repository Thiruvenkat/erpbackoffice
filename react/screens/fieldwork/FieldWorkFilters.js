/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-vars */
/* eslint-disable prettier/prettier */
import _ from 'lodash';
import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import {
  Icon,
  Input,
  Label,
} from 'native-base';
import Modal from 'react-native-modal';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { DateFormat, DATE_UI, DATE_TIME_SERVER, DATE_TIME_UI, TIME_UI, TIME_RAIL, TimeFormat } from 'react-native-erp-mobile-library/react/common/Common';
import { messages } from 'react-native-erp-mobile-library/react/i18n/i18n';
import { CurrentYear, CurrentMonth } from './FieldworkActivityList';
import Toast from 'react-native-simple-toast';
import Colors from 'react-native-erp-mobile-library/react/style/Colors';

export const FilterTypes = {
  FieldActivityStartDate: 'fieldActivityStartDate',
  FieldActivityEndDate: 'fieldActivityEndDate',
  StartFromTime: 'startFromTime',
  StartToTime: 'startToTime',
  EndFromTime: 'endFromTime',
  EndToTime: 'endToTime',
  FromCount: 'fromCount',
  ToCount: 'toCount',
};

export default class FieldWorkFilters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: props.isVisible,
      isDateTimePickerVisible: false,
      isTimePickerVisible: false,
      minimumDate: DateFormat(new Date(), DATE_TIME_SERVER),
      pickerViewType: '',
      pickerType: '',
      currentDate: DateFormat(new Date(), DATE_TIME_UI),
      currentTime: DateFormat(new Date(), TIME_RAIL),
      defaultFromDate: DateFormat(new Date(CurrentYear, CurrentMonth, 1), DATE_TIME_UI),
      defaultFromTime: DateFormat(new Date(CurrentYear, CurrentMonth, 1), TIME_RAIL),
      defaultToTime: '23:59:59',
      fromValueDate: DateFormat(new Date(), DATE_TIME_UI),
      toValueDate: DateFormat(new Date(), DATE_TIME_UI),
      fromValueTime: '00:00:00',
      toValueTime: '23:59:59',
      isFormValid: true,
      ...props,
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({ ...nextProps });
  }

  _handleDatePicked = date => {
    let isValid = true;
    let fromValueDate = this.state.fromValueDate;
    let toValueDate = this.state.toValueDate;
    let fromValueTime = this.state.fromValueTime;
    let toValueTime = this.state.toValueTime;
    let pickerType = this.state.pickerType;
    if (pickerType === FilterTypes.FieldActivityStartDate) {
      isValid = this.validateFitler(DateFormat(date, DATE_TIME_UI), toValueDate, 'date');
      if (!isValid) {
        this.setState({ isDateTimePickerVisible: false, fromValueDate: DateFormat(date, DATE_TIME_UI) }, () => {
          this._updateStateValues(pickerType, { fromValue: DateFormat(date, DATE_TIME_UI), isFromDateValid: false });
        });
      } else {
        this.setState({ isDateTimePickerVisible: false, fromValueDate: DateFormat(date, DATE_TIME_UI) }, () => {
          this._updateStateValues(pickerType, { fromValue: DateFormat(date, DATE_TIME_UI), isFromDateValid: true, isToDateValid: true });
        });
      }
    } else if (pickerType === FilterTypes.FieldActivityEndDate) {
      isValid = this.validateFitler(fromValueDate, DateFormat(date, DATE_TIME_UI), 'date');
      if (!isValid) {
        this.setState({ isDateTimePickerVisible: false, toValueDate: DateFormat(date, DATE_TIME_UI) }, () => {
          this._updateStateValues(pickerType, { toValue: DateFormat(date, DATE_TIME_UI), isToDateValid: false });
        });
      } else {
        this.setState({ isDateTimePickerVisible: false, toValueDate: DateFormat(date, DATE_TIME_UI) }, () => {
          this._updateStateValues(pickerType, { toValue: DateFormat(date, DATE_TIME_UI), isFromDateValid: true, isToDateValid: true });
        });
      }
    } else if (pickerType === FilterTypes.StartFromTime) {
      isValid = this.validateFitler(DateFormat(date, TIME_RAIL), toValueTime, 'time');
      if (!isValid) {
        this.setState({ isTimePickerVisible: false, fromValueTime: DateFormat(date, TIME_RAIL) }, () => {
          this._updateStateValues(pickerType, { fromValue: DateFormat(date, TIME_RAIL), isFromTimeValid: false });
        });
      } else {
        this.setState({ isTimePickerVisible: false, fromValueTime: DateFormat(date, TIME_RAIL) }, () => {
          this._updateStateValues(pickerType, { fromValue: DateFormat(date, TIME_RAIL), isFromTimeValid: true, isToTimeValid: true });
        });
      }
    } else if (pickerType === FilterTypes.StartToTime) {
      isValid = this.validateFitler(fromValueTime, DateFormat(date, TIME_RAIL), 'time');
      if (!isValid) {
        this.setState({ isTimePickerVisible: false, toValueTime: DateFormat(date, TIME_RAIL) }, () => {
          this._updateStateValues(pickerType, { toValue: DateFormat(date, TIME_RAIL), isToTimeValid: false });
        });
      } else {
        this.setState({ isTimePickerVisible: false, toValueTime: DateFormat(date, TIME_RAIL) }, () => {
          this._updateStateValues(pickerType, { toValue: DateFormat(date, TIME_RAIL), isFromTimeValid: true, isToTimeValid: true });
        });
      }
    } else if (pickerType === FilterTypes.EndFromTime) {
      isValid = this.validateFitler(DateFormat(date, TIME_RAIL), toValueTime, 'time');
      if (!isValid) {
        this.setState({ isTimePickerVisible: false, fromValueTime: DateFormat(date, TIME_RAIL) }, () => {
          this._updateStateValues(pickerType, { fromValue: DateFormat(date, TIME_RAIL), isFromTimeValid: false });
        });
      } else {
        this.setState({ isTimePickerVisible: false, fromValueTime: DateFormat(date, TIME_RAIL) }, () => {
          this._updateStateValues(pickerType, { fromValue: DateFormat(date, TIME_RAIL), isFromTimeValid: true, isToTimeValid: true });
        });
      }
    } else if (pickerType === FilterTypes.EndToTime) {
      isValid = this.validateFitler(fromValueTime, DateFormat(date, TIME_RAIL), 'time');
      if (!isValid) {
        this.setState({ isTimePickerVisible: false, toValueTime: DateFormat(date, TIME_RAIL) }, () => {
          this._updateStateValues(pickerType, { toValue: DateFormat(date, TIME_RAIL), isToTimeValid: false });
        });
      } else {
        this.setState({ isTimePickerVisible: false, toValueTime: DateFormat(date, TIME_RAIL) }, () => {
          this._updateStateValues(pickerType, { toValue: DateFormat(date, TIME_RAIL), isFromTimeValid: true, isToTimeValid: true });
        });
      }
    }
    this.setState({ isFormValid: isValid });
  };

  _hideDateTimePicker = () => {
    this.setState({
      isDateTimePickerVisible: false,
      isTimePickerVisible: false,
    });
  };

  resetDateAndTime = (pickerType) => {
    if (pickerType === FilterTypes.FieldActivityStartDate) {
      this.setState({ isDateTimePickerVisible: false }, () => {
        this._updateStateValues(pickerType, { fromValue: this.state.defaultFromDate, isFromDateValid: true });
      });
    } else if (pickerType === FilterTypes.FieldActivityEndDate) {
      this.setState({ isDateTimePickerVisible: false }, () => {
        this._updateStateValues(pickerType, { toValue: this.state.currentDate, isToDateValid: true });
      });
    } else if (pickerType === FilterTypes.StartFromTime) {
      this.setState({ isTimePickerVisible: false }, () => {
        this._updateStateValues(pickerType, { fromValue: this.state.defaultFromTime, isFromTimeValid: true });
      });
    } else if (pickerType === FilterTypes.StartToTime) {
      this.setState({ isTimePickerVisible: false }, () => {
        this._updateStateValues(pickerType, { toValue: this.state.defaultToTime, isToTimeValid: true });
      });
    } else if (pickerType === FilterTypes.EndFromTime) {
      this.setState({ isTimePickerVisible: false }, () => {
        this._updateStateValues(pickerType, { fromValue: this.state.defaultFromTime, isFromTimeValid: true });
      });
    } else if (pickerType === FilterTypes.EndToTime) {
      this.setState({ isTimePickerVisible: false }, () => {
        this._updateStateValues(pickerType, { toValue: this.state.defaultToTime, isToTimeValid: true });
      });
    } else if (pickerType === FilterTypes.FromCount) {
      this._updateStateValues(pickerType, { fromValue: '0', isFromCountValid: true });
    } else if (pickerType === FilterTypes.ToCount) {
      this._updateStateValues(pickerType, { toValue: '50', isToCountValid: true });
    }
    this.setState({ isFormValid: true });
  }
  _updateStateValues(name, updateValues) {
    let filterTypes = this.state.filterTypes;
    for (let index = 0; index < filterTypes.length; index++) {
      let view = filterTypes[index];
      let pickerType = view.pickerType;
      if (pickerType.fromKey === name) {
        _.keys(updateValues).forEach(key => {
          view[key] = updateValues[key];
        });
      } else if (pickerType.toKey === name) {
        _.keys(updateValues).forEach(key => {
          view[key] = updateValues[key];
        });
      }
      filterTypes[index] = view;
    }
    this.setState({ filterTypes: filterTypes });
  }
  _enablePicker(pickerType, pickerViewType) {
    if (pickerType === 'fieldActivityStartDate' || pickerType === 'fieldActivityEndDate' && pickerViewType === 'date') {
      this.setState({ pickerViewType: pickerViewType, pickerType: pickerType }, () => {
        this.setState({ isDateTimePickerVisible: true });
      });
    } else if (pickerType === 'fromCount') {
      this.setState({ pickerType: pickerType });
    } else if (pickerType === 'toCount') {
      this.setState({ pickerType: pickerType });
    } else {
      this.setState({ pickerViewType: pickerViewType, pickerType: pickerType }, () => {
        this.setState({ isTimePickerVisible: true });
      });
    }
  }
  onPressResetButton() {
    const FieldWorkFiltersDetail = [
      { key: FilterTypes.FieldActivityStartDate },
      { key: FilterTypes.FieldActivityEndDate },
      { key: FilterTypes.StartFromTime },
      { key: FilterTypes.StartToTime },
      { key: FilterTypes.EndFromTime },
      { key: FilterTypes.EndToTime },
      { key: FilterTypes.FromCount },
      { key: FilterTypes.ToCount },
    ];
    for (let index = 0; index < FieldWorkFiltersDetail.length; index++) {
      const pickerType = FieldWorkFiltersDetail[index].key;
      this.resetDateAndTime(pickerType);
    }
  }
  validateFitler(fromValue, toValue, type) {
    if (type === 'date') {
      const from = Date.parse(fromValue);
      const to = Date.parse(toValue);
      if ((from > to)) {
        return false;
      } else {
        return true;
      }
    } else if (type === 'time') {
      const from = Date.parse('1970-01-01T' + fromValue);
      const to = Date.parse('1970-01-01T' + toValue);
      if ((from > to)) {
        return false;
      } else {
        return true;
      }
    } else if (type === 'input') {
      if ((fromValue > toValue || fromValue === '' || toValue === '')) {
        return false;
      } else {
        return true;
      }
    }
  }

  render() {
    return (
      <Modal isVisible={this.state.isVisible}>
        <View style={{
          backgroundColor: 'white',
          padding: 22,
          borderRadius: 4,
          borderColor: 'rgba(0, 0, 0, 0.1)',
        }}>
          <View>
            <Text style={{ fontSize: 20, color: 'black', fontWeight: 'bold', textAlign: 'left' }}>Filter</Text>
            <TouchableOpacity
              style={{ position: 'absolute', right: 10 }}
              onPress={() => {
                this.setState({ isVisible: false }, () => {
                  this.props.closeBtnClicked();
                });
              }} >
              <Icon name="close-circle"
                type="MaterialCommunityIcons"
                style={{
                  color: Colors.headerBackground,
                }}
              />
            </TouchableOpacity>
          </View>
          <View style={{ height: 1, backgroundColor: 'darkgray', marginLeft: 5, marginTop: 5, marginRight: 5 }} />
          <DateTimePicker
            isVisible={this.state.isDateTimePickerVisible}
            mode="date"
            is24Hour={false}
            //maximumDate={new Date()}
            onConfirm={this._handleDatePicked}
            onCancel={this._hideDateTimePicker}
          />
          <DateTimePicker
            isVisible={this.state.isTimePickerVisible}
            mode="time"
            is24Hour={false}
            onConfirm={this._handleDatePicked}
            onCancel={this._hideDateTimePicker}
          />

          {this.state.filterTypes.map((view) => {
            // let isValid = this.validateFitler(view.fromValue, view.toValue, view.type);
            switch (view.type) {
              case 'date':
                return (<View>
                  <View style={{ flexDirection: 'row', paddingLeft: 5 }}>
                    <Text style={{ marginTop: 3, fontWeight: 'bold', color: Colors.headerBackground }}>
                      {messages(view.key)}
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row', paddingLeft: 5, height: 5 }}>
                    <View style={{ flex: 1 }}>
                      {!view.isFromDateValid ? (
                        <Label style={{ color: 'black', fontWeight: 'bold', fontSize: 12}}>
                          {'InValid From Date'}
                        </Label>
                      ) : null}
                    </View>
                    <View style={{ flex: 1 }}>
                      {!view.isToDateValid ? (
                        <Label style={{ color: 'black', fontWeight: 'bold', fontSize: 12}}>
                          {'InValid To Date'}
                        </Label>
                      ) : null}
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({ fromValueDate: view.fromValue, toValueDate: view.toValue }, () => {
                          this._enablePicker(view.pickerType.fromKey, view.type);
                        })
                      }
                      style={{ flexDirection: 'column', padding: 5, flex: 1 }}>
                      <View style={{ flexDirection: 'row', flex: 1, alignItems: 'baseline' }}>
                        <Text style={{ marginTop: 1, fontWeight: 'bold', paddingRight: 5 }}>
                          {messages('from')}
                        </Text>
                        <Icon name="calendar"
                          type="FontAwesome"
                          style={{ color: Colors.headerBackground, fontSize: 14 }}
                        />
                      </View>
                      <Text
                        style={[{
                          borderRadius: 3,
                          padding: 4,
                          fontSize: 14,
                          fontWeight: 'bold',
                          backgroundColor: '#ebebeb',
                        }]}
                      >
                        {/* {view.fromValue ? view.fromValue : ""} */}
                        {DateFormat(view.fromValue, DATE_UI)}
                      </Text>
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'column', padding: 5 }}>
                      <View style={{ marginTop: 20 }} />
                      {view.fromValue !== '' && <TouchableOpacity
                        onPress={() =>
                          this.resetDateAndTime(view.pickerType.fromKey)
                        }>
                        <Icon
                          name="close"
                          type="MaterialCommunityIcons"
                          style={{ color: 'darkgrey' }} />
                      </TouchableOpacity>}
                    </View>
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({ fromValueDate: view.fromValue, toValueDate: view.toValue }, () => {
                          this._enablePicker(view.pickerType.toKey, view.type);
                        })
                      }
                      style={{ flexDirection: 'column', padding: 5, flex: 1 }}>
                      <View style={{ flexDirection: 'row', flex: 1, alignItems: 'baseline' }}>
                        <Text style={{ marginTop: 3, fontWeight: 'bold', paddingRight: 5 }}>
                          {messages('to')}
                        </Text>
                        <Icon name="calendar"
                          type="FontAwesome"
                          style={{ color: Colors.headerBackground, fontSize: 14, textAlign: 'left' }}
                        />
                      </View>
                      <Text
                        style={[{
                          borderRadius: 3,
                          padding: 4,
                          fontSize: 14,
                          fontWeight: 'bold',
                          backgroundColor: '#ebebeb',
                        }]}
                      >
                        {/* {view.toValue ? view.toValue : ""} */}
                        {DateFormat(view.toValue, DATE_UI)}
                      </Text>
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'column', padding: 5 }}>
                      <View style={{ marginTop: 20 }} />
                      {view.toValue !== '' && <TouchableOpacity
                        onPress={() =>
                          this.resetDateAndTime(view.pickerType.toKey)
                        }>
                        <Icon
                          name="close"
                          type="MaterialCommunityIcons"
                          style={{ color: 'darkgrey' }} />
                      </TouchableOpacity>}
                    </View>
                  </View>
                  <View style={{ height: 1, backgroundColor: 'darkgray', marginLeft: 5, marginTop: 5, marginRight: 5 }} />
                </View>
                );
              case 'time':
                return (
                  <View>
                    <View style={{ flexDirection: 'row', paddingLeft: 5 }}>
                      <Text style={{ marginTop: 3, fontWeight: 'bold', color: Colors.headerBackground }}>
                        {messages(view.key)}
                      </Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingLeft: 5, height: 5 }}>
                      <View style={{ flex: 1 }}>
                        {!view.isFromTimeValid ? (
                          <Label style={{ color: 'black', fontWeight: 'bold', fontSize: 12}}>
                            {'InValid From Time'}
                          </Label>
                        ) : null}
                      </View>
                      <View style={{ flex: 1 }}>
                        {!view.isToTimeValid ? (
                          <Label style={{ color: 'black', fontWeight: 'bold', fontSize: 12}}>
                            {'InValid To Time'}
                          </Label>
                        ) : null}
                      </View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <TouchableOpacity
                        onPress={() =>
                          this.setState({ fromValueTime: view.fromValue, toValueTime: view.toValue }, () => {
                            this._enablePicker(view.pickerType.fromKey, view.type);
                          })
                        }
                        style={{ flexDirection: 'column', padding: 5, flex: 1 }}>
                        <View style={{ flexDirection: 'row', flex: 1, alignItems: 'baseline' }}>
                          <Text style={{ marginTop: 3, fontWeight: 'bold', paddingRight: 5 }}>
                            {messages('from')}
                          </Text>
                          <Icon name="clock-o"
                            type="FontAwesome"
                            style={{ color: Colors.headerBackground, fontSize: 16, textAlign: 'left' }}
                          />
                        </View>
                        <Text
                          style={[{
                            borderRadius: 3,
                            padding: 4,
                            fontSize: 14,
                            fontWeight: 'bold',
                            backgroundColor: '#ebebeb',
                          }]}
                        >
                          {TimeFormat('1970-01-01T' + view.fromValue, TIME_UI)}
                        </Text>
                      </TouchableOpacity>
                      <View style={{ flexDirection: 'column', padding: 5 }}>
                        <View style={{ marginTop: 20 }} />
                        {view.fromValue !== '' && <TouchableOpacity
                          onPress={() =>
                            this.resetDateAndTime(view.pickerType.fromKey)
                          }>
                          <Icon
                            name="close"
                            type="MaterialCommunityIcons"
                            style={{ color: 'darkgrey' }} />
                        </TouchableOpacity>}
                      </View>
                      <TouchableOpacity style={{ flexDirection: 'column', padding: 5, flex: 1 }}
                        onPress={() =>
                          this.setState({ fromValueTime: view.fromValue, toValueTime: view.toValue }, () => {
                            this._enablePicker(view.pickerType.toKey, view.type);
                          })
                        }>
                        <View style={{ flexDirection: 'row', flex: 1, alignItems: 'baseline' }}>
                          <Text style={{ marginTop: 3, fontWeight: 'bold', paddingRight: 5 }}>
                            {messages('to')}
                          </Text>
                          <Icon name="clock-o"
                            type="FontAwesome"
                            style={{ color: Colors.headerBackground, fontSize: 16, textAlign: 'left' }}
                          />
                        </View>
                        <Text
                          style={[{
                            borderRadius: 3,
                            padding: 4,
                            fontSize: 14,
                            fontWeight: 'bold',
                            backgroundColor: '#ebebeb',
                          }]}
                        >
                          {TimeFormat('1970-01-01T' + view.toValue, TIME_UI)}
                        </Text>
                      </TouchableOpacity>
                      <View style={{ flexDirection: 'column', padding: 5 }}>
                        <View style={{ marginTop: 20 }} />
                        {view.toValue !== '' && <TouchableOpacity
                          onPress={() =>
                            this.resetDateAndTime(view.pickerType.toKey)
                          }>
                          <Icon
                            name="close"
                            type="MaterialCommunityIcons"
                            style={{ color: 'darkgrey' }} />
                        </TouchableOpacity>}
                      </View>
                    </View>
                    <View style={{ height: 1, backgroundColor: 'darkgray', marginLeft: 5, marginTop: 5, marginRight: 5 }} />
                  </View>
                );
              case 'input':
                  return (
                    <View>
                      <View style={{ paddingLeft: 5 }}>
                        <Text style={{ marginTop: 3, fontWeight: 'bold', color: Colors.headerBackground }}>
                          {messages(view.key)}
                        </Text>
                      </View>
                      <View style={{ flexDirection: 'row', paddingLeft: 5, height: 5 }}>
                        <View style={{ flex: 1 }}>
                          {!view.isFromCountValid ? (
                            <Label style={{ color: 'black', fontWeight: 'bold', fontSize: 12}}>
                              {'InValid From Count'}
                            </Label>
                          ) : null}
                        </View>
                        <View style={{ flex: 1 }}>
                          {!view.isToCountValid ? (
                            <Label style={{ color: 'black', fontWeight: 'bold', fontSize: 12}}>
                              {'InValid To Time'}
                            </Label>
                          ) : null}
                        </View>
                      </View>
                      <View style={{ flexDirection: 'row' }}>
                        <View style={{ flexDirection: 'column', padding: 5, flex: 1, height: 60 }}>
                          <TouchableOpacity
                            style={{ flexDirection: 'row', flex: 1, alignItems: 'baseline' }}
                            onPress={() =>
                              this.setState({ pickerType: view.pickerType.fromKey })
                            }>
                            <Text style={{ marginTop: 3, fontWeight: 'bold', paddingRight: 5 }}>
                              {messages('from')}
                            </Text>
                            <Icon name="format-list-numbered"
                              type="MaterialIcons"
                              style={{ color: Colors.headerBackground, fontSize: 16 }}
                            />
                          </TouchableOpacity>
                          <Input
                            style={{ fontSize: 15, padding: 4, backgroundColor: '#ebebeb', borderRadius: 3 }}
                            bordered
                            keyboardType="numeric"
                            value={view.fromValue ? view.fromValue : ''}
                            autoFocus={true}
                            onChangeText={(text) => {
                              let isFromCountValid = this.validateFitler(text, view.toValue, view.type);
                              if (!isFromCountValid) {
                                this._updateStateValues(view.pickerType.fromKey, { fromValue: text, isFromCountValid: false });
                              } else {
                                this._updateStateValues(view.pickerType.fromKey, { fromValue: text, isFromCountValid: true, isToCountValid: true });
                              }
                            }} />
                        </View>
                        <View style={{ flexDirection: 'column', padding: 5 }}>
                          <View style={{ marginTop: 20 }} />
                          <TouchableOpacity
                            onPress={() =>
                              this.resetDateAndTime(view.pickerType.fromKey)
                            }>
                            <Icon
                              name="close"
                              type="MaterialCommunityIcons"
                              style={{ color: 'darkgrey' }} />
                          </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: 'column', padding: 5, flex: 1 }}>
                          <TouchableOpacity
                            style={{ flexDirection: 'row', flex: 1, alignItems: 'baseline' }}
                            onPress={() =>
                              this.setState({ pickerType: view.pickerType.toKey })
                            }>
                            <Text style={{ marginTop: 3, fontWeight: 'bold', paddingRight: 5 }}>
                              {messages('to')}
                            </Text>
                            <Icon name="format-list-numbered"
                              type="MaterialIcons"
                              style={{ color: Colors.headerBackground, fontSize: 16 }}
                            />
                          </TouchableOpacity>
                          <Input
                            style={{ fontSize: 15, padding: 4, backgroundColor: '#ebebeb', borderRadius: 3 }}
                            bordered
                            keyboardType="numeric"
                            value={view.toValue ? view.toValue : ''}
                            autoFocus={true}
                            onChangeText={(text) => {
                              let isFromCountValid = this.validateFitler(view.fromValue, text, view.type);
                              if (!isFromCountValid) {
                                this._updateStateValues(view.pickerType.toKey, { toValue: text, isToCountValid: false });
                              } else {
                                this._updateStateValues(view.pickerType.toKey, { toValue: text, isFromCountValid: true, isToCountValid: true });
                              }
                            }} />
                        </View>
                        <View style={{ flexDirection: 'column', padding: 5 }}>
                          <View style={{ marginTop: 20 }} />
                          <TouchableOpacity
                            onPress={() =>
                              this.resetDateAndTime(view.pickerType.toKey)
                            }>
                            <Icon
                              name="close"
                              type="MaterialCommunityIcons"
                              style={{ color: 'darkgrey' }} />
                          </TouchableOpacity>
                        </View>
                      </View>
                      <View style={{ height: 1, backgroundColor: 'darkgray', marginLeft: 5, marginTop: 5, marginRight: 5 }} />
                    </View>
                  );
              default:
                break;
            }
          })}
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <TouchableOpacity
              style={{ flex: 1, padding: 2 }}
              onPress={() => {
                this.onPressResetButton();
              }}>
              <Text style={{ textAlign: 'center', padding: 10, marginTop: 10, color: 'white', backgroundColor: Colors.headerBackground, borderRadius: 5 }}>
                {'Reset'}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ flex: 1, padding: 2 }}
              onPress={() => {
                if (this.state.isFormValid) {
                  this.setState({ isVisible: false }, () => {
                    this.props.onPressApplyButton(this.state.filterTypes);
                  });
                } else {
                  Toast.show(messages('pleaseEnterValidData'), Toast.LONG);
                }
              }}>
              <Text style={{ textAlign: 'center', padding: 10, marginTop: 10, color: 'white', backgroundColor: Colors.headerBackground, borderRadius: 5 }}>
                {'Apply'}
              </Text>
            </TouchableOpacity>
          </View>
        </View >
      </Modal >
    );
  }
}


