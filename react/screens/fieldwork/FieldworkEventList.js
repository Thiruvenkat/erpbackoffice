/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable prettier/prettier */
import { Button, Fab, Icon } from 'native-base';
import React from 'react';
import NetInfo from '@react-native-community/netinfo';
import { ActivityIndicator, Dimensions, SectionList, StatusBar, StyleSheet, Text, View } from 'react-native';
import { DateFormat, DATE_TIME_SERVER } from 'react-native-erp-mobile-library/react/common/Common';
import PreparingProgress from 'react-native-erp-mobile-library/react/component/PreparingProgress';
import BaseComponent from 'react-native-erp-mobile-library/react/screens/masters/BaseComponent';
import Toast from 'react-native-simple-toast';
import MasterConstants from '../../constant/MasterConstants';
import { messages } from 'react-native-erp-mobile-library/react/i18n/i18n';
import { getAllFieldActivityEvents } from '../../controller/FieldworkController';
import { getAllOrderDetails, getAllUnClearedList, getAllARAPFinanceList } from 'react-native-erp-mobile-library/react/controller/SyncDataController';
import Route from '../../router/Route';
import { GetAllOrders } from 'react-native-erp-mobile-library/react/screens/orders/dbHelper/OrderDBHelper';
import OrderListItemView from 'react-native-erp-mobile-library/react/screens/orders/OrderListItemView';
import { GetAllFieldworkEvents } from './dbHelper/FieldWorkDBHelper';
import { GetAllUnClearedList } from 'react-native-erp-mobile-library/react/screens/finance/dbHelper/FinanceDBHelper';
import UnClearedItemView from '../finance/UnClearedItemView';
import ARAPFinanceItemView from '../finance/ARAPFinanceItemView';
import {renderHeaderView } from 'react-native-erp-mobile-library/react/util/HeaderUtil';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import { GetAllARAPList } from 'react-native-erp-mobile-library/react/screens/finance/dbHelper/FinanceDBHelper';
import { sessionInfo } from 'react-native-erp-mobile-library';
import ChangeStatus from 'react-native-erp-mobile-library/react/screens/masters/changeStatus';
import { Styles } from 'react-native-erp-mobile-library/react/style/Styles';
import Colors from 'react-native-erp-mobile-library/react/style/Colors';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

// create a component
export default class FieldworkEventList extends BaseComponent {
  constructor(props) {
     super(props);
    this.state = {
      customerTypeId: this.props.route.params.customerTypeId,
      isPreparing: false,
      isLoading: false,
      orderId: [],
      orderList: [],
      isOrderSynced: false,
      financeId: [],
      financeList: [],
      isFinanceSynced: false,
      stockTakingId: [],
      stockTakingList: [],
      isStockTakingSynced: false,
      arapFinanceList: [],
      arapFinanceId: [],
      isArapFinanceSynced: false,
      selectedValues: {},
      session: {},
      loadMore: false,
      offset: 0,
      limit: 10,
      isServiceCalled: false,
      fieldActivityCustomerVisit: this.props.route.params.fieldActivityVisit,
      superParentActivity: this.props.route.params.parentActivity,
      isShowCustomerSearchModal: false,
      fabActive: true,
      dbName: null,
      statusModalVisibility: false,
      formStatusId: null,
      orderToStatusList: [],
    };
    this.onPressItem = this.onPressItem.bind(this);
  }

  componentDidMount() {
    this._initHeader();
    this._locationEnable();
    const session = sessionInfo({ type: 'get', key: 'loginInfo' });
    this.setState({ session: session }, () => {
      this._getAllEventDetails();
    });
  }

  _initHeader() {
    this.props.navigation.setParams({
      headerTitle: this.props.route.params?.headerTitle ?? messages('fieldworkEventList'),
      headerType: this.props.route.params?.headerType ?? HeaderTypes.HeaderBack,
    });
    this.props.navigation.setOptions(renderHeaderView(this));
  }

  componentWillUnmount() {
    // this.backHandler.remove();
  }

  _getAllEventDetails() {
    var filter = {
      limit: this.state.limit,
      offset: this.state.offset,
      orderBy: 'last_updated desc',
      conditions: [{
        column: 'field_activity_customer_visit_id',
        value: [this.state.fieldActivityCustomerVisit.fieldActivityCustomerVisitId],
        operator: 'in',
      }],
    };
    GetAllFieldworkEvents((events) => {
      if (events.length > 0) {
        let orderId = [], financeId = [], stockTakingId = [], arapFinanceId = [];
        for (let index = 0; index < events.length; index++) {
          const event = events[index];
          if (event.eventTypeId === MasterConstants.orderTypeSalesInvoice || event.eventTypeId === MasterConstants.orderTypeQuotation || event.eventTypeId === MasterConstants.orderTypeSalesOrder) {
            orderId.push(event.eventReferenceId);
          } else if (event.eventTypeId === MasterConstants.masterFinanceReceiptTypeId) {
            financeId.push(event.eventReferenceId);
          } else if (event.eventTypeId === MasterConstants.orderTypeStockTaking) {
            stockTakingId.push(event.eventReferenceId);
          }
          if (event.eventTypeId === MasterConstants.orderTypeSalesInvoice) {
            arapFinanceId.push(event.eventReferenceId);
          }
        }
        this.setState({
          isLoading: false,
          isOrderSynced: !(orderId.length > 0),
          orderId: orderId,
          isFinanceSynced: !(financeId.length > 0),
          financeId: financeId,
          isStockTakingSynced: !(stockTakingId.length > 0),
          stockTakingId: stockTakingId,
          isArapFinanceSynced: !(arapFinanceId.length > 0),
          arapFinanceId: arapFinanceId,
        }, () => {
          if (orderId.length > 0) {
            this._getAllOrderDetails();
          }
          if (financeId.length > 0) {
            this._getAllUnclearedList();
          }
          if (arapFinanceId.length > 0) {
            this._getAllARAPFinanceList();
          }
          if (stockTakingId.length > 0) {
            this._getAllStockTakingOrderDetails();
          }
        });
      } else if (!this.state.isServiceCalled) {
        this._getAllFieldActivityEventsFromServer();
      } else {
        this.setState({ isLoading: false, loadMore: false, isOrderSynced: true, isFinanceSynced: true, isStockTakingSynced: true });
      }
    }, filter);
  }

  _getAllFieldActivityEventsFromServer() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        let filter = {
          orderBy: '',
          limit: 500,
          offset: 0,
        };
        filter.conditions = [{
          column: 'fieldActivityCustomerVisit.fieldActivityCustomerVisitId',
          value: this.state.fieldActivityCustomerVisit.fieldActivityCustomerVisitId,
          dataType: 'String',
          operator: 'in',
        }];
        getAllFieldActivityEvents(this.state.session, filter, (isSuccess) => {
          this.setState({ isServiceCalled: true });
          if (isSuccess) {
            this._getAllEventDetails();
          } else {
            this.setState({ isPreparing: false, isLoading: false, loadMore: false, isOrderSynced: true, isFinanceSynced: true, isStockTakingSynced: true });
          }
        });
      } else {
        this.setState({ isPreparing: false, isLoading: false, loadMore: false });
      }
    });
  }
  onRefresh() {
    NetInfo.fetch().then(isConnected => {
      this.setState({ isLoading: isConnected }, () => {
        if (isConnected) {
          this.setState({ offset: 0, isServiceCalled: false }, () => {
            this._getAllFieldActivityEventsFromServer();
          });
        } else {
          Toast.show(messages('makeInternetConnection'), Toast.SHORT);
        }
      });
    });
  }
  _getAllOrderDetails() {
    let filter = {
      orderBy: 'last_updated desc',
      conditions: [{
        value: this.state.orderId,
        column: 'order_id',
        operator: 'in',
      }],
    };
    GetAllOrders(this.state.session.databaseName, (datas) => {
      if (!this.state.isOrderSynced) {
        // !this.state.isOrderSynced && (datas.length != this.state.orderId.length || this.state.orderList.length < datas.length)
        this._getAllOrderDetailsFromServer(this.state.orderId, 'order');
      } else {
        this.setState({ orderList: datas, isOrderSynced: true });
      }
    }, filter);
  }

  _getAllStockTakingOrderDetails() {
    let filter = {
      orderBy: 'last_updated desc',
      conditions: [{
        value: this.state.stockTakingId,
        column: 'order_id',
        operator: 'in',
      }],
    };
    GetAllOrders(this.state.session.databaseName, (datas) => {
      if (!this.state.isStockTakingSynced && (datas.length !== this.state.stockTakingId.length || this.state.stockTakingList.length < datas.length)) {
        this._getAllOrderDetailsFromServer(this.state.stockTakingId, 'stockTaking');
      } else {
        this.setState({ stockTakingList: datas, isStockTakingSynced: true });
      }
    }, filter);
  }

  _getAllOrderDetailsFromServer(orderId, type) {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        let syncValue = {
          conditions: [{
            'value': orderId.join(','),
            'column': 'orderId',
            'operator': 'in',
            'dataType': 'String',
          }],
        };
        getAllOrderDetails(this.state.session, syncValue, (isSuccess) => {
          if (type === 'stockTaking') {
            this.setState({ isStockTakingSynced: isSuccess }, () => {
              if (isSuccess)
                {this._getAllStockTakingOrderDetails();}
            });
          } else {
            this.setState({ isOrderSynced: isSuccess }, () => {
              if (isSuccess)
                {this._getAllOrderDetails();}
            });
          }
        });
      } else {
        this.setState({ isOrderSynced: false });
      }
    });
  }

  _getAllUnclearedList() {
    var filter = {
      orderBy: 'last_update_time desc',
      conditions: [{
        'value': this.state.financeId,
        'column': 'uc_payment_id',
        'operator': 'in',
        'dataType': 'String',
      }],
    };
    GetAllUnClearedList((datas) => {
      if ((datas.length !== this.state.financeId.length || this.state.stockTakingList.length < datas.length) && !this.state.isFinanceSynced) {
        this._getAllUnclearedListFromServer();
      } else {
        this.setState({ financeList: datas, isFinanceSynced: true });
      }
    }, filter);
  }

  _getAllUnclearedListFromServer() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        let filter = {
          orderBy: 'up.last_updated',
          conditions: [
            {
              'value': this.state.financeId.join(','),
              'column': 'up.uc_payment_id',
              'operator': 'in',
              'dataType': 'String',
            },
          ],
        };
        getAllUnClearedList(this.state.session, filter, (isSuccess) => {
          this.setState({ isFinanceSynced: isSuccess });
          if (isSuccess) {
            this._getAllUnclearedList();
          }
        });
      } else {
        this.setState({ isFinanceSynced: false });
      }
    });
  }

  _getAllARAPFinanceList() {
    var filter = {
      orderBy: 'last_update_time desc',
      conditions: [{
        column: 'in_out_flag',
        value: ['1'],
        operator: 'in',
      },
      {
        column: 'trans_id',
        value: this.state.arapFinanceId,
        operator: 'in',
      },
      ],
    };
    GetAllARAPList((datas) => {
      if (datas.length > 0) {
        this.setState({
          arapFinanceList: datas,
          isArapFinanceSynced: true,
        });
      } else {
        this._getAllARAPFinanceListFromServer();
      }
    }, filter);
  }

  _getAllARAPFinanceListFromServer() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        let filter = {
          orderBy: 'al.trans_date',
          conditions: [
            {
              column: 'al.last_updated',
              value: '1483228800000',
              dataType: 'Date',
              operator: '>',
            },
            {
              column: 'al.in_out_flag',
              value: [1],
              dataType: 'Long',
              operator: 'in',
            },
            {
              column: 'trans_id',
              value: this.state.arapFinanceId,
              dataType: 'String',
              operator: 'in',
            },

          ],
        };
        getAllARAPFinanceList(this.state.session, filter, (isSuccess) => {
          if (isSuccess) {
            this._getAllARAPFinanceList();
          }
        });
      } else {
        this.setState({ isArapFinanceSynced: false });
      }
    });
  }
  onPressItem(finance) {
    if (MasterConstants.arApTransType.includes(finance.trans_type_id) && finance.status_id === MasterConstants.savedPendingStatus) {
      if (Number(finance.ucAmount) !== Number(finance.amount) - Number(finance.adjust_amount)) {
        const selectedValues = this.state.selectedValues;
        const transactionIds = Object.keys(selectedValues);
        if (transactionIds.length === 0 || selectedValues[transactionIds[0]].ass_acc_id === finance.ass_acc_id) {
          if (!transactionIds.includes(finance.trans_id)) {
            selectedValues[finance.trans_id] = finance;
          } else {
            delete selectedValues[finance.trans_id];
          }
          this.state.selectedValues = selectedValues;
          return true;
        } else {
          Toast.show('select same account', Toast.SHORT);
          return false;
        }
      } else {
        Toast.show('This Transaction not allowed', Toast.SHORT);
        return false;
      }
    } else {
      Toast.show('This Transaction not allowed', Toast.SHORT);
    }
  }

  // renderItem({ item, index }) {
  //   return (
  //     <FieldworkEventItemView
  //       events={item}
  //       session={this.state.session}
  //       navigation={this.props.navigation}
  //       index={index}
  //       userLocation={this.state.userLocation}
  //     />
  //   );
  // }

  _renderNoDataView() {
    return (
      // <View style={{ padding: 30, alignItems: 'center', justifyContent: 'center' }}>
      //   <Text style={{ color: Colors.headerBackground, fontSize: 14 }}>
      //     {"No data found"}
      //   </Text>
      // </View>
      <View
        style={{
          // width: DEVICE_WIDTH,
          height: 140,
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
        }}
      >
        <Icon
          name="cube"
          type="FontAwesome"
          style={{ color: Colors.headerBackground, fontSize: 40 }}
        />
        <Text style={{ textAlign: 'center', color: 'darkgrey', marginTop: 5 }}>
          No Orders
          </Text>
      </View>
    );
  }

  _renderLoadingView() {
    return (
      <View style={{ padding: 30, alignItems: 'center', justifyContent: 'center' }}>
        <ActivityIndicator color={Colors.headerBackground} size="small" />
      </View>
    );
  }

  _onPressChangeOrderStatus(item) {
    if (item.orderStatusId !== MasterConstants.waitingForApproval) {
      this.setState({
        statusModalVisibility: item.orderStatusId !== MasterConstants.waitingForApproval ? item.orderStatusId !== MasterConstants.cancelled ? true : false : false,
        orderId: item.orderId,
        orderTypeId: item.orderType,
        formStatusId: item.orderStatusId,
        orderToStatusList: [],
      });
    }
  }

  _renderOrderItem({ item, index }) {
    if (!this.state.isOrderSynced) {
      return this._renderLoadingView();
    }
    if (item === 'empty') {
      return this._renderNoDataView();
    }
    return (
      <OrderListItemView
        order={item}
        navigation={this.props.navigation}
        parentProps={this.props}
        index={index}
        onPressChangeOrderStatus={this._onPressChangeOrderStatus.bind(this)}
      />
    );
  }

  _renderFinanceItem({ item, index }) {
    if (!this.state.isFinanceSynced) {
      return this._renderLoadingView();
    }
    if (item === 'empty') {
      return (this._renderNoDataView());
    }
    return (
      <UnClearedItemView
        finance={item}
        index={index}
        session={this.state.session}
        navigation={this.props.navigation}
        parentProps={this.props}
      />
    );
  }

  _renderArapFinanceItem({ item, index }) {
    if (!this.state.isArapFinanceSynced) {
      return this._renderLoadingView();
    }
    if (item === 'empty') {
      return (this._renderNoDataView());
    }
    return (
      <ARAPFinanceItemView
        finance={item}
        index={index}
        session={this.state.session}
        navigation={this.props.navigation}
        parentProps={this.props}
        onPressItem={this.onPressItem}
      />
    );
  }

  _renderStockTakingItem({ item, index }) {
    if (!this.state.isStockTakingSynced) {
      return this._renderLoadingView();
    }
    if (item === 'empty') {
      return (this._renderNoDataView());
    }
    return (
      <OrderListItemView
        order={item}
        navigation={this.props.navigation}
        parentProps={this.props}
        index={index}
        onPressChangeOrderStatus={this._onPressChangeOrderStatus.bind(this)}
      />
    );
  }

  ListEmptyView = () => {
    return (
      <View
        style={{
          width: DEVICE_WIDTH,
          height: DEVICE_HEIGHT - 40,
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
        }}
      >
        <Icon
          name="calendar-plus-o"
          type="FontAwesome"
          style={{ color: Colors.headerBackground, fontSize: 40 }}
        />
        <Text style={{ textAlign: 'center', color: 'darkgrey', marginTop: 5 }}>
          You have no event's.
                </Text>
      </View>
    );
  };

  render() {
    if (this.state.isPreparing) {
      return (
        <PreparingProgress />
      );
    } else if (this.state.userLocation === 'unknown') {
      return (
        <PreparingProgress />
      );
    } else {
      return (
        <View
          padder
          style={{
            backgroundColor: Styles.appViewBackground.backgroundColor,
          }}
        >
          <StatusBar
            backgroundColor={Styles.statusBar.backgroundColor}
            barStyle="light-content"
          />
          <View style={{ height: DEVICE_HEIGHT - 80 }}>
            <SectionList
              renderSectionHeader={({ section: { title } }) => (
                <View style={{ flex: 1, flexDirection: 'row', backgroundColor: Colors.headerBackground }}>
                  <Text style={{ flex: 7, fontWeight: 'bold', fontSize: 18, padding: 10, color: 'white' }}>{title}</Text>
                </View>
              )}
              sections={[
                { title: 'Orders', data: this.state.orderList.length > 0 ? this.state.orderList : ['empty'], renderItem: this._renderOrderItem.bind(this) },
                { title: 'Finance', data: this.state.financeList.length > 0 ? this.state.financeList : ['empty'], renderItem: this._renderFinanceItem.bind(this) },
                // { title: 'AR/AP List', data: this.state.arapFinanceList.length > 0 ? this.state.arapFinanceList : ['empty'], renderItem: this._renderArapFinanceItem.bind(this) },
                { title: 'Stock Taking', data: this.state.stockTakingList.length > 0 ? this.state.stockTakingList : ['empty'], renderItem: this._renderStockTakingItem.bind(this) },
              ]}
              keyExtractor={(item, index) => item + index}
              refreshing={this.state.isLoading}
              onRefresh={() => { this.onRefresh(); }}
            />
            <ChangeStatus
              orderId={this.state.orderId}
              orderTypeId={this.state.orderTypeId}
              formStatusId={this.state.formStatusId}
              orderToStatusList={typeof this.state.orderToStatusList !== 'undefined' ? this.state.orderToStatusList : []}
              visibleModal={this.state.statusModalVisibility}
              closeChangeStatusModal={() => { this.setState({ statusModalVisibility: false }); }} />
          </View>
          {this.state.fieldActivityCustomerVisit.statusId === MasterConstants.statusWorkInProgressId && (this.props.route.params.screenType !== undefined &&
            this.props.route.params.screenType === 'editable' &&
            this.state.fieldActivityCustomerVisit.statusId !== MasterConstants.statusWorkClosedId) ?
            <Fab style={{ backgroundColor: Colors.headerBackground, size: 5 }}
              active={this.state.fabActive}
              direction="up"
              position="bottomRight"
              onPress={() => this.setState({ fabActive: !this.state.fabActive })}>
              <Icon type="FontAwesome" name="plus" style={styles.fabicon} />
              <Button style={{ backgroundColor: '#e67e22' }}
                onPress={() => {
                  this.setState({ fabActive: false }, () => {
                    this.props.navigation.navigate(Route.CreateOrder, {
                      orderTypeId: MasterConstants.orderTypeQuotation,
                      headerTitle: messages(MasterConstants[MasterConstants.orderTypeQuotation]),
                      fieldwork: this._getFieldworkOrderEvent(),
                      headerType: HeaderTypes.HeaderBack,
                      customerId: this.state.fieldActivityCustomerVisit.customerId,
                      customerName: this.state.fieldActivityCustomerVisit.customerName,
                    });
                  });
                }}>
                <View
                  style={{ width: 30, height: 30, borderRadius: 30 / 2, backgroundColor: 'transparent', justifyContent: 'center' }}>
                  <Text style={[styles.text, { color: 'white', fontSize: 13, textAlign: 'center' }]}>
                    {'QO'}
                  </Text>
                </View>
              </Button>
              {this.state.customerTypeId === MasterConstants.CustomerTypeRetailerId && <Button style={{ backgroundColor: '#e67e22' }}
                onPress={() => {
                  this.setState({ fabActive: false }, () => {
                    this.props.navigation.navigate(Route.StockTaking, {
                      orderTypeId: MasterConstants.orderTypeStockTaking,
                      headerTitle: messages(MasterConstants[MasterConstants.orderTypeStockTaking]),
                      fieldwork: this._getFieldworkOrderEvent(),
                      headerType: HeaderTypes.HeaderBack,
                      dbName: this.state.session.databaseName,
                      customerId: this.state.fieldActivityCustomerVisit.customerId,
                      customerName: this.state.fieldActivityCustomerVisit.customerName,
                    });
                  });
                }}>
                <View
                  style={{ width: 30, height: 30, borderRadius: 30 / 2, backgroundColor: 'transparent', justifyContent: 'center' }}>
                  <Text style={[styles.text, { color: 'white', fontSize: 13, textAlign: 'center' }]}>
                    {'STK'}
                  </Text>
                </View>
              </Button>}
              <Button style={{ backgroundColor: '#e67e22' }}
                onPress={() => {
                  this.setState({ fabActive: false }, () => {
                    this.props.navigation.navigate(Route.CreateOrder, {
                      orderTypeId: MasterConstants.orderTypeSalesOrder,
                      headerTitle: messages(MasterConstants[MasterConstants.orderTypeSalesOrder]),
                      headerType: HeaderTypes.HeaderBack,
                      fieldwork: this._getFieldworkOrderEvent(),
                      customerId: this.state.fieldActivityCustomerVisit.customerId,
                      customerName: this.state.fieldActivityCustomerVisit.customerName,
                    });
                  });
                }}>
                <View
                  style={{ width: 30, height: 30, borderRadius: 30 / 2, backgroundColor: 'transparent', justifyContent: 'center' }}>
                  <Text style={[styles.text, { color: 'white', fontSize: 13, textAlign: 'center' }]}>
                    {'SO'}
                  </Text>
                </View>
              </Button>
              <Button style={{ backgroundColor: 'green' }}
                onPress={() => {
                  this.setState({ fabActive: false }, () => {
                    this.props.navigation.navigate(Route.CreateReceiptPayout, {
                      inOutFlag: MasterConstants.inOutFlagReceivable,
                      fieldwork: this._getFieldworkFinanceEvent(),
                      headerTitle: 'Receipt',
                      headerType: HeaderTypes.HeaderBack,
                      customerId: this.state.fieldActivityCustomerVisit.customerId,
                      customerName: this.state.fieldActivityCustomerVisit.customerName,
                      transList: this.state.selectedValues,
                    });
                  });
                }}>
                <View
                  style={{ width: 30, height: 30, borderRadius: 30 / 2, backgroundColor: 'transparent', justifyContent: 'center' }}>
                  <Text style={[styles.text, { color: 'white', fontSize: 13, textAlign: 'center' }]}>
                    {'RE'}
                  </Text>
                </View>
              </Button>
            </Fab> : null}
        </View>
      );
    }
  }

  _getFieldworkOrderEvent() {
    return {
      customerId: this.state.fieldActivityCustomerVisit.customerId,
      fieldActivityEvents: {
        fieldActivityCustomerVisit: { id: this.state.fieldActivityCustomerVisit.fieldActivityCustomerVisitId },
        eventDateTime: DateFormat(new Date(), DATE_TIME_SERVER),
        tenant: {
          id: this.state.session.info.tenantId,
        },
        eventLocation: this.state.userLocation && this.state.userLocation !== 'unknown' ? JSON.stringify({ longitude: this.state.userLocation.longitude, latitude: this.state.userLocation.latitude, altitude: 0, accuracy: 0, bearing: 0, verticalAccuracyMeters: 0, provider: 0 }) : JSON.stringify({ longitude: 1, latitude: 0, altitude: 0, accuracy: 0, bearing: 0, verticalAccuracyMeters: 0, provider: 0 }),
        addressComplaintFlag: 1,
        eventTypeId: MasterConstants.orderTypeStockTaking,
      },
    };
  }

  _getFieldworkFinanceEvent() {
    return {
      customerId: this.state.fieldActivityCustomerVisit.customerId,
      fieldActivityEvents: {
        fieldActivityCustomerVisitId: this.state.fieldActivityCustomerVisit.fieldActivityCustomerVisitId,
        eventDateTime: DateFormat(new Date(), DATE_TIME_SERVER),
        tenantId: this.state.session.info.tenantId,
        eventLocation: this.state.userLocation && this.state.userLocation !== 'unknown' ? JSON.stringify({ longitude: this.state.userLocation.longitude, latitude: this.state.userLocation.latitude, altitude: 0, accuracy: 0, bearing: 0, verticalAccuracyMeters: 0, provider: 0 }) : JSON.stringify({ longitude: 1, latitude: 0, altitude: 0, accuracy: 0, bearing: 0, verticalAccuracyMeters: 0, provider: 0 }),
        addressComplaintFlag: 1,
      },
    };
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  currency: {
    fontSize: 14,
    fontWeight: 'normal',
  },
  fabicon: {
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'white',
    fontSize: 22,
  },
  leftContainer: {
    paddingLeft: 10,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
  },
  rightContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
});
