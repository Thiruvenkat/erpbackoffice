/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { Fab, Icon, Input } from 'native-base';
import {
  ActivityIndicator,
  Dimensions,
  FlatList,
  StatusBar,
  StyleSheet,
  Switch,
  Text,
  TouchableOpacity,
  View,
  Alert,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Modal from 'react-native-modal';
import Toast from 'react-native-simple-toast';
import DateTimePicker from 'react-native-modal-datetime-picker';
import NetInfo from '@react-native-community/netinfo';

import {
  DateFormat,
  DATE_TIME_SERVER,
  DATE_TIME_UI,
  DATE_UI,
} from 'react-native-erp-mobile-library/react/common/Common';
import PreparingProgress from 'react-native-erp-mobile-library/react/component/PreparingProgress';
import BaseComponent from 'react-native-erp-mobile-library/react/screens/masters/BaseComponent';
import AppConstants from '../../constant/AppConstants';
import MasterConstants from '../../constant/MasterConstants';
import {
  createFieldActivity,
  getAllFieldActivity,
} from '../../controller/FieldworkController';
import {
  GetAllActivityDetails,
  InsertFieldActivity,
} from './dbHelper/FieldWorkDBHelper';
import FieldworkActivityItemView from './FieldworkActivityItemView';
import { sessionInfo } from 'react-native-erp-mobile-library';
import Route from '../../router/Route';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import { renderHeaderView } from 'react-native-erp-mobile-library/react/util/HeaderUtil';
import { messages } from 'react-native-erp-mobile-library/react/i18n/i18n';
import FieldWorkFilters from './FieldWorkFilters';
import { GetAllCustomers } from 'react-native-erp-mobile-library/react/screens/customers/dbHelper/CustomerDB';
import { GetAllAppConfigDetail } from 'react-native-erp-mobile-library/react/screens/orders/dbHelper/CreateOrderDBHelper';
import { Styles } from 'react-native-erp-mobile-library/react/style/Styles';
import Colors from 'react-native-erp-mobile-library/react/style/Colors';
import Loader from 'react-native-erp-mobile-library/react/component/Loader';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

var UUID = require('react-native-uuid');

export const CurrentDate = new Date(),
  CurrentYear = CurrentDate.getFullYear(),
  CurrentMonth = CurrentDate.getMonth();
// create a component
export default class FieldworkActivityList extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      isPreparing: true,
      isLoading: true,
      activityList: [],
      session: sessionInfo({ type: 'get', key: 'loginInfo' }),
      loadMore: false,
      offset: 0,
      limit: 10,
      userId: null,
      createActivityModalVisible: false,
      isDateTimePickerVisible: false,
      isServiceCalled: false,
      fieldActivityId: null,
      fromScrnType: props.route.params?.fromScrnType ?? 'view',
      trackingType: '',
      isFieldWorkFilterEnabled: false,
      currentDate: DateFormat(new Date(), DATE_UI),
      defaultDate: '',
      appConfig: {},
      userLocation: '',
      gpsStatus: false,
      filterTypes: [
        {
          key: 'fieldActivityDate',
          fromValue: DateFormat(
            new Date(CurrentYear, CurrentMonth, 1),
            DATE_TIME_UI,
          ),
          toValue: DateFormat(new Date(), DATE_TIME_UI),
          isFromDateValid: true,
          isToDateValid: true,
          pickerType: {
            fromKey: 'fieldActivityStartDate',
            toKey: 'fieldActivityEndDate',
          },
          column: 'date',
          type: 'date',
        },
        {
          key: 'startTime',
          fromValue: '00:00:00',
          toValue: '23:59:59',
          isFromTimeValid: true,
          isToTimeValid: true,
          pickerType: { fromKey: 'startFromTime', toKey: 'startToTime' },
          column: 'start_time',
          type: 'time',
        },
        {
          key: 'endTime',
          fromValue: '00:00:00',
          toValue: '23:59:59',
          isFromTimeValid: true,
          isToTimeValid: true,
          pickerType: { fromKey: 'endFromTime', toKey: 'endToTime' },
          column: 'end_time',
          type: 'time',
        },
        {
          key: 'visitCount',
          fromValue: '0',
          toValue: '50',
          isFromCountValid: true,
          isToCountValid: true,
          pickerType: { fromKey: 'fromCount', toKey: 'toCount' },
          column: 'visit_count',
          type: 'input',
        },
      ],
      tenantAddressLocation: null,
      newActivity: {
        date: DateFormat(new Date(), DATE_TIME_SERVER),
        work: true,
        notes: '',
        reason: {
          id: MasterConstants.reasonNone,
        },
      },
      fabButton: false,
    };
  }

  componentDidMount() {
    this._initHeader();
    this._initValues();
    this._locationEnable();
  }

  onPressFilter() {
    this.setState({ isFieldWorkFilterEnabled: true });
  }

  _initValues() {
    this.fetchTenantLocation();
    this.getAppConfigDetail();
  }

  checkActivityChangeDate(activity) {
    let appConf = JSON.parse(this.state.appConfig);
    if (
      appConf.tracking.fieldActivity.endTime &&
      appConf.tracking.fieldActivity.endTime !== undefined &&
      appConf.tracking.fieldActivity.endTime != null
    ) {
      //activity start time to close the previous activity
      return `${DateFormat(new Date(activity.startTime), DATE_TIME_UI)}T${
        appConf.tracking.fieldActivity.endTime
        }`;
    } else {
      return DateFormat(new Date(), DATE_TIME_SERVER);
    }
  }

  _initTrackingData() {
    this.setState(
      {
        userId:
          this.props.route.params?.usersId ?? this.state.session.info.userId,
        trackingType:
          this.props.route.params?.trackingType ?? MasterConstants.fieldWork,
      },
      () => {
        this._getAllActivityFromServer();
      },
    );
  }

  getAppConfigDetail() {
    const appConfigId = MasterConstants.createFieldwork;
    let filter = {
      conditions: [
        {
          column: 'app_config_id',
          value: [appConfigId],
          operator: '=',
        },
      ],
    };
    GetAllAppConfigDetail(appConfigDetails => {
      if (appConfigDetails.length > 0 && appConfigDetails) {
        const appConfig = JSON.parse(appConfigDetails[0].details);

        this.setState({ appConfig }, () => {
          if (this.props.route.params.screenType === 'editable' || this.props.route.params.screenType === 'attendence') {
            this.setState({ fabButton: true });
          }
          this._initTrackingData();
        });
      } else {
        Alert.alert(
          'Info',
          'Master data not found for make next process. Please sync master details(Menu->Sync data->Master)',
          [
            {
              text: 'Ok',
              onPress: () => {
                this.setState({
                  isPreparing: false,
                  isLoading: false,
                  loadMore: false,
                  isServiceCalled: false,
                });
              },
            },
          ],
          { cancelable: false },
        );
      }
    }, filter);
  }

  fetchTenantLocation() {
    const session = sessionInfo({ type: 'get', key: 'loginInfo' });
    if (session) {
      var filterTenantAddress = {
        conditions: [
          {
            column: 'customer_id',
            value: session.info.tenantCustomerId,
            operator: '=',
          },
          {
            column: 'address_type_id',
            value: [MasterConstants.billingAddress],
            operator: '=',
          },
        ],
      };
      GetAllCustomers(
        session.databaseName,
        datas => {
          if (datas.length > 0) {
            const data = datas[datas.length - 1];
            this.state.tenantAddressLocation = JSON.stringify({
              latitude: data.latitude,
              longitude: data.longitude,
            });
          }
        },
        filterTenantAddress,
      );
    }
  }
  _initHeader() {
    this.props.navigation.setParams({
      headerTitle: this.props.route.params?.headerTitle ?? "Field Activity's",
      headerType:
        this.props.route.params?.headerType ?? HeaderTypes.HeaderHomeMenu,
      headerRight: HeaderTypes.HeaderRightFieldActivityList,
    });
    this.props.navigation.setOptions(renderHeaderView(this));
  }

  componentWillUnmount() {
    // this.backHandler.remove();
  }

  _getAllActivityDetails() {
    var filter = {
      limit: this.state.limit,
      offset: this.state.offset,
      orderBy: 'date desc',
      conditions: [
        {
          column: 'user_id',
          value: this.state.userId,
          operator: '=',
        },
        {
          column: 'tenant_id',
          value: [...this.state.session.info.applicableTenants],
          operator: 'in',
        },
      ],
    };
    if (this.state.filterTypes.length > 0) {
      this.state.filterTypes.forEach(filterType => {
        if (filterType.fromValue !== '' && filterType.toValue !== '') {
          let condition = {
            column: filterType.column,
            value: { from: filterType.fromValue, to: filterType.toValue },
            operator: 'between',
            dataType: filterType.type,
          };
          if (filterType.type === 'time') {
            condition = {
              or: [
                condition,
                {
                  column: filterType.column,
                  value: '',
                  operator: '=',
                  dataType: filterType.type,
                },
              ],
            };
          }
          filter.conditions = [condition, ...filter.conditions];
        }
      });
    }
    GetAllActivityDetails(activities => {
      this.state.activityList = [];
      if (activities.length > 0) {
        this.setState({
          activityList: this.state.activityList.concat(activities),
          isPreparing: false,
          isLoading: false,
          loadMore: false,
          offset: this.state.offset + this.state.activityList.length,
          isServiceCalled: false,
        });
      } else if (!this.state.isServiceCalled) {
        this._getAllActivityFromServer();
      } else {
        this.setState({ isPreparing: false, isLoading: false, loadMore: false });
      }
    }, filter);
  }

  _getAllActivityFromServer() {
    this.state.activityList = [];
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        let filter = {
          orderBy: 'last_updated',
          limit: this.state.limit,
          offset: this.state.offset,
        };
        filter.conditions = [
          {
            column: 'user_id',
            value: [this.state.userId],
            dataType: 'String',
            operator: 'in',
          },
        ];
        getAllFieldActivity(this.state.session, filter, isSuccess => {
          if (isSuccess) {
            this._getAllActivityDetails();
          } else {
            this.setState({
              isPreparing: false,
              isLoading: false,
              loadMore: false,
            });
          }
          this.setState({
            isPreparing: false,
            isLoading: false,
            isServiceCalled: true,
            fieldActivityId: null,
          });
        });
      } else {
        this.setState({ isPreparing: false, isLoading: false, loadMore: false });
      }
    });
  }

  onRefresh() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        this.setState({ offset: 0, isServiceCalled: false }, () => {
          this._getAllActivityFromServer();
        });
      } else {
        this.setState({ isLoading: false });
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    });
  }

  onPressStartLocationButton(activity) {
    if (activity.startLocation.longitude !== undefined && activity.startLocation.longitude !== 1) {
      this.props.navigation.navigate(Route.TravelLocationMapView, {
        startLocation: activity.startLocation,
        fieldActivityId: activity.fieldActivityId,
        fromScrnType: this.props.route.params.screenType,
        headerTitle: "Location's",
        headerType: HeaderTypes.HeaderBack,
        // If it is FieldworkEmployeeList(Menu name: Field work By Rep) show the all travel location's of sales person
      });
    }
  }

  onPressEndLocationButton(activity) {
    if (
      activity.endLocation.longitude !== undefined &&
      activity.endLocation.longitude !== 1
    ) {
      this.props.navigation.navigate(Route.TravelLocationMapView, {
        endLocation: activity.endLocation,
        startLocation: activity.startLocation,
        fieldActivityId: activity.fieldActivityId,
        fromScrnType: this.state.fromScrnType,
        headerTitle: "Location's",
        headerType: HeaderTypes.HeaderBack,
        // If it is FieldworkEmployeeList(Menu name: Field work By Rep) show the all travel location's of sales person
      });
    }
  }

  onPressActivityItemView(activity) {
    this.props.navigation.navigate(Route.FieldworkCustomerList, {
      fieldActivity: activity,
      headerTitle: DateFormat(activity.date, 'MMM DD'),
      headerType: HeaderTypes.HeaderBack,
      screenType: this.props.route.params.screenType,
      appConfig: JSON.parse(this.state.appConfig),
    });
  }

  onPressChangeStatus(activity, refresh) {
    let userLocation = this.state.userLocation;
    let activityStartDate = activity.startTime;
    this._isLocationEnabled().then(isEnabled => {
      if (isEnabled) {
        Alert.alert(
          'Confirmation',
          'Are you sure want to ' +
          (activity.statusId === MasterConstants.statusWorkNotStartedId
            ? ' start '
            : activity.statusId === MasterConstants.statusWorkInProgressId
              ? ' stop '
              : activity.statusId === MasterConstants.statusWorkClosedId
                ? ' restart '
                : ' update ') +
          ' work?',
          [
            {
              text: 'No',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            {
              text: 'Yes',
              onPress: () => {
                activity.lastUpdated = DateFormat(new Date(), DATE_TIME_SERVER);
                if (activity.statusId === MasterConstants.statusWorkNotStartedId && typeof userLocation !== 'undefined') {
                  activity.startTime = DateFormat(new Date(), DATE_TIME_SERVER);
                  activity.endTime = '';
                  activity.statusId = MasterConstants.statusWorkInProgressId;
                  activity.statusName =
                    MasterConstants.statusWorkInProgressName;
                  activity.startLocation =
                    userLocation !== 'unknown'
                      ? {
                        longitude: userLocation.longitude,
                        latitude: userLocation.latitude,
                        altitude: 0,
                        accuracy: 0,
                        bearing: 0,
                        verticalAccuracyMeters: 0,
                        provider: 0,
                      }
                      : {
                        longitude: 1,
                        latitude: 0,
                        altitude: 0,
                        accuracy: 0,
                        bearing: 0,
                        verticalAccuracyMeters: 0,
                        provider: 0,
                      };
                } else if (
                  activity.statusId ===
                  MasterConstants.statusWorkInProgressId &&
                  typeof userLocation !== 'undefined'
                ) {
                  activity.endTime =
                    this.state.trackingType === MasterConstants.attendence
                      ? this.validatePastDateActivity(activityStartDate)
                        ? this.checkActivityChangeDate(activity)
                        : DateFormat(new Date(), DATE_TIME_SERVER)
                      : DateFormat(new Date(), DATE_TIME_SERVER);
                  activity.statusId = MasterConstants.statusWorkClosedId;
                  activity.statusName = MasterConstants.statusWorkClosedName;
                  activity.endLocation =
                    userLocation !== 'unknown'
                      ? {
                        longitude: userLocation.longitude,
                        latitude: userLocation.latitude,
                        altitude: 0,
                        accuracy: 0,
                        bearing: 0,
                        verticalAccuracyMeters: 0,
                        provider: 0,
                      }
                      : {
                        longitude: 1,
                        latitude: 0,
                        altitude: 0,
                        accuracy: 0,
                        bearing: 0,
                        verticalAccuracyMeters: 0,
                        provider: 0,
                      };
                } else if (
                  activity.statusId === MasterConstants.statusWorkClosedId &&
                  typeof userLocation !== 'undefined'
                ) {
                  activity.endTime = '';
                  activity.statusId = MasterConstants.statusWorkInProgressId;
                  activity.statusName =
                    MasterConstants.statusWorkInProgressName;
                  activity.endLocation = {
                    longitude: 1,
                    latitude: 0,
                    altitude: 0,
                    accuracy: 0,
                    bearing: 0,
                    verticalAccuracyMeters: 0,
                    provider: 0,
                  };
                }
                this._trackLocation(activity);
                refresh(activity);
              },
            },
          ],
          { cancelable: false },
        );
      } else {
        this._locationEnable();
      }
    });
  }

  validatePastDateActivity(startTime) {
    const activityDate = DateFormat(startTime, DATE_TIME_UI);
    const currentDate = DateFormat(new Date(), DATE_TIME_UI);
    if (activityDate !== currentDate) {
      return true;
    }
    return false;
  }

  _trackLocation(activity) {
    const appConfig = this.state.appConfig;
    if (activity.statusId === MasterConstants.statusWorkInProgressId) {
      AsyncStorage.setItem(
        AppConstants.scheduler,
        JSON.stringify({
          isTrackingEnabled: true,
          extraData: { activity },
          lastUpdated: null,
          trackingType: this.state.trackingType,
          appConfig: JSON.parse(appConfig),
        }),
        () => {
          const appState = sessionInfo({ type: 'get', key: 'appState' });
          if (appState) {
            appState.startTracking();
          }
        },
      );
    } else {
      AsyncStorage.getItem(AppConstants.scheduler).then(scheduler => {
        if (scheduler) {
          const appState = sessionInfo({ type: 'get', key: 'appState' });
          if (appState) {
            appState.stopTracking(JSON.parse(scheduler));
          }
        }
      });
    }
  }

  _prepareCreateActivity() {
    var request = {
      fieldActivityId: UUID.v1(),
      date: DateFormat(this.state.newActivity.date, DATE_TIME_SERVER),
      userId: this.state.userId,
      startTime: '',
      endTime: '',
      statusId: this.state.newActivity.work ? MasterConstants.statusWorkNotStartedId : MasterConstants.statusLeaveId,
      statusName: this.state.newActivity.work ? MasterConstants.statusWorkNotStartedName : MasterConstants.statusLeaveName,
      startLocation: JSON.stringify({
        longitude: 1,
        latitude: 0,
        altitude: 0,
        accuracy: 0,
        bearing: 0,
        verticalAccuracyMeters: 0,
        provider: 0,
      }),
      endLocation: JSON.stringify({
        longitude: 1,
        latitude: 0,
        altitude: 0,
        accuracy: 0,
        bearing: 0,
        verticalAccuracyMeters: 0,
        provider: 0,
      }),
      reasonId: this.state.newActivity.reason.id,
      reasonName: this.state.newActivity.reason.name,
      notes: this.state.newActivity.notes,
      lastUpdated: DateFormat(new Date(), DATE_TIME_SERVER),
      tenantId: this.state.session.info.tenantId,
      optLock: 0,
      visitCount: 0,
      fieldPlanId: '',
      seqNo: 0,
      syncStatus: 'Not Updated',
    };
    let filter = {
      conditions: [{
        column: 'user_id',
        value: this.state.userId,
        operator: '=',
      }, {
        column: 'tenant_id',
        value: [...this.state.session.info.applicableTenants],
        operator: 'in',
      }, {
        column: 'date(date)',
        value: DateFormat(request.date, DATE_TIME_UI),
        operator: '=',
      }],
    };
    GetAllActivityDetails(datas => {
      if (datas.length === 0) {
        InsertFieldActivity([request], isSuccess => {
          if (isSuccess) {
            GetAllActivityDetails(activities => {
              let object = JSON.parse(activities[0].createJson);
              createFieldActivity(this.state.session, object, updatedSuccess => {
                if (updatedSuccess) {
                  this.setState({ isLoading: true }, () => {
                    this.onRefresh();
                    this._getAllActivityFromServer();
                  });
                  Toast.show('Created successfully', Toast.SHORT);
                } else {
                  Toast.show('Create failure', Toast.SHORT);
                }
              });
            }, filter);
            this.setState(
              { offset: 0, createActivityModalVisible: false },
              () => {
                this._getAllActivityDetails();
              },
            );
          }
        });
      } else {
        Toast.show('Date already exist', Toast.SHORT);
      }
    }, filter);
  }

  renderItem({ item, index }) {
    return (
      <FieldworkActivityItemView
        activity={item}
        session={this.state.session}
        navigation={this.props.navigation}
        parentProps={this.props}
        screenType={this.props.route.params.screenType}
        index={index}
        activityList={this.state.activityList}
        tenantAddressLocation={this.state.tenantAddressLocation}
        userLocation={this.state.userLocation}
        onPressStartLocationButton={this.onPressStartLocationButton.bind(this)}
        onPressEndLocationButton={this.onPressEndLocationButton.bind(this)}
        onPressActivityItemView={this.onPressActivityItemView.bind(this)}
        onPressChangeStatus={this.onPressChangeStatus.bind(this)}
      />
    );
  }

  ListEmptyView = () => {
    return (
      <View
        style={{
          width: DEVICE_WIDTH,
          height: DEVICE_HEIGHT - 40,
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
        }}>
        <Icon
          name="briefcase"
          type="FontAwesome"
          style={{ color: Colors.headerBackground, fontSize: 40 }}
        />
        <Text style={{ textAlign: 'center', color: 'darkgrey', marginTop: 5 }}>
          You have no activities.
        </Text>
      </View>
    );
  };

  _handleDatePicked = date => {
    let newActivity = this.state.newActivity;
    newActivity.date = DateFormat(date, DATE_TIME_SERVER);
    if (
      DateFormat(date, DATE_TIME_UI) === DateFormat(new Date(), DATE_TIME_UI)
    ) {
      newActivity.work = true;
    } else {
      newActivity.work = false;
    }
    this.setState({
      newActivity: newActivity,
    });
    this.setState({
      newActivity: newActivity,
      isDateTimePickerVisible: false,
    });
  };

  _hideDateTimePicker = () => {
    this.setState({
      isDateTimePickerVisible: false,
    });
  };

  onPressApplyButton = filterTypes => {
    this.state.filterTypes = filterTypes;
    this.state.offset = 0;
    this.state.isFieldWorkFilterEnabled = false;
    Toast.show(messages('filterApplied'), Toast.SHORT);
    this._getAllActivityDetails();
    AsyncStorage.setItem(
      AppConstants.keyFieldWorkFielters,
      JSON.stringify(filterTypes),
    );
  };
  _renderCreateActivityModal = () => (
    <View
      style={{
        backgroundColor: 'white',
        padding: 22,
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
      }}>
      <View>
        <Text
          style={{
            fontSize: 20,
            color: 'black',
            fontWeight: 'bold',
            textAlign: 'left',
          }}>
          {this.props.route.params.screenType === 'attendence' ? 'Attendence' : 'Field Activity'}
        </Text>
        <Icon
          onPress={() => {
            this.setState({ createActivityModalVisible: false });
          }}
          name="md-close"
          style={{
            color: 'darkgrey',
            position: 'absolute',
            right: 10,
          }}
        />
      </View>
      <View
        style={{
          height: 1,
          backgroundColor: '#d7d9dd',
          marginLeft: 5,
          marginTop: 5,
          marginRight: 5,
        }}
      />
      <View>
        <View style={{ marginTop: 5, flexDirection: 'row' }}>
          <Text style={[styles.leftContainer, { fontSize: 15 }]}>Date</Text>
          <TouchableOpacity
            style={[styles.rightContainer]}
            onPress={() => this.setState({ isDateTimePickerVisible: true })}>
            <Text
              style={[
                styles.rightContainer,
                {
                  borderRadius: 3,
                  padding: 5,
                  fontSize: 14,
                  fontWeight: 'bold',
                  backgroundColor: '#eff0f2',
                },
              ]}>
              {DateFormat(this.state.newActivity.date, DATE_UI)}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ marginTop: 10, flexDirection: 'row' }}>
          <Text style={[styles.leftContainer, { fontSize: 15 }]}>Work</Text>
          <View style={[styles.rightContainer]}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                alignItems: 'flex-start',
                justifyContent: 'flex-start',
              }}>
              <Text
                style={{
                  fontWeight: 'bold',
                  color: this.state.newActivity.work ? 'black' : 'red',
                  marginTop: 3,
                }}>
                {this.state.newActivity.work ? 'On' : 'Off'}
              </Text>
              <Switch
                onValueChange={value => {
                  let newActivity = this.state.newActivity;
                  newActivity.work = value;
                  this.setState({
                    newActivity: newActivity,
                  });
                }}
                value={this.state.newActivity.work}
              />
            </View>
          </View>
        </View>
        <View style={{ marginTop: 10, flexDirection: 'row' }}>
          <Text style={[styles.leftContainer, { fontSize: 15 }]}>Notes</Text>
          <Input
            autoFocus={true}
            value={this.state.newActivity.notes + ''}
            style={[
              styles.rightContainer,
              { height: 40, borderRadius: 3, backgroundColor: '#f2f3f4' },
            ]}
            onChangeText={text => {
              let newActivity = this.state.newActivity;
              newActivity.notes = text;
              this.setState({ newActivity: newActivity });
            }}
          />
        </View>
      </View>
      <TouchableOpacity
        onPress={() => {
          this._prepareCreateActivity();
        }}>
        <Text
          style={{
            textAlign: 'center',
            padding: 10,
            marginTop: 10,
            color: 'white',
            backgroundColor: Colors.headerBackground,
            borderRadius: 5,
          }}>
          {'Submit'}
        </Text>
      </TouchableOpacity>
    </View>
  );

  render() {
    if (this.state.isPreparing && !this.state.gpsStatus) {
      return <PreparingProgress />;
    } else {
      return (
        <View
          padder
          style={{
            backgroundColor: Styles.appViewBackground.backgroundColor,
            ...StyleSheet.absoluteFillObject,
          }}>
          <StatusBar
            backgroundColor={Styles.statusBar.backgroundColor}
            barStyle="light-content"
          />
          <Loader loading={this.state.isLoading} />
          <Modal isVisible={this.state.createActivityModalVisible}>
            {this._renderCreateActivityModal()}
          </Modal>
          <FieldWorkFilters
            isVisible={this.state.isFieldWorkFilterEnabled}
            filterTypes={this.state.filterTypes}
            closeBtnClicked={() => {
              this.setState({ isFieldWorkFilterEnabled: false });
            }}
            onPressApplyButton={this.onPressApplyButton}
          />
          <DateTimePicker
            isVisible={this.state.isDateTimePickerVisible}
            mode="date"
            minimumDate={new Date()}
            is24Hour={false}
            onConfirm={this._handleDatePicked}
            onCancel={this._hideDateTimePicker}
          />
          <FlatList
            extraData={this.state}
            data={this.state.activityList}
            ItemSeparatorComponent={this.FlatListItemSeparator}
            renderItem={(item, index) => this.renderItem(item, index)}
            keyExtractor={(item, index) => index.toString()}
            onEndReachedThreshold={0.1}
            onEndReached={({ distanceFromEnd }) => {
              if (
                !this.state.loadMore &&
                this.state.offset >= this.state.limit &&
                this.state.isServiceCalled
              ) {
                this.setState({ loadMore: true }, () => {
                  this._getAllActivityDetails();
                });
              }
            }}
            onRefresh={() => this.onRefresh()}
            refreshing={this.state.isLoading}
            ListEmptyComponent={this.ListEmptyView}
            ListFooterComponent={
              this.state.loadMore ? (
                <View
                  style={{
                    padding: 7,
                    flex: 1,
                    flexDirection: 'row',
                    borderRadius: 2,
                    backgroundColor: Colors.headerBackground,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <ActivityIndicator size="small" color="white" />
                  <Text style={{ color: 'white', fontWeight: 'bold' }}>
                    {' Loading..'}
                  </Text>
                </View>
              ) : null
            }
          />
          {this.state.fabButton &&
            (this.props.route.params.screenType !== undefined && (this.props.route.params.screenType === 'attendence' || this.props.route.params.screenType === 'editable' ||
              this.props.route.params.screenType === 'view')) ? (
              <Fab
                style={{ backgroundColor: Colors.headerBackground, size: 5 }}
                direction="up"
                position="bottomRight"
                onPress={() => {
                  NetInfo.fetch().then(isConnected => {
                    if (isConnected) {
                      if (
                        this.state.activityList.filter(
                          node =>
                            node.statusId ===
                            MasterConstants.statusWorkInProgressId,
                        ).length === 0
                      ) {
                        if (this.state.appConfig.length > 0) {
                          this.setState({ createActivityModalVisible: true });
                        } else {
                          Toast.show('Please sync master data', Toast.SHORT);
                        }
                      } else {
                        this.state.trackingType !== MasterConstants.attendence
                          ? Toast.show(
                            "Please close the Field Activity's",
                            Toast.SHORT,
                          )
                          : Toast.show(
                            'Please close previous attendence',
                            Toast.SHORT,
                          );
                      }
                    } else {
                      Toast.show('Make Internet Connection', Toast.SHORT);
                    }
                  });
                }}>
                <Icon type="FontAwesome" name="plus" style={styles.fabicon} />
              </Fab>
            ) : null}
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  currency: {
    fontSize: 14,
    fontWeight: 'normal',
  },
  fabicon: {
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'white',
    fontSize: 22,
  },
  leftContainer: {
    paddingLeft: 10,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
  },
  rightContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
});
