/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import { Fab, Icon, Root, Picker, Textarea, InputGroup, Input } from 'native-base';
import React from 'react';
import { ActivityIndicator, Dimensions, FlatList, StatusBar, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { DateFormat, DATE_TIME_SERVER, DATE_UI } from 'react-native-erp-mobile-library/react/common/Common';
import PreparingProgress from 'react-native-erp-mobile-library/react/component/PreparingProgress';
import BaseComponent from 'react-native-erp-mobile-library/react/screens/masters/BaseComponent';
import Toast from 'react-native-simple-toast';
import MasterConstants from '../../constant/MasterConstants';
import { createFieldActivityVisit, getAllFieldActivityVisit, updateFieldActivityVisit } from '../../controller/FieldworkController';
import CustomSearchView, { SearchType } from 'react-native-erp-mobile-library/react/utilities/CustomSearchView';
import { GetAllActivityVisitDetails, InsertFieldActivityVisits, DeleteAllCustomerVisit } from './dbHelper/FieldWorkDBHelper';
import FieldworkCustomerItemView from './FieldworkCustomerItemView';
import { renderHeaderView } from 'react-native-erp-mobile-library/react/util/HeaderUtil';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import { messages } from 'react-native-erp-mobile-library/react/i18n/i18n';
import Modal from 'react-native-modal';
import { GetAllReasons } from 'react-native-erp-mobile-library/react/screens/masters/dbHelper/MasterDBHelper';
import _ from 'lodash';
import FieldWorkFilters from './FieldWorkFilters';
import { sessionInfo } from 'react-native-erp-mobile-library';
import { Styles } from 'react-native-erp-mobile-library/react/style/Styles';
import Colors from 'react-native-erp-mobile-library/react/style/Colors';
import NetInfo from '@react-native-community/netinfo';

var UUID = require('react-native-uuid');

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

// create a component
export default class FieldworkCustomerList extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      isPreparing: true,
      isLoading: true,
      activityList: [],
      session: {},
      loadMore: false,
      offset: 0,
      limit: 10,
      searchQuery: '',
      isServiceCalled: false,
      fieldActivityId: props.route.params.fieldActivity.fieldActivityId,
      isShowCustomerSearchModal: false,
      isShowFieldWorkNotesModal: false,
      reasonId: null,
      reasonName: '',
      notes: '',
      parentActivityDetails: props.route.params.fieldActivity,
      fieldPlanId: props.route.params.fieldActivity.fieldPlanId,
      reasonList: [],
      fieldWorkReason: null,
      fieldWorkNotes: '',
      isFieldWorkFilterEnabled: false,
      currentDate: DateFormat(new Date(), DATE_UI),
      defaultDate: '',
      filterTypes: [
        { key: 'startTime', fromValue: '00:00:00', toValue: '23:59:59', isFromTimeValid: true, isToTimeValid: true, pickerType: { fromKey: 'startFromTime', toKey: 'startToTime' }, column: 'start_time', type: 'time' },
        { key: 'endTime', fromValue: '00:00:00', toValue: '23:59:59', isFromTimeValid: true, isToTimeValid: true, pickerType: { fromKey: 'endFromTime', toKey: 'endToTime' }, column: 'end_time', type: 'time' },
        { key: 'eventsCount', fromValue: '0', toValue: '50', isFromCountValid: true, isToCountValid: true, pickerType: { fromKey: 'fromCount', toKey: 'toCount' }, column: 'events_count', type: 'input' },
      ],
      fabButton: true,
      appConfig: props.route.params.appConfig,
    };

    this.onPressEditCustomer = this.onPressEditCustomer.bind(this);
    this.updateActivity = this.updateActivity.bind(this);
  }

  componentDidMount() {
    this._locationEnable();
    this._initHeader();
    const session = sessionInfo({ type: 'get', key: 'loginInfo' });
    this.setState({ session: session }, () => {
      if (this.state.activityList.length > 0) {
        this.deleteAndGetCustomerVisitData();
      } else {
        this._getAllActivityVisitsFromServer();
      }
      this._getAllFieldWorkReasons();
    });
  }

  onPressFilter() {
    this.setState({ isFieldWorkFilterEnabled: true });
  }

  componentWillUnmount() {
    // this.backHandler.remove();
  }

  _initHeader() {
    this.props.navigation.setParams({
      headerTitle: this.props.route.params?.headerTitle ?? 'Visit\'s',
      headerType: this.props.route.params?.headerType ?? HeaderTypes.HeaderBack,
      headerRight: HeaderTypes.HeaderRightFieldActivityCustomerList,
    });
    this.props.navigation.setOptions(renderHeaderView(this));
  }

  _getAllCustomerVisitDetails() {
    var filter = {
      limit: this.state.limit,
      offset: this.state.offset,
      orderBy: 'seq_no asc',
      conditions: [{
        column: 'field_activity_id',
        value: [this.state.fieldActivityId],
        operator: 'in',
      }],
    };
    if (this.state.searchQuery.length > 0) {
      filter.conditions = [{
        value: '%' + this.state.searchQuery + '%',
        column: 'customer_name',
        operator: 'like',
      }, ...filter.conditions];
    }
    if (this.state.filterTypes.length > 0) {
      this.state.filterTypes.forEach(filterType => {
        if (filterType.fromValue !== '' && filterType.toValue !== '') {
          let condition = {
            column: filterType.column,
            value: { from: filterType.fromValue, to: filterType.toValue },
            operator: 'between',
            dataType: filterType.type,
          };
          if (filterType.type === 'time') {
            condition = {
              or: [
                condition, {
                  column: filterType.column,
                  value: '',
                  operator: '=',
                  dataType: filterType.type,
                }],
            };
          }
          filter.conditions = [condition, ...filter.conditions];
        }
      });
    }
    GetAllActivityVisitDetails((activities) => {
      if (activities.length > 0 || this.state.searchQuery.length > 0) {
        this.setState({
          activityList: activities,
          isPreparing: false,
          isLoading: false,
          loadMore: false,
          offset: this.state.offset + this.state.activityList.length,
          isServiceCalled: false,
        });
      } else if (!this.state.isServiceCalled) {
        this._getAllActivityVisitsFromServer();
      } else {
        this.setState({ isLoading: false, loadMore: false });
      }
    }, filter);
  }

  deleteAndGetCustomerVisitData() {
    DeleteAllCustomerVisit((isSuccess) => {
      if (isSuccess) {
        this._getAllActivityVisitsFromServer();
      } else {
        Toast.show('Error while getting Data from server.Please do sync and continue!', Toast.SHORT);
      }
    });
  }

  _getAllActivityVisitsFromServer() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        let filter = {
          orderBy: 'last_updated',
          limit: this.state.limit,
          offset: this.state.offset,
        };
        filter.conditions = [{
          column: 'field_activity_id',
          value: [this.state.fieldActivityId],
          dataType: 'String',
          operator: 'in',
        }];
        getAllFieldActivityVisit(this.state.session, filter, (isSuccess) => {
          this.setState({ isServiceCalled: true });
          if (isSuccess) {
            this._getAllCustomerVisitDetails();
          } else {
            this.setState({ isPreparing: false, isLoading: false, loadMore: false });
          }
        });
      } else {
        this.setState({ isPreparing: false, isLoading: false, loadMore: false });
      }
    });
  }

  onRefresh() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        this.setState({ offset: 0, isServiceCalled: false }, () => {
          this._getAllActivityVisitsFromServer();
        });
      } else {
        this.setState({ isLoading: false });
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    });
  }

  _prepareCreateActivityVisit(customer) {
    var request = {
      fieldActivityCustomerVisitId: UUID.v1(),
      fieldActivityId: this.state.fieldActivityId,
      customerId: customer.id,
      customerName: customer.name,
      startTime: '',
      endTime: '',
      statusId: MasterConstants.statusWorkNotStartedId,
      statusName: MasterConstants.statusWorkNotStartedName,
      startLocation: JSON.stringify({ longitude: 1, latitude: 0, altitude: 0, accuracy: 0, bearing: 0, verticalAccuracyMeters: 0, provider: 0 }),
      endLocation: JSON.stringify({ longitude: 1, latitude: 0, altitude: 0, accuracy: 0, bearing: 0, verticalAccuracyMeters: 0, provider: 0 }),
      reasonId: MasterConstants.reasonNone,
      reasonName: 'None',
      notes: '',
      lastUpdated: DateFormat(new Date(), DATE_TIME_SERVER),
      tenantId: this.state.session.info.tenantId,
      optLock: 0,
      eventCount: 0,
      seqNo: 0,
      startEndLocationMatched: false,
      orderMandatory: 0,
      syncStatus: 'Not Updated',
    };
    let filter = {
      conditions: [{
        column: 'customer_id',
        value: customer.id,
        operator: '=',
      }, {
        column: 'field_activity_id',
        value: this.state.fieldActivityId,
        operator: '=',
      }],
    };
    GetAllActivityVisitDetails((datas) => {
      if (datas.length === 0) {
        InsertFieldActivityVisits([request], (isSuccess) => {
          if (isSuccess) {
            GetAllActivityVisitDetails((activities) => {
              let object = JSON.parse(activities[0].createJson);
              createFieldActivityVisit(this.state.session, object, (updatedSuccess) => {
                if (updatedSuccess) {
                  Toast.show('Added successfully', Toast.SHORT);
                } else {
                  Toast.show('Create failure', Toast.SHORT);
                }
              });
            }, filter);
            this.setState({ offset: 0 }, () => {
              this._getAllCustomerVisitDetails();
            });
          }
        });
      } else {
        Toast.show('Customer already added', Toast.SHORT);
      }
    }, filter);
  }

  updateActivity(activity) {
    var fieldActivity = {
      fieldActivityCustomerVisitId: activity.fieldActivityCustomerVisitId,
      fieldActivityId: activity.fieldActivityId,
      customerId: activity.customerId,
      customerName: activity.customerName,
      startTime: activity.startTime,
      endTime: activity.endTime,
      statusId: activity.statusId,
      statusName: activity.statusName,
      startLocation: JSON.stringify(activity.startLocation),
      endLocation: JSON.stringify(activity.endLocation),
      reasonId: activity.reasonId,
      reasonName: activity.reasonName,
      notes: activity.notes,
      lastUpdated: activity.lastUpdated,
      tenantId: activity.tenantId,
      optLock: activity.optLock,
      startEndLocationMatched: false,
      eventCount: activity.eventCount,
      seqNo: activity.seqNo,
      orderMandatory: 0,
      syncStatus: 'Not Updated',
    };
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        let filter = {
          conditions: [{
            column: 'field_activity_customer_visit_id',
            value: activity.fieldActivityCustomerVisitId,
            dataType: 'String',
            operator: '=',
          }],
        };
        getAllFieldActivityVisit(this.state.session, filter, () => {
          InsertFieldActivityVisits([fieldActivity], (isSuccess) => {
            if (isSuccess) {
              GetAllActivityVisitDetails((activities) => {
                let object = JSON.parse(activities[0].createJson);
                updateFieldActivityVisit(this.state.session, object, (isSuccesss) => {
                  if (isSuccesss) {
                    if (this.updatableActivity) {
                      this.updatableActivity = null;
                      this._getAllCustomerVisitDetails();
                    }
                    // Toast.show('Updated successfully', Toast.SHORT)
                  } else {
                    Toast.show('Update failure', Toast.SHORT);
                  }
                });
              }, filter);
            }
          });
        });
      } else {
        InsertFieldActivityVisits([fieldActivity], (isSuccess) => {
          if (isSuccess) {

          }
        });
      }
    });
  }

  _getAllFieldWorkReasons() {
    this.setState({ isPreparing: true });
    let filter = {
      conditions: [{
        column: 'reason_group_id',
        value: MasterConstants.reasonGroupFieldWorkReason,
        dataType: 'String',
        operator: '=',
      }],
    };
    GetAllReasons((data) => {
      this.setState({ isPreparing: false });
      if (data.length > 0) {
        this.setState({ reasonList: data, fieldWorkReason: data[0].reason_id });
      } else {
        Toast.show('Reasons List is Empty', Toast.LONG);
      }
    }, filter);
  }

  onPressEditCustomer = (activity) => {
    this.updatableActivity = activity;
    this.setState({ isShowCustomerSearchModal: true });
  }
  onPressAddNotes = (activity) => {
    this.updatableActivity = activity;
    this.setState({ isShowFieldWorkNotesModal: true, fieldWorkNotes: activity.notes });
  }

  onSelectCustomer(customer) {
    if (this.updatableActivity) {
      this.updatableActivity.customerId = customer.id;
      this.updatableActivity.customerName = customer.name;
      this.updateActivity(this.updatableActivity);
    } else {
      this._prepareCreateActivityVisit(customer);
    }
  }
  _addFieldWorkReason() {
    this.setState({ isShowFieldWorkNotesModal: false }, () => {
      if (this.updatableActivity) {
        this.updatableActivity.notes = this.state.fieldWorkNotes;
        this.updatableActivity.reasonId = this.state.fieldWorkReason;
        this.updatableActivity.reasonName = _.find(this.state.reasonList, _.pick({ reason_id: this.state.fieldWorkReason })).reason_name;
        this.updateActivity(this.updatableActivity);
      }
    });
  }
  onPressApplyButton = (filterTypes) => {
    this.state.filterTypes = filterTypes;
    this.state.offset = 0;
    this.state.isFieldWorkFilterEnabled = false;
    Toast.show(messages('filterApplied'), Toast.SHORT);
    this._getAllCustomerVisitDetails();
  }
  _renderNotesModal = () => (
    <View style={{
      backgroundColor: 'white',
      padding: 22,
      borderRadius: 4,
      borderColor: 'rgba(0, 0, 0, 0.1)',
    }}>
      <View>
        <Text style={{ fontSize: 20, color: 'black', fontWeight: 'bold', textAlign: 'left' }}>Notes</Text>
        <Icon
          onPress={() => {
            this.setState({ isShowFieldWorkNotesModal: false });
          }}
          name="md-close"
          style={{
            color: 'darkgrey',
            position: 'absolute',
            right: 10,
          }} />
      </View>
      <View style={{ height: 1, backgroundColor: '#d7d9dd', marginLeft: 5, marginTop: 5, marginRight: 5 }} />
      <View>
        <View style={{ marginTop: 10, flexDirection: 'row', alignItems: 'center' }}>
          <Text style={[styles.leftContainer, { fontSize: 15, flex: 0.35 }]}>{'Reason'}</Text>
          <Picker
            mode="dropdown"
            iosIcon={<Icon name="ios-arrow-down-outline" />}
            headerBackButtonText="Back"
            style={{ height: 20, flex: 1 }}
            selectedValue={this.state.fieldWorkReason}
            onValueChange={value => {
              this.setState({ fieldWorkReason: value });
            }}
          >
            {this.state.reasonList.map((item) => {
              return (
                <Picker.Item
                  label={item.reason_name}
                  value={item.reason_id}
                />
              );
            })}
          </Picker>
        </View>
        <View style={{ marginTop: 10, flexDirection: 'row' }}>
          <Text style={[styles.leftContainer, { fontSize: 15, flex: 0.4 }]}>{'Notes'}</Text>
          <Textarea
            value={this.state.fieldWorkNotes}
            maxLength={40}
            numberOfLines={2}
            style={[styles.rightContainer, { borderRadius: 3, flex: 1, backgroundColor: '#f2f3f4' }]}
            onChangeText={(text) => {
              this.setState({ fieldWorkNotes: text });
            }} />
        </View>
      </View>
      <TouchableOpacity onPress={() => {
        this._addFieldWorkReason();
      }}>
        <Text style={{ textAlign: 'center', padding: 10, marginTop: 10, color: 'white', backgroundColor: Colors.headerBackground, borderRadius: 5 }}>
          {'Submit'}
        </Text>
      </TouchableOpacity>
    </View>
  );

  renderItem({ item, index }) {
    return (
      <FieldworkCustomerItemView
        fieldPlanId={this.state.fieldPlanId}
        parentActivityDetails={this.state.parentActivityDetails}
        activity={item}
        session={this.state.session}
        activityList={this.state.activityList}
        navigation={this.props.navigation}
        index={index}
        parentProps={this.props}
        userLocation={this.state.userLocation}
        appConfig={this.state.appConfig}
        onPressEditCustomer={this.onPressEditCustomer}
        updateActivity={this.updateActivity}
        onPressAddNotes={this.onPressAddNotes}
      />
    );
  }

  ListEmptyView = () => {
    return (
      <View
        style={{
          width: DEVICE_WIDTH,
          height: DEVICE_HEIGHT - 40,
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
        }}
      >
        <Icon
          name="user"
          type="FontAwesome"
          style={{ color: Colors.headerBackground, fontSize: 40 }}
        />
        <Text style={{ textAlign: 'center', color: 'darkgrey', marginTop: 5 }}>
          You have no visit's.
                </Text>
      </View>
    );
  };

  render() {
    if (this.state.isPreparing) {
      return (
        <PreparingProgress />
      );
    } else if (this.state.userLocation === 'unknown' && !this.state.gpsStatus) {
      return (
        <PreparingProgress />
      );
    } else {
      return (
        <Root>
          <View
            padder
            style={{
              backgroundColor: Styles.appViewBackground.backgroundColor,
              ...StyleSheet.absoluteFillObject,
            }}>
            <StatusBar
              backgroundColor={Styles.statusBar.backgroundColor}
              barStyle="light-content"
            />
            <InputGroup borderType="rounded">
              <Input
                style={{ marginTop: 5 }}
                placeholder={messages('searchCustomerName')}
                onChangeText={text => {
                  this.setState({ searchQuery: text, offset: 0 }, () => {
                    this._getAllCustomerVisitDetails();
                  });
                }} />
              {this.state.searchQuery.length <= 0 ? <Icon
                name="search"
                type="FontAwesome"
                style={{ color: Colors.headerBackground, fontSize: 20, marginRight: 8 }}
              /> : <Icon
                  name="close"
                  type="MaterialCommunityIcons"
                  style={{ color: Colors.headerBackground, fontSize: 25, marginRight: 8 }}
                  onPress={() => {
                    this.setState({ searchQuery: '', offset: 0 }, () => {
                      this._getAllCustomerVisitDetails();
                    });
                  }}
                />}
            </InputGroup>
            <CustomSearchView
              modalVisible={this.state.isShowCustomerSearchModal}
              searchType={SearchType.customers}
              conditions={[{
                column: 'customer_type_id',
                value: [MasterConstants.CustomerTypeCustomer, MasterConstants.CustomerTypeDistributorId, MasterConstants.CustomerTypeRetailerId],
                operator: 'in',
              }]}
              session={this.state.session}
              hideModel={() => {
                this.setState({ isShowCustomerSearchModal: false });
              }}
              onChooseSearchValue={(customer) => {
                this.setState({ isShowCustomerSearchModal: false }, () => {
                  this.onSelectCustomer(customer);
                });
              }}
            />
            <FlatList
              extraData={this.state}
              data={this.state.activityList}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              renderItem={(item, index) => this.renderItem(item, index)}
              keyExtractor={(item, index) => index.toString()}
              onEndReachedThreshold={0.1}
              onEndReached={() => {
                if (!this.state.loadMore && this.state.offset >= this.state.limit && this.state.isServiceCalled) {
                  this.setState({ loadMore: true }, () => {
                    this._getAllCustomerVisitDetails();
                  });
                }
              }}
              onRefresh={() => this.onRefresh()}
              refreshing={this.state.isLoading}
              ListEmptyComponent={this.ListEmptyView}
              ListFooterComponent={
                this.state.loadMore ?
                  <View style={{ padding: 7, flex: 1, flexDirection: 'row', borderRadius: 2, backgroundColor: Colors.headerBackground, alignItems: 'center', justifyContent: 'center' }}>
                    <ActivityIndicator size="small" color="white" />
                    <Text style={{ color: 'white', fontWeight: 'bold' }}>{' Loading..'}</Text>
                  </View>
                  : null
              }
            />
            <Modal isVisible={this.state.isShowFieldWorkNotesModal}>
              {this._renderNotesModal()}
            </Modal>
            <FieldWorkFilters
              isVisible={this.state.isFieldWorkFilterEnabled}
              filterTypes={this.state.filterTypes}
              closeBtnClicked={() => { this.setState({ isFieldWorkFilterEnabled: false }); }}
              onPressApplyButton={this.onPressApplyButton}
            />

            {this.state.fabButton && (this.props.route.params.screenType !== undefined && this.props.route.params.screenType === 'editable') ?
              <Fab
                style={{ backgroundColor: Colors.headerBackground, size: 5 }}
                direction="up"
                position="bottomRight"
                onPress={() => {
                  NetInfo.fetch().then(isConnected => {
                    if (isConnected) {
                      if (this.state.activityList.filter(node => node.statusId === MasterConstants.statusWorkInProgressId).length === 0) {
                        this.setState({ isShowCustomerSearchModal: true });
                      } else {
                        Toast.show('Please close the other visit\'s and then add', Toast.SHORT);
                      }
                    } else {
                      Toast.show('Make Internet Connection', Toast.SHORT);
                    }
                  });
                }}>
                <Icon type="FontAwesome" name="plus" style={styles.fabicon} />
              </Fab>
              : null}
          </View>
        </Root>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  currency: {
    fontSize: 14,
    fontWeight: 'normal',
  },
  fabicon: {
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'white',
    fontSize: 22,
  },
  leftContainer: {
    paddingLeft: 10,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
  },
  rightContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
});
