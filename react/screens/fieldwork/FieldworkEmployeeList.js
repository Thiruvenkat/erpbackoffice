/* eslint-disable eqeqeq */
/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable react-native/no-inline-styles */
import {
  Body,
  Button,
  Card,
  CardItem,
  Container,
  Icon,
  Input,
  InputGroup,
  Label,
  Left,
  Picker,
  Root,
  Text,
  View,
} from 'native-base';
import React from 'react';
import {
  ActivityIndicator,
  Animated,
  Dimensions,
  FlatList,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import Loader from 'react-native-erp-mobile-library/react/component/Loader';
import PreparingProgress from 'react-native-erp-mobile-library/react/component/PreparingProgress';
import {ngetAllCustomerDetails} from 'react-native-erp-mobile-library/react/controller/SyncDataController';
import {
  GetAllCustomerCompanyList,
  GetAllCustomersBasicDetails,
} from 'react-native-erp-mobile-library/react/screens/customers/dbHelper/CustomerDB';
import Toast from 'react-native-simple-toast';
import FontAsoIcon from 'react-native-vector-icons/FontAwesome';
import MasterConstants from '../../constant/MasterConstants';
import Route from '../../router/Route';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import {renderHeaderView} from 'react-native-erp-mobile-library/react/util/HeaderUtil';
import {messages} from 'react-native-erp-mobile-library/react/i18n/i18n';
import {sessionInfo} from 'react-native-erp-mobile-library';
import {
  Styles,
  IconColor,
} from 'react-native-erp-mobile-library/react/style/Styles';
import Colors from 'react-native-erp-mobile-library/react/style/Colors';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

export default class FieldworkEmployeeList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      isConnected: true,
      active: 'false',
      session: {},
      isPreparing: true,
      customerList: [],
      filterEnabled: false,
      filterCompanyList: [],
      filterCompanyId: '',
      filterCondition: [],
      visibleFilterModal: false,
      offset: 0,
      limit: 10,
      searchQuery: '',
      isOnlineSearchEnabled: false,
      isServiceCalled: false,
      conditions: [
        {
          value: props.route.params.customerTypeId,
          column: 'customer_type_id',
          operator: 'in',
        },
      ],
    };
    this.springValue = new Animated.Value(100);
    this._showFilterOptions = this._showFilterOptions.bind(this);
  }

  componentDidMount() {
    this._initHeader();
    const session = sessionInfo({type: 'get', key: 'loginInfo'});
    this.setState({session: session}, () => {
      this._getAllCustomers();
    });
  }

  componentWillUnmount() {
    // this.backHandler.remove();
  }

  _initHeader() {
    this.props.navigation.setParams({
      headerTitle: this.props.route.params?.headerTitle ?? 'Sales Person',
      headerType:
        this.props.route.params?.headerType ?? HeaderTypes.HeaderHomeMenu,
    });
    this.props.navigation.setOptions(renderHeaderView(this));
  }

  _callCustomToast() {
    Animated.sequence([
      Animated.spring(this.springValue, {
        toValue: -0.15 * 100,
        duration: 10,
        useNativeDriver: true,
      }),
      Animated.timing(this.springValue, {
        toValue: 100,
        duration: 900,
        useNativeDriver: true,
      }),
    ]).start();
  }

  _getAllCustomers() {
    var filter = {
      limit: this.state.limit,
      offset: this.state.offset,
      orderBy: 'last_updated desc',
      conditions: this.state.conditions,
    };
    if (this.state.searchQuery.length > 0) {
      filter.conditions = [
        {
          value: '%' + this.state.searchQuery + '%',
          column: 'customer_name',
          operator: 'like',
        },
        ...filter.conditions,
      ];
    }
    // GetAllCustomers(this.state.session.databaseName, (datas) => {
    //   if (datas.length > 0) {
    //     this.setState({ customerList: (this.state.offset == 0) ? datas : this.state.customerList.concat(datas), loadMore: false, isLoading: false, isPreparing: false, offset: this.state.offset + datas.length, isServiceCalled: false });
    //   } else if (!this.state.isServiceCalled) {
    //     this._getAllCustomerDetailsFromServer();
    //   } else {
    //     this.setState({ isLoading: false, loadMore: false });
    //   }
    // }, filter);

    GetAllCustomersBasicDetails(
      this.state.session.databaseName,
      datas => {
        if (datas.length > 0) {
          this.setState({
            customerList:
              this.state.offset == 0
                ? datas
                : this.state.customerList.concat(datas),
            loadMore: false,
            isLoading: false,
            isPreparing: false,
            offset: this.state.offset + datas.length,
            isServiceCalled: false,
          });
        } else if (!this.state.isServiceCalled) {
          this._getAllCustomerDetailsFromServer();
        } else {
          this.setState({isLoading: false, loadMore: false});
        }
      },
      filter,
    );
  }

  _getAllCustomerDetailsFromServer() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        let syncValue = {
          limit: this.state.limit,
          offset: this.state.offset,
          conditions: [
            {
              column: 'st.status_id',
              value: MasterConstants.active,
              dataType: 'String',
              operator: '=',
            },
          ],
          staticConditions: [
            {
              column: 'customerTypeId',
              value: this.props.route.params?.customerTypeId.join(','),
              dataType: 'String',
              operator: 'in',
            },
            {
              column: 'lastUpdated',
              value: '1483228800000',
              dataType: 'Date',
              operator: '>',
            },
          ],
        };
        if (
          this.state.searchQuery != null &&
          this.state.searchQuery != undefined
        ) {
          syncValue.conditions.push({
            column: 'cr.customer_name',
            value: '%' + this.state.searchQuery + '%',
            dataType: 'String',
            operator: 'like',
          });
        }
        ngetAllCustomerDetails(this.state.session, syncValue, isSuccess => {
          this.setState({isServiceCalled: true});
          if (isSuccess) {
            this._getAllCustomers();
          } else {
            this.setState({
              isLoading: false,
              loadMore: false,
              isPreparing: false,
            });
          }
        });
      } else {
        this.setState({isLoading: false, loadMore: false, isPreparing: false});
      }
    });
  }

  onRefresh() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        this.setState(
          {offset: 0, isPreparing: true, isServiceCalled: false},
          () => {
            this._getAllCustomerDetailsFromServer();
          },
        );
      } else {
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    });
  }

  _showFilterOptions() {
    GetAllCustomerCompanyList(
      null,
      null,
      [
        {
          value: '',
          column: 'customer_parent_id',
          operator: '!=',
        },
      ],
      datas => {
        this.setState({
          filterCompanyList: datas,
          filterCompanyId: datas[0].customer_parent_id,
        });
      },
    );
    this.setState({visibleFilterModal: true});
  }

  _filterData() {
    this.setState({isLoading: true, customerList: []});
    this._getAllCustomers();
  }

  renderItem({item, index}) {
    return (
      <CustomerItemView
        customerData={item}
        navigation={this.props.navigation}
        parentProps={this.props}
        loginToken={this.state.loginToken}
        publicIp={this.state.publicIp}
        index={index}
      />
    );
  }

  ListEmptyView = () => {
    return (
      <View
        style={{
          marginTop: 50,
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
        }}>
        <Icon
          name="users"
          type="FontAwesome"
          style={{color: Colors.headerBackground, fontSize: 40}}
        />
        <Text style={{textAlign: 'center', color: 'darkgrey', marginTop: 10}}>
          No data found.
        </Text>
      </View>
    );
  };

  _renderFilterModal = () => (
    <View style={styles.modalContent}>
      <View>
        <Text
          style={{
            fontSize: 20,
            color: 'black',
            fontWeight: 'bold',
            textAlign: 'left',
          }}>
          Filter
        </Text>
        <Icon
          onPress={() => {
            this.setState({visibleFilterModal: false});
          }}
          name="md-close"
          style={{
            color: 'darkgrey',
            position: 'absolute',
            right: 10,
          }}
        />
      </View>
      <View
        style={{
          height: 1,
          backgroundColor: '#d7d9dd',
          marginLeft: 5,
          marginTop: 5,
          marginRight: 5,
        }}
      />
      <View style={styles.itemView}>
        <Label style={{marginTop: 20, color: Colors.headerBackground}}>
          Choose Company
        </Label>
        <Picker
          mode="dropdown"
          iosIcon={<Icon name="ios-arrow-down-outline" />}
          headerBackButtonText="Back"
          selectedValue={this.state.filterCompanyId}
          onValueChange={value => {
            this.setState({filterCompanyId: value});
          }}>
          {this.state.filterCompanyList.map((val, index) => {
            return (
              <Picker.Item
                key={index}
                style={{marginLeft: 20, marginRight: 20, color: 'white'}}
                label={val.customer_parent_name}
                value={val.customer_parent_id}
              />
            );
          })}
        </Picker>
      </View>
      <View style={styles.itemView}>
        <TouchableOpacity
          onPress={() => {
            this.setState(
              {
                visibleFilterModal: false,
                filterCondition: [
                  {
                    column: 'customer_parent_id',
                    value: this.state.filterCompanyId,
                    operator: '=',
                  },
                ],
              },
              () => {
                this._getAllCustomers();
              },
            );
          }}>
          <Text
            style={{
              marginTop: 5,
              fontSize: 20,
              fontWeight: 'bold',
              padding: 10,
              width: '100%',
              backgroundColor: Colors.headerBackground,
              color: 'white',
              textAlign: 'center',
            }}>
            Filter
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );

  render() {
    if (this.state.isPreparing) {
      return <PreparingProgress />;
    } else {
      return (
        <Root>
          <Container>
            <StatusBar
              backgroundColor={Styles.statusBar.backgroundColor}
              barStyle="light-content"
            />
            <Loader loading={this.state.isLoading} />
            <View>
              <Card>
                <InputGroup borderType="rounded">
                  <Input
                    style={{marginTop: 5}}
                    placeholder="Search"
                    onChangeText={text => {
                      this.setState({searchQuery: text, offset: 0}, () => {
                        this._getAllCustomers();
                      });
                    }}
                  />
                  <Button
                    onPress={() => {
                      // this._filterData(this.state.query);
                    }}
                    style={{
                      backgroundColor: 'lightGrey',
                      paddingHorizontal: 20,
                      borderRadius: 4,
                      marginTop: 5,
                      marginRight: 5,
                    }}>
                    <FontAsoIcon
                      name="search"
                      style={{fontSize: 20, color: Colors.headerBackground}}
                    />
                  </Button>
                </InputGroup>
              </Card>
            </View>
            <View style={{height: DEVICE_HEIGHT - 145}}>
              <FlatList
                extraData={this.state}
                data={this.state.customerList}
                onEndReachedThreshold={0.1}
                onEndReached={({distanceFromEnd}) => {
                  if (
                    !this.state.loadMore &&
                    this.state.searchQuery.length == 0 &&
                    !this.state.isServiceCalled
                  ) {
                    this.setState({loadMore: true}, () => {
                      this._getAllCustomers();
                    });
                  }
                }}
                ItemSeparatorComponent={this.FlatListItemSeparator}
                renderItem={item => this.renderItem(item)}
                keyExtractor={item => item.customer_id.toString()}
                onRefresh={() => this.onRefresh()}
                refreshing={this.state.isLoading}
                ListEmptyComponent={this.ListEmptyView}
                ListFooterComponent={
                  this.state.loadMore && this.state.searchQuery.length == 0 ? (
                    <View
                      style={{
                        padding: 7,
                        flex: 1,
                        flexDirection: 'row',
                        borderRadius: 2,
                        backgroundColor: Colors.headerBackground,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <ActivityIndicator size="small" color="white" />
                      <Text style={{color: 'white', fontWeight: 'bold'}}>
                        {'Loading..'}
                      </Text>
                    </View>
                  ) : null
                }
              />
            </View>
            <Animated.View
              style={[
                styles.animatedView,
                {transform: [{translateY: this.springValue}]},
              ]}>
              <Text style={styles.exitTitleText}>
                Sorry this Technician is Unavailable !
              </Text>
            </Animated.View>
          </Container>
        </Root>
      );
    }
  }
}

class CustomerItemView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      customerData: {},
      ...props,
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({...nextProps});
  }

  _navigate() {
    if (
      this.state.customerData.user_id == null ||
      this.state.customerData.user_id == 'null'
    ) {
      Toast.show(
        "This person don't have any user authentication ",
        Toast.SHORT,
      );
    } else {
      this.props.parentProps.navigation.navigate(Route.FieldworkActivityList, {
        usersId: this.state.customerData.user_id,
        screenType: 'view',
        headerTitle: "Visit's",
        headerType: HeaderTypes.HeaderBack,
      });
    }
  }

  render() {
    const {index} = this.props;
    return (
      <Card style={styles.card}>
        <TouchableOpacity
          onPress={() => {
            this._navigate();
          }}>
          <CardItem>
            <Left>
              <TouchableOpacity
                onPress={() => {
                  this._navigate();
                }}>
                <View
                  style={{
                    width: 70,
                    height: 70,
                    borderRadius: 70 / 2,
                    backgroundColor: IconColor[index % IconColor.length],
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={[
                      styles.text,
                      {color: 'white', textAlign: 'center'},
                    ]}>
                    {this.state.customerData.customer_name
                      .substring(0, 2)
                      .toUpperCase()}{' '}
                  </Text>
                </View>
              </TouchableOpacity>
              <Body>
                <Text style={styles.text}>
                  {' '}
                  {this.state.customerData.customer_name}
                </Text>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.text}> Customer Code : </Text>
                  <Text> {this.state.customerData.customer_code}</Text>
                </View>
                {this.state.customerData.mobile_no !== null &&
                this.state.customerData.mobile_no !== 'null' ? (
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styles.text}> Mobile No. : </Text>
                    <Text> {this.state.customerData.mobile_no}</Text>
                  </View>
                ) : null}
                {this.state.customerData.email_id !== null &&
                this.state.customerData.email_id !== 'null' ? (
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styles.text}> E-mail id : </Text>
                    <Text> {this.state.customerData.email_id}</Text>
                  </View>
                ) : null}
                {this.state.customerData.parent_customer_id == undefined ||
                this.state.customerData.customer_type_id !=
                  MasterConstants.CustomerTypeEmployee ? null : (
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styles.text}> Company : </Text>
                    <View>
                      {this.state.customerData.parent_customer_name !== null &&
                      this.state.customerData.parent_customer_name !==
                        'null' ? (
                        <Text
                          style={{
                            backgroundColor: 'orange',
                            borderRadius: 3,
                            color: 'white',
                            fontSize: 12,
                            marginTop: 3,
                          }}>
                          {' '}
                          {this.state.customerData.parent_customer_name}
                        </Text>
                      ) : null}
                    </View>
                  </View>
                )}
              </Body>
            </Left>
          </CardItem>
        </TouchableOpacity>
      </Card>
    );
  }
}
const styles = StyleSheet.create({
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  card: {
    borderRadius: 3,
    elevation: 4,
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  animatedView: {
    width: DEVICE_WIDTH,
    backgroundColor: '#eb4d4b',
    elevation: 7,
    position: 'absolute',
    bottom: 0,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  exitTitleText: {
    textAlign: 'center',
    color: '#ffffff',
    marginRight: 10,
  },
});
