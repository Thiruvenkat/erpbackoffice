/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-shadow */
/* eslint-disable prettier/prettier */
import haversine from 'haversine';
import React from 'react';
import { Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import MapView, { Polyline } from 'react-native-maps';
import NetInfo from '@react-native-community/netinfo';
import Colors from '../../style/Colors';
import { getAllFieldActivityAgentTracking } from '../../controller/FieldworkController';
import Route from '../../router/Route';
import { sessionInfo } from 'react-native-erp-mobile-library';
import PreparingProgress from 'react-native-erp-mobile-library/react/component/PreparingProgress';
import Geolocation from '@react-native-community/geolocation';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import { renderHeaderView } from 'react-native-erp-mobile-library/react/util/HeaderUtil';

const LATITUDE = 13.022780;
const LONGITUDE = 77.684340;
const LATITUDE_DELTA = 0.009;
const LONGITUDE_DELTA = 0.009;

class TravelLocationMapView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isPreparing: true,
      latitude: LATITUDE,
      longitude: LONGITUDE,
      routeCoordinates: [],
      distanceTravelled: 0,
      markers: [],
      session: sessionInfo({ type: 'get', key: 'loginInfo' }),
    };
  }

  componentDidMount() {
    this._initHeader();
    this._getLongitudeAndLatitude(this.props.route.params).then((markers) => {
      this.setState({ markers, isPreparing: false }, () => {
        const markers = this.state.markers;
        if (markers.length > 0) {
          let routeCoordinates = [], distanceTravelled = 0, prevPosition = {}, currentPosition = {};
          for (let index = 0; index < markers.length; index++) {
            const marker = markers[index];
            routeCoordinates[index] = marker.coordinates;
          }
          if (routeCoordinates.length > 1) {
            for (let index = 0; index < routeCoordinates.length - 1; index++) {
              prevPosition = routeCoordinates[index];
              currentPosition = routeCoordinates[index + 1];
              distanceTravelled += this.calcDistance(prevPosition, currentPosition);
            }
          } else {
            currentPosition = routeCoordinates[0];
          }

          this.setState({
            latitude: currentPosition.latitude,
            longitude: currentPosition.longitude,
            routeCoordinates: routeCoordinates,
            distanceTravelled: distanceTravelled,
          });
          if (Platform.OS === 'android') {
            if (this.marker) {
              this.marker._component.animateMarkerToCoordinate(
                routeCoordinates[routeCoordinates.length - 1],
                500
              );
            }
          } else {
            routeCoordinates.timing(routeCoordinates[routeCoordinates.length - 1]).start();
          }
        }
      });
    });
  }

  _initHeader(){
    this.props.navigation.setParams({
      headerTitle: this.props.route.params?.headerTitle ?? "Location's",
      headerType: this.props.route.params?.headerType ?? HeaderTypes.HeaderBack,
    });
    this.props.navigation.setOptions(renderHeaderView(this));
  }

  componentWillUnmount() {
    Geolocation.clearWatch(this.watchID);
  }

  _getLongitudeAndLatitude(dataProps) {
    return new Promise((resolve, reject) => {
      var responseArray = [];
      // If fromScrnType is FieldworkEmployeeList(Menu name: Field work By Rep) show the all travel location's of sales person
      if (dataProps.fromScrnType === Route.FieldworkEmployeeList) {
        NetInfo.fetch().then(isConnected => {
          if (isConnected) {
            let filter = {
              orderBy: 'lastUpdatedDateTime',
              limit: 500,
              offset: 0,
              conditions: [{
                column: 'fieldActivity.fieldActivityId',
                value: dataProps.fieldActivityId,
                dataType: 'String',
                operator: 'in',
              }],
            };
            getAllFieldActivityAgentTracking(this.state.session, filter)
              .then((datas) => {
                this.setState({ isServiceCalled: true });
                if (datas.length > 0) {
                  for (let index = 0; index < datas.length; index++) {
                    const detail = datas[index];
                    responseArray.push({
                      title: `Location - ${index + 1}`,
                      pinColor: 'green',
                      coordinates: {
                        latitude: detail.location.latitude,
                        longitude: detail.location.longitude,
                      },
                    });
                  }
                } else {
                  this.setState({ isPreparing: false });
                }
                resolve(responseArray);
              }).catch(error => {
                this.setState({ isPreparing: false });
                reject();
              });
          } else {
            this.setState({ isPreparing: false });
            reject();
          }
        });
      } else {
        if (dataProps.startLocation != null) {
          responseArray.push({
            title: 'start',
            pinColor: 'green',
            coordinates: {
              latitude: dataProps.startLocation.latitude,
              longitude: dataProps.startLocation.longitude,
            },
          });
        }
        if (dataProps.endLocation != null) {
          responseArray.push({
            title: 'end',
            pinColor: 'red',
            coordinates: {
              latitude: dataProps.endLocation.latitude,
              longitude: dataProps.endLocation.longitude,
            },
          });
        }
        resolve(responseArray);
      }
    });
  }

  calcDistance = (prevLatLng, newLatLng) => {
    return haversine(prevLatLng, newLatLng) || 0;
  };

  getMapRegion = () => ({
    latitude: this.state.latitude,
    longitude: this.state.longitude,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA,
  });

  render() {
    if (this.state.isPreparing) {
      return (
        <PreparingProgress />
      );
    }
    if (this.state.markers.length === 0) {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ fontSize: 15, color: 'black' }}>{'No location\'s found'}</Text>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          showUserLocation
          followUserLocation
          loadingEnabled
          region={this.getMapRegion()}
        >
          <Polyline coordinates={this.state.routeCoordinates} strokeWidth={5} strokeColor={Colors.headerBackground} />
          {this.state.markers.map(marker => (
            <MapView.Marker
              coordinate={marker.coordinates}
              title={marker.title}
              pinColor={marker.pinColor}
            />
          ))}
        </MapView>
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={[styles.bubble, styles.button]}>
            <Text style={styles.bottomBarContent}>
              {parseFloat(this.state.distanceTravelled).toFixed(2)} km
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    flex: 1,
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
});

export default TravelLocationMapView;
