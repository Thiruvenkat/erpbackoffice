/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {Card, CardItem, Body, Icon, Left} from 'native-base';
import AppConstants from '../../constant/AppConstants';
import {
  RemoveSeparator,
  NumberFormat,
  DateFormat,
  DATE_TIME_LAST_UPDATED,
} from 'react-native-erp-mobile-library/react/common/Common';
import MasterConstants from '../../constant/MasterConstants';
import {
  IconColor,
  STATUS_COLOR,
} from 'react-native-erp-mobile-library/react/style/Styles';

export default class UnClearedItemView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      finance: null,
      index: 0,
      session: {},
      ...props,
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({...nextProps});
  }

  render() {
    const finance = this.state.finance;
    console.log(finance);

    const session = this.state.session;
    var shortCode =
      finance.in_out_flag === MasterConstants.inOutFlagReceivable ? 'RE' : 'PA';
    return (
      <Card>
        <CardItem style={{backgroundColor: 'white'}}>
          <Left>
            <View
              style={{
                width: 40,
                height: 40,
                borderRadius: 40 / 2,
                backgroundColor: IconColor[this.state.index % IconColor.length],
                justifyContent: 'center',
                top: 5,
              }}>
              <Text
                style={{
                  color: 'white',
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}>
                {shortCode.toUpperCase()}
              </Text>
            </View>
            <Body>
              <Text style={{fontSize: 13, textAlign: 'right'}}>
                <Icon
                  type="FontAwesome"
                  name="history"
                  style={{fontSize: 15}}
                />{' '}
                {DateFormat(finance.last_update_time, DATE_TIME_LAST_UPDATED)}
              </Text>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                {finance.account_name != null && finance.account_name !== 'null'
                  ? finance.account_name
                  : ''}
              </Text>
              <Text style={{marginTop: 1}}>{finance.associated_acc_name}</Text>
              <Text style={{marginTop: 1}}>{finance.pay_type_name}</Text>
              <View style={{flex: 1, flexDirection: 'row', marginBottom: 3}}>
                <Text
                  style={{
                    marginTop: 2,
                    color: 'black',
                    textAlign: 'left',
                    flex: 1,
                  }}>
                  <Icon
                    name={session.info[AppConstants.keyCurrency]}
                    type="FontAwesome"
                    style={styles.currency}
                  />
                  {' ' +
                    NumberFormat(
                      RemoveSeparator(finance.amount, session.info.exponent),
                      session.info,
                    )}
                </Text>
                <View style={{flex: 1, alignItems: 'flex-end'}}>
                  <Text
                    note
                    style={{
                      borderRadius: 3,
                      backgroundColor: STATUS_COLOR[finance.status_id],
                      textAlign: 'center',
                      color: 'white',
                    }}>
                    {' '}
                    {finance.status_name}
                  </Text>
                </View>
              </View>
            </Body>
          </Left>
        </CardItem>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  currency: {
    fontSize: 14,
    fontWeight: 'normal',
  },
});
