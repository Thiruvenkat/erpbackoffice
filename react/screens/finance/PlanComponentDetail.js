/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/no-did-mount-set-state */
import React from 'react';
import {FlatList} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import Colors from '../../style/Colors';
import {sessionInfo} from 'react-native-erp-mobile-library/index';
import {getAllBsPlanDetailList} from 'react-native-erp-mobile-library/react/screens/finance/dbHelper/FinanceDBHelper';
import {getAllBsPlanAllocationDetailList} from 'react-native-erp-mobile-library/react/controller/SyncDataController';
import {View, Text, Icon} from 'native-base';
import {ActivityIndicator} from 'react-native-paper';
import {PlanSummaryDetailItemView} from './PlanSummaryDetailItemView';
import {messages} from 'react-native-erp-mobile-library/react/i18n/i18n';
import Toast from 'react-native-simple-toast';

export default class PlanSummaryDetail extends React.Component {
  static navigationOptions = {
    title: "Target Detail's",
    headerTintColor: 'white',
    headerStyle: {
      backgroundColor: Colors.headerBackground,
    },
    headerTitleStyle: {
      fontWeight: 'bold',
      color: Colors.headerTitleColor,
    },
  };
  constructor(props) {
    super(props);
    this.state = {
      dashboardData: [],
      isLoading: false,
      loadMore: false,
      isRefresh: false,
      apiCall: true,
      offset: 0,
      limit: 10,
    };
  }

  componentDidMount() {
    const sessions = sessionInfo({type: 'get', key: 'loginInfo'});
    console.log(sessions);
    this.setState(
      {
        session: sessions,
      },
      () => {
        this.getSummaryDetail();
      },
    );
  }

  getSummaryDetail() {
    this.getAllBsPlanSummary(datas => {
      if (datas.length > 0) {
        let dashboardData = [];
        let dashboardDataMap = {};
        for (let index = 0; index < datas.length; index++) {
          const summaryDetail = datas[index];
          console.log(summaryDetail);
          if (dashboardDataMap.hasOwnProperty(summaryDetail.component_id)) {
            let chartData =
              dashboardDataMap[summaryDetail.bs_plan_allocation_id];
            chartData.planValue =
              chartData.planValue + summaryDetail.plan_allocation_value;
            chartData.planAchieveValue =
              chartData.planAchieveValue + summaryDetail.achieve_value;
          } else {
            let chartData = {
              id: summaryDetail.bs_plan_allocation_id,
              name: summaryDetail.component_name,
              planValue: summaryDetail.plan_allocation_value,
              planAchieveValue: summaryDetail.achieve_value,
              exponent: summaryDetail.exponent,
              vaildFrom: summaryDetail.start_date,
              vaildTo: summaryDetail.end_date,
            };
            dashboardDataMap[summaryDetail.component_id] = chartData;
          }
        }
        dashboardData = Object.values(dashboardDataMap);
        console.log(dashboardData);
        this.setState({
          dashboardData: dashboardData,
          isLoading: false,
          loadMore: false,
          offset: this.state.offset + datas.length,
        });
      } else {
        this.getAllBsPlanSummaryFromServer();
      }
    });
  }

  getAllBsPlanSummary(callback) {
    var filter = {
      orderBy: 'start_date desc',
      conditions: [],
    };
    filter.conditions.push({
      value: this.props.navigation.state.params.id,
      column: 'bs_plan_allocation_id',
      operator: '=',
      dataType: 'String',
    });
    // filter.conditions.push({
    //   value: this.state.filterDate + "T23:59:59.999",
    //   column: "last_updated",
    //   operator: "<=",
    //   dataType: "String"
    // });
    getAllBsPlanDetailList(datas => {
      callback(datas);
    }, filter);
  }

  getAllBsPlanSummaryFromServer() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected && this.state.apiCall) {
        console.log('calling service..');
        let filter = {
          orderBy: 'bpad.last_updated',
          conditions: [
            {
              column: 'bpad.last_updated',
              value: '1483228800000',
              dataType: 'Date',
              operator: '>',
            },
            {
              value: this.props.navigation.state.params.id,
              column: 'bsps.bs_plan_allocation_summary_id',
              operator: '=',
              dataType: 'String',
            },
          ],
        };
        getAllBsPlanAllocationDetailList(
          this.state.session,
          filter,
          isSuccess => {
            this.getSummaryDetail();
          },
        );
        this.setState({
          apiCall: false,
        });
      } else {
        this._updateLoadingState();
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    });
  }

  _updateLoadingState() {
    this.setState({
      isLoading: false,
      loadMore: false,
      isPreparing: false,
      isRefresh: false,
    });
  }

  onRefresh() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        this.setState({offset: 0, apiCall: true}, () => {
          this.getAllBsPlanSummaryFromServer();
        });
      } else {
        this.setState({offset: 0, apiCall: true}, () => {
          this.getSummaryDetail();
        });
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    });
  }

  // randomColor = () => ('#' + (Math.random() * 0xFFFFFF << 0).toString(16) + '000000').slice(0, 7)
  showSummaryDetail(item) {}

  renderItem({item}) {
    return (
      <PlanSummaryDetailItemView
        cardData={item}
        navigation={this.props.navigation}
        onPressSummary={this.showSummaryDetail.bind(this)}
      />
    );
  }

  ListEmptyView = () => {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
          marginTop: 200,
          backgroundColor: 'white',
        }}>
        <Icon
          name="shopping-cart"
          type="FontAwesome"
          style={{color: Colors.headerBackground, fontSize: 40}}
        />
        <Text style={{textAlign: 'center', color: 'darkgrey', marginTop: 10}}>
          You have no Target's
        </Text>
      </View>
    );
  };

  render() {
    return (
      <FlatList
        extraData={this.state}
        data={this.state.dashboardData}
        renderItem={this.renderItem.bind(this)}
        keyExtractor={(item, index) => index.toString()}
        onRefresh={() => this.onRefresh()}
        refreshing={this.state.isRefresh}
        onEndReachedThreshold={0.1}
        onEndReached={({distanceFromEnd}) => {
          if (!this.state.loadMore && this.state.offset >= this.state.limit) {
            this.setState({loadMore: true}, () => {
              this.getSummaryDetail();
            });
          }
        }}
        ListEmptyComponent={this.ListEmptyView}
        ListFooterComponent={
          this.state.loadMore ? (
            <View
              style={{
                padding: 7,
                flex: 1,
                flexDirection: 'row',
                borderRadius: 2,
                backgroundColor: Colors.headerBackground,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <ActivityIndicator size="small" color="white" />
              <Text style={{color: 'white', fontWeight: 'bold'}}>
                {' Loading..'}
              </Text>
            </View>
          ) : null
        }
      />
    );
  }
}
