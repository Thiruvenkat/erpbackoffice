import React, { Component } from 'react';
import { FlatList, StyleSheet, View, Text, TouchableOpacity, ActivityIndicator, NetInfo, BackHandler } from 'react-native';
import { Card, CardItem, Body, Icon, Left, InputGroup, Input, Button, Fab, } from 'native-base';
import MyIcon from 'react-native-vector-icons/FontAwesome';
import AppConstants from '../../constant/AppConstants';
import { Styles, IconColor, STATUS_COLOR } from '../../style/Styles';
import Colors from '../../style/Colors';
import Toast from 'react-native-simple-toast';
import { RemoveSeparator, NumberFormat, Currency, DateFormat, DATE_TIME_UI, DATE_TIME_LAST_UPDATED, DATE_TIME_SERVER } from 'react-native-erp-mobile-library/react/common/Common';

export default class ARAPFinanceItemView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // finance: null,
            // index: 0,
            // session: {},
            // selectedValues: {},
            isSelected: false,
            ...props
        };
        this.onPressItem = this.onPressItem.bind(this)
    }
    UNSAFE_componentWillReceiveProps(nextProps) {
        this.setState({ ...nextProps });
    }

    onPressItem() {
        let selectedValues = this.state.selectedValues;
        if (this.props.onPressItem(this.state.finance)) {
            this.setState({
                isSelected: Object.keys(selectedValues).includes(this.state.finance.trans_id)
            });
        }
    }

    render() {
        const { navigation } = this.props;
        const finance = this.state.finance;
        const session = this.state.session;
        const isSelected = Object.keys(this.state.selectedValues).includes(this.state.finance.trans_id)
        var shortCode = finance.dc_type_name == "Credit" ? "C" : "D";
        if (finance.trans_type_name != "null" && finance.trans_type_name != "") {
            let strArr = finance.trans_type_name.split(" ");
            shortCode = "";
            for (let index = 0; index < strArr.length; index++) {
                const element = strArr[index];
                if (strArr.length > 1) {
                    shortCode += (element.substring(0, 1));
                } else {
                    shortCode += (element.substring(0, 2));
                }
            }
        }

        return (
            <TouchableOpacity key={this.props.index} style={{ backgroundColor: isSelected ? Styles.headerBackground : 'white' }} onPress={this.onPressItem}>
                <Card style={{ backgroundColor: isSelected ? '#b3d2e6' : 'white', marginTop: 0.5, marginBottom: 0.5 }}>
                    <CardItem style={{ backgroundColor: isSelected ? '#b3d2e6' : 'white' }}>
                        <Left>
                            <View
                                style={{
                                    width: 40,
                                    height: 40,
                                    borderRadius: 40 / 2,
                                    backgroundColor: IconColor[this.state.index % IconColor.length],
                                    justifyContent: "center",
                                    top: 5
                                }}
                            >
                                <Text style={{ color: 'white', fontWeight: 'bold', textAlign: 'center' }}>{shortCode.toUpperCase()}</Text>
                            </View>
                            <Body>
                                <Text style={{ fontSize: 13, textAlign: "right" }}>
                                    <Icon
                                        type="FontAwesome" name="history"
                                        style={{ fontSize: 15 }}
                                    />{" "}
                                    {DateFormat(finance.trans_date, DATE_TIME_LAST_UPDATED)}
                                </Text>
                                <Text style={{ fontSize: 16, fontWeight: "bold" }}>
                                    {finance.ass_acc_name != null && finance.ass_acc_name != 'null' ? finance.ass_acc_name : ""}
                                </Text>
                                <Text style={{ marginTop: 1 }}>
                                    {finance.trans_request_number}
                                </Text>
                                <View style={{ flex: 1, flexDirection: 'row', marginBottom: 3 }}>
                                    <Text style={{ marginTop: 1, flex: 1 }}>
                                        {finance.trans_type_name}
                                    </Text>
                                    <View style={{ flex: 1, alignItems: 'flex-end', }}>
                                        <Text note style={{ borderRadius: 3, backgroundColor: STATUS_COLOR[finance.status_id], textAlign: 'center', color: 'white' }}> {finance.status_name}</Text>
                                    </View>
                                </View>
                            </Body>
                        </Left>
                    </CardItem>
                    <View style={{ flex: 1, flexDirection: 'row', marginBottom: 3 }}>
                        <View style={{ flex: 1, flexDirection: 'column' }}>
                            <Text style={{ textAlign: 'center' }}>{'Amount'}</Text>
                            <Text style={{ marginTop: 2, color: 'black', textAlign: 'center' }}>
                                <Icon name={session.info[AppConstants.keyCurrency]} type="FontAwesome" style={styles.currency} />
                                {" " + NumberFormat(RemoveSeparator(finance.amount, session.info.exponent), session.info)}
                            </Text>
                        </View>
                        <View style={{ flex: 1 }}>
                            <Text style={{ textAlign: 'center' }}>{'Pending'}</Text>
                            <Text style={{ marginTop: 2, color: 'red', textAlign: 'center' }}>
                                <Icon name={session.info[AppConstants.keyCurrency]} type="FontAwesome" style={styles.currency} />
                                {" " + NumberFormat(RemoveSeparator(Number(finance.amount) - Number(finance.adjust_amount), session.info.exponent), session.info)}
                            </Text>
                        </View>
                        <View style={{ flex: 1 }}>
                            <Text style={{ textAlign: 'center' }}>{'Uncleared'}</Text>
                            <Text style={{ marginTop: 2, color: 'black', textAlign: 'center' }}>
                                <Icon name={session.info[AppConstants.keyCurrency]} type="FontAwesome" style={styles.currency} />
                                {" " + NumberFormat(RemoveSeparator(finance.ucAmount, session.info.exponent), session.info)}
                            </Text>
                        </View>
                    </View>
                </Card >
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center"
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    },
    currency: {
        fontSize: 14,
        fontWeight: 'normal'
    }
});