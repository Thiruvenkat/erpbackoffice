import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import Colors from '../../style/Colors';
import { sessionInfo } from "react-native-erp-mobile-library/index";
import { View, Text, Icon, CardItem, Left, Right, Card } from 'native-base';
import { RemoveSeparator, DateFormat } from 'react-native-erp-mobile-library/react/common/Common';

export class PlanSummaryDetailItemView extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      cardData: [],
      ...props,
    }
  }

  // Initialize the states
   UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({ ...nextProps }, () => {
      console.log(this.state.cardData);
    });
  }

  render() {
    const item = this.state.cardData;
    const sessions = sessionInfo({ type: 'get', key: 'loginInfo' });
    const { navigation } = this.props;
    const achivedPercentage = ((item.planAchieveValue / item.planValue) * 100).toFixed(item.exponent);
    const pendingPercentage = (((item.planValue - item.planAchieveValue) / item.planValue) * 100).toFixed(item.exponent);
    return (
      <View>
        <Card style={{ flex: 1, flexDirection: 'column', }}>
          <TouchableOpacity
            onPress={() => { this.props.onPressSummary(item) }}>
            <CardItem bordered >
              <View style={{ justifyContent: 'center', flex: 1 }}>
                <Text style={styles.headerText}> {item.name} </Text>
              </View>
            </CardItem>
            <CardItem style={{ paddingBottom: 0 }}>
              <View style={{ flexDirection: "row" }}>
                <Text>Target</Text>
                <Right>
                  <Text>{RemoveSeparator(item.planValue, item.exponent)}</Text>
                </Right>
              </View>
            </CardItem>
            <CardItem style={{ paddingBottom: 0 }}>
              <View style={{ flexDirection: "row" }}>
                <Left>
                  <Text>Achieved</Text>
                </Left>
                <Text style={{ color: achivedPercentage > 80 ? 'green' : achivedPercentage > 60 ? 'orange' : 'red' }}>{"% " + achivedPercentage}</Text>
                <Right>
                  <Text style={{ color: achivedPercentage > 80 ? 'green' : achivedPercentage > 60 ? 'orange' : 'red' }}>{RemoveSeparator(item.planAchieveValue, item.exponent) + ''}</Text>
                </Right>
              </View>
            </CardItem>
            <CardItem bordered>
              <View style={{ flexDirection: "row" }}>
                <Left>
                  <Text>Pending Target</Text>
                </Left>
                <Text style={{ color: achivedPercentage > 80 ? 'green' : achivedPercentage > 60 ? 'orange' : 'red' }}>{"% " + pendingPercentage}</Text>
                <Right>
                  <Text style={{ color: achivedPercentage > 80 ? 'green' : achivedPercentage > 60 ? 'orange' : 'red' }}>{(RemoveSeparator(item.planValue, item.exponent) - RemoveSeparator(item.planAchieveValue, item.exponent)).toFixed(item.exponent)}</Text>
                </Right>
              </View>
            </CardItem>
            <CardItem>
              <Left>
                <View style={{ flexDirection: "row" }}>
                  <Icon
                    name="access-time"
                    type="MaterialIcons"
                    style={{ color: Colors.headerBackground, fontSize: 20 }}
                  />
                  <Text style={styles.text}> {DateFormat(item.vaildFrom, sessions.info.dateFormat.toUpperCase())} </Text>
                </View>
              </Left>
              <Right>
                <View style={{ flexDirection: "row" }}>
                  <Icon
                    name="timer-off"
                    type="MaterialIcons"
                    style={{ color: Colors.headerBackground, fontSize: 20 }}
                  />
                  <Text style={styles.text}> {DateFormat(item.vaildTo, sessions.info.dateFormat.toUpperCase())}</Text>
                </View>
              </Right>
            </CardItem>
          </TouchableOpacity>
        </Card>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  text: {
    fontSize: 16,
    fontWeight: "bold",
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center'
  },
  headerText: {
    fontSize: 16,
    fontWeight: "bold",
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    color: Colors.headerBackground
  },
});
