/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/no-did-mount-set-state */
import _ from 'lodash';
import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {Icon} from 'native-base';
import * as nb from 'native-base';

import Modal from 'react-native-modal';

import Colors from 'react-native-erp-mobile-library/react/style/Colors';
import CustomSearchView, {
  SearchType,
} from 'react-native-erp-mobile-library/react/utilities/CustomSearchView';
import {messages} from 'react-native-erp-mobile-library/react/i18n/i18n';

export const FilterTypes = {
  associateAccount: 1,
  status: 2,
};

export default class FinanceFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: props.isVisible,
      filterTypes: [],
    };
  }

  componentDidMount() {
    if (this.props.filterTypes) {
      this.props.filterTypes.forEach(filterType => {
        this.prepareFilter(filterType);
      });
      this.setState({...this.state.filterTypes});
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.state = {...nextProps};
    this.state.filterTypes = [];
    if (nextProps.filterTypes) {
      nextProps.filterTypes.forEach(filterType => {
        this.prepareFilter(filterType);
      });
    }
    this.setState({...this.state.filterTypes});
  }

  prepareFilter(filterType) {
    let filter = null;
    switch (filterType.type) {
      case FilterTypes.associateAccount:
        filter = {
          label: messages('associateAccount'),
          isSearchEnabled: false,
          searchType: SearchType.associateAccount,
        };
        break;
      case FilterTypes.status:
        filter = {
          label: messages('status'),
          isSearchEnabled: false,
          searchType: SearchType.financeStatus,
        };
        break;
      default:
        break;
    }
    if (filter) {
      filter = {...filterType, ...filter};
      filter.selectedValues = {...filterType.selectedValues};
      this.state.filterTypes.push(filter);
    }
  }

  _updateStateValues(name, updateValues) {
    let views = this.state.filterTypes;
    let data;
    for (let index = 0; index < views.length; index++) {
      const view = views[index];
      if (view.type === name) {
        data = view;
        break;
      }
    }
    _.keys(updateValues).forEach(key => {
      data[key] = updateValues[key];
    });

    for (let index = 0; index < views.length; index++) {
      const view = views[index];
      if (view.type === name) {
        views[index] = data;
      }
    }
    this.setState({filterTypes: views});
  }

  getFilterTypeDetail(type) {
    for (let index = 0; index < this.state.filterTypes.length; index++) {
      const filterType = this.state.filterTypes[index];
      if (filterType.type === type) {
        return filterType;
      }
    }
    return null;
  }

  render() {
    return (
      <Modal isVisible={this.state.isVisible}>
        <View
          style={{
            backgroundColor: 'white',
            padding: 22,
            borderRadius: 4,
            borderColor: 'rgba(0, 0, 0, 0.1)',
          }}>
          <View>
            <Text
              style={{
                fontSize: 20,
                color: 'black',
                fontWeight: 'bold',
                textAlign: 'left',
              }}>
              Filter
            </Text>
            <Icon
              onPress={() => {
                this.setState({isVisible: false}, () => {
                  this.props.closeBtnClicked();
                });
              }}
              name="md-close"
              style={{
                color: 'darkgrey',
                position: 'absolute',
                right: 10,
              }}
            />
          </View>
          <View
            style={{
              height: 1,
              backgroundColor: 'darkgray',
              marginLeft: 5,
              marginTop: 5,
              marginRight: 5,
            }}
          />
          <View style={{padding: 5}}>
            {this.state.filterTypes.map(object => {
              return (
                <View style={{flexDirection: 'column'}}>
                  <TouchableOpacity
                    onPress={() => {
                      this._updateStateValues(object.type, {
                        isSearchEnabled: true,
                      });
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        height: 40,
                        alignItems: 'center',
                        marginTop: 5,
                        marginBottom: 3,
                        borderColor: Colors.headerBackground,
                        borderWidth: 0.5,
                        padding: 5,
                        borderRadius: 4,
                      }}>
                      <Text
                        style={{
                          color: Colors.headerBackground,
                          fontWeight: 'bold',
                          flex: 8,
                        }}>
                        {object.label}
                      </Text>
                      <Text
                        onPress={() => {
                          this._updateStateValues(object.type, {
                            selectedValues: {},
                          });
                        }}
                        style={{
                          color:
                            Object.keys(object.selectedValues).length > 0
                              ? 'black'
                              : 'lightgray',
                          flex: 2,
                        }}>
                        {messages('reset')}
                      </Text>
                    </View>
                    <CustomSearchView
                      key={object.type}
                      modalVisible={object.isSearchEnabled}
                      searchType={object.searchType}
                      conditions={object.filters}
                      hideModel={() => {
                        this._updateStateValues(object.type, {
                          isSearchEnabled: false,
                        });
                      }}
                      onChooseSearchValue={searchValue => {
                        this._updateStateValues(object.type, {
                          isSearchEnabled: false,
                          selectedValues: {
                            ...object.selectedValues,
                            [searchValue.id]: searchValue,
                          },
                        });
                      }}
                    />
                  </TouchableOpacity>
                  <View
                    style={{
                      flexWrap: 'wrap',
                      flexDirection: 'row',
                      backgroundColor: 'rgba(232, 239, 249, 1)',
                      borderRadius: 4,
                    }}>
                    {_.values(object.selectedValues).map(item => {
                      return (
                        <nb.Button
                          key={item.id}
                          style={{
                            margin: 1.5,
                            height: 25,
                            borderRadius: 15,
                            backgroundColor: Colors.headerBackground,
                          }}
                          onPress={() => {
                            const selectedValues = object.selectedValues;
                            delete selectedValues[item.id];
                            this._updateStateValues(object.type, {
                              selectedValues,
                            });
                          }}>
                          <nb.Text style={{color: 'white', fontSize: 12}}>
                            {item.name}
                          </nb.Text>
                        </nb.Button>
                      );
                    })}
                  </View>
                </View>
              );
            })}
          </View>
          <TouchableOpacity
            onPress={() => {
              this.setState({isVisible: false}, () => {
                this.props.onPressApplyButton(this.state.filterTypes);
              });
            }}>
            <Text
              style={{
                textAlign: 'center',
                padding: 10,
                marginTop: 10,
                color: 'white',
                backgroundColor: Colors.headerBackground,
                borderRadius: 5,
              }}>
              {'Apply'}
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}
