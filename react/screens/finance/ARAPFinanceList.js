/* eslint-disable eqeqeq */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable prettier/prettier */
import React from 'react';
import { FlatList, StyleSheet, View, Text, StatusBar, Dimensions, ActivityIndicator } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Icon, Fab, InputGroup, Input } from 'native-base';
import { Styles } from '../../style/Styles';
import Colors from '../../style/Colors';
import Toast from 'react-native-simple-toast';
import { DateFormat, DATE_TIME_SERVER } from 'react-native-erp-mobile-library/react/common/Common';
import { GetAllARAPList, GetAllCustomerAccounts } from 'react-native-erp-mobile-library/react/screens/finance/dbHelper/FinanceDBHelper';
import { getAllARAPFinanceList } from 'react-native-erp-mobile-library/react/controller/SyncDataController';
import PreparingProgress from 'react-native-erp-mobile-library/react/component/PreparingProgress';
import Route from '../../router/Route';
import { sessionInfo } from 'react-native-erp-mobile-library';
import MasterConstants from '../../constant/MasterConstants';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import { renderHeaderView} from 'react-native-erp-mobile-library/react/util/HeaderUtil';
import { messages } from 'react-native-erp-mobile-library/react/i18n/i18n';
import ARAPFinanceItemView from './ARAPFinanceItemView';
import AppConstants from 'react-native-erp-mobile-library/react/constant/AppConstants';
import FinanceFilter, { FilterTypes } from './FinanceFilter';
import AsyncStorage from '@react-native-community/async-storage';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

// create a component
export default class ARAPFinanceList extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            isRefresh: true,
            financeList: [],
            accountIds: [],
            customerId: null,
            customerData: props.route.params.parentProps.customerData,
            session: sessionInfo({ type: 'get', key: 'loginInfo' }),
            filter: {
                startDate: DateFormat('2010-01-01', DATE_TIME_SERVER),
                endDate: DateFormat(new Date(), DATE_TIME_SERVER),
            },
            filterDate: props.route.params?.filterDate,
            inOutFlag: props.inOutFlag ? props.inOutFlag : props.route.params?.inOutFlag,
            dashboardType: props.route.params?.dashboardType,
            offset: 0,
            limit: 10,
            isServiceCalled: false,
            customerAccountData: [],
            selectedValues: {},
            sendValues: {},
            enableScrollViewScroll: true,
            isFinanceFilterEnabled: false,
            filterTypes: [
                { type: FilterTypes.status, filters: [], selectedValues: {}, column: 'status_id' },
            ],
            filterEnabled: '',
            searchQuery: '',
            searchEnabled: false,
            initialFilter: props.route.params?.initialFilter ?? '',
        };
        this.onPressItem = this.onPressItem.bind(this);
        // this.props.parent.addRefs(this);
        this.searchTimeOut = null;
    }

    componentDidMount() {
        this._initHeader();
        // this._initValues();
        if (this.state.customerData && this.state.customerData.customer_id) {
            this.setState({
                customerId: this.state.customerData.customer_id,
            }, () => {
                this._getCustomerAccountDetails();
            });
        } else {
            this._getAllARAPFinanceList();
        }
    }

    applyFilter = () => {
        this.setState({ isFinanceFilterEnabled: true });
    }
    // _initValues() {
    //     if (typeof this.props.screenProps != 'undefined' && typeof this.props.screenProps.parentNavigation != 'undefined') {
    //         const customer = this.props.screenProps.parentNavigation.getParam('customerData', "");
    //         if (customer) {
    //             this.setState({ customerData: customer }, () => {
    //                 console.log("--Successfully--");
    //             })
    //         }
    //     }
    // }

    _getCustomerAccountDetails() {
        let filter = {
            conditions: [{
                column: 'customer_id',
                value: this.state.customerId,
                operator: '=',
            }],
        };
        GetAllCustomerAccounts((datas) => {
            this.setState({ customerAccountData: datas }, () => {
                this._getPayableReceivableACount();
            });
        }, filter);
    }
    _getPayableReceivableACount() {
        var accountIds = [];
        let accountDatas = [];
        accountDatas = this.state.customerAccountData;
        if (accountDatas.length > 0) {
            for (let index = 0; index < accountDatas.length; index++) {
                accountIds.push(accountDatas[index].account_id);
            }
            this.setState({
                accountIds: accountIds,
            }, () => {
                this._getAllARAPFinanceList();
            });
        }
    }

    componentWillUnmount() {
        // this.backHandler.remove();
    }

    _initHeader() {
        const inOutFlag = this.props.route.params.inOutFlag;
        this.props.navigation.setParams({
            headerTitle: (this.props.route.params?.headerTitle ?? inOutFlag === MasterConstants.inOutFlagReceivable ? 'Receivable' : inOutFlag === MasterConstants.inOutFlagPayable ? 'Payable' : 'Finance'),
            headerType: this.props.route.params?.headerType ?? HeaderTypes.HeaderHomeMenu,
            headerRight: HeaderTypes.headerRightFinanceList,
            parentProps: this,
        });
        this.props.navigation.setOptions(renderHeaderView(this));
        // alert(JSON.stringify(this.props));
    }

    _updateLoadingState() {
        this.setState({
            isLoading: false,
            isPreparing: false,
            isRefresh: false,
            loadMore: false,
        });
    }

    onPressApplyButton = (filterTypes) => {
        this.state.filterTypes = filterTypes;
        this.state.offset = 0;
        this.state.isFinanceFilterEnabled = false;
        this.state.initialFilter = '';
        this._getAllARAPFinanceList();
        AsyncStorage.setItem(AppConstants.keyFinanceFilters, JSON.stringify(filterTypes));
        Toast.show(messages('filterApplied'), Toast.SHORT);
    }

    _getAllARAPFinanceList() {
        var filter = {
            limit: this.state.limit,
            offset: this.state.offset,
            orderBy: 'last_update_time desc',
            conditions: [{
                column: 'in_out_flag',
                value: [this.state.inOutFlag],
                operator: 'in',
            }],
        };
        if (this.state.initialFilter != ''){
            filter.conditions.push({
                value: this.state.initialFilter,
                column: 'status_id',
                operator:'=',
            });
        }
        if (this.state.searchQuery.length === 0) {
            filter.conditions.push({
                value: this._getFilterStartDate(),
                column: 'trans_date',
                operator: '>=',
            }, {
                value: this._getFilterEndDate(),
                column: 'trans_date',
                operator: '<=',
            });
        }
        if (this.state.session.info.protectedViewEnabled === true) {
            filter.conditions.push({
                value: this.state.session.info.loggedInCustomerId,
                column: 'sales_agent_id',
                operator: '=',
            });
        }
        if (this.state.searchQuery.length > 0) {
            filter.conditions.push({
                column: 'ass_acc_name',
                value: '%' + this.state.searchQuery + '%',
                operator: 'like',
            });
        }

        if (this.state.customerData) {
            if (this.state.accountIds) {
                filter.conditions.push({
                    column: 'ass_acc_id',
                    value: this.state.accountIds,
                    operator: 'in',
                });
            }
        }
        if (this.state.filterTypes.length > 0) {
            this.state.filterTypes.forEach(filterType => {
                const values = Object.keys(filterType.selectedValues);
                if (values.length > 0) {
                    filter.conditions = [{
                        value: values,
                        column: filterType.column,
                        operator: 'in',
                    }, ...filter.conditions];
                }
            });
        }
        GetAllARAPList((datas) => {
            if (this.state.searchQuery.length > 0 && datas.length > 0) {
                this.setState({
                    financeList: datas, isServiceCalled: false,
                });
                this._updateLoadingState();
            } else if (this.state.searchQuery.length === 0 && !this.state.searchEnabled && datas.length > 0) {
                this.setState({
                    financeList: (this.state.offset === 0) ? datas : this.state.financeList.concat(datas),
                    offset: this.state.offset + datas.length,
                    isServiceCalled: false,
                });
                this._updateLoadingState();
            } else if (!this.state.isServiceCalled) {
                this._getAllARAPFinanceListFromServer();
            } else {
                this.setState({
                    financeList: datas, offset: 0, isServiceCalled: false,
                });
                this._updateLoadingState();
            }
        }, filter);
    }

    _getAllARAPFinanceListFromServer() {
        NetInfo.fetch().then(isConnected => {
            if (isConnected) {
                let filter = {
                    limit: this.state.limit,
                    offset: this.state.offset,
                    orderBy: 'al.trans_date',
                    conditions: [
                        {
                            column: 'al.last_updated',
                            value: '1483228800000',
                            dataType: 'Date',
                            operator: '>',
                        },

                        // {
                        //     column: "al.trans_type_id",
                        //     value: "151101",
                        //     dataType: "String",
                        //     operator: "in"
                        // },
                        {
                            column: 'al.trans_date',
                            value: DateFormat(this.state.filter.startDate, 'YYYY-MM-DD HH:mm:ss'),
                            dataType: 'SD',
                            operator: '>=',
                        },
                        {
                            column: 'al.trans_date',
                            value: DateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'),
                            dataType: 'SD',
                            operator: '<=',
                        },
                        {
                            column: 'al.in_out_flag',
                            value: this.state.inOutFlag,
                            dataType: 'Long',
                            operator: 'in',
                        },
                        // ,
                        // {
                        //     column: 1 != 1 ? "ac.account_id" : "assc.account_id",
                        //     value: "1483228800000",
                        //     dataType: "String",
                        //     operator: "in"
                        // }
                    ],
                };
                if (this.state.accountIds.length > 0) {
                    filter.conditions.push({
                        'column': 'assc.account_id',
                        'value': this.state.accountIds,
                        'dataType': 'String',
                        'operator': '=',
                    });
                }
                getAllARAPFinanceList(this.state.session, filter, (isSuccess) => {
                    this.setState({ isServiceCalled: true });
                    if (isSuccess) {
                        this._getAllARAPFinanceList();
                    }
                });
            } else {
                this.setState({ isLoading: false, loadMore: false, isServiceCalled: true });
                console.log('make internet connection..!');
            }
        });
    }

    _getFilterStartDate() {
        if (this.state.dashboardType) {
            if (this.state.dashboardType === MasterConstants.monthly) {
                const filterDate = new Date(this.state.filterDate);
                filterDate.setDate(1);
                return `${DateFormat(filterDate, 'YYYY')}-${DateFormat(filterDate, 'MM-DDT00:00:00')}`;
            }
        }

        return this.state.filter.startDate;
    }

    _getFilterEndDate() {
        if (this.state.dashboardType) {
            if (this.state.dashboardType === MasterConstants.monthly) {
                const filterDate = new Date(this.state.filterDate);
                filterDate.setDate(1);
                filterDate.setMonth(filterDate.getMonth() + 1);
                filterDate.setDate(filterDate.getDate() - 1);
                return `${DateFormat(filterDate, 'YYYY')}-${DateFormat(filterDate, 'MM-DDT23:59:59')}`;
            }
        }
        return this.state.filter.endDate;
    }

    onRefresh() {
        NetInfo.fetch().then(isConnected => {
            if (isConnected) {
                this.setState({ offset: 0, isServiceCalled: false }, () => {
                    this._getAllARAPFinanceListFromServer();
                });
                // this._getAllARAPFinanceList((datas) => {
                //     if (datas.length > 0) {
                //         this.setState({ financeList: datas, isLoading: false, loadMore: false, isPreparing: false, isRefresh: false, offset: this.state.offset + datas.length });
                //     } else {
                //         this._getAllARAPFinanceListFromServer(null);
                //     }
                // });
            } else {
                Toast.show(messages('makeInternetConnection'), Toast.SHORT);
            }
        });
    }

    onPressItem(finance) {
        if (MasterConstants.arApTransType.includes(finance.trans_type_id) && finance.status_id === MasterConstants.savedPendingStatus) {
            if (Number(finance.ucAmount) !== Number(finance.amount) - Number(finance.adjust_amount)) {
                const selectedValues = this.state.selectedValues;
                const transactionIds = Object.keys(selectedValues);
                if (transactionIds.length === 0 || selectedValues[transactionIds[0]].ass_acc_id === finance.ass_acc_id) {
                    if (!transactionIds.includes(finance.trans_id)) {
                        selectedValues[finance.trans_id] = finance;
                    } else {
                        delete selectedValues[finance.trans_id];
                    }
                    this.state.selectedValues = selectedValues;
                    return true;
                } else {
                    Toast.show('select same account', Toast.SHORT);
                    return false;
                }
            } else {
                Toast.show('This Transaction not allowed', Toast.SHORT);
                return false;
            }
        } else {
            Toast.show('This Transaction not allowed', Toast.SHORT);
        }
    }

    renderItem({ item, index }) {
        return (
            <ARAPFinanceItemView
                finance={item}
                index={index}
                session={this.state.session}
                navigation={this.props.navigation}
                parentProps={this.props}
                selectedValues={this.state.selectedValues}
                onPressItem={this.onPressItem.bind(this)}
            />
        );
    }

    ListEmptyView = () => {
        return (
            <View
                style={{
                    width: DEVICE_WIDTH,
                    height: DEVICE_HEIGHT - 40,
                    alignItems: 'center',
                    justifyContent: 'center',
                    flex: 1,
                }}
            >
                <Icon
                    name="cube"
                    type="FontAwesome"
                    style={{ color: Colors.headerBackground, fontSize: 40 }}
                />
                <Text style={{ textAlign: 'center', color: 'darkgrey', justifyContent: 'center' }}>
                    No data found.
            </Text>
            </View>
        );
    };

    onEnableScroll = (value) => {
        this.setState({
            enableScrollViewScroll: value,
        });
    };

    render() {
        if (this.state.isLoading) {
            return (
                <PreparingProgress />
            );
        } else {
            return (
                <View
                    padder
                    style={{
                        backgroundColor: Styles.appViewBackground.backgroundColor,
                        ...StyleSheet.absoluteFillObject,
                    }}>
                    <StatusBar
                        backgroundColor={Styles.statusBar.backgroundColor}
                        barStyle="light-content"
                    />

                    <InputGroup borderType="rounded">
                        <Input
                            placeholder="Search Accounter's name"
                            placeholderTextColor={'grey'}
                            onChangeText={text => {
                                this.setState({ searchQuery: text, offset: 0 }, () => {
                                    if (this.searchTimeOut) {
                                        clearTimeout(this.searchTimeOut);
                                    }
                                    this.searchTimeOut = setTimeout(()=>{
                                        this._getAllARAPFinanceList();
                                        clearTimeout(this.searchTimeOut);
                                        this.searchTimeOut = null;
                                    }, 500);
                                });
                            }}
                        />
                    </InputGroup>

                    <FinanceFilter
                        isVisible={this.state.isFinanceFilterEnabled}
                        filterTypes={this.state.filterTypes}
                        closeBtnClicked={() => {
                            this.setState({ isFinanceFilterEnabled: false }, () => {
                                //
                            });
                        }}
                        onPressApplyButton={this.onPressApplyButton.bind(this)}
                    />
                    <FlatList
                        extraData={this.state}
                        data={this.state.financeList}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={(item, index) => this.renderItem(item, index)}
                        keyExtractor={item => item.trans_request_number.toString()}
                        onEndReachedThreshold={0.1}
                        onEndReached={() => {
                            if (!this.state.loadMore && this.state.offset >= this.state.limit) {
                                this.setState({ loadMore: true }, () => {
                                    this._getAllARAPFinanceList();
                                });
                            }
                        }}
                        onRefresh={() => this.onRefresh()}
                        refreshing={this.state.isLoading}
                        ListEmptyComponent={this.ListEmptyView}
                        ListFooterComponent={
                            this.state.loadMore && this.state.searchQuery.length === 0 ?
                                <View style={{ padding: 7, flex: 1, flexDirection: 'row', borderRadius: 2, backgroundColor: Colors.headerBackground, alignItems: 'center', justifyContent: 'center' }}>
                                    <ActivityIndicator size="small" color="white" />
                                    <Text style={{ color: 'white', fontWeight: 'bold' }}>{' Loading..'}</Text>
                                </View>
                                : null
                        }
                    />
                    {<Fab
                        style={{ backgroundColor: Colors.headerBackground, size: 5 }}
                        direction="up"
                        position="bottomRight"
                        onPress={() => {
                            // this.focusListener = this.props.navigation.addListener('willFocus', ()=>{
                            //     this.setState({selectedValues: {}})
                            //     this.focusListener.remove()
                            // })
                            this.setState({
                                sendValues: this.state.selectedValues,
                                selectedValues: {},
                            }, () => {
                                this.props.navigation.navigate(Route.CreateReceiptPayout, {
                                    inOutFlag: this.state.inOutFlag,
                                    transList: this.state.sendValues,
                                    customerId: this.state.customerId,
                                });
                            });
                        }}
                    >
                        <Icon type="MaterialCommunityIcons" name="currency-inr" style={styles.fabicon} />
                    </Fab>}
                </View>
            );
        }
    }
}

// class ARAPFinanceItemView extends React.Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             finance: null,
//             index: 0,
//             session: {},
//             isSelected: false
//         };
//         this.onPressItem = this.onPressItem.bind(this)
//     }

//     componentWillMount() {
//         this.setState({ ...this.props });
//     }

//     componentWillReceiveProps(nextProps) {
//         this.setState({ ...nextProps });
//     }

//     onPressItem() {
//         if (this.props.onPressItem(this.state.finance)) {
//             this.setState(previousState => { return { isSelected: !previousState.isSelected }});
//         }
//     }

//     render() {
//         const { navigation } = this.props;
//         const finance = this.state.finance;
//         const session = this.state.session;
//         var shortCode = finance.dc_type_name == "Credit" ? "C" : "D";
//         if (finance.trans_type_name != "null" && finance.trans_type_name != "") {
//             let strArr = finance.trans_type_name.split(" ");
//             shortCode = "";
//             for (let index = 0; index < strArr.length; index++) {
//                 const element = strArr[index];
//                 if (strArr.length > 1) {
//                     shortCode += (element.substring(0, 1));
//                 } else {
//                     shortCode += (element.substring(0, 2));
//                 }
//             }
//         }

//         return (
//             <TouchableOpacity style={{backgroundColor: this.state.isSelected ? Styles.headerBackground : 'white' }} onPress={this.onPressItem}>
//                 <Card style={{backgroundColor: this.state.isSelected ? '#b3d2e6' : 'white', marginTop: 0.5, marginBottom: 0.5 }}>
//                     <CardItem style={{backgroundColor: this.state.isSelected ? '#b3d2e6' : 'white' }}>
//                         <Left>
//                             <View
//                                 style={{
//                                     width: 40,
//                                     height: 40,
//                                     borderRadius: 40 / 2,
//                                     backgroundColor: IconColor[this.state.index % IconColor.length],
//                                     justifyContent: "center",
//                                     top: 5
//                                 }}
//                             >
//                                 <Text style={{ color: 'white', fontWeight: 'bold', textAlign: 'center' }}>{shortCode.toUpperCase()}</Text>
//                             </View>
//                             <Body>
//                                 <Text style={{ fontSize: 13, textAlign: "right" }}>
//                                     <Icon
//                                         type="FontAwesome" name="history"
//                                         style={{ fontSize: 15 }}
//                                     />{" "}
//                                     {DateFormat(finance.trans_date, DATE_TIME_LAST_UPDATED)}
//                                 </Text>
//                                 <Text style={{ fontSize: 16, fontWeight: "bold" }}>
//                                     {finance.ass_acc_name != null && finance.ass_acc_name != 'null' ? finance.ass_acc_name : ""}
//                                 </Text>
//                                 <Text style={{ marginTop: 1 }}>
//                                     {finance.trans_request_number}
//                                 </Text>
//                                 <View style={{ flex: 1, flexDirection: 'row', marginBottom: 3 }}>
//                                     <Text style={{ marginTop: 1, flex: 1 }}>
//                                         {finance.trans_type_name}
//                                     </Text>
//                                     <View style={{ flex: 1, alignItems: 'flex-end', }}>
//                                         <Text note style={{ borderRadius: 3, backgroundColor: STATUS_COLOR[finance.status_id], textAlign: 'center', color: 'white' }}> {finance.status_name}</Text>
//                                     </View>
//                                 </View>
//                             </Body>
//                         </Left>
//                     </CardItem>
//                     <View style={{ flex: 1, flexDirection: 'row', marginBottom: 3 }}>
//                         <View style={{ flex: 1, flexDirection: 'column' }}>
//                             <Text style={{ textAlign: 'center' }}>{'Amount'}</Text>
//                             <Text style={{ marginTop: 2, color: 'black', textAlign: 'center' }}>
//                                 <Icon name={session.info[AppConstants.keyCurrency]} type="FontAwesome" style={styles.currency} />
//                                 {" " + NumberFormat(RemoveSeparator(finance.amount, session.info.exponent), session.info)}
//                             </Text>
//                         </View>
//                         <View style={{ flex: 1 }}>
//                             <Text style={{ textAlign: 'center' }}>{'Pending'}</Text>
//                             <Text style={{ marginTop: 2, color: 'red', textAlign: 'center' }}>
//                                 <Icon name={session.info[AppConstants.keyCurrency]} type="FontAwesome" style={styles.currency} />
//                                 {" " + NumberFormat(RemoveSeparator(Number(finance.amount) - Number(finance.adjust_amount), session.info.exponent), session.info)}
//                             </Text>
//                         </View>
//                         <View style={{ flex: 1 }}>
//                             <Text style={{ textAlign: 'center' }}>{'Uncleared'}</Text>
//                             <Text style={{ marginTop: 2, color: 'black', textAlign: 'center' }}>
//                                 <Icon name={session.info[AppConstants.keyCurrency]} type="FontAwesome" style={styles.currency} />
//                                 {" " + NumberFormat(RemoveSeparator(finance.ucAmount, session.info.exponent), session.info)}
//                             </Text>
//                         </View>
//                     </View>
//                 </Card >
//             </TouchableOpacity>
//         );
//     }
// }

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10,
    },
    currency: {
        fontSize: 14,
        fontWeight: 'normal',
    },
});
