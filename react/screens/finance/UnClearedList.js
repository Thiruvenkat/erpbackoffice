/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/no-did-mount-set-state */
import React from 'react';
import {
  FlatList,
  View,
  Text,
  StatusBar,
  Dimensions,
  BackHandler,
  ActivityIndicator,
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import {Icon, InputGroup, Input} from 'native-base';
import {Styles} from '../../style/Styles';
import Colors from '../../style/Colors';
import Toast from 'react-native-simple-toast';
import {
  DateFormat,
  DATE_TIME_SERVER,
} from 'react-native-erp-mobile-library/react/common/Common';
import {
  GetAllUnClearedList,
  GetAllCustomerAccounts,
} from 'react-native-erp-mobile-library/react/screens/finance/dbHelper/FinanceDBHelper';
import {getAllUnClearedList} from 'react-native-erp-mobile-library/react/controller/SyncDataController';
import PreparingProgress from 'react-native-erp-mobile-library/react/component/PreparingProgress';
import UnClearedItemView from './UnClearedItemView';
import {sessionInfo} from 'react-native-erp-mobile-library';
import {messages} from 'react-native-erp-mobile-library/react/i18n/i18n';
import FinanceFilter, {FilterTypes} from './FinanceFilter';
import AsyncStorage from '@react-native-community/async-storage';
import AppConstants from 'react-native-erp-mobile-library/react/constant/AppConstants';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import {renderHeaderView} from 'react-native-erp-mobile-library/react/util/HeaderUtil';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

// create a component
export default class UnClearedList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      financeList: [],
      AccountIds: [],
      customerId: null,
      customerData: props.route.params?.parentProps.customerData ?? '',
      session: sessionInfo({type: 'get', key: 'loginInfo'}),
      filter: {
        startDate: DateFormat('2018-01-01', DATE_TIME_SERVER),
        endDate: DateFormat(new Date(), DATE_TIME_SERVER),
      },
      loadMore: false,
      offset: 0,
      limit: 10,
      conditions: [],
      searchQuery: '',
      isServiceCalled: false,
      isFinanceFilterEnabled: false,
      filterTypes: [
        {
          type: FilterTypes.status,
          filters: [],
          selectedValues: {},
          column: 'status_id',
        },
      ],
      filterEnabled: '',
    };
    // this.props.parent.addRefsUnclear(this);
  }

  componentDidMount() {
    this.props.navigation.setParams({
      headerTitle: this.props.route.params?.headerTitle ?? 'Finance',
      headerType: this.props.route.params?.headerType ?? HeaderTypes.HeaderBack,
      parentProps: this,
    });
    this.props.navigation.setOptions(renderHeaderView(this));
    if (this.state.customerData && this.state.customerData.customer_id) {
      this.setState(
        {
          customerId: this.state.customerData.customer_id,
        },
        () => {
          this._getCustomerAccountDetails();
        },
      );
    } else {
      this._getAllUnclearedList();
    }
  }

  applyFilter = () => {
    this.setState({isFinanceFilterEnabled: true});
  };

  onPressApplyButton = filterTypes => {
    this.state.filterTypes = filterTypes;
    this.state.offset = 0;
    this.state.isFinanceFilterEnabled = false;
    this._getAllUnclearedList();
    AsyncStorage.setItem(
      AppConstants.keyFinanceFilters,
      JSON.stringify(filterTypes),
    );
    Toast.show(messages('filterApplied'), Toast.SHORT);
  };

  _getCustomerAccountDetails() {
    let filter = {
      conditions: [
        {
          column: 'customer_id',
          value: this.state.customerId,
          operator: '=',
        },
      ],
    };
    GetAllCustomerAccounts(datas => {
      this.setState({customerAccountData: datas}, () => {
        this._getPayableReceivableAmount();
      });
    }, filter);
  }

  _getPayableReceivableAmount() {
    var accountIds = [];
    let accountDatas = [];
    accountDatas = this.state.customerAccountData;
    if (accountDatas.length > 0) {
      for (let index = 0; index < accountDatas.length; index++) {
        accountIds.push(accountDatas[index].account_id);
      }
      this.setState(
        {
          AccountIds: accountIds,
        },
        () => {
          this._getAllUnclearedList();
        },
      );
    }
  }

  componentWillUnmount() {
    // this.backHandler.remove();
  }

  _getAllUnclearedList() {
    var filter = {
      limit: this.state.limit,
      offset: this.state.offset,
      orderBy: 'last_update_time desc',
      conditions: [],
    };
    if (this.state.customerData) {
      if (this.state.AccountIds.length > 0) {
        filter.conditions.push({
          column: 'account_id',
          value: this.state.AccountIds,
          dataType: 'String',
          operator: 'in',
        });
      }
    }
    if (this.state.searchQuery.length > 0) {
      filter.conditions.push({
        column: 'account_name',
        value: '%' + this.state.searchQuery + '%',
        operator: 'like',
      });
    }
    if (this.state.filterTypes.length > 0) {
      this.state.filterTypes.forEach(filterType => {
        const values = Object.keys(filterType.selectedValues);
        if (values.length > 0) {
          filter.conditions = [
            {
              value: values,
              column: filterType.column,
              operator: 'in',
            },
            ...filter.conditions,
          ];
        }
      });
    }
    GetAllUnClearedList(datas => {
      ///////
      if (this.state.searchQuery.length > 0 && datas.length > 0) {
        this.setState({
          financeList: datas,
          offset: 0,
          isServiceCalled: false,
          isLoading: false,
          loadMore: false,
        });
      } else if (datas.length > 0) {
        this.setState({
          financeList:
            this.state.offset === 0
              ? datas
              : this.state.financeList.concat(datas),
          isLoading: false,
          loadMore: false,
          offset: this.state.offset + datas.length,
          isServiceCalled: true,
        });
      } else if (!this.state.isServiceCalled) {
        this.setState({isLoading: false});
        this._getAllUnclearedListFromServer();
      } else {
        this.setState({
          financeList: datas,
          offset: 0,
          isServiceCalled: false,
          isLoading: false,
          loadMore: false,
        });
      }
    }, filter);
  }

  _getAllUnclearedListFromServer() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        let filter = {
          limit: this.state.limit,
          offset: this.state.offset,
          orderBy: 'up.last_updated',
          conditions: [
            {
              column: 'up.last_updated',
              value: '1483228800000',
              dataType: 'Date',
              operator: '>',
            },
          ],
        };
        getAllUnClearedList(this.state.session, filter, isSuccess => {
          this.setState({isServiceCalled: true});
          if (isSuccess) {
            this._getAllUnclearedList();
          }
        });
      } else {
        this.setState({
          isLoading: false,
          loadMore: false,
          isServiceCalled: true,
        });
        console.log('make internet connection..!');
      }
    });
  }

  onRefresh() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        this.setState({offset: 0, isServiceCalled: false}, () => {
          this._getAllUnclearedListFromServer();
        });
      } else {
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    });
  }

  renderItem({item, index}) {
    return (
      <UnClearedItemView
        finance={item}
        index={index}
        session={this.state.session}
        parentProps={this.props}
        navigation={this.props.navigation}
      />
    );
  }

  ListEmptyView = () => {
    return (
      <View
        style={{
          width: DEVICE_WIDTH,
          height: DEVICE_HEIGHT,
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
        }}>
        <Icon
          name="cube"
          type="FontAwesome"
          style={{color: Colors.headerBackground, fontSize: 40}}
        />
        <Text style={{textAlign: 'center', color: 'darkgrey', marginTop: 5}}>
          No data found.
        </Text>
      </View>
    );
  };

  render() {
    if (this.state.isLoading) {
      return <PreparingProgress />;
    } else {
      return (
        <View
          padder
          style={{
            backgroundColor: Styles.appViewBackground.backgroundColor,
            height: '100%',
          }}>
          <StatusBar
            backgroundColor={Styles.statusBar.backgroundColor}
            barStyle="light-content"
          />
          <InputGroup borderType="rounded">
            <Input
              placeholder="search accounter's name"
              placeholderTextColor={'grey'}
              onChangeText={text => {
                this.setState({searchQuery: text, offset: 0}, () => {
                  this._getAllUnclearedList();
                });
              }}
            />
          </InputGroup>
          <FinanceFilter
            isVisible={this.state.isFinanceFilterEnabled}
            filterTypes={this.state.filterTypes}
            closeBtnClicked={() => {
              this.setState({isFinanceFilterEnabled: false}, () => {
                //
              });
            }}
            onPressApplyButton={this.onPressApplyButton.bind(this)}
          />

          <FlatList
            extraData={this.state}
            data={this.state.financeList}
            ItemSeparatorComponent={this.FlatListItemSeparator}
            renderItem={item => this.renderItem(item)}
            keyExtractor={item => item.trans_request_number}
            onEndReachedThreshold={0.1}
            onEndReached={() => {
              if (
                !this.state.loadMore &&
                this.state.offset >= this.state.limit &&
                this.state.isServiceCalled
              ) {
                this.setState({loadMore: true}, () => {
                  this._getAllUnclearedList();
                });
              }
            }}
            onRefresh={() => this.onRefresh()}
            refreshing={this.state.isLoading}
            ListEmptyComponent={this.ListEmptyView}
            ListFooterComponent={
              this.state.loadMore && this.state.searchQuery.length === 0 ? (
                <View
                  style={{
                    padding: 7,
                    flex: 1,
                    flexDirection: 'row',
                    borderRadius: 2,
                    backgroundColor: Colors.headerBackground,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <ActivityIndicator size="small" color="white" />
                  <Text style={{color: 'white', fontWeight: 'bold'}}>
                    {' Loading..'}
                  </Text>
                </View>
              ) : null
            }
          />
        </View>
      );
    }
  }
}
