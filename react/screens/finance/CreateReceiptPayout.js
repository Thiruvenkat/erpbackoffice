/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/no-did-mount-set-state */
import React from 'react';
import {
  Alert,
  TouchableOpacity,
  Text,
  View,
  Modal,
  StyleSheet,
  Dimensions,
} from 'react-native';
import Toast from 'react-native-simple-toast';
import MasterConstants from '../../constant/MasterConstants';
import AppConstants from '../../constant/AppConstants';
import Colors from '../../style/Colors';
import UIReceiptJson from '../../../assets/jsons/receipt.json';
import UIPayoutJson from '../../../assets/jsons/payout.json';
import {messages} from 'react-native-erp-mobile-library/react/i18n/i18n';
import {
  Card,
  Root,
  Content,
  Icon,
  Footer,
  Picker,
  Input,
  Textarea,
  FooterTab,
  Button,
} from 'native-base';
import _ from 'lodash';
import CustomSearchView, {
  SearchType,
} from 'react-native-erp-mobile-library/react/utilities/CustomSearchView';
import {
  DateFormat,
  RemoveSeparator,
  NumberFormat,
  DATE_TIME_LAST_UPDATED,
  DATE_TIME_SERVER,
  AddSeparator,
} from 'react-native-erp-mobile-library/react/common/Common';
import DateTimePicker from 'react-native-modal-datetime-picker';
import {
  GetAllPaymentMode,
  GetAllBankAccount,
  GetAllPaymentAccountGroup,
  GetCustomerCurrency,
  GetAllCustomerAccounts,
} from 'react-native-erp-mobile-library/react/screens/finance/dbHelper/FinanceDBHelper';
import Controller from 'react-native-erp-mobile-library/react/controller/Controller';
import Loader from 'react-native-erp-mobile-library/react/component/Loader';
import {sessionInfo} from 'react-native-erp-mobile-library';
import {GetAllCustomers} from 'react-native-erp-mobile-library/react/screens/customers/dbHelper/CustomerDB';
import {FlatList, ScrollView} from 'react-native';
// import { CheckBox, colors } from 'react-native-elements'
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import {Styles} from 'react-native-erp-mobile-library/react/style/Styles';
import {renderHeaderView} from 'react-native-erp-mobile-library/react/util/HeaderUtil';
var UUID = require('react-native-uuid');

const DEVICE_WIDTH = Dimensions.get('window').width;

export default class CreateReceiptPayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      session: sessionInfo({type: 'get', key: 'loginInfo'}),
      inOutFlag: this.props.route.params.inOutFlag,
      UIJson:
        this.props.route.params.inOutFlag ===
        MasterConstants.inOutFlagReceivable
          ? UIReceiptJson
          : UIPayoutJson,
      sortedKey: [],
      view: {},
      pendingInvoiceModalVisble: false,
      selectedTransValue: _.cloneDeep(
        Object.values(this.props.route.params?.transList, []),
      ),
      checked: false,
      pendingInvoiceTotalAmount: 0.0,
    };
    this.totalPendingAmount = null;
    this.onChangePendingAmountValue = this.onChangePendingAmountValue.bind(
      this,
    );
  }

  componentDidMount() {
    this.props.navigation.setParams({
      headerTitle:
        this.props.route.params.inOutFlag ===
        MasterConstants.inOutFlagReceivable
          ? 'Receipt'
          : 'Payout',
      headerType:
        this.props.route.params?.headerTitle ?? HeaderTypes.HeaderBack,
    });
    this.props.navigation.setOptions(renderHeaderView(this));
    let sortedKey = this._arrangeUIJsonAsSorted();
    var view = {};
    for (let index = 0; index < sortedKey.length; index++) {
      const key = sortedKey[index];

      try {
        let object = this.state.UIJson.headers[key];
        if (
          object.enabled !== undefined &&
          object.enabled !== 'none' &&
          object.inputType !== undefined
        ) {
          let inputType = object.inputType.type;
          let viewItem = {
            key: key,
            title: messages(object.labelName),
            inputType: inputType,
            value:
              inputType === 'search' || inputType === 'customdropdown'
                ? {id: '', name: ''}
                : inputType === 'date'
                ? DateFormat(new Date(), DATE_TIME_SERVER)
                : '',
            dropDownList: [],
            placeHolder:
              inputType === 'search' || inputType === 'date'
                ? 'Choose ' + messages(object.labelName)
                : '',
            mandatory: object.mandatory[0].validation === 'true',
            mandatorySymbol: object.mandatorySymbol,
            errorMessage: messages(object.mandatory[0].errorMsg),
            isValid: true,
            isClearButtonEnabled:
              inputType === 'text' || inputType === 'textArea',
            isSearchEnabled: false,
            isDatePickerEnabled: false,
            isDisabled: false,
            textType:
              inputType === 'text' && object.inputType.textType === 'currency'
                ? 'numeric'
                : 'default',
            searchType:
              key === 'account' || key === 'assAccount'
                ? SearchType.accounts
                : key === 'bankAccount'
                ? SearchType.bankAccount
                : 0,
            searchFilters: [],
            onPress: () => {},
          };
          switch (key) {
            case 'account':
              viewItem.searchFilters.push({
                column: 'account_group_id',
                value: MasterConstants.accountGroupMain,
                operator: '=',
              });
              viewItem.searchFilters.push({
                column: 'tenant_id',
                value: [...this.state.session.info.applicableTenants],
                operator: 'in',
              });
              break;
            case 'assAccount':
              viewItem.searchFilters.push({
                column: 'tenant_id',
                value: [...this.state.session.info.applicableTenants],
                operator: 'in',
              });
              if (
                object.filter !== undefined &&
                object.filter.searchAccountGroupIds !== undefined
              ) {
                viewItem.searchFilters.push({
                  column: 'account_group_id',
                  value: JSON.parse(
                    '[' + object.filter.searchAccountGroupIds + ']',
                  ),
                  operator: 'in',
                });
              }
              if (
                this.state.selectedTransValue &&
                this.state.selectedTransValue.length > 0
              ) {
                viewItem.value = {
                  id: this.state.selectedTransValue[0].ass_acc_id,
                  name: this.state.selectedTransValue[0].ass_acc_name,
                };
                viewItem.isDisabled = true;
              }
              break;
            case 'paymentMode':
              viewItem.searchFilters.push({
                column: 'tenant_id',
                value: [...this.state.session.info.applicableTenants],
                operator: 'in',
              });
              break;
            case 'bankAccount':
              viewItem.searchFilters.push({
                column: 'tenant_id',
                value: [...this.state.session.info.applicableTenants],
                operator: 'in',
              });
              break;
            case 'amount':
              if (
                this.state.selectedTransValue &&
                this.state.selectedTransValue.length > 0
              ) {
                viewItem.value = this.state.pendingInvoiceTotalAmount.toString();
                viewItem.isDisabled = true;
              }
              break;
          }
          if (key === 'pendingAmount') {
            if (
              this.state.selectedTransValue &&
              this.state.selectedTransValue.length > 0
            ) {
              this.state.checked = true;
              this.callSelectAllPendingInvoices(false);
              viewItem.value = this.totalPendingAmount.toString();
              viewItem.isDisabled = true;
              viewItem.onPress = () => {
                this.setState({pendingInvoiceModalVisble: true});
              };
            } else {
              continue;
            }
          }
          view[key] = viewItem;
        }
      } catch (error) {
        console.log('create receipt-payout :::: ' + JSON.stringify(error));
      }
    }
    this.setState(
      {
        view: view,
        sortedKey: sortedKey,
        isLoading: false,
        pendingInvoiceModalVisble: this.state.selectedTransValue.length > 0,
      },
      () => {
        this._setPaymentMode();
        this._setAccount();
      },
    );
  }

  componentWillUnmount() {
    // this.backHandler.remove();
  }

  // arrange the ui json based on same order view like wed application
  _arrangeUIJsonAsSorted() {
    let arrangedKey = [
      'account',
      'assAccount',
      'paymentMode',
      'bankAccount',
      'pendingAmount',
      'accountBalance',
      'amount',
      'pendingInvoice',
      'cheque',
      'paymentDetails',
      'paymentDate',
      'comments',
      '',
    ];
    let sortedKey = [];
    for (let index = 0; index < arrangedKey.length; index++) {
      if (this.state.UIJson.headers[arrangedKey[index]] !== undefined) {
        sortedKey.push(arrangedKey[index]);
      }
    }
    return sortedKey;
  }

  _setPaymentMode() {
    if (_.keys(this.state.view).includes('paymentMode')) {
      GetAllPaymentMode(
        paymentMode => {
          if (paymentMode.length > 0) {
            this._updateStateValues('paymentMode', {
              dropDownList: paymentMode,
              value: paymentMode[0],
            });
            this._setBankAccount(paymentMode[0]);
          } else {
            Alert.alert(
              'Info',
              'Master data not found for make next process. Please sync master details(Menu->Sync data->Master)',
              [
                {
                  text: 'Ok',
                  onPress: () => {
                    this.props.navigation.goBack();
                  },
                },
              ],
              {cancelable: false},
            );
          }
        },
        {filters: {conditions: this.state.view.paymentMode.searchFilters}},
      );
    }
  }
  _setAccount() {
    if (_.keys(this.state.view).includes('account')) {
      const filter = {
        conditions: [
          this.state.view.account.searchFilters[0],
          {
            column: 'tenant_id',
            value: this.state.session.info.tenantId,
            operator: '=',
          },
        ],
      };
      GetAllCustomerAccounts(datas => {
        if (datas.length > 0) {
          this._updateStateValues('account', {
            value: {
              id: datas[0].account_id,
              name: datas[0].account_name,
            },
          });
        } else {
          Alert.alert(
            'Info',
            'Master data not found for make next process. Please sync master details(Menu->Sync data->Customer)',
            [
              {
                text: 'Ok',
                onPress: () => {
                  this.props.navigation.goBack();
                },
              },
            ],
            {cancelable: false},
          );
        }
      }, filter);
    }
    let customer_ID =
      this.props.route.params?.customerId ??
      this.state.session.info.loggedInCustomerId;
    if (_.keys(this.state.view).includes('assAccount') && customer_ID) {
      GetAllCustomers(
        AppConstants.affordeBackOfficeDB,
        datas => {
          this._updateStateValues('assAccount', {
            value: {
              id: datas[0].account_id,
              name: datas[0].account_name,
            },
            isDisabled: true,
          });
        },
        {
          conditions: [
            ...this.state.view.assAccount.searchFilters,
            {
              column: 'customer_id',
              value: this.props.route.params.customerId,
              operator: '=',
            },
          ],
        },
      );
    }
  }

  _setBankAccount(paymentMode) {
    GetAllPaymentAccountGroup(
      paymentModeAccountGroup => {
        console.log(
          'paymentModeAccountGroup ::: ' +
            JSON.stringify(paymentModeAccountGroup),
        );
        if (
          paymentModeAccountGroup.length > 0 &&
          _.keys(this.state.view).includes('bankAccount')
        ) {
          let filter = {
            conditions: [
              {
                column: 'tenant_id',
                value: [...this.state.session.info.applicableTenants],
                operator: 'in',
              },
              {
                column: 'account_group_id',
                value: paymentModeAccountGroup[0].id,
                operator: '=',
              },
            ],
          };
          GetAllBankAccount(datas => {
            console.log('bankAccount ::: ' + JSON.stringify(datas));
            this._updateStateValues('bankAccount', {
              searchFilters: [...filter.conditions],
              value: datas.length > 0 ? datas[0] : {id: '', name: ''},
            });
          }, filter);
          console.log(filter);
        } else {
          Alert.alert(
            'Info',
            'Master data not found for make next process. Please sync master details(Menu->Sync data->Master)',
            [
              {
                text: 'Ok',
                onPress: () => {
                  this.props.navigation.goBack();
                },
              },
            ],
            {cancelable: false},
          );
        }
      },
      {
        conditions: [
          {column: 'payment_type_id', value: paymentMode.id, operator: '='},
        ],
      },
    );
  }

  _updateStateValues(name, updateValues) {
    let view = this.state.view;
    let data = view[name];
    _.keys(updateValues).forEach(key => {
      data[key] = updateValues[key];
    });
    view[name] = data;
    this.setState({view: view});
  }

  _validateForm() {
    var view = this.state.view;
    var keys = _.keys(view);
    let count = 0;
    for (let index = 0; index < keys.length; index++) {
      var viewItem = view[keys[index]];
      if (viewItem.mandatory) {
        if (
          viewItem.inputType === 'search' ||
          viewItem.inputType === 'customdropdown'
        ) {
          viewItem.isValid = !(
            viewItem.value.id === '' || viewItem.value.id === undefined
          );
          count =
            viewItem.value.id === '' || viewItem.value.id === undefined
              ? count + 1
              : count;
          console.log(keys[index] + '' + count);
        } else {
          viewItem.isValid = !(
            viewItem.value === '' || viewItem.value === undefined
          );
          count =
            viewItem.value === '' || viewItem.value === undefined
              ? count + 1
              : count;
          console.log(keys[index] + '' + count);
        }
      }
      view[keys[index]] = viewItem;
    }
    this.setState({view: view});
    return count === 0;
  }

  _renderPendingInvoiceTotalAmount = () => (
    <PendingInvoiceTotalAmount
      ref={node => {
        this.pendingInvoiceTotalAmountRef = node;
      }}
      session={this.state.session}
      totalAmount={this.state.pendingInvoiceTotalAmount}
    />
  );

  callSelectAllPendingInvoices(updateState = true) {
    let selectedValue = this.state.selectedTransValue;

    var total = 0;
    this.state.selectedTransValue.forEach((value, index) => {
      if (this.state.checked) {
        let inputAmount =
          RemoveSeparator(value.amount, this.state.session.info.exponent) -
          RemoveSeparator(
            value.adjust_amount,
            this.state.session.info.exponent,
          );
        selectedValue[index].inputAmount = inputAmount;
        total += inputAmount;
      } else {
        selectedValue[index].inputAmount = 0;
      }
    });
    if (!this.totalPendingAmount) {
      this.totalPendingAmount = total;
    }
    this.state.selectedTransValue = selectedValue;
    this.state.pendingInvoiceTotalAmount = total;
    if (updateState) {
      this._updateStateValues('amount', {value: total.toString()});
    }
    if (this.pendingInvoiceTotalAmountRef) {
      this.pendingInvoiceTotalAmountRef.setState({totalAmount: total});
    }
  }

  _renderPendingInvoiceModal() {
    return (
      <View
        style={{
          backgroundColor: 'white',
          padding: 3,
          borderRadius: 2,
          borderColor: 'rgba(0, 0, 0, 0.1)',
        }}>
        <View
          style={{
            flexDirection: 'row',
            padding: 5,
          }}>
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Icon
              style={{
                color: 'darkgrey',
                position: 'absolute',
                left: 10,
                padding: 5,
              }}
              name={
                this.state.checked
                  ? 'checkbox-marked'
                  : 'checkbox-blank-outline'
              }
              type="MaterialCommunityIcons"
              onPress={() => {
                this.setState({checked: !this.state.checked}, () => {
                  this.callSelectAllPendingInvoices();
                });
              }}
            />
          </View>
          <View
            style={{
              flex: 4,
              padding: 4,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: 20,
                color: 'black',
                fontWeight: 'bold',
                textAlign: 'left',
              }}>
              Pending Invoice{'   '}
            </Text>
            {this._renderPendingInvoiceTotalAmount()}
          </View>
        </View>
        <View
          style={{
            height: 1,
            backgroundColor: '#d7d9dd',
            marginLeft: 5,
            marginTop: 5,
            marginRight: 5,
          }}
        />
        <ScrollView style={{marginTop: 5}}>
          <FlatList
            extraData={this.state}
            data={this.state.selectedTransValue}
            ItemSeparatorComponent={this.FlatListItemSeparator}
            renderItem={({item, index}) => this.renderItem(item, index)}
            keyExtractor={(item, index) => index.toString()}
          />
        </ScrollView>
        <View />
        <Footer
          style={{
            backgroundColor: Styles.statusBar.backgroundColor,
            height: 50,
          }}>
          <FooterTab
            style={{
              backgroundColor: Colors.headerBackground,
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: 0.5,
              borderColor: 'white',
              borderBottomColor: 'transparent',
            }}>
            <Button
              onPress={() => {
                this.setState({
                  pendingInvoiceModalVisble: false,
                });
              }}
              style={{backgroundColor: Colors.headerBackground}}>
              <Text
                style={{
                  marginTop: 5,
                  fontSize: 18,
                  fontWeight: 'bold',
                  padding: 10,
                  width: '100%',
                  backgroundColor: 'transparent',
                  color: 'white',
                  textAlign: 'center',
                }}>
                Done
              </Text>
            </Button>
          </FooterTab>
        </Footer>
      </View>
    );
  }

  renderItem(item, index) {
    if (item.inputAmount) {
      return (
        <PendingOrderList
          finance={item}
          index={index}
          pendingAmount={
            this.state.checked
              ? 0
              : item.amount -
                item.adjust_amount -
                AddSeparator(item.inputAmount, this.state.session.info.exponent)
          }
          session={this.state.session}
          onChangePendingAmountValue={this.onChangePendingAmountValue}
          inputValue={Number(item.inputAmount)}
        />
      );
    } else {
      return (
        <PendingOrderList
          finance={item}
          index={index}
          pendingAmount={item.amount - item.adjust_amount}
          session={this.state.session}
          onChangePendingAmountValue={this.onChangePendingAmountValue}
          inputValue={0}
        />
      );
    }
  }

  onChangePendingAmountValue(amount, index) {
    if (this.pendingInvoiceTotalAmountRef) {
      let selectedValue = this.state.selectedTransValue;
      if (!selectedValue[index].inputAmount) {
        selectedValue[index].inputAmount = 0;
      }
      selectedValue[index].inputAmount = Number(amount);
      var total = 0;
      selectedValue.forEach(value => {
        if (value.inputAmount) {
          total = total + Number(value.inputAmount);
        }
      });
      this.state.selectedTransValue = selectedValue;
      this.state.pendingInvoiceTotalAmount = total;
      this._updateStateValues('amount', {value: total.toString()});
      this.pendingInvoiceTotalAmountRef.setState({totalAmount: total});
    }
    if (this.state.checked) {
      this.setState({checked: !this.state.checked});
    }
  }

  render() {
    return (
      <Root>
        <Loader loading={this.state.isLoading} />
        <Modal
          visible={this.state.pendingInvoiceModalVisble}
          onRequestClose={() => {
            this.setState({pendingInvoiceModalVisble: false});
          }}>
          {this._renderPendingInvoiceModal()}
        </Modal>
        <Content style={{backgroundColor: 'white'}}>
          {_.values(this.state.view).map(object => {
            switch (object.inputType) {
              case 'search':
                return (
                  <Card style={styles.card}>
                    <View style={styles.cardItem}>
                      <View style={styles.headerItem}>
                        <Text style={styles.title}>
                          {object.title}
                          {object.mandatory ? (
                            <Text style={{color: 'red'}}>
                              {' ' + object.mandatorySymbol}
                            </Text>
                          ) : null}
                        </Text>
                        {object.mandatory && !object.isValid ? (
                          <Text style={styles.errorMessage}>
                            {object.errorMessage}
                          </Text>
                        ) : null}
                      </View>
                      <TouchableOpacity
                        style={styles.inputTypeItem}
                        onPress={() => {
                          this._updateStateValues(object.key, {
                            isSearchEnabled: true,
                          });
                        }}>
                        <Text style={styles.inputType}>
                          {object.value.id !== undefined &&
                          object.value.id.length > 0
                            ? object.value.name
                            : object.placeHolder}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    {!object.isDisabled ? (
                      <CustomSearchView
                        modalVisible={object.isSearchEnabled}
                        searchType={object.searchType}
                        conditions={object.searchFilters}
                        session={this.state.session}
                        hideModel={() => {
                          this._updateStateValues(object.key, {
                            isSearchEnabled: false,
                          });
                        }}
                        onChooseSearchValue={searchValue => {
                          this._updateStateValues(object.key, {
                            isSearchEnabled: false,
                            value: searchValue,
                          });
                        }}
                      />
                    ) : null}
                  </Card>
                );
              case 'customdropdown':
                return (
                  <Card style={styles.card}>
                    <View style={styles.cardItem}>
                      <View style={styles.headerItem}>
                        <Text style={styles.title}>
                          {object.title}
                          {object.mandatory ? (
                            <Text style={{color: 'red'}}>
                              {' ' + object.mandatorySymbol}
                            </Text>
                          ) : null}
                        </Text>
                        {object.mandatory && !object.isValid ? (
                          <Text style={styles.errorMessage}>
                            {object.errorMessage}
                          </Text>
                        ) : null}
                      </View>
                      <Picker
                        style={{
                          width: DEVICE_WIDTH - 15,
                          height: 35,
                          backgroundColor: '#f4f5f7',
                          borderRadius: 5,
                          marginStart: 3,
                          marginEnd: 3,
                        }}
                        mode="dropdown"
                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                        headerBackButtonText="Back"
                        selectedValue={this.state.view[object.key].value.id}
                        onValueChange={value => {
                          let view = this.state.view;
                          let picker = view[object.key];
                          var item = _.find(picker.dropDownList, ['id', value]);
                          if (_.isObject(item)) {
                            picker.value = item;
                            view[object.key] = picker;
                          }
                          this.setState({view: view}, () => {
                            if (object.key === 'paymentMode') {
                              console.log(item);
                              this._setBankAccount(item);
                            }
                          });
                        }}>
                        {this.state.view[object.key].dropDownList.map(
                          (val, index) => {
                            return (
                              <Picker.Item
                                key={index}
                                style={{
                                  marginLeft: 20,
                                  marginRight: 20,
                                  color: 'white',
                                }}
                                label={val.name}
                                value={val.id}
                              />
                            );
                          },
                        )}
                      </Picker>
                    </View>
                  </Card>
                );
              case 'text':
                return (
                  <Card style={styles.card}>
                    <View style={styles.cardItem}>
                      <View style={styles.headerItem}>
                        <Text style={styles.title}>
                          {object.title}
                          {object.mandatory ? (
                            <Text style={{color: 'red'}}>
                              {' ' + object.mandatorySymbol}
                            </Text>
                          ) : null}
                        </Text>
                        {object.mandatory && !object.isValid ? (
                          <Text style={styles.errorMessage}>
                            {object.errorMessage}
                          </Text>
                        ) : null}
                      </View>
                      <View style={styles.inputContainer}>
                        <Input
                          style={[styles.inputTypeItem, {fontSize: 15}]}
                          bordered
                          keyboardType={object.textType}
                          value={object.value}
                          placeholder={object.placeHolder}
                          disabled={object.isDisabled}
                          clearButtonMode="always"
                          onChangeText={text => {
                            this._updateStateValues(object.key, {value: text});
                          }}
                        />
                        {object.key === 'pendingAmount' && (
                          <TouchableOpacity
                            style={styles.clearIconItem}
                            onPress={() => {
                              object.onPress();
                            }}>
                            <Icon
                              name="form"
                              type="AntDesign"
                              style={{
                                color: Colors.headerBackground,
                                padding: 5,
                                fontSize: 16,
                              }}
                            />
                            <Text
                              style={{
                                fontSize: 13,
                                color: Colors.headerBackground,
                                fontWeight: 'bold',
                              }}>
                              {'Edit'}
                            </Text>
                          </TouchableOpacity>
                        )}
                        {!object.isDisabled && (
                          <TouchableOpacity
                            style={styles.clearIconItem}
                            onPress={() => {
                              this._updateStateValues(object.key, {value: ''});
                            }}>
                            <Icon
                              name="clear"
                              type="MaterialIcons"
                              style={styles.clearIcon}
                            />
                          </TouchableOpacity>
                        )}
                      </View>
                    </View>
                  </Card>
                );
              case 'textArea':
                return (
                  <Card style={styles.card}>
                    <View style={styles.cardItem}>
                      <View style={styles.headerItem}>
                        <Text style={styles.title}>
                          {object.title}
                          {object.mandatory ? (
                            <Text style={{color: 'red'}}>
                              {' ' + object.mandatorySymbol}
                            </Text>
                          ) : null}
                        </Text>
                        {object.mandatory && !object.isValid ? (
                          <Text style={styles.errorMessage}>
                            {object.errorMessage}
                          </Text>
                        ) : null}
                      </View>
                      <View style={styles.inputContainer}>
                        <Textarea
                          style={[
                            styles.inputTypeItem,
                            {
                              height: 70,
                              fontSize: 15,
                              justifyContent: 'flex-start',
                            },
                          ]}
                          rowSpan={4}
                          clearButtonMode="always"
                          bordered
                          value={object.value}
                          placeholder={object.placeHolder}
                          onChangeText={text => {
                            this._updateStateValues(object.key, {value: text});
                          }}
                        />
                        <TouchableOpacity
                          style={styles.clearIconItem}
                          onPress={() => {
                            this._updateStateValues(object.key, {value: ''});
                          }}>
                          <Icon
                            name="clear"
                            type="MaterialIcons"
                            style={styles.clearIcon}
                          />
                        </TouchableOpacity>
                      </View>
                    </View>
                  </Card>
                );
              case 'date':
                return (
                  <Card style={styles.card}>
                    <View style={styles.cardItem}>
                      <View style={styles.headerItem}>
                        <Text style={styles.title}>
                          {object.title}
                          {object.mandatory ? (
                            <Text style={{color: 'red'}}>
                              {' ' + object.mandatorySymbol}
                            </Text>
                          ) : null}
                        </Text>
                        {object.mandatory && !object.isValid ? (
                          <Text style={styles.errorMessage}>
                            {object.errorMessage}
                          </Text>
                        ) : null}
                      </View>
                      <TouchableOpacity
                        style={styles.inputTypeItem}
                        onPress={() => {
                          this._updateStateValues(object.key, {
                            isDatePickerEnabled: true,
                          });
                        }}>
                        <Text style={styles.inputType}>
                          {object.value.length > 0
                            ? DateFormat(object.value, DATE_TIME_LAST_UPDATED)
                            : object.placeHolder}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <DateTimePicker
                      isVisible={object.isDatePickerEnabled}
                      mode="datetime"
                      is24Hour={false}
                      onConfirm={date => {
                        this._updateStateValues(object.key, {
                          isDatePickerEnabled: false,
                          value: DateFormat(date, DATE_TIME_SERVER),
                        });
                      }}
                      onCancel={() => {
                        this._updateStateValues(object.key, {
                          isDatePickerEnabled: false,
                        });
                      }}
                    />
                  </Card>
                );
              default:
                break;
            }
          })}
        </Content>
        <Footer style={{backgroundColor: Colors.headerBackground, height: 50}}>
          <TouchableOpacity
            onPress={() => {
              if (this._validateForm()) {
                Alert.alert(
                  'Confirmation',
                  'Are you sure, do you want to create ' +
                    (this.props.route.params.inOutFlag ===
                    MasterConstants.inOutFlagReceivable
                      ? ' receipt'
                      : ' payout') +
                    '?',
                  [
                    {
                      text: 'No',
                      onPress: () => console.log('Cancel Pressed'),
                      style: 'cancel',
                    },
                    {
                      text: 'Yes',
                      onPress: () => {
                        console.log(' View JSON :::: ');
                        console.log(this.state.view);
                        this.prepareReceiptPayoutRequest();
                      },
                    },
                  ],
                  {cancelable: false},
                );
              }
            }}>
            <Text
              style={{
                marginTop: 5,
                fontSize: 20,
                fontWeight: 'bold',
                padding: 6,
                width: DEVICE_WIDTH - 10,
                backgroundColor: 'transparent',
                color: 'white',
                textAlign: 'center',
              }}>
              {'Create' +
                (this.props.route.params.inOutFlag ===
                MasterConstants.inOutFlagReceivable
                  ? ' Receipt'
                  : ' Payout')}
            </Text>
          </TouchableOpacity>
        </Footer>
      </Root>
    );
  }

  refreshARAPlist(transIds) {
    // DeletePastTransactions(filter, (isSuccess) => {
    //   if (isSuccess) {
    //     this.props.navigation.goBack();
    //   }
    // })
  }

  prepareReceiptPayoutRequest() {
    GetCustomerCurrency(
      currencyCode => {
        var object = {
          request: {
            envelope: {
              sourceApplication: 'Afforde UI',
              requestDate: DateFormat(new Date(), DATE_TIME_SERVER),
              requestFromType: 'android',
            },
            content: {
              bo_data: {
                type: this.state.UIJson.mobileStaticTypes.urlType,
                amount: 0,
                receiptPayout: {
                  transId: UUID.v1(),
                  lastUpdated: DateFormat(new Date(), DATE_TIME_SERVER),
                  instrumentDate: DateFormat(new Date(), DATE_TIME_SERVER),
                  date:
                    this.state.view.paymentDate !== undefined
                      ? this.state.view.paymentDate.value
                      : DateFormat(new Date(), DATE_TIME_SERVER),
                  salesAgent: this.state.session.info.loggedInCustomerId,
                  payType: this.state.view.paymentMode.value,
                  tenant: {
                    id: this.state.session.info.tenantId,
                  },
                  currencyCode: {
                    id: currencyCode,
                  },
                  status: {
                    id: this.state.UIJson.mobileStaticTypes.statusId,
                  },
                  rcpyType: {
                    id:
                      this.props.route.params.inOutFlag ===
                      MasterConstants.inOutFlagReceivable
                        ? MasterConstants.masterFinanceReceiptTypeId
                        : MasterConstants.masterFinancePayoutTypeId,
                  },
                  inOutFlag: this.props.route.params.inOutFlag,
                  description: this.state.view.comments.value,
                  instrumentNumber:
                    this.state.view.paymentDetail !== undefined
                      ? this.state.view.paymentDetail.value
                      : '',
                  clearedFlag: 1,
                  glAccount: this.state.view.bankAccount.value,
                  user: {
                    id: this.state.session.info.userId,
                  },
                  account: this.state.view.account.value,
                  associatedAcc: this.state.view.assAccount.value,
                  createOrderFlag: 0,
                  amount: AddSeparator(
                    this.state.view.amount.value,
                    this.state.session.info.exponent,
                  ),
                },
              },
            },
          },
        };

        // notes: update pending invoice amount detail.
        var pendingAmountList = this.state.selectedTransValue;
        if (pendingAmountList !== undefined && pendingAmountList.length > 0) {
          let totalAmount = 0;
          var updatedPendingAmountList = [];
          for (let index = 0; index < pendingAmountList.length; index++) {
            const receiptPayout = pendingAmountList[index];
            const inputAmount = AddSeparator(
              receiptPayout.inputAmount,
              this.state.session.info.exponent,
            );
            if (inputAmount > 0) {
              totalAmount += inputAmount;
              updatedPendingAmountList.push({
                transId: receiptPayout.trans_id,
                transCode: receiptPayout.trans_request_number,
                clearedAmount: receiptPayout.adjust_amount + inputAmount,
                invoiceAmt: inputAmount,
              });
            }
          }
          if (updatedPendingAmountList.length > 0) {
            object.request.content.bo_data.changeInvoices = updatedPendingAmountList;
            object.request.content.bo_data.amount = totalAmount;
          }
        }
        if (this.props.route.params.fieldwork !== undefined) {
          object.request.content.bo_data.fieldActivityEvents = this.props.route.params.fieldwork.fieldActivityEvents;
        }

        console.log(' Create Receipt Payout Request :::: ');
        console.log(object);
        this.setState({isLoading: true}, () => {
          new Controller()
            .playPostApi(
              this.state.session.loginToken,
              this.state.session.publicIp +
                this.state.UIJson.mobileStaticTypes.url,
              object,
            )
            .then(response => {
              this.setState({isLoading: false});
              if (response.status === 200) {
                Toast.show('Request sent successfully!', Toast.SHORT);
                this.props.navigation.goBack();
                // if (object.request.content.bo_data.changeInvoices.length) {
                //   object.request.content.bo_data.changeInvoices.forEach(trans => {
                //     transIds.push(trans.transId);
                //   });
                //   this.refreshARAPlist(transIds);
                // }
              } else {
                Toast.show('Problem occurred!', Toast.SHORT);
              }
            });
        });
      },
      {
        conditions: [
          {
            column: 'tenant_id',
            value: [...this.state.session.info.applicableTenants],
            operator: 'in',
          },
          {
            column: 'customer_id',
            value: this.state.session.info.tenantCustomerId,
            operator: '=',
          },
        ],
      },
    );
  }
}

class PendingOrderList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      finance: null,
      index: 0,
      pendingAmount: '',
      session: {},
      inputValue: 0,
      ...props,
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({...nextProps});
  }

  pendingAmount() {
    this.props.pendingAmount(this.state);
  }

  render() {
    return (
      <Card style={{padding: 5}}>
        <Text style={{fontWeight: '600'}}>
          {this.state.finance.trans_request_number} (
          {this.state.finance.trans_number})
        </Text>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            marginBottom: 3,
            padding: 5,
            marginTop: 2,
          }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              alignItems: 'flex-start',
            }}>
            <Text style={{textAlign: 'center'}}>{'Total Amount'}</Text>
            <Text
              style={{
                fontSize: 14,
                color: 'black',
                textAlign: 'center',
                marginTop: 3,
              }}>
              <Icon
                name={this.state.session.info[AppConstants.keyCurrency]}
                type="FontAwesome"
                style={[styles.currency, {fontSize: 15}]}
              />
              {' ' +
                NumberFormat(
                  RemoveSeparator(
                    this.state.finance.amount,
                    this.state.session.info.exponent,
                  ),
                  this.state.session.info,
                )}
            </Text>
          </View>
          <View style={{flex: 1, alignItems: 'flex-start'}}>
            <Text style={{textAlign: 'center'}}>{'Pending'}</Text>
            <Text
              style={{
                fontSize: 14,
                color: 'red',
                textAlign: 'center',
                marginTop: 3,
              }}>
              <Icon
                name={this.state.session.info[AppConstants.keyCurrency]}
                type="FontAwesome"
                style={[styles.currency, {fontSize: 15}]}
              />
              {' ' +
                NumberFormat(
                  RemoveSeparator(
                    this.state.pendingAmount,
                    this.state.session.info.exponent,
                  ),
                  this.state.session.info,
                )}
            </Text>
          </View>
          <View style={{flex: 1, alignItems: 'flex-start'}}>
            <Text style={{textAlign: 'center'}}>{'Amount'}</Text>
            {/*<Text style={{ marginTop: 2, color: 'black', textAlign: 'center' }}> */}
            <Input
              keyboardType="numeric"
              autoFocus={true}
              value={this.state.inputValue.toString()}
              style={{
                borderRadius: 3,
                marginTop: 3,
                backgroundColor: 'white',
                height: 30,
                width: 150,
                borderColor: 'orange',
                borderWidth: 0.8,
                fontSize: 15,
                padding: 1,
              }}
              onChangeText={text => {
                const changeAmount =
                  this.state.finance.amount -
                  this.state.finance.adjust_amount -
                  AddSeparator(Number(text), this.state.session.info.exponent);
                if (changeAmount >= 0) {
                  this.setState(
                    {
                      pendingAmount: changeAmount,
                      inputValue: text,
                    },
                    () => {
                      this.props.onChangePendingAmountValue(
                        text,
                        this.state.index,
                      );
                    },
                  );
                }
              }}
            />
            {/* </Text> */}
          </View>
        </View>
      </Card>
    );
  }
}

class PendingInvoiceTotalAmount extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      session: null,
      totalAmount: 0,
      ...props,
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({...nextProps});
  }

  render() {
    const amount = this.state.totalAmount.toFixed(2);
    return (
      <Text
        style={{
          fontSize: 14,
          color: 'red',
          textAlign: 'center',
          marginTop: 3,
        }}>
        <Icon
          name={this.state.session.info[AppConstants.keyCurrency]}
          type="FontAwesome"
          style={[
            styles.currency,
            {fontWeight: '300', fontSize: 15, color: 'red'},
          ]}
        />
        {' ' + amount}
      </Text>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    flex: 1,
  },
  cardItem: {
    flex: 1,
    flexDirection: 'column',
    paddingTop: 5,
    paddingBottom: 5,
  },
  headerItem: {
    flex: 1,
    flexDirection: 'row',
  },
  title: {
    fontSize: 14,
    color: Colors.headerBackground,
    padding: 5,
  },
  inputType: {
    fontSize: 16,
    color: 'black',
    padding: 5,
  },
  inputTypeItem: {
    margin: 5,
    padding: 3,
    backgroundColor: '#f4f5f7',
    borderRadius: 3,
    flex: 9,
    height: 35,
  },
  inputContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  clearIconItem: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  clearIcon: {
    padding: 2,
    color: 'lightgrey',
  },
  errorMessage: {
    fontSize: 12,
    color: 'red',
    padding: 5,
  },
});
