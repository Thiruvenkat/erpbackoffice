import React, { Component } from 'react';
import { View, Text, StyleSheet, ActivityIndicator, StatusBar,TouchableOpacity } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import Colors from '../../style/Colors';
import { Root, Container, Content } from 'native-base';
import AppConstants from '../../constant/AppConstants';
import { Styles } from '../../style/Styles';

// create a component
class CreateFollowup extends React.Component {
    static navigationOptions = {
        headerTitle: 'Create Follow up',
        headerTintColor: Colors.headerTitleColor,
        headerStyle: {
            backgroundColor: Colors.headerBackground,
        },
        headerTitleStyle: {
            fontWeight: 'bold',
            color: Colors.headerTitleColor,
        },
    };

    constructor(props) {
        super(props);
        this.state = {
            isPreparing: false,
        }
    }

    componentDidMount() {
        NetInfo.fetch().then(isConnected => {
            this.setState({ isConnected: isConnected });
            if (isConnected) {
                AsyncStorage.getItem(AppConstants.keyLoginInfo).then(loginInfo => {
                    const data = JSON.parse(loginInfo);
                    this.setState({
                        loginToken: data[AppConstants.keyLoginToken],
                        publicIp: data[AppConstants.keyPublicIp],
                        isPreparing: false
                    }, () => this._getServiceOrder());
                });
            }
        });
    }

    _getServiceOrder(){
        
    }

    render() {
        if (this.state.isPreparing) {
            return (
                <View style={[styles.loadingContainer, styles.horizontal]}>
                    <ActivityIndicator size="small" color={Colors.headerBackground} />
                </View>
            )
        } else {
            const { navigation } = this.props;
            return (
                <Root>
                    <Container>
                        <Content padder style={{ backgroundColor: '#f4f6f9' }}>
                            <StatusBar
                                backgroundColor={Styles.statusBar.backgroundColor}
                                barStyle="light-content" />
                            <View style={{ flexDirection: "row", padding: 6, marginTop: 10 }}>
                                <Text style={[styles.leftContainer, { marginTop: 3, fontWeight: 'bold' }]}>
                                    Next Followup Date
                                    </Text>
                                <TouchableOpacity
                                    style={[styles.rightContainer]}
                                    onPress={() =>
                                        this.setState({
                                            isDateTimePickerVisible: true,
                                        })
                                    }
                                >
                                    <Text
                                        style={[{
                                            borderRadius: 3,
                                            padding: 5,
                                            fontSize: 14,
                                            fontWeight: 'bold',
                                            backgroundColor: "lightgrey",
                                        }]}
                                    >
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </Content>
                    </Container>
                </Root>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

//make this component available to the app
export default CreateFollowup;
