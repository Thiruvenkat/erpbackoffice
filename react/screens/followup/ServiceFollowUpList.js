/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  StyleSheet,
  ActivityIndicator,
  FlatList,
  StatusBar,
  TouchableOpacity,
  TouchableNativeFeedback,
} from 'react-native';
import {Root, Card, CardItem, Left, Body, Text, Icon, Right} from 'native-base';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Toast from 'react-native-simple-toast';

import MasterConstants from '../../constant/MasterConstants';
import Route from '../../router/Route';
import {GetAllFollowup} from './dbHelper/FollowUpDB';
import {
  DateFormat,
  DATE_TIME_LAST_UPDATED,
  DATE_TIME_UI,
  DATE_UI,
} from 'react-native-erp-mobile-library/react/common/Common';
import {getAllFollowUpDetails} from '../../controller/SyncDataController';
import Loader from 'react-native-erp-mobile-library/react/component/Loader';
import PreparingProgress from 'react-native-erp-mobile-library/react/component/PreparingProgress';
import {messages} from 'react-native-erp-mobile-library/react/i18n/i18n';
import {sessionInfo} from 'react-native-erp-mobile-library';
import NetInfo from '@react-native-community/netinfo';
import Colors from 'react-native-erp-mobile-library/react/style/Colors';
import {
  Styles,
  IconColor,
  STATUS_COLOR,
} from 'react-native-erp-mobile-library/react/style/Styles';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import {renderHeaderView} from 'react-native-erp-mobile-library/react/util/HeaderUtil';

export default class ServiceFollowUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      followupList: [],
      isPreparing: true,
      isLoading: true,
      isRefresh: false,
      loadMore: false,
      fetchAll: false,
      isConnected: true,
      filterDate: DateFormat(new Date(), DATE_TIME_UI),
      isDateTimePickerVisible: false,
      offset: 0,
      limit: 10,
      session: sessionInfo({type: 'get', key: 'loginInfo'}),
      conditions: [
        {
          value: MasterConstants.ServiceTypeFollowUpId,
          column: 'follow_up_type_id',
          operator: '=',
          // dataType : "String"
        },
      ],
    };
  }

  componentDidMount() {
    this._initHeader();
    this._getAllFollowUp(datas => {
      if (datas.length > 0) {
        this.setState({
          followupList:
            this.state.offset === 0
              ? datas
              : this.state.followupList.concat(datas),
          isLoading: false,
          loadMore: false,
          isPreparing: false,
          offset: this.state.offset + datas.length,
        });
      } else {
        this._getAllFollowUpFromServer(null);
      }
    });
  }

  _initHeader() {
    this.props.navigation.setParams({
      headerTitle:
        this.props.route.params?.headerTitle ?? messages('service/followUp'),
      headerType:
        this.props.route.params?.headerType ?? HeaderTypes.HeaderHomeMenu,
    });
    this.props.navigation.setOptions(renderHeaderView(this));
  }

  _getAllFollowUp(callback) {
    var filter = {
      limit: this.state.limit,
      offset: this.state.offset,
      orderBy: 'last_updated desc',
      conditions: [...this.state.conditions],
    };
    if (!this.state.fetchAll) {
      filter.conditions.push({
        value: this.state.filterDate + 'T00:00:00.000',
        column: 'next_follow_up_date',
        operator: '>=',
        dataType: 'String',
      });
      filter.conditions.push({
        value: this.state.filterDate + 'T23:59:59.999',
        column: 'next_follow_up_date',
        operator: '<=',
        dataType: 'String',
      });
    }
    filter.conditions.push({
      value: this.state.session.info.userId,
      column: 'followUpUserId',
      operator: '=',
      dataType: 'String',
    });
    GetAllFollowup(datas => {
      callback(datas);
    }, filter);
  }

  _getAllFollowUpFromServer() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        console.log('calling service..');
        let syncValue = {
          limit: this.state.limit,
          offset: this.state.offset,
          conditions: [
            {
              value: MasterConstants.ServiceTypeFollowUpId,
              column: 'followUpType.followUpTypeId',
              operator: 'in',
              dataType: 'String',
            },
            {
              value: this.state.session.info.userId,
              column: 'followUpUserId',
              operator: '=',
              dataType: 'String',
            },
          ],
        };
        if (!this.state.fetchAll) {
          syncValue.conditions.push({
            value: '' + new Date(this.state.filterDate + 'T00:00:00').getTime(),
            column: 'nextFollowUpDate',
            operator: '>=',
            dataType: 'Date',
          });
          syncValue.conditions.push({
            value: '' + new Date(this.state.filterDate + 'T23:59:59').getTime(),
            column: 'nextFollowUpDate',
            operator: '<=',
            dataType: 'Date',
          });
        }
        getAllFollowUpDetails(this.state.session, syncValue, isSuccess => {
          if (isSuccess) {
            this._getAllFollowUp(datas => {
              if (datas.length > 0) {
                this.setState({
                  followupList:
                    this.state.offset === 0
                      ? datas
                      : this.state.followupList.concat(datas),
                  isLoading: false,
                  loadMore: false,
                  isPreparing: false,
                  offset: this.state.offset + datas.length,
                });
              }
            });
          } else {
            this.setState({
              isLoading: false,
              loadMore: false,
              isPreparing: false,
              isRefresh: false,
            });
          }
        });
      } else {
        this.setState({
          isLoading: false,
          loadMore: false,
          isPreparing: false,
          isRefresh: false,
        });
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    });
  }

  onRefresh() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        this.setState({followupList: [], offset: 0}, () => {
          this._getAllFollowUpFromServer(null);
        });
      } else {
        this.state.offset = 0;
        this._getAllFollowUp(datas => {
          if (datas.length > 0) {
            this.setState(
              {followupList: datas, offset: this.state.offset + datas.length},
              () => {
                this.setState({
                  isLoading: false,
                  loadMore: false,
                  isPreparing: false,
                  isRefresh: false,
                });
              },
            );
          }
        });
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    });
  }

  _handleDatePicked = date => {
    this.setState(
      {
        followupList: [],
        offset: 0,
        fetchAll: false,
        isLoading: true,
        filterDate: DateFormat(date, DATE_TIME_UI),
      },
      () => {
        this._getAllFollowUp(datas => {
          if (datas.length > 0) {
            this.setState({
              followupList: datas,
              isLoading: false,
              isPreparing: false,
              loadMore: false,
              offset: this.state.offset + datas.length,
            });
          } else {
            this._getAllFollowUpFromServer(null);
          }
        });
        this._hideDateTimePicker();
      },
    );
  };

  _hideDateTimePicker = () => {
    this._getAllFollowUpFromServer();
    this.setState({
      isDateTimePickerVisible: false,
    });
  };

  renderItem({item, index}) {
    return (
      <ItemView
        navigation={this.props.navigation}
        itemDetails={item}
        index={index}
        session={this.state.session}
      />
    );
  }

  listEmptyView = () => {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
          marginTop: 200,
          backgroundColor: 'white',
        }}>
        <Icon
          name="info-circle"
          type="FontAwesome"
          style={{color: Colors.headerBackground, fontSize: 40}}
        />
        <Text
          style={{
            textAlign: 'center',
            color: 'darkgrey',
            justifyContent: 'center',
          }}>
          No Follow-up Found
        </Text>
      </View>
    );
  };

  render() {
    if (this.state.isPreparing) {
      return <PreparingProgress />;
    } else {
      return (
        <Root>
          <View
            padder
            style={{
              backgroundColor: Styles.appViewBackground.backgroundColor,
              ...StyleSheet.absoluteFillObject,
            }}>
            <StatusBar
              backgroundColor={Styles.statusBar.backgroundColor}
              barStyle="light-content"
            />
            <Loader loading={this.state.isLoading} />
            <DateTimePicker
              date={new Date(this.state.filterDate)}
              isVisible={this.state.isDateTimePickerVisible}
              mode="date"
              onConfirm={this._handleDatePicked}
              onCancel={this._hideDateTimePicker}
            />
            <Card>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                }}>
                <TouchableNativeFeedback
                  onPress={() => {
                    this.setState(
                      {
                        followupList: [],
                        offset: 0,
                        fetchAll: true,
                        isLoading: true,
                        filterDate: DateFormat(new Date(), DATE_TIME_UI),
                      },
                      () => {
                        this._getAllFollowUp(datas => {
                          if (datas.length > 0) {
                            this.setState({
                              followupList: datas,
                              isLoading: false,
                              isPreparing: false,
                              loadMore: false,
                              offset: this.state.offset + datas.length,
                            });
                          } else {
                            this._getAllFollowUpFromServer(null);
                          }
                        });
                      },
                    );
                  }}>
                  <Text
                    style={{
                      margin: 10,
                      padding: 5,
                      paddingLeft: 20,
                      paddingRight: 20,
                      color: '#fff',
                      backgroundColor: Colors.headerBackground,
                      fontSize: 18,
                      fontWeight: 'bold',
                    }}>
                    All
                  </Text>
                </TouchableNativeFeedback>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    padding: 10,
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      var date = new Date(this.state.filterDate);
                      date.setDate(date.getDate() - 1);
                      this.setState(
                        {
                          followupList: [],
                          offset: 0,
                          fetchAll: false,
                          isLoading: true,
                          filterDate: DateFormat(date, DATE_TIME_UI),
                        },
                        () => {
                          this._getAllFollowUp(datas => {
                            if (datas.length > 0) {
                              this.setState({
                                followupList: datas,
                                isLoading: false,
                                isPreparing: false,
                                loadMore: false,
                                offset: this.state.offset + datas.length,
                              });
                            } else {
                              this._getAllFollowUpFromServer(null);
                            }
                          });
                        },
                      );
                    }}>
                    <Icon
                      name="chevron-circle-left"
                      type="FontAwesome"
                      style={{fontSize: 27, color: Colors.headerBackground}}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      marginRight: 10,
                      marginLeft: 10,
                      fontSize: 16,
                      marginTop: 5,
                    }}
                    onPress={() =>
                      this.setState({
                        isDateTimePickerVisible: true,
                      })
                    }>
                    <Text>{DateFormat(this.state.filterDate, DATE_UI)}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      var date = new Date(this.state.filterDate);
                      date.setDate(date.getDate() + 1);
                      this.setState(
                        {
                          followupList: [],
                          offset: 0,
                          fetchAll: false,
                          isLoading: true,
                          filterDate: DateFormat(date, DATE_TIME_UI),
                        },
                        () => {
                          this._getAllFollowUp(datas => {
                            if (datas.length > 0) {
                              this.setState({
                                followupList: datas,
                                isLoading: false,
                                isPreparing: false,
                                loadMore: false,
                                offset: this.state.offset + datas.length,
                              });
                            } else {
                              this._getAllFollowUpFromServer(null);
                            }
                          });
                        },
                      );
                    }}>
                    <Icon
                      name="chevron-circle-right"
                      type="FontAwesome"
                      style={{
                        fontSize: 27,
                        color: Colors.headerBackground,
                        marginRight: 40,
                      }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => {}} />
                </View>
              </View>
            </Card>
            <FlatList
              extraData={this.state}
              data={this.state.followupList}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              renderItem={item => this.renderItem(item)}
              onEndReachedThreshold={0.1}
              onEndReached={({distanceFromEnd}) => {
                if (
                  !this.state.loadMore &&
                  this.state.offset >= this.state.limit
                ) {
                  this.setState({loadMore: true}, () => {
                    this._getAllFollowUp(datas => {
                      if (datas.length > 0) {
                        this.setState({
                          followupList:
                            this.state.offset === 0
                              ? datas
                              : this.state.followupList.concat(datas),
                          isLoading: false,
                          isPreparing: false,
                          loadMore: false,
                          offset: this.state.offset + datas.length,
                        });
                      } else {
                        this._getAllFollowUpFromServer(null);
                      }
                    });
                  });
                }
              }}
              keyExtractor={(item, index) => index.toString()}
              onRefresh={() => this.onRefresh()}
              refreshing={this.state.isRefresh}
              ListEmptyComponent={this.listEmptyView}
              ListFooterComponent={
                this.state.loadMore ? (
                  <View
                    style={{
                      padding: 7,
                      flex: 1,
                      flexDirection: 'row',
                      borderRadius: 2,
                      backgroundColor: Colors.headerBackground,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <ActivityIndicator size="small" color="white" />
                    <Text style={{color: 'white', fontWeight: 'bold'}}>
                      {' Loading..'}
                    </Text>
                  </View>
                ) : null
              }
            />
          </View>
        </Root>
      );
    }
  }
}
class ItemView extends React.Component {
  constructor(props) {
    super(props);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({...nextProps});
  }

  render() {
    const {navigation, itemDetails, index} = this.props;
    return (
      <Card style={styles.card}>
        <TouchableOpacity
          onPress={() => {
            if (
              this.state.itemDetails.status_id ===
              MasterConstants.followUpInProgress
            ) {
              navigation.navigate(Route.ViewFollowUp, {
                followup: JSON.parse(this.state.itemDetails.json),
                followupObjectId: itemDetails.object_id,
                scrnType: 'serviceFollowUp',
              });
            }
          }}>
          <CardItem>
            <Left>
              <View
                style={{
                  width: 70,
                  height: 70,
                  borderRadius: 70 / 2,
                  backgroundColor: IconColor[index % IconColor.length],
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={[styles.text, {color: 'white'}]}>
                  {itemDetails.object_ref_no.substring(0, 2).toUpperCase()}{' '}
                </Text>
              </View>
              <Body>
                <Text style={styles.createDate}>
                  <Icon
                    type="FontAwesome"
                    name="history"
                    style={{fontSize: 15}}
                  />{' '}
                  {DateFormat(itemDetails.last_updated, DATE_TIME_LAST_UPDATED)}
                </Text>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.text}> Order No : </Text>
                  <Text> {itemDetails.object_ref_no}</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.text}> Follow-up Count : </Text>
                  <Text> {JSON.parse(itemDetails.json).followUpCount}</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.text}> Next Follow-up : </Text>
                  <Text>
                    {' '}
                    {DateFormat(
                      JSON.parse(itemDetails.json).nextFollowUpDate,
                      DATE_TIME_LAST_UPDATED,
                    )}
                  </Text>
                </View>
              </Body>
            </Left>
          </CardItem>
        </TouchableOpacity>
        <CardItem>
          <Left />
          <Right>
            <View
              style={[
                styles.statusContainer,
                {
                  backgroundColor:
                    STATUS_COLOR[this.state.itemDetails.status_id],
                },
              ]}>
              <Text note style={styles.statusText}>
                {' '}
                {this.state.itemDetails.status_name}
              </Text>
            </View>
          </Right>
        </CardItem>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  card: {
    borderRadius: 3,
    elevation: 10,
  },
  icon: {
    fontSize: 25,
    color: 'black',
    paddingLeft: 10,
    paddingRight: 10,
  },
  fabicon: {
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'white',
    fontSize: 22,
  },
  createDate: {
    fontSize: 13,
    textAlign: 'right',
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  statusContainer: {
    paddingBottom: 2,
    paddingLeft: 3,
    paddingRight: 3,
    borderRadius: 3,
  },
  statusText: {
    fontWeight: 'bold',
    color: 'white',
  },
});
