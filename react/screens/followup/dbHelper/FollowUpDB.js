import { openDatabase } from "react-native-erp-mobile-library/react/database/DatabaseHandler";
import { TABLE } from 'react-native-erp-mobile-library/react/database/DBConstants';
import { QueryBuilder } from "react-native-erp-mobile-library/react/database/DBUtilities";
import { DateFormat, DATE_TIME_SERVER } from "react-native-erp-mobile-library/react/common/Common";
import AppConstants from "../../../constant/AppConstants";

export function GetAllFollowup(callback, filter) {
    let DBInstance = openDatabase(AppConstants.affordeBackOfficeDB);
    DBInstance.transaction((db) => {
        var query = QueryBuilder('SELECT follow_up_id,next_follow_up_date,follow_up_type_id,status_id,status_name,follow_up_count,last_updated, rating,reason_id,reason_name,object_ref_no, json, tenant_id, followUpUserId, object_id FROM ' + TABLE.Followup, filter);
        db.executeSql(query, [],
            (tx, results) => {
                console.log('Select Followup Data Length :' + results.rows.length);
                var advertisement = [];
                for (let index = 0; index < results.rows.length; index++) {
                    const row = results.rows.item(index);
                    let ad = {
                        followup_id: row.follow_up_id,
                        object_ref_no: row.object_ref_no,
                        followup_count: row.followup_count,
                        next_followup_date: row.next_followup_date,
                        status_id: row.status_id,
                        status_name: row.status_name,
                        last_updated: row.last_updated,
                        rating: row.rating,
                        reason_id: row.reason_id,
                        reason_name: row.reason_name,
                        tenant_id: row.tenant_id,
                        json: row.json,
                        followUpUserId: row.followUpUserId,
                        object_id: row.object_id
                    };
                    advertisement[index] = ad;
                }
                console.log('Calling call back :' + advertisement);
                callback(advertisement)
            },
            (error) => {
                console.log('Select Followup Error occured :' + JSON.stringify(error));
                callback([])
            })
    })
}
export function updateFollowup(followUpId, nexFollowUpDate, statusId, statusName) {
    let DBInstance = openDatabase(AppConstants.affordeBackOfficeDB);
    DBInstance.transaction((db) => {
        db.executeSql("UPDATE " + TABLE.Followup + " SET next_follow_up_date = " + nexFollowUpDate + ", status_id = " + statusId + ", status_name= " + statusName + " WHERE follow_up_id = " + followUpId)
    },
        () => {
            // update success    
            console.log('Update Followup Successfully');
        },
        (error) => {
            // update failure  
            console.log('Update Followup Error occured :' + JSON.stringify(error));
        });
}

export function InsertFollowup(datas, callback) {
    if (datas.length > 0) {
        let DBInstance = openDatabase(AppConstants.affordeBackOfficeDB);
        DBInstance.transaction((db) => {
            let length = datas.length;
            for (let index = 0; index < length; index++) {
                let values = datas[index];
                db.executeSql("INSERT OR REPLACE INTO " + TABLE.Followup + " (" +
                    "follow_up_id,next_follow_up_date,follow_up_type_id,status_id,status_name,follow_up_count,last_updated, rating,reason_id,reason_name,object_ref_no, json, tenant_id,followUpUserId, object_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [
                        values.followup_id,
                        values.next_followup_date,
                        values.followup_type_id,
                        values.status_id,
                        values.status_name,
                        values.followup_count,
                        values.last_updated,
                        values.rating,
                        values.reason_id,
                        values.reason_name,
                        values.object_ref_no,
                        (values.json).replace("'", '`'),
                        values.tenant_id,
                        values.followUpUserId,
                        values.object_id
                    ],
                    () => {
                        // insert success                        
                        console.log('Insert Followup Successfully :' + values.followup_type_id);
                    },
                    (error) => {
                        // insert failure                        
                        console.log('Insert Followup Error occured :' + JSON.stringify(error));
                    })
            }
            callback(true)
        });
    }
}
