/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-alert */
/* eslint-disable react/no-did-mount-set-state */
import React from 'react';
import {
  View,
  Text,
  Alert,
  StyleSheet,
  ActivityIndicator,
  Linking,
  StatusBar,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {
  Root,
  Content,
  Container,
  Card,
  Footer,
  Picker,
  Textarea,
  Icon,
  Button,
} from 'native-base';
import AppConstants from '../../constant/AppConstants';
import {Styles} from '../../style/Styles';
import Colors from '../../style/Colors';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Loader from 'react-native-erp-mobile-library/react/component/Loader';
import Toast from 'react-native-simple-toast';
import {messages} from 'react-native-erp-mobile-library/react/i18n/i18n';
import MasterConstants from '../../constant/MasterConstants';
import {
  DateFormat,
  DATE_TIME_LAST_UPDATED,
  DATE_TIME_SERVER,
} from 'react-native-erp-mobile-library/react/common/Common';
import Controller from 'react-native-erp-mobile-library/react/controller/Controller';
import {sessionInfo} from 'react-native-erp-mobile-library';
import {updateFollowup} from './dbHelper/FollowUpDB';
import {GetAllOrders} from 'react-native-erp-mobile-library/react/screens/orders/dbHelper/OrderDBHelper';
import {getOrder} from 'react-native-erp-mobile-library/react/controller/SyncDataController';
import {renderHeaderView} from 'react-native-erp-mobile-library/react/util/HeaderUtil';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import Route from '../../router/Route';
import {
  GetAllOrderService,
  InsertOrderService,
} from 'react-native-erp-mobile-library/react/screens/services/dbHelper/ServiceOrderDB';

const followUpStatusList = {
  104102: {statusId: '104102', statusName: 'In Progress'},
  104201: {statusId: '104201', statusName: 'Accepted'},
  104127: {statusId: '104127', statusName: 'Rejected'},
};

const serviceFollowUpStatusList = {
  104243: {statusId: '104243', statusName: 'FollowUp InProgress'},
  104222: {statusId: '104222', statusName: 'Service Completed'},
};

// create a component
class ViewFollowUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isPreparing: true,
      isLoading: false,
      followUp: this.props.route.params?.followup,
      reasonList: [],
      session: sessionInfo({type: 'get', key: 'loginInfo'}),
      isDateTimePickerVisible: false,
      followUpDate: DateFormat(new Date(), DATE_TIME_LAST_UPDATED),
      oFollowUpDate: DateFormat(new Date(), DATE_TIME_SERVER),
      followupObjectId: this.props.route.params.followupObjectId,
      followUpCustomerData: null,
      scrnType: this.props.route.params?.scrnType ?? 'orderFollowUp',
      orderServiceData: null,
      followUpComments: '',
      followUpStatus: '',
      followUpReason: '',
    };
  }

  componentDidMount() {
    this._initHeader();
    this._getAllReasons();
    if (this.isScrnTypeOrderFollowUp()) {
      this._getAllOrderDetails();
    } else if (this.isScrnTypeServiceFollowUp()) {
      this._getAllOrderService();
    } else {
      this._getAllOrderDetails();
    }
    this.setState(
      {
        followUpReason:
          this.state.followUp.reason != null
            ? this.state.followUp.reason.id
            : null,
        followUpStatus: this.state.followUp.status.id,
        followUpComments: '',
      },
      () => {},
    );
  }

  isScrnTypeServiceFollowUp() {
    return this.state.scrnType === 'serviceFollowUp';
  }

  isScrnTypeOrderFollowUp() {
    return this.state.scrnType === 'orderFollowUp';
  }

  _initHeader() {
    this.props.navigation.setParams({
      headerTitle: this.props.route.headerTitle ?? 'View Follow-up',
      headerType: this.props.route.params?.headerType ?? HeaderTypes.HeaderBack,
    });
    this.props.navigation.setOptions(renderHeaderView(this));
  }

  _getAllOrderService() {
    // this.setState({ isPreparing : true})
    var filter = {
      limit: this.state.limit,
      offset: this.state.offset,
      orderBy: 'last_updated desc',
      conditions: [
        {
          value: this.state.followupObjectId,
          column: 'order_service_id',
          operator: '=',
          dataType: 'String',
        },
      ],
    };
    GetAllOrderService(
      AppConstants.affordeBackOfficeDB,
      datas => {
        if (datas.length > 0) {
          this.setState({orderServiceData: JSON.parse(datas[0].json)}, () => {
            this._getAllOrderDetails(this.state.orderServiceData.order.id);
          });
        } else {
          this.getServiceDetail();
        }
      },
      filter,
    );
  }

  _getAllOrderDetails(orderId) {
    // this.setState({ isPreparing: true });
    const order_id =
      orderId === undefined || orderId == null
        ? this.state.followupObjectId
        : orderId;
    var filter = {
      limit: this.state.limit,
      offset: this.state.offset,
      orderBy: 'last_updated desc',
      conditions: [
        {
          value: order_id,
          column: 'order_id',
          operator: '=',
          dataType: 'String',
        },
      ],
    };
    if (
      this.state.session.loggedInUserType ===
      AppConstants.loggedInUserTypeRetailer
    ) {
      filter.conditions.push({
        value: this.state.session.info.loggedInCustomerId,
        column: 'order_to_id',
        operator: '=',
        dataType: 'String',
      });
    }
    GetAllOrders(
      this.state.session.databaseName,
      datas => {
        if (datas.length > 0) {
          this.setState(
            {followUpCustomerData: JSON.parse(datas[0].json)},
            () => {
              const orderContacts = this.state.followUpCustomerData
                .orderContact[0];
              this.setState({followUpCustomerData: orderContacts}, () => {
                this.setState({isPreparing: false});
              });
            },
          );
        } else {
          this._getOrder(order_id);
        }
      },
      filter,
    );
  }

  _getOrder(orderId) {
    // this.setState({ isPreparing: true, });
    getOrder(this.state.session, orderId)
      .then(() => {
        // this.setState({ isPreparing: false })
        this._getAllOrderDetails(orderId);
      })
      .catch(() => {
        this.setState({isPreparing: false});
      });
  }

  getServiceDetail() {
    // this.setState({ isPreparing: true, });
    new Controller()
      .playGetApi(
        this.state.session.loginToken,
        this.state.session.publicIp +
          AppConstants.getOrderService +
          this.state.followupObjectId,
      )
      .then(response => {
        // this.setState({ isPreparing: false })
        if (response.status === 200) {
          let datas = [];
          const result = JSON.parse(response._bodyText).response.content.data;
          let ad = {
            order_service_id: result.orderServiceId,
            order_to_id: result.order.text4,
            order_to_name: result.order.text5,
            order_code: result.order.code,
            service_date: result.serviceDate,
            status_id: result.status.id,
            status_name: result.status.name,
            service_type_id: result.serviceType.id,
            service_type_name: result.serviceType.name,
            classification_type_id: result.serviceClassificationType.id,
            classification_type_name: result.serviceClassificationType.name,
            tenantId: result.tenant.id,
            orderId: result.order.id,
            productId: JSON.parse(result.jsonData).productId,
            last_updated: result.lastUpdated,
            json: JSON.stringify(result),
          };
          datas.push(ad);
          InsertOrderService(
            this.state.session.databaseName,
            datas,
            callback => {
              if (callback) {
                this._getAllOrderService();
              }
            },
          );
        } else {
          Toast.show('Problem occurred!', Toast.SHORT);
        }
      });
  }

  _handleDatePicked = date => {
    this.setState({
      followUpDate: DateFormat(date, DATE_TIME_LAST_UPDATED),
      oFollowUpDate: DateFormat(date, DATE_TIME_SERVER),
      isDateTimePickerVisible: false,
    });
  };

  _hideDateTimePicker = () => {
    this.setState({
      isDateTimePickerVisible: false,
    });
  };

  _onPressMobileNumber(url) {
    const URL = `tel:${url}`;
    Linking.canOpenURL(URL)
      .then(supported => {
        if (!supported) {
          alert(
            'This number not available or incorrect, Please check the number!',
          );
          console.log("Can't handle url: " + URL);
        } else {
          return this.isScrnTypeOrderFollowUp() ? Linking.openURL(URL) : null;
        }
      })
      .catch(err => console.error('An error occurred', err));
  }

  _formValidation() {
    Alert.alert(
      'Confirmation',
      'Are you sure want to submit ?',
      [
        {
          text: 'No',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Yes', onPress: () => this._createFollowupRequest()},
      ],
      {cancelable: false},
    );
  }

  _getAllReasons() {
    this.setState({isPreparing: true});
    return new Controller()
      .playPostApi(
        this.state.session.loginToken,
        this.state.session.publicIp + AppConstants.getAllReasons,
        {
          filters: {
            limit: 50,
            conditions: [
              {
                column: 'reasonGroup.reasonGroupId',
                value: MasterConstants.reasonGroupFollowUp,
                operator: '=',
              },
            ],
          },
        },
      )
      .then(response => {
        // this.setState({ isPreparing: false });
        if (response.status === 200) {
          const data = JSON.parse(response._bodyText).response.content.dataList;
          if (data.length > 0) {
            this.setState({reasonList: data, followUpReason: data[0].reasonId});
          } else {
            Toast.show('Unable to get Reasons List', Toast.LONG);
          }
        } else {
          Toast.show('Problem occurred!', Toast.SHORT);
        }
      });
  }

  _updateFollowUp() {
    this.setState({isLoading: true});
    var request = {
      request: {
        envelope: {
          sourceApplication: 'Afforde UI',
          requestDate: DateFormat(new Date(), DATE_TIME_SERVER),
        },
        content: {
          bo_data: {
            type: 'updateserviceReqFollowup_BOO',
            followupId: this.state.followUp.followUpId,
            reasonId: this.state.followUpReason,
            statusId: this.state.followUpStatus,
            followUpComments: this.state.followUpComments,
            nextFollowupDate: this.state.oFollowUpDate,
          },
        },
      },
    };
    new Controller()
      .playPostApi(
        this.state.session.loginToken,
        this.state.session.publicIp + AppConstants.updateServiceReqFollowup,
        request,
      )
      .then(response => {
        this.setState({isLoading: false});
        if (response.status === 200) {
          Toast.show('Sent successfully!', Toast.SHORT);
          updateFollowup(
            this.state.followUp.followUpId,
            this.state.oFollowUpDate,
            this.state.followUpStatus,
            this.isScrnTypeServiceFollowUp()
              ? serviceFollowUpStatusList[this.state.followUpStatus].statusName
              : followUpStatusList[this.state.followUpStatus].statusName,
          );
          this.props.navigation.goBack();
        } else {
          Toast.show('Problem occurred!', Toast.SHORT);
        }
      });
  }

  _createFollowupRequest() {
    this.setState({isLoading: true});
    var request = {
      request: {
        envelope: {
          sourceApplication: 'Afforde UI',
          requestDate: DateFormat(new Date(), DATE_TIME_SERVER),
        },
        content: {
          bo_data: {
            type: 'createFollowUp_BOO',
            followUp: {
              optLock: this.state.followUp.optLock,
              objectRefNo: this.state.followUp.objectRefNo,
              tenant: {
                id: this.state.followUp.tenant.id,
              },
              followUpUserId: this.state.session.info.userId,
              followUpComments: this.state.followUpComments,
              nextFollowUpDate: this.state.oFollowUpDate,
              lastFollowUpDate: this.state.followUp.nextFollowUpDate,
              lastFollowUpComments:
                this.state.followUp.lastFollowUpComments != null
                  ? this.state.followUp.lastFollowUpComments
                  : '',
              followUpCount: Number(this.state.followUp.followUpCount),
              status: {
                id: this.state.followUpStatus,
              },
              reason: {
                id: this.state.followUpReason,
              },
              followUpId: this.state.followUp.followUpId,
              followUpType: {
                id: this.state.followUp.followUpType.id,
              },
              objectId: this.state.followUp.objectId,
            },
            createEditFlag: false,
            followUpStatus: JSON.stringify({
              '104201': {
                isUpdateStatus: 'true',
                tableName: 'orders',
                statusColumnName: 'order_status_id',
                statusId: '104201',
                tablePkColumn: 'order_id',
              },
              '104127': {
                isUpdateStatus: 'true',
                tableName: 'orders',
                statusColumnName: 'order_status_id',
                statusId: '104127',
                tablePkColumn: 'order_id',
              },
            }),
          },
        },
      },
    };
    if (this.state.followUp.lastFollowUpReason != null) {
      request.request.content.bo_data.followUp.lastFollowUpReason = this.state.followUp.lastFollowUpReason;
    }
    new Controller()
      .playPostApi(
        this.state.session.loginToken,
        this.state.session.publicIp + AppConstants.updateFollowup,
        request,
      )
      .then(response => {
        this.setState({isLoading: false});
        if (response.status === 200) {
          let statusName = this.isScrnTypeServiceFollowUp()
            ? serviceFollowUpStatusList[this.state.followUpStatus].statusName
            : followUpStatusList[this.state.followUpStatus].statusName;
          Toast.show('Sent successfully!', Toast.SHORT);
          updateFollowup(
            this.state.followUp.followUpId,
            this.state.oFollowUpDate,
            this.state.followUpStatus,
            statusName,
          );
          this.props.navigation.goBack();
        } else {
          Toast.show('Problem occurred!', Toast.SHORT);
        }
      });
  }

  render() {
    if (this.state.isPreparing) {
      return (
        <View
          style={{
            padding: 7,
            flex: 1,
            flexDirection: 'row',
            borderRadius: 2,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <ActivityIndicator size="small" color={Colors.headerBackground} />
          <Text style={{color: Colors.headerBackground, fontWeight: 'bold'}}>
            {' Loading..'}
          </Text>
        </View>
      );
    } else {
      return (
        <Root>
          <Container>
            <Content padder style={{backgroundColor: '#f4f6f9'}}>
              <StatusBar
                backgroundColor={Styles.statusBar.backgroundColor}
                barStyle="light-content"
              />
              <Loader loading={this.state.isLoading} />
              <DateTimePicker
                isVisible={this.state.isDateTimePickerVisible}
                mode="datetime"
                is24Hour={false}
                onConfirm={this._handleDatePicked}
                onCancel={this._hideDateTimePicker}
              />
              <Card style={{paddingBottom: 5, paddingTop: 5}}>
                <View style={{flexDirection: 'row', padding: 5, marginTop: 10}}>
                  <Text style={[styles.leftContainer]}>
                    {messages('ReferenceNo')}{' '}
                  </Text>
                  <TouchableOpacity
                    style={[styles.rightContainer]}
                    onPress={() => {
                      !this.isScrnTypeServiceFollowUp()
                        ? this.props.navigation.navigate(
                            Route.ViewOrderTabNavigator,
                            {
                              orderId: this.state.followupObjectId,
                              toScreen: Route.ViewOrderDetailTabNavigator,
                            },
                          )
                        : null;
                    }}>
                    <Text
                      style={[
                        {
                          padding: 5,
                          fontSize: 16,
                          color: Colors.headerBackground,
                          textDecorationLine: 'underline',
                        },
                      ]}>
                      {'' + this.state.followUp.objectRefNo}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row', padding: 5, marginTop: 10}}>
                  <Text style={[styles.leftContainer, {marginTop: 3}]}>
                    {messages('customerName')}
                  </Text>
                  <View style={[styles.rightContainer]}>
                    <Text style={[{color: 'black', padding: 5, fontSize: 16}]}>
                      {this.state.followUpCustomerData !== null &&
                      this.state.followUpCustomerData &&
                      this.state.followUpCustomerData.contactPerson !==
                        undefined
                        ? '' + this.state.followUpCustomerData.contactPerson
                        : ''}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 5, marginTop: 10}}>
                  <Text style={[styles.leftContainer, {marginTop: 3}]}>
                    {messages('contactNumber')}
                  </Text>
                  <TouchableOpacity
                    onPress={() => {
                      this._onPressMobileNumber(
                        this.state.followUpCustomerData.mobileNo,
                      );
                    }}
                    style={[styles.rightContainer]}>
                    <Text style={[{color: 'black', padding: 5, fontSize: 16}]}>
                      {this.state.followUpCustomerData !== null &&
                      this.state.followUpCustomerData &&
                      this.state.followUpCustomerData.mobileNo !== undefined
                        ? '' + this.state.followUpCustomerData.mobileNo
                        : ''}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row', padding: 5, marginTop: 10}}>
                  <Text style={[styles.leftContainer, {marginTop: 3}]}>
                    {messages('LastFollowUpDate')}
                  </Text>
                  <View style={[styles.rightContainer]}>
                    <Text
                      style={[
                        {
                          borderRadius: 3,
                          padding: 5,
                          fontSize: 14,
                          fontWeight: 'bold',
                          // backgroundColor: "lightgrey",
                        },
                      ]}>
                      {'' +
                        DateFormat(
                          this.state.followUp.nextFollowUpDate,
                          DATE_TIME_LAST_UPDATED,
                        )}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 5, marginTop: 10}}>
                  <Text style={[styles.leftContainer, {marginTop: 3}]}>
                    {messages('NextFollowUpDate')}
                  </Text>
                  <TouchableOpacity
                    style={[styles.rightContainer]}
                    onPress={() =>
                      !this.isScrnTypeServiceFollowUp()
                        ? this.setState({
                            isDateTimePickerVisible: true,
                          })
                        : null
                    }>
                    <Text
                      style={[
                        {
                          borderRadius: 3,
                          padding: 5,
                          fontSize: 14,
                          fontWeight: 'bold',
                          backgroundColor: 'lightgrey',
                        },
                      ]}>
                      {'' + this.state.followUpDate}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={{flexDirection: 'column', padding: 5, marginTop: 10}}>
                  <Text style={[styles.leftContainer]}>
                    {messages('FollowUpComments')}
                  </Text>
                  <View
                    style={{
                      borderRadius: 3,
                      paddingLeft: Dimensions.get('screen').width / 2 - 17,
                      marginTop: -13,
                    }}>
                    <Textarea
                      rowSpan={3}
                      editable={this.isScrnTypeOrderFollowUp()}
                      value={this.state.followUpComments}
                      style={[
                        styles.rightContainer,
                        {
                          justifyContent: 'flex-end',
                          paddingLeft: 20,
                          borderRadius: 3,
                          backgroundColor: '#f2f3f4',
                        },
                      ]}
                      onChangeText={text => {
                        this.setState({followUpComments: text});
                      }}
                    />
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 5, marginTop: 10}}>
                  <Text style={[styles.leftContainer]}>
                    {messages('FollowUpMode')}
                  </Text>
                  <View style={styles.rightContainer}>
                    {this.isScrnTypeOrderFollowUp() ? (
                      <Picker
                        mode="dropdown"
                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                        headerBackButtonText="Back"
                        selectedValue={this.state.followUpReason}
                        onValueChange={value => {
                          this.setState({followUpReason: value});
                        }}>
                        {this.state.reasonList.map((item, index) => {
                          return (
                            <Picker.Item
                              label={item.reasonName}
                              value={item.reasonId}
                            />
                          );
                        })}
                      </Picker>
                    ) : (
                      <Text>{this.state.followUp.reason.name}</Text>
                    )}
                  </View>
                </View>
                <View style={{flexDirection: 'row', padding: 5, marginTop: 10}}>
                  <Text style={[styles.leftContainer]}>
                    {messages('FollowUpStatus')}
                  </Text>
                  <View style={styles.rightContainer}>
                    {this.isScrnTypeOrderFollowUp() ? (
                      <Picker
                        mode="dropdown"
                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                        headerBackButtonText="Back"
                        selectedValue={this.state.followUpStatus}
                        onValueChange={value => {
                          this.setState({followUpStatus: value});
                        }}>
                        {Object.values(followUpStatusList).map(
                          (item, index) => {
                            return (
                              <Picker.Item
                                label={item.statusName}
                                value={item.statusId}
                              />
                            );
                          },
                        )}
                      </Picker>
                    ) : (
                      <Text>{this.state.followUp.status.name}</Text>
                    )}
                  </View>
                </View>
              </Card>
            </Content>
            {this.isScrnTypeOrderFollowUp() && (
              <Footer style={{backgroundColor: 'transparent'}}>
                <Button
                  full
                  onPress={this._formValidation.bind(this)}
                  style={{
                    backgroundColor: Colors.headerBackground,
                    width: Dimensions.get('screen').width,
                    height: 70,
                  }}>
                  <Text
                    style={{
                      color: 'white',
                      marginBottom: 15,
                      fontSize: 20,
                      fontWeight: 'bold',
                    }}>
                    Done
                  </Text>
                </Button>
              </Footer>
            )}
          </Container>
        </Root>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  leftContainer: {
    color: Colors.headerBackground,
    fontSize: 16,
    paddingLeft: 10,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
  },
  rightContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
});

//make this component available to the app
export default ViewFollowUp;
