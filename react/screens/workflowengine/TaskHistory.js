// import React from "react";
// import {
//   StyleSheet,
//   StatusBar,
//   Dimensions,
//   TouchableOpacity,
//   ActivityIndicator,
//   FlatList,
//   NetInfo,
//   Alert,
// } from "react-native";
// import {
//   Icon,
//   Left,
//   Body,
//   Text,
//   Right,
//   Card,
//   View,
//   CardItem,
// } from "native-base";

// import { Styles, IconColor } from "../../style/Styles";
// import Colors from "../../style/Colors";
// import Route from "../../router/Route";
// import AppConstants from "../../constant/AppConstants";
// import MasterConstants from "../../constant/MasterConstants";
// import { messages } from "react-native-erp-mobile-library/react/i18n/i18n";
// import Toast from 'react-native-simple-toast';
// import { getAllTasks, completeTask } from "react-native-erp-mobile-library/react/controller/WorkFlowController";
// import { GetAllTasks, DeleteTask, DeleteAllTask } from "react-native-erp-mobile-library/react/screens/workflow/dbHelper/WorkflowDBHelper";
// import { getAllOrderDetails } from "react-native-erp-mobile-library/react/controller/SyncDataController";
// import { GetOrder } from 'react-native-erp-mobile-library/react/screens/orders/dbHelper/OrderDBHelper';
// import { DateFormat, DATE_TIME_LAST_UPDATED, DATE_TIME_SERVER } from "react-native-erp-mobile-library/react/common/Common";
// import PreparingProgress from "react-native-erp-mobile-library/react/component/PreparingProgress";
// import { renderHeaderView, backHandler } from "react-native-erp-mobile-library/react/util/HeaderUtil";
// import { sessionInfo } from "react-native-erp-mobile-library";
// import HeaderTypes from "react-native-erp-mobile-library/react/util/HeaderTypes";
// import { getTaskHistory } from "../../controller/WorkflowEngineController";

// const DEVICE_WIDTH = Dimensions.get("window").width;
// const DEVICE_HEIGHT = Dimensions.get("window").height;

// export default class TaskHistory extends React.Component {
//   static navigationOptions = ({ navigation }) => {
//     return renderHeaderView(navigation);
//   }

//   constructor(props) {
//     if (props.screenProps) {
//       props.navigation = props.screenProps.navigation
//     }
//     props.navigation.state.params = { ...props.navigation.state.params, ...props.screenProps }
//     super(props);
//     this.state = {
//       session: sessionInfo({ type: 'get', key: 'loginInfo' }),
//       isLoading: false,
//       loadMore: false,
//       isConnected: true,
//       isPreparing: false,
//       loginToken: "",
//       publicIp: "",
//       isServiceCalled: false,
//       limit: 10,
//       offset: 0,
//       taskList: []
//     };
//   }

//   componentDidMount() {
//     // getTaskHistory((datas) => {
//     //   if (datas) {
//     //     console.log('datas_history' + JSON.stringify(datas));
//     //   }
//     // })
//     this._initHeader();
//   }

//   componentWillUnmount() {
//     this.backHandler.remove();
//   }

//   _initHeader() {
//     this.props.navigation.setParams({
//       headerTitle: this.props.navigation.getParam('title', `history`),
//       headerType: this.props.navigation.getParam('headerType', HeaderTypes.HeaderHomeMenu),
//     });
//     this.backHandler = backHandler(this.props.navigation);
//   }

//   _getAllTaskList() {
//     var filter = {
//       orderBy: 'last_updated desc',
//       limit: this.state.limit,
//       offset: this.state.offset,
//       conditions: [
//         {
//           column: 'tenant_id',
//           value: [this.state.session.info[AppConstants.keyTenantId]],
//           operator: 'in'
//         },
//         {
//           column: 'status_id',
//           value: MasterConstants.StatusAssigned,
//           operator: '='
//         }
//       ]
//     }
//     GetAllTasks((datas) => {
//       console.log(datas);
//       this.state.taskList = [];
//       this.setState({ taskList: this.state.offset == 0 ? datas : this.state.taskList.concat(datas) });
//       if (datas.length > 0) {
//         this.setState({ loadMore: false, isLoading: false, isPreparing: false });
//       } else if (!this.state.isServiceCalled) {
//         // this._getAllTaskListFromServer();
//       }
//     }, filter);
//   }

//   _getAllTaskListFromServer() {
//     NetInfo.isConnected.fetch().then(isConnected => {
//       if (isConnected) {
//         console.log('calling service..');
//         let syncValue = {
//           url: AppConstants.ngetAllMyTasks,
//           filter: {
//             filters: {
//               orderBy: "t.last_updated",
//               limit: this.state.limit,
//               offset: this.state.offset,
//               conditions: [{
//                 column: 't.status_id',
//                 value: MasterConstants.StatusAssigned,
//                 dataType: 'String',
//                 operator: 'in'
//               },
//               {
//                 column: 't.claimed_by',
//                 value: this.state.session.info.userId,
//                 dataType: 'String',
//                 operator: 'in'
//               }]
//             }
//           }
//         }
//         getAllTasks(this.state.session, syncValue, (isSuccess) => {
//           if (isSuccess) {
//             // this._getAllTaskList();
//           }
//           this.setState({ taskList: [], isServiceCalled: true, isLoading: false, isPreparing: false });
//         })
//       } else {
//         this.setState({ isLoading: false, loadMore: false });
//         console.log('make internet connection..!');
//       }
//     })
//   }

//   onRefresh() {
//     NetInfo.isConnected.fetch().then(isConnected => {
//       if (isConnected) {
//         let filter = {
//           conditions: [{
//             column: 'status_id',
//             value: MasterConstants.StatusAssigned,
//             operator: '='
//           }]
//         }
//         DeleteAllTask(filter);
//         this.setState({ offset: 0, isServiceCalled: false, isLoading: true }, () => {
//           // this._getAllTaskListFromServer();
//         });
//       } else {
//         Toast.show(messages('makeInternetConnection'), Toast.SHORT);
//       }
//     });
//   }

//   _refresh() {
//     this.setState({ taskList: [], isLoading: true, offset: 0 }, () => {
//       this.onRefresh();
//     })
//   }

//   renderItem({ item, index }) {
//     return (
//       <TaskItemView
//         navigation={this.props.navigation}
//         index={index}
//         taskId={item.taskId}
//         task={JSON.parse(item.json)}
//         session={this.state.session}
//         refresh={this._refresh.bind(this)}
//       />
//     );
//   }

//   ListEmptyView = () => {
//     return (
//       <View
//         style={{
//           width: DEVICE_WIDTH,
//           marginTop: 200,
//           alignItems: "center",
//           justifyContent: "center",
//           flex: 1
//         }}
//       >
//         <Icon
//           name="bell"
//           type="FontAwesome"
//           style={{ color: Colors.headerBackground, fontSize: 40 }}
//         />
//         <Text style={{ textAlign: "center", color: "darkgrey", marginTop: 10 }}>
//           No task's found.
//                 </Text>
//       </View>
//     );
//   };
//   render() {
//     const { navigation } = this.props;
//     if (this.state.isPreparing) {
//       return (
//         <PreparingProgress height={DEVICE_HEIGHT - 130} />
//       );
//     } else {
//       return (
//         <View
//           padder
//           style={{
//             backgroundColor: Styles.appViewBackground.backgroundColor,
//             ...StyleSheet.absoluteFillObject
//           }}
//         >
//           <StatusBar
//             backgroundColor={Styles.statusBar.backgroundColor}
//             barStyle="light-content"
//           />

//           <FlatList
//             extraData={this.state}
//             data={this.state.taskList}
//             ItemSeparatorComponent={this.FlatListItemSeparator}
//             renderItem={item => this.renderItem(item)}
//             keyExtractor={item => item.taskId}
//             onEndReachedThreshold={0.1}
//             onRefresh={() => this.onRefresh()}
//             refreshing={this.state.isLoading}
//             ListEmptyComponent={this.ListEmptyView}
//             ListFooterComponent={
//               (this.state.loadMore ?
//                 <View style={{ padding: 7, flex: 1, flexDirection: 'row', borderRadius: 2, backgroundColor: Colors.headerBackground, alignItems: 'center', justifyContent: 'center' }}>
//                   <ActivityIndicator size="small" color="white" />
//                   <Text style={{ color: 'white', fontWeight: 'bold' }}>{' Loading..'}</Text>
//                 </View>
//                 : null)
//             }
//           />
//         </View>
//       );
//     }
//   }
// }

// class TaskItemView extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       index: 0,
//       task: {},
//       taskId: '',
//       session: {},
//       isLoading: false
//     };
//   }

//   componentWillMount() {
//     this.setState({ ...this.props });
//   }

//   componentWillReceiveProps(nextProps) {
//     this.setState({ ...nextProps });
//   }

//   _completeTask(buttonAction) {
//     NetInfo.isConnected.fetch().then(isConnected => {
//       this.setState({ isLoading: isConnected })
//       if (isConnected) {
//         var request = {
//           "request": {
//             "envelope": {
//               "sourceApplication": "sourceApplication",
//               "requestDate": DateFormat(new Date(), DATE_TIME_SERVER),
//               "sourceRequestId": "sourceRequestId",
//               "additionalSource1": "additionalSource1",
//               "additionalSource2": "additionalSource2",
//               "additionalReference1": "additionalReference1",
//               "additionalReference2": "additionalReference2"
//             },
//             "content": {
//               "bo_data": {
//                 "type": "completeTask_BOO",
//                 "autoCompleteTaskId": this.state.task.taskId,
//                 "buttonAction": buttonAction
//               }
//             }
//           }
//         }

//         completeTask(this.state.session, request, (response) => {
//           if (response.status === 200) {
//             DeleteTask(this.state.taskId);
//             let task = this.state.task;
//             task.status['id'] = '';
//             this.setState({ task: task, isLoading: false })
//             Toast.show("Task " + buttonAction + " Successfully!")
//           }
//         });
//       } else {
//         Toast.show(messages('makeInternetConnection'), Toast.SHORT);
//       }
//     })
//   }

//   _viewOrderDetail() {
//     let orderId = JSON.parse(this.state.task.taskData)._id;
//     var filter = {
//       conditions: [{
//         value: orderId,
//         column: "order_id",
//         operator: "="
//       }]
//     }
//     GetOrder(this.state.session.databaseName, (data) => {
//       if (data.json != undefined) {
//         this.props.navigation.navigate(Route.ViewOrderTabNavigator, { orderId: orderId, fromScreen: Route.Workflow, taskDetail: this.state.task, refresh: () => { this.props.refresh() } })
//       } else {
//         NetInfo.isConnected.fetch().then(isConnected => {
//           this.setState({ isLoading: isConnected })
//           if (isConnected) {
//             let filter = {
//               limit: 1,
//               offset: 0,
//               conditions: [{
//                 "value": orderId,
//                 "column": "orderId",
//                 "operator": "=",
//                 "dataType": "String"
//               }]
//             }
//             getAllOrderDetails(this.state.session, filter, (isSuccess) => {
//               if (isSuccess) {
//                 this.setState({ isLoading: false }, () => {
//                   this.props.navigation.navigate(Route.ViewOrderTabNavigator, { orderId: orderId, fromScreen: Route.Workflow, taskDetail: this.state.task, refresh: () => { this.props.refresh() } })
//                 });
//               }
//             })
//           } else {
//             Toast.show(messages('makeInternetConnection'), Toast.SHORT);
//           }
//         })
//       }
//     }, filter);
//   }

//   render() {
//     const { navigation, index } = this.props;
//     const task = this.state.task;
//     while (task.workflowInstance.name.includes('~')) {
//       task.workflowInstance["name"] = task.workflowInstance.name.replace('~~', ' > ');
//     }
//     var buttons = JSON.parse(task.buttonDetails).buttons;
//     if (task.status.id == MasterConstants.StatusUnAssigned) {
//       buttons[buttons.length] = "claim";
//     }
//     return (
//       <View>
//         <Card style={[styles.card]}>
//           <TouchableOpacity
//             onPress={() => {
//               if (MasterConstants.workflowEvents[task.workflowInstance.text3] != undefined && MasterConstants.workflowEvents[task.workflowInstance.text3].route != undefined) {
//                 let route = MasterConstants.workflowEvents[task.workflowInstance.text3].route;
//                 if (route == Route.ViewOrderTabNavigator) {
//                   if (route == Route.ViewOrderTabNavigator) {
//                     this._viewOrderDetail();
//                   }
//                 }
//               }
//             }}>
//             <CardItem>
//               <Left>
//                 <View
//                   style={{
//                     width: 35,
//                     height: 35,
//                     borderRadius: 35 / 2,
//                     backgroundColor: IconColor[index % IconColor.length],
//                     justifyContent: "center",
//                     top: 5
//                   }}
//                 >
//                   {this.state.isLoading ?
//                     <ActivityIndicator size="small" color="white" />
//                     :
//                     <Text style={[styles.text, { color: "white", fontSize: 15, }]}>
//                       {task.shortName.substring(0, task.shortName.length > 2 ? 3 : 2)}
//                     </Text>
//                   }
//                 </View>
//                 <Body>
//                   <View style={{ marginTop: 1, flexDirection: "row", flex: 1, justifyContent: 'flex-end' }}>
//                     <Text style={{ borderRadius: 5, paddingLeft: 3, paddingRight: 5, color: 'white', backgroundColor: '#36c159' }}>
//                       <Icon name='users' type='FontAwesome' style={{ fontSize: 13, color: 'white', backgroundColor: '#36c159' }} />
//                       {task.group.name}
//                     </Text>
//                   </View>
//                   <View style={{ flexDirection: "row" }}>
//                     <Icon name='user' type='FontAwesome' style={{ fontSize: 13, marginTop: 3 }} />
//                     <Text> {" " + task.workflowInstance.text1}</Text>
//                   </View>
//                   <View style={{ flexDirection: "row" }}>
//                     <Icon name='bookmark' type='FontAwesome' style={{ fontSize: 13, marginTop: 3 }} />
//                     <Text> {" " + task.description}</Text>
//                   </View>
//                   <View style={{ flexDirection: "row" }}>
//                     <Icon name='folder-open' type='FontAwesome' style={{ fontSize: 13, marginTop: 3 }} />
//                     <Text> {" " + task.workflowInstance.name}</Text>
//                   </View>
//                   <View style={{ flexDirection: "row" }}>
//                     <Icon name='share' type='FontAwesome' style={{ fontSize: 13, marginTop: 3 }} />
//                     <Text> {" " + task.workflowInstance.text1 + " - " + task.workflowInstance.text2}</Text>
//                   </View>
//                   <View style={{ flexDirection: "row" }}>
//                     <Icon name='check' type='FontAwesome' style={{ fontSize: 13, marginTop: 3 }} />
//                     <Text> {" " + DateFormat(new Date(task.dueDate), DATE_TIME_LAST_UPDATED)}</Text>
//                   </View>
//                 </Body>
//               </Left>
//             </CardItem>
//           </TouchableOpacity>
//           <CardItem style={{ backgroundColor: '#e8eaed', flexDirection: task.status.id == '' ? 'row' : 'row-reverse' }}>
//             {task.status.id == '' ?
//               <Left>
//                 <Text style={{ padding: 3, borderRadius: 3, backgroundColor: 'orange', color: 'white' }}>
//                   Request sent
//                                 </Text>
//               </Left>
//               :
//               <Right style={{ flexDirection: 'row-reverse' }}>
//                 {(buttons).map((val, index) => {
//                   console.log(val);
//                   if (task.status.id == MasterConstants.StatusAssigned) {
//                     return (
//                       <TouchableOpacity key={index} onPress={() => {
//                         if (MasterConstants.workflowEvents[task.workflowInstance.text3] != undefined && MasterConstants.workflowEvents[task.workflowInstance.text3].route != undefined) {
//                           let route = MasterConstants.workflowEvents[task.workflowInstance.text3].route;
//                           if (route == Route.ViewOrderTabNavigator) {
//                             this._viewOrderDetail();
//                           }
//                         } else {
//                           Alert.alert(
//                             val,
//                             "Do you want to " + val.toLowerCase() + "?",
//                             [
//                               {
//                                 text: "No",
//                                 onPress: () => console.log("Cancel Pressed"),
//                                 style: "cancel"
//                               },
//                               {
//                                 text: "Yes",
//                                 onPress: () => {
//                                   this._completeTask(val.toLowerCase())
//                                 }
//                               }
//                             ],
//                             { cancelable: false }
//                           );
//                         }
//                       }}>
//                         <Icon
//                           name={val.toLowerCase() == "approve" ? "check" : val.toLowerCase() == "resend" ? 'share' : val.toLowerCase() == "reject" ? "times" : ''}
//                           type='FontAwesome'
//                           style={{
//                             fontSize: 22,
//                             color: val.toLowerCase() == "approve" ? "#0d7c2d" : val.toLowerCase() == "resend" ? '#4b7b8c' : val.toLowerCase() == "reject" ? "#c1140b" : '',
//                             paddingLeft: 10,
//                             paddingRight: 10
//                           }} />
//                       </TouchableOpacity>
//                     )
//                   }
//                 })}
//               </Right>
//             }
//           </CardItem>
//         </Card>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: "center"
//   },
//   horizontal: {
//     flexDirection: "row",
//     justifyContent: "space-around",
//     padding: 10
//   },
//   createDate: {
//     fontSize: 13,
//     textAlign: "right"
//   },
//   text: {
//     fontSize: 16,
//     fontWeight: "bold",
//     justifyContent: 'center',
//     alignItems: 'center',
//     textAlign: 'center'
//   },
// });
