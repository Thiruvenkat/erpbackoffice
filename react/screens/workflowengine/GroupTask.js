/* eslint-disable eqeqeq */
/* eslint-disable no-shadow */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  StyleSheet,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
  Alert,
} from 'react-native';
import {Icon, Left, Body, Text, Right, Card, View, CardItem} from 'native-base';
import {Styles, IconColor} from '../../style/Styles';
import Colors from '../../style/Colors';
import Route from '../../router/Route';
import MasterConstants from '../../constant/MasterConstants';
import {messages} from 'react-native-erp-mobile-library/react/i18n/i18n';
import Toast from 'react-native-simple-toast';
import NetInfo from '@react-native-community/netinfo';
import {
  DateFormat,
  DATE_TIME_SERVER,
  DATE_TIME_LAST_UPDATED,
  NumberFormat,
  RemoveSeparator,
} from 'react-native-erp-mobile-library/react/common/Common';
import {completeTask} from 'react-native-erp-mobile-library/react/controller/WorkFlowController';
import {GetOrder} from 'react-native-erp-mobile-library/react/screens/orders/dbHelper/OrderDBHelper';
import {
  getAllOrderDetails,
  getAllOrderService,
  getAllUserTaskDetails,
} from 'react-native-erp-mobile-library/react/controller/SyncDataController';
import PreparingProgress from 'react-native-erp-mobile-library/react/component/PreparingProgress';
import {renderHeaderView} from 'react-native-erp-mobile-library/react/util/HeaderUtil';
import {sessionInfo} from 'react-native-erp-mobile-library';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import {claimTaskToServer} from '../../controller/WorkflowEngineController';
import {GetAllOrderService} from 'react-native-erp-mobile-library/react/screens/services/dbHelper/ServiceOrderDB';
import {
  GetAllWorkflowTasks,
  DeleteWorkFlowAllTask,
} from 'react-native-erp-mobile-library/react/screens/workflow/dbHelper/WorkflowEngineDBHelper';
import {GetUserDetails} from 'react-native-erp-mobile-library/react/screens/masters/dbHelper/MasterDBHelper';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

export default class GroupTask extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      session: sessionInfo({type: 'get', key: 'loginInfo'}),
      isLoading: true,
      loadMore: false,
      isConnected: true,
      isPreparing: false,
      isServiceCalled: false,
      loginToken: '',
      publicIp: '',
      limit: 50,
      offset: 0,
      taskList: [],
      userGroupIds: [],
    };
    this.serviceCallCount = 0;
  }

  componentDidMount() {
    this._getUserGroups();
    this._initHeader();
    // this._getGroupTasksFromServer();
  }

  componentWillUnmount() {
    // this.backHandler.remove();
  }

  _initHeader() {
    this.props.navigation.setParams({
      headerTitle: this.props.route.params?.headerTitle ?? 'Group Task',
      headerType:
        this.props.route.params?.headerType ?? HeaderTypes.HeaderHomeMenu,
    });
    // this.backHandler = backHandler(this.props.navigation);
    this.props.navigation.setOptions(renderHeaderView(this));
  }

  _getUserGroups() {
    let filter = {
      conditions: [
        {
          column: 'user_id',
          value: this.state.session.info.userId,
          operator: '=',
        },
      ],
    };
    GetUserDetails(filter, data => {
      if (data.length > 0) {
        let userGroups = JSON.parse(data[0].json_data).userGroups;
        let groupIds = [];
        userGroups.forEach(groups => {
          groupIds.push(groups.group.id);
        });
        this.setState({userGroupIds: groupIds}, () => {
          this._getAllGroupTask();
        });
      } else {
        this.setState({loadMore: false, isLoading: false, isPreparing: false});
      }
    });
  }

  _getAllGroupTask() {
    let filter = {
      limit: this.state.limit,
      offset: this.state.offset,
      orderBy: 'created_date desc',
      conditions: [
        {
          column: 'status_id',
          value: MasterConstants.StatusWorkflowUnAssigned,
          operator: '=',
        },
        {
          column: 'tenant_id',
          value: this.state.session.info.tenantId,
          operator: '=',
        },
        {
          column: 'groups',
          value: this.state.userGroupIds,
          operator: 'in',
        },
      ],
    };
    GetAllWorkflowTasks(datas => {
      if (datas.length > 0) {
        this.state.taskList = [];
        this.setState({
          taskList:
            this.state.offset === 0 ? datas : this.state.taskList.concat(datas),
          loadMore: false,
          isLoading: false,
          isPreparing: false,
          offset: this.state.offset + datas.length,
          isServiceCalled: true,
        });
      } else if (!this.state.isServiceCalled) {
        this._getGroupTasksFromServer();
      } else {
        // Toast.show('No task(s)', Toast.SHORT);
        this.setState({loadMore: false, isLoading: false, isPreparing: false});
      }
    }, filter);
  }

  _getGroupTasksFromServer() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        this.setState({isLoading: true, isServiceCalled: true}, () => {
          var filter = {
            order: 'created_date desc',
            include: {processInstance: 'event'},
            where: {
              statusId: MasterConstants.StatusWorkflowUnAssigned,
              tenantId: this.state.session.info.tenantId,
            },
          };
          if (this.state.userGroupIds.length) {
            filter.where.groups = {
              inq: this.state.userGroupIds,
            };
          }
          let url = `${
            this.state.session.workflowServerIp
          }/api/userTasks/getUserTasks?filter=${JSON.stringify(filter)}`;
          getAllUserTaskDetails(url, isSuccess => {
            if (isSuccess) {
              if (this.serviceCallCount === 0) {
                this.serviceCallCount = 1;
                this._getAllGroupTask();
              }
            } else {
              this.setState({
                loadMore: false,
                isLoading: false,
                isPreparing: false,
              });
            }
          });
        });
      } else {
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    });
  }

  onRefresh() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        let filter = {
          conditions: [
            {
              column: 'status_id',
              value: MasterConstants.StatusWorkflowUnAssigned,
              operator: '=',
            },
          ],
        };
        DeleteWorkFlowAllTask(filter, isSuccess => {
          if (isSuccess) {
            this.setState(
              {
                offset: 0,
                taskList: [],
                isServiceCalled: false,
                isLoading: true,
              },
              () => {
                this.serviceCallCount = 0;
                this._getGroupTasksFromServer();
              },
            );
          }
        });
      } else {
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    });
  }

  _refresh() {
    this.setState({isLoading: true, offset: 0}, () => {
      this.onRefresh();
    });
  }

  renderItem({item, index}) {
    const items = JSON.parse(item.process_instance_data);
    return (
      <TaskItemView
        navigation={this.props.navigation}
        parentProps={this.props}
        index={index}
        taskId={items.taskId}
        task={items}
        session={this.state.session}
        refresh={this._refresh.bind(this)}
      />
    );
  }

  ListEmptyView = () => {
    return (
      <View
        style={{
          width: DEVICE_WIDTH,
          marginTop: 200,
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
        }}>
        <Icon
          name="bell"
          type="FontAwesome"
          style={{color: Colors.headerBackground, fontSize: 40}}
        />
        <Text style={{textAlign: 'center', color: 'darkgrey', marginTop: 10}}>
          No task's found.
        </Text>
      </View>
    );
  };

  render() {
    if (this.state.isPreparing) {
      return <PreparingProgress height={DEVICE_HEIGHT - 130} />;
    } else {
      return (
        <View
          padder
          style={{
            backgroundColor: Styles.appViewBackground.backgroundColor,
            ...StyleSheet.absoluteFillObject,
          }}>
          <StatusBar
            backgroundColor={Styles.statusBar.backgroundColor}
            barStyle="light-content"
          />
          <FlatList
            extraData={this.state}
            data={this.state.taskList}
            ItemSeparatorComponent={this.FlatListItemSeparator}
            renderItem={item => this.renderItem(item)}
            keyExtractor={item => item.task_id.toString()}
            onEndReachedThreshold={0.1}
            onRefresh={() => this.onRefresh()}
            refreshing={this.state.isLoading}
            ListEmptyComponent={this.ListEmptyView}
            // onEndReached={({ distanceFromEnd }) => {
            //     if (!this.state.loadMore && this.state.offset >= this.state.limit) {
            //         this.setState({ loadMore: true, }, () => {
            //             this._getAllGroupTask();
            //         });
            //     }
            // }}
            ListFooterComponent={
              this.state.loadMore ? (
                <View
                  style={{
                    padding: 7,
                    flex: 1,
                    flexDirection: 'row',
                    borderRadius: 2,
                    backgroundColor: Colors.headerBackground,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <ActivityIndicator size="small" color="white" />
                  <Text style={{color: 'white', fontWeight: 'bold'}}>
                    {' Loading..'}
                  </Text>
                </View>
              ) : null
            }
          />
        </View>
      );
    }
  }
}

class TaskItemView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      task: {},
      taskId: '',
      session: sessionInfo({type: 'get', key: 'loginInfo'}),
      isLoading: false,
      ...props,
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({...nextProps});
  }

  claimTaskDetail(taskId) {
    NetInfo.fetch().then(isConnected => {
      this.setState({isLoading: isConnected});
      if (isConnected) {
        var request = {
          task_id: taskId,
          claimed_by: this.state.session.info.userId,
          status_id: MasterConstants.StatusWorkflowAssigned,
          assignee: this.state.session.info.userId,
          tenant_id: this.state.session.info.tenantId,
        };
        let url = `${
          this.state.session.workflowServerIp
        }/api/workflowApis/claimTask`;
        claimTaskToServer(url, request, response => {
          if (response.status === 200) {
            this.props.refresh();
          }
        });
      } else {
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    });
  }

  _completeTask(buttonAction) {
    NetInfo.fetch().then(isConnected => {
      this.setState({isLoading: isConnected});
      if (isConnected) {
        var request = {
          request: {
            envelope: {
              sourceApplication: 'sourceApplication',
              requestDate: DateFormat(new Date(), DATE_TIME_SERVER),
              sourceRequestId: 'sourceRequestId',
              additionalSource1: 'additionalSource1',
              additionalSource2: 'additionalSource2',
              additionalReference1: 'additionalReference1',
              additionalReference2: 'additionalReference2',
            },
            content: {
              bo_data: {
                type: 'completeTask_BOO',
                autoCompleteTaskId: this.state.task.taskId,
                buttonAction: buttonAction,
              },
            },
          },
        };

        completeTask(this.state.session, request, response => {
          if (response.status === 200) {
            // DeleteTask(this.state.task.taskId);
            let task = this.state.task;
            task.status.id = '';
            this.setState({task: task, isLoading: false});
            Toast.show('Task ' + buttonAction + ' Successfully!');
          }
        });
      } else {
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    });
  }

  _viewOrderServiceDetail(task) {
    const orderIds = JSON.parse(task.processInstance.instanceData).orderId;
    const filter = {
      conditions: [
        {
          value: orderIds,
          column: 'order_service_id',
          operator: '=',
        },
      ],
    };
    GetAllOrderService(
      this.state.session.databaseName,
      () => {
        // if (data != undefined) {
        //   this.getProductDetails(data[0].productId, (product) => {
        //     if (product) {
        //       task["productDetails"] = product;
        //       this.props.navigation.navigate(Route.ViewOrderTabNavigator, {
        //         orderId: orderId,
        //         fromScreen: Route.WorkflowEngineTabNavigator,
        //         taskDetail: task,
        //         orderTypeId: orderType,
        //         refresh: () => {
        //           this.props.refresh()
        //         }
        //       })
        //     }
        //   })

        // } else {
        NetInfo.fetch().then(isConnected => {
          this.setState({isLoading: isConnected});
          if (isConnected) {
            let filter = {
              limit: 1,
              offset: 0,
              conditions: [
                {
                  value: orderIds,
                  column: 'orderServiceId',
                  operator: '=',
                  dataType: 'String',
                },
              ],
            };
            getAllOrderService(this.state.session, filter, isSuccess => {
              if (isSuccess) {
                this.setState({isLoading: false}, () => {
                  this.props.navigation.navigate(Route.ViewOrderTabNavigator, {
                    orderId: orderIds,
                    fromScreen: Route.WorkflowEngineTabNavigator,
                    taskDetail: task,
                    toScreen: Route.AllocateTask,
                    refresh: () => {
                      this.props.refresh();
                    },
                  });
                });
              }
            });
          } else {
            Toast.show(messages('makeInternetConnection'), Toast.SHORT);
          }
        });
        // }
      },
      filter,
    );
  }

  _viewOrderDetail(task) {
    const orderIds = JSON.parse(task.processInstance.instanceData).orders
      .orderId;
    const filter = {
      conditions: [
        {
          value: orderIds,
          column: 'order_id',
          operator: '=',
        },
      ],
    };
    GetOrder(
      this.state.session.databaseName,
      data => {
        if (data.json !== undefined) {
          this.props.navigation.navigate(Route.ViewOrderTabNavigator, {
            orderId: orderIds,
            fromScreen: Route.WorkflowEngineTabNavigator,
            taskDetail: task,
            toScreen: Route.ViewOrderDetailTabNavigator,
            refresh: () => {
              this.props.refresh();
            },
          });
        } else {
          NetInfo.fetch().then(isConnected => {
            this.setState({isLoading: isConnected});
            if (isConnected) {
              let filter = {
                limit: 1,
                offset: 0,
                conditions: [
                  {
                    value: orderIds,
                    column: 'orderId',
                    operator: '=',
                    dataType: 'String',
                  },
                ],
              };
              getAllOrderDetails(this.state.session, filter, isSuccess => {
                if (isSuccess) {
                  this.setState({isLoading: false}, () => {
                    this.props.navigation.navigate(
                      Route.ViewOrderTabNavigator,
                      {
                        orderId: orderIds,
                        fromScreen: Route.WorkflowEngineTabNavigator,
                        taskDetail: task,
                        toScreen: Route.ViewOrderDetailTabNavigator,
                        refresh: () => {
                          this.props.refresh();
                        },
                      },
                    );
                  });
                }
              });
            } else {
              Toast.show(messages('makeInternetConnection'), Toast.SHORT);
            }
          });
        }
      },
      filter,
    );
  }

  getTaskName(eventName) {
    const event = eventName.split(' ');
    let val = [];
    for (let i = 1; i < event.length; i++) {
      val.push(event[i].substring(0, 1));
    }
    return val.join('');
  }

  render() {
    const {index} = this.props;
    const task = this.state.task;
    const parentData = this.state.task.parentData;
    const data = this.state.task.data;
    const eventName = task.processInstance.event.description;

    if (task.statusId == MasterConstants.StatusWorkflowUnAssigned) {
      var buttons = ['claim'];
    } else {
      var buttons = task.taskAction.split(',');
    }
    if (parentData.length) {
      return (
        <View>
          <Card style={{elevation: 1}}>
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                justifyContent: 'flex-end',
                position: 'relative',
                right: '8%',
                padding: 4,
              }}>
              <Text
                style={{
                  fontSize: 14,
                  borderRadius: 5,
                  paddingLeft: 3,
                  paddingRight: 7,
                  color: 'white',
                  backgroundColor: '#36c159',
                }}>
                <Icon
                  name="user"
                  type="FontAwesome"
                  style={{
                    fontSize: 13,
                    color: 'white',
                    backgroundColor: '#36c159',
                  }}
                />
                {' ' + task.name}
              </Text>
            </View>
            <TouchableOpacity
              key={index}
              onPress={() => {
                if (
                  MasterConstants.workflowEvents[task.formIdentifier] != null &&
                  MasterConstants.workflowEvents[task.formIdentifier].route !=
                    null
                ) {
                  let route =
                    MasterConstants.workflowEvents[task.formIdentifier].route;
                  if (route === Route.AllocateTask) {
                    this._viewOrderServiceDetail(task);
                  } else if (route === Route.ViewOrderTabNavigator) {
                    this._viewOrderDetail(task);
                  }
                }
              }}>
              <CardItem style={{flexDirection: 'row', marginTop: 0}}>
                <Left style={{flex: 0.3}}>
                  <View
                    style={{
                      width: 50,
                      height: 50,
                      borderRadius: 25,
                      backgroundColor: IconColor[index % IconColor.length],
                      justifyContent: 'center',
                    }}>
                    {this.state.isLoading ? (
                      <ActivityIndicator size="small" color="white" />
                    ) : (
                      <Text
                        style={[styles.text, {color: 'white', fontSize: 15}]}>
                        {'' + this.getTaskName(eventName)}
                      </Text>
                    )}
                  </View>
                </Left>
                <Body style={{flex: 1}}>
                  <View
                    style={{
                      width: DEVICE_WIDTH,
                      height: 120,
                      borderRadius: 10,
                      justifyContent: 'space-between',
                    }}>
                    {parentData.map((key, index) => {
                      return (
                        <View key={index} style={{flexDirection: 'row'}}>
                          {key.toLowerCase().includes('amount') ||
                          key.toLowerCase().includes('price') ? (
                            <Icon
                              name="rupee"
                              type="FontAwesome"
                              style={{
                                flex: 0.1,
                                fontSize: 15,
                                fontWeight: 'bold',
                                marginLeft: 1,
                                marginTop: 2,
                                color: 'black',
                              }}
                            />
                          ) : key.toLowerCase().includes('quantity') ? (
                            <Icon
                              name="cubes"
                              type="FontAwesome"
                              style={{
                                flex: 0.1,
                                fontSize: 15,
                                marginTop: 2,
                                color: 'black',
                              }}
                            />
                          ) : index % 2 == 0 ? (
                            <Icon
                              name="mail-forward"
                              type="FontAwesome"
                              style={{
                                flex: 0.1,
                                fontSize: 15,
                                marginTop: 2,
                                color: 'black',
                              }}
                            />
                          ) : (
                            <Icon
                              name="check"
                              type="FontAwesome"
                              style={{
                                flex: 0.1,
                                fontSize: 15,
                                marginTop: 2,
                                color: 'black',
                              }}
                            />
                          )}
                          <Text
                            style={{
                              flex: 1.8,
                              marginTop: 1,
                              fontWeight: 'bold',
                              fontSize: 15,
                            }}>
                            {data[key] !== undefined
                              ? '' + data[key].toString()
                              : key.toLowerCase().includes('amount')
                              ? NumberFormat(
                                  RemoveSeparator(
                                    data.amount,
                                    this.state.session.info.exponent,
                                  ),
                                  this.state.session.info,
                                )
                              : key.toLowerCase().includes('price')
                              ? NumberFormat(
                                  RemoveSeparator(
                                    data.price,
                                    this.state.session.info.exponent,
                                  ),
                                  this.state.session.info,
                                )
                              : key.toLowerCase().includes('quantity')
                              ? NumberFormat(
                                  RemoveSeparator(
                                    data.quantity,
                                    this.state.session.info.quantityExponent,
                                  ),
                                  this.state.session.info,
                                )
                              : ' '}
                          </Text>
                        </View>
                      );
                    })}
                    <Text style={{fontWeight: 'bold'}}>
                      <Icon
                        name="calendar"
                        type="FontAwesome"
                        style={{
                          flex: 0.1,
                          fontWeight: 'bold',
                          fontSize: 13,
                          marginTop: 7,
                          color: 'black',
                        }}
                      />
                      {' ' +
                        DateFormat(
                          new Date(task.created_date),
                          DATE_TIME_LAST_UPDATED,
                        )}
                    </Text>
                  </View>
                </Body>
              </CardItem>
              <CardItem
                style={{
                  height: 40,
                  backgroundColor: '#e8eaed',
                  flexDirection: task.statusId == '' ? 'row' : 'row-reverse',
                }}>
                {task.statusId == '' ? (
                  <Left>
                    <Text
                      style={{
                        padding: 3,
                        borderRadius: 3,
                        backgroundColor: 'orange',
                        color: 'white',
                      }}>
                      Request sent
                    </Text>
                  </Left>
                ) : null}
                <Right style={{flexDirection: 'row-reverse'}}>
                  {buttons.map((val, index) => {
                    if (task.statusId) {
                      return (
                        <TouchableOpacity
                          key={index}
                          onPress={() => {
                            if (
                              MasterConstants.workflowEvents[
                                task.formIdentifier
                              ] != undefined &&
                              MasterConstants.workflowEvents[
                                task.formIdentifier
                              ].route != undefined
                            ) {
                              let route =
                                MasterConstants.workflowEvents[
                                  task.formIdentifier
                                ].route;
                              if (route === Route.AllocateTask) {
                                this._viewOrderServiceDetail(task);
                              } else if (
                                route === Route.ViewOrderTabNavigator
                              ) {
                                this._viewOrderDetail(task);
                              }
                            } else {
                              Alert.alert(
                                val,
                                'Do you want to ' + val.toLowerCase() + '?',
                                [
                                  {
                                    text: 'No',
                                    onPress: () =>
                                      console.log('Cancel Pressed'),
                                    style: 'cancel',
                                  },
                                  {
                                    text: 'Yes',
                                    onPress: () => {
                                      this._completeTask(val.toLowerCase());
                                    },
                                  },
                                ],
                                {cancelable: false},
                              );
                            }
                          }}>
                          <Icon
                            name={
                              val.toLowerCase() == 'approve'
                                ? 'check'
                                : val.toLowerCase() == 'resend'
                                ? 'share'
                                : val.toLowerCase() == 'reject'
                                ? 'times'
                                : ''
                            }
                            type="FontAwesome"
                            style={{
                              fontSize: 22,
                              color:
                                val.toLowerCase() == 'approve'
                                  ? '#0d7c2d'
                                  : val.toLowerCase() == 'resend'
                                  ? '#4b7b8c'
                                  : val.toLowerCase() == 'reject'
                                  ? '#c1140b'
                                  : '',
                              paddingLeft: 10,
                              paddingRight: 10,
                            }}
                          />
                        </TouchableOpacity>
                      );
                    } else if (val.toLowerCase() === 'claim') {
                      return (
                        <TouchableOpacity
                          key={index}
                          onPress={() => {
                            Alert.alert(
                              'Claim',
                              'Do you want to claim?',
                              [
                                {
                                  text: 'No',
                                  onPress: () => console.log('Cancel Pressed'),
                                  style: 'cancel',
                                },
                                {
                                  text: 'Yes',
                                  onPress: () => {
                                    this.claimTaskDetail(task.taskId);
                                  },
                                },
                              ],
                              {cancelable: false},
                            );
                          }}>
                          <Icon
                            name="check"
                            type="FontAwesome"
                            style={{
                              fontSize: 22,
                              color: Colors.headerBackground,
                              paddingLeft: 10,
                              paddingRight: 10,
                            }}
                          />
                        </TouchableOpacity>
                      );
                    }
                  })}
                </Right>
              </CardItem>
            </TouchableOpacity>
          </Card>
        </View>
      );
    } else {
      return (
        <View>
          <Card>
            <CardItem>
              <View>
                <Text>{'No Task found!'}</Text>
              </View>
            </CardItem>
          </Card>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  createDate: {
    fontSize: 13,
    textAlign: 'right',
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
});
