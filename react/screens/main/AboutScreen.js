/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, View, Image, Text, StatusBar} from 'react-native';
import logoImg from '../../../assets/ta_logo_final.png';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import {Styles} from 'react-native-erp-mobile-library/react/style/Styles';
import {messages} from 'react-native-erp-mobile-library/react/i18n/i18n';
import {renderHeaderView} from 'react-native-erp-mobile-library/react/util/HeaderUtil';
import Colors from 'react-native-erp-mobile-library/react/style/Colors';

export default class AboutScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      appVersion: '',
    };
  }
  componentDidMount() {
    this._initHeader();
    this._apkInformation();
  }

  _initHeader() {
    this.props.navigation.setParams({
      headerTitle: this.props.route.params.headerTitle
        ? this.props.route.params.headerTitle
        : messages('About'),
      headerType: this.props.route.params.headerType
        ? this.props.route.params.headerType
        : HeaderTypes.HeaderHomeMenu,
    });
    this.props.navigation.setOptions(renderHeaderView(this));
  }

  _apkInformation() {
    // ApkManager.getAppInformation('com.erpbackoffice').then((data) => {
    //     console.log(data);
    //     this.setState({ appVersion: data.versionName })
    //     // {appName: "test", packageName: "com.test", versionName: "1.0.0", versionCode: "1", installLocation: 0, firstInstallTime: 123234456, lastUpdateTime: 123234456}
    // }).catch((error) => {
    //     console.log(error);
    //     // 400 Get AppInfomation Fail
    // });
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={Styles.statusBar.backgroundColor}
          barStyle="light-content"
        />
        <Text
          style={{
            textAlign: 'center',
            color: 'black',
            fontSize: 15,
            fontWeight: 'bold',
            padding: 25,
          }}>
          [ 10-MAY-2020 ]
        </Text>
        <Image source={logoImg} style={[styles.logo_image]} />
        <Text
          style={{
            textAlign: 'center',
            color: Colors.headerBackground,
            fontSize: 20,
            fontWeight: 'bold',
          }}>
          Back Office
        </Text>
        {this.state.appVersion.length > 0 && (
          <Text
            style={{
              textAlign: 'center',
              color: 'red',
              fontSize: 14,
              fontWeight: 'bold',
              padding: 20,
            }}>
            {'Version : ' + this.state.appVersion}
          </Text>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.headerTitleColor,
  },
  logo_image: {
    width: 100,
    height: 100,
  },
});
