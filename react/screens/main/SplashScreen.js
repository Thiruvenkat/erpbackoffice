/* eslint-disable no-shadow */
/* eslint-disable eqeqeq */
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  Linking,
  Platform,
  PermissionsAndroid,
} from 'react-native';
import {Spinner} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import logoImg from '../../../assets/ta_logo_final.png';
// import logoImg from "../../../assets/sim.png";
import AppConstants from '../../constant/AppConstants';
import Route from '../../router/Route';
import {getInitialNecessaryInfo} from 'react-native-erp-mobile-library/react/utilities/OnloadServicesUtilities';
import {sessionInfo} from 'react-native-erp-mobile-library';
import MasterConstants from '../../constant/MasterConstants';
import {
  GetAllMenu,
  InsertMenuDetail,
} from 'react-native-erp-mobile-library/react/navigation/dbHelper/MenuDBHelper';
import {
  GetAllWorkflowTasks,
  DeleteWorkFlowAllTask,
} from 'react-native-erp-mobile-library/react/screens/workflow/dbHelper/WorkflowEngineDBHelper';
import {
  getAllUserTaskDetails,
  getSessionStatusByCustomerServices,
} from 'react-native-erp-mobile-library/react/controller/SyncDataController';
import {
  GetLoginDetails,
  GetUserDetails,
  InsertUserDetails,
} from 'react-native-erp-mobile-library/react/screens/masters/dbHelper/MasterDBHelper';
import Controller from 'react-native-erp-mobile-library/react/controller/Controller';
import LoginController from '../../controller/LoginController';
import {Currency} from 'react-native-erp-mobile-library/react/common/Common';
import * as NotificationUtil from 'react-native-erp-mobile-library/react/notification/Utils';
import Colors from 'react-native-erp-mobile-library/react/style/Colors';
import {messages} from 'react-native-erp-mobile-library/react/i18n/i18n';
import NetInfo from '@react-native-community/netinfo';

var loginCreditionals = null;
export default class SplashScreen extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      isServiceCalled: false,
      firebaseToken: null,
    };
  }

  componentDidMount() {
    this.requestPermissions();
    setTimeout(() => {
      this.getLoggedInDatas();
    }, 2000);
  }

  getLoggedInDatas() {
    let filter = {
      offset: 0,
      limit: 1,
    };
    GetLoginDetails(
      AppConstants.affordeBackOfficeDB,
      loginDatas => {
        if (loginDatas.length > 0) {
          loginCreditionals = JSON.parse(loginDatas[0].session_details);
          sessionInfo({
            type: 'set',
            key: 'loginInfo',
            value: loginCreditionals,
          });
          let loginData = {
            loginId: loginDatas[0].login_id,
            password: loginDatas[0].password,
            tenantCode: loginCreditionals.tenantCode,
            tenantMappingIp: loginDatas[0].tenant_mapping_ip,
          };
          this.getLoggedInCustomerData(loginCreditionals, loginData)
            .then(status => {
              if (status === true) {
                this.getLink(loginCreditionals, linkCall => {
                  if (linkCall || !linkCall) {
                    if (loginDatas.length > 0) {
                      // retailer my order screen menu id
                      var filter = {
                        orderBy: 'sequence asc',
                        conditions: [
                          {
                            column: 'menu_id',
                            value: '116',
                            operator: '=',
                          },
                        ],
                      };
                      GetAllMenu(
                        loginCreditionals.databaseName,
                        menus => {
                          if (menus.length > 0) {
                            sessionInfo({
                              type: 'set',
                              key: 'loginInfo',
                              value: {
                                ...loginCreditionals,
                                loggedInUserType:
                                  AppConstants.loggedInUserTypeRetailer,
                              },
                            });
                            this._validateFirstTimeLogin();
                          } else {
                            sessionInfo({
                              type: 'set',
                              key: 'loginInfo',
                              value: {
                                ...loginCreditionals,
                                loggedInUserType:
                                  AppConstants.loggedInUserTypeCommon,
                              },
                            });
                            this._validateFirstTimeLogin();
                          }
                        },
                        filter,
                      );
                    }
                  }
                });
              } else {
                Toast.show('Connect to server!', Toast.SHORT);
                var filter = {
                  orderBy: 'sequence asc',
                  conditions: [
                    {
                      column: 'menu_id',
                      value: '116',
                      operator: '=',
                    },
                  ],
                };
                GetAllMenu(
                  loginCreditionals.databaseName,
                  menus => {
                    if (menus.length > 0) {
                      sessionInfo({
                        type: 'set',
                        key: 'loginInfo',
                        value: {
                          ...loginCreditionals,
                          loggedInUserType:
                            AppConstants.loggedInUserTypeRetailer,
                        },
                      });
                      this._validateFirstTimeLogin();
                    } else {
                      sessionInfo({
                        type: 'set',
                        key: 'loginInfo',
                        value: {
                          ...loginCreditionals,
                          loggedInUserType: AppConstants.loggedInUserTypeCommon,
                        },
                      });
                      this._validateFirstTimeLogin();
                    }
                  },
                  filter,
                );
              }
            })
            .catch(() => {
              //
            });
        } else {
          this.props.navigation.replace(Route.Login);
        }
      },
      filter,
    );
  }

  getLoggedInCustomerData(session, loginData) {
    return new Promise(resolve => {
      let syncValue = {
        limit: 1,
        offset: 0,
        conditions: [
          {
            column: 'cr.customer_id',
            value: session.info.loggedInCustomerId,
            dataType: 'String',
            operator: 'in',
          },
        ],
        staticConditions: [
          {
            column: 'customerTypeId',
            value: [MasterConstants.CustomerTypes],
            dataType: 'String',
            operator: 'in',
          },
          {
            column: 'lastUpdated',
            value: '1483228800000',
            dataType: 'Date',
            operator: '>',
          },
        ],
      };
      getSessionStatusByCustomerServices(session, syncValue, statusCode => {
        if (statusCode == 200) {
          resolve(true);
        } else if (statusCode == 401) {
          this._callLoginService(loginData, success => {
            if (success) {
              resolve(true);
            } else {
              resolve(false);
            }
          });
        }
      });
    });
  }

  _callLoginService(loginData, callbackListener) {
    new LoginController()
      .loginBackOffice(loginData.tenantMappingIp, {
        loginId: loginData.loginId,
        password: loginData.password,
        tenantCode: loginData.tenantCode,
      })
      .then(response => {
        if (response.status === 200) {
          AsyncStorage.setItem(AppConstants.keyLoggedInStatus, 'loggedIn');
          this.setState(
            {
              tenantList: JSON.parse(response._bodyText).tenantMapping,
              isLoading: false,
            },
            () => {
              const responseBody = JSON.parse(response._bodyText)
                .responseTokenId;
              if (this.state.tenantList != 'undefined') {
                let parsedResponseData = JSON.parse(responseBody).response
                  .content.bo_data;
                parsedResponseData.menus.forEach(element => {
                  if (element.menuId == '1002') {
                    parsedResponseData.protectedViewEnabled = true;
                  }
                });
                if (!parsedResponseData.protectedViewEnabled) {
                  parsedResponseData.protectedViewEnabled = false;
                }
                AsyncStorage.setItem(
                  AppConstants.keyLoginInfo,
                  JSON.stringify({
                    [AppConstants.keyDatabaseName]:
                      AppConstants.affordeBackOfficeDB,
                    [AppConstants.keyLoginToken]: JSON.parse(responseBody)
                      .response.header.taAuthToken,
                    [AppConstants.keyPublicIp]: this.state.tenantList.publicIp,
                    [AppConstants.keyTenantId]: this.state.tenantList.tenantId,
                    [AppConstants.keySessionInfo]: JSON.stringify(
                      parsedResponseData,
                    ),
                  }),
                );
                const sessionData = JSON.parse(responseBody).response.content
                  .bo_data;
                sessionData.protectedViewEnabled = '';
                sessionData[AppConstants.keyCurrency] = Currency(sessionData);
                const publicIp = this.state.tenantList.publicIp.split(':');
                const workflowServerIp =
                  publicIp[0] +
                  ':' +
                  publicIp[1] +
                  ':' +
                  MasterConstants.tenantConfig.tenantId.loopbackPort;
                const imageIp =
                  publicIp[0] +
                  ':' +
                  publicIp[1] +
                  ':' +
                  MasterConstants.tenantConfig.tenantId.nginxPort;
                let sessiondetails = {
                  databaseName: AppConstants.affordeBackOfficeDB,
                  loginToken: JSON.parse(responseBody).response.header
                    .taAuthToken,
                  publicIp: this.state.tenantList.publicIp,
                  tenantCode: this.state.tenantCode,
                  workflowServerIp,
                  info: sessionData,
                  imageIp,
                };
                sessionInfo({
                  type: 'set',
                  key: 'loginInfo',
                  value: sessiondetails,
                });
                InsertMenuDetail(
                  AppConstants.affordeBackOfficeDB,
                  sessionData.menus,
                  () => {
                    this.getFirebaseToken(sessiondetails);
                    loginCreditionals = sessiondetails;
                    callbackListener(true);
                  },
                );
              }
            },
          );
        } else {
          this.setState({isLoading: false});
          callbackListener(false);
        }
      });
  }

  getFirebaseToken(session) {
    NotificationUtil.getToken().then(token => {
      this.setState({firebaseToken: token || ''}, () => {});
      this._registerGCMToken(session);
    });
  }

  _registerGCMToken(session) {
    new LoginController()
      .registerGCMToken(
        session.loginToken,
        session.publicIp + AppConstants.registerGCMToken,
        {
          tokenId: this.state.firebaseToken,
          userId: session.info.userId,
          appType: 'BackOffice',
          deviceId: '1234',
          osType: Platform.OS === 'android' ? 'android' : 'iOS',
          tenantId: session.info.tenantId,
        },
      )
      .then(response => {
        if (response.status === 200) {
          console.log('Firebase registration success : ', response.status);
        } else {
          console.log('Firebase registration failure: ', response.status);
        }
      });
  }

  getLink(session, linkCall) {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        Linking.getInitialURL()
          .then(url => {
            if (url) {
              var data = url.split('/');
              if (data.length > 3) {
                this._getAllTaskList(session, data[3], cbTaskList => {
                  if (cbTaskList) {
                    linkCall(true);
                  } else {
                    Toast.show('No task(s) for this order', Toast.SHORT);
                    linkCall(false);
                  }
                });
              } else {
                linkCall(false);
              }
            } else {
              linkCall(false);
            }
          })
          .catch(err => {
            console.log('An error occurred', err);
          });
      } else {
        linkCall(false);
      }
    });
  }

  _validateFirstTimeLogin() {
    let session = sessionInfo({type: 'get', key: 'loginInfo'});
    let url = session.publicIp + AppConstants.getUser + session.info.userId;
    let filter = {
      limit: 1,
      offset: 0,
      conditions: [
        {
          column: 'user_id',
          value: session.info.userId,
          operator: '=',
        },
      ],
    };
    GetUserDetails(filter, detail => {
      if (detail.length > 0) {
        //Already logged in user
        if (detail[0].first_time_login_flag == 1) {
          if (
            session.loggedInUserType == AppConstants.loggedInUserTypeRetailer
          ) {
            this.props.navigation.replace(Route.DrawerRetailer);
          } else {
            this.props.navigation.replace(Route.DrawerCommon);
          }
          getInitialNecessaryInfo();
        }
      } else {
        // sending request to server to get user info
        new Controller().playGetApi(session.loginToken, url).then(response => {
          if (response.status === 200) {
            let responseData = JSON.parse(response._bodyText).response.content
              .data;
            if (responseData.firstTimeLoginFlag == '1') {
              let userData = {
                userId: responseData.userId,
                loginId: responseData.loginId,
                domainName: responseData.domainName,
                customerId: responseData.customerId,
                firstTimeLoginFlag: responseData.firstTimeLoginFlag,
                jsonData: JSON.stringify(responseData),
              };
              InsertUserDetails(userData, isSuccess => {
                if (isSuccess) {
                  //store user details into sqlite
                  if (
                    session.loggedInUserType ===
                    AppConstants.loggedInUserTypeRetailer
                  ) {
                    this.props.navigation.replace(Route.DrawerRetailer);
                  } else {
                    this.props.navigation.replace(Route.DrawerCommon);
                  }
                  getInitialNecessaryInfo();
                } else {
                  //
                }
              });
            } else {
              this.props.navigation.replace(Route.ChangePassword, {
                headerTitle: 'Change Password',
                userData: responseData,
                customerId: responseData.customerId,
                scrnType: 'firstTimeLogin',
                sessionDetails: session,
              });
            }
          } else {
            Toast.show('Network error!', Toast.SHORT);
            this.props.navigation.replace(Route.Login);
          }
        });
      }
    });
  }

  _getAllTaskList(session, taskId, cbTaskList) {
    let filter = {
      conditions: [
        {
          column: 'task_id',
          value: taskId,
          operator: '=',
        },
        {
          column: 'tenant_id',
          value: session.info.tenantId,
          operator: '=',
        },
      ],
    };
    GetAllWorkflowTasks(datas => {
      if (datas.length > 0) {
        cbTaskList(true);
        let task = JSON.parse(datas[0].process_instance_data);
        let orderIds = task.businessKey;
        if (
          MasterConstants.workflowEvents[task.formIdentifier] != null &&
          MasterConstants.workflowEvents[task.formIdentifier].route != null
        ) {
          let route = MasterConstants.workflowEvents[task.formIdentifier].route;
          if (route === Route.AllocateTask) {
            this.props.navigation.navigate(Route.ViewOrderTabNavigator, {
              orderId: orderIds,
              fromScreen: Route.WorkflowEngineTabNavigator,
              taskDetail: task,
              toScreen: Route.AllocateTask,
              refresh: () => {
                DeleteWorkFlowAllTask();
              },
            });
          } else if (route === Route.ViewOrderTabNavigator) {
            this.props.navigation.navigate(Route.ViewOrderTabNavigator, {
              orderId: orderIds,
              fromScreen: Route.WorkflowEngineTabNavigator,
              taskDetail: task,
              toScreen: Route.ViewOrderDetailTabNavigator,
              refresh: () => {
                DeleteWorkFlowAllTask();
              },
            });
          }
        }
      } else if (!this.state.isServiceCalled) {
        this._getClaimedTasksFromServer(session, taskId, serviceCall => {
          if (serviceCall) {
            //
          } else {
            cbTaskList(false);
          }
        });
      } else {
        // Toast.show('No task(s)', Toast.SHORT);
      }
    }, filter);
  }

  _getClaimedTasksFromServer(session, taskId, serviceCall) {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        this.setState({isServiceCalled: true}, () => {
          let filter = {
            order: 'created_date desc',
            include: {processInstance: 'event'},
            where: {
              assignee: session.info.userId,
              taskId: taskId,
              tenantId: session.info.tenantId,
            },
          };
          let url = `${
            session.workflowServerIp
          }/api/userTasks/getUserTasks?filter=${JSON.stringify(filter)}`;
          getAllUserTaskDetails(url, isSuccess => {
            if (isSuccess) {
              serviceCall(true);
              this._getAllTaskList();
            } else {
              serviceCall(false);
            }
          });
        });
      } else {
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    });
  }

  async requestPermissions() {
    try {
      const granted = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        PermissionsAndroid.PERMISSIONS.CAMERA,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
      ]);
      console.log(granted);
    } catch (err) {
      console.warn(err);
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={Colors.headerBackground}
          barStyle="light-content"
        />
        <Image source={logoImg} style={styles.logo_image} />
        <Spinner />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo_image: {
    width: 60,
    height: 60,
  },
});
