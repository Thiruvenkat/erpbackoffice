/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { sessionInfo } from 'react-native-erp-mobile-library/index';
import { KeyboardAvoidingView, View, StatusBar, StyleSheet, Text, Image, TouchableOpacity, Dimensions, Platform, DeviceEventEmitter } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import * as nb from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import LoginController from '../../controller/LoginController';
import MasterConstants from '../../constant/MasterConstants';
import AppConstants from '../../constant/AppConstants';
import firebase from 'react-native-firebase';
import Modal from 'react-native-modal';
import PropTypes from 'prop-types';
import { copilot, walkthroughable, CopilotStep } from 'react-native-copilot';

import Route from '../../router/Route';
import logoImg from '../../../assets/ta_logo_final.png';
// import logoImg from "../../../assets/user_ic.png";
import Loader from 'react-native-erp-mobile-library/react/component/Loader';
import { InsertMenuDetail } from 'react-native-erp-mobile-library/react/navigation/dbHelper/MenuDBHelper';
import { Currency } from 'react-native-erp-mobile-library/react/common/Common';
import { getOrderParty } from 'react-native-erp-mobile-library/react/utilities/OnloadServicesUtilities';
import { messages } from 'react-native-erp-mobile-library/react/i18n/i18n';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-erp-mobile-library/react/utilities/PixelRatioConverter';
import * as NotificationUtil from 'react-native-erp-mobile-library/react/notification/Utils';
import { InsertLoginDetails } from 'react-native-erp-mobile-library/react/screens/masters/dbHelper/MasterDBHelper';
import Colors from 'react-native-erp-mobile-library/react/style/Colors';
import Controller from 'react-native-erp-mobile-library/react/controller/Controller';
import NetInfo from '@react-native-community/netinfo';

const DEVICE_WIDTH = Dimensions.get('window').width;
const width = DEVICE_WIDTH / 100;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const height = DEVICE_HEIGHT / 100;
const WalkthroughableView = walkthroughable(View);
const MARGIN = 100;

// wallpaper view
class BackgroundView extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <LinearGradient
        colors={['#17C8FF', '#329BFF', '#4C64FF']}
        start={{ x: 1.0, y: 1.0 }}
        end={{ x: 1.0, y: 0.0 }}
        style={{ alignItems: 'center', justifyContent: 'center' }}
      >
        <View>{this.props.children}</View>
      </LinearGradient>
    );
  }
}

// logo view
class LogoView extends React.Component {
  render() {
    return (
      <View style={styles.logo_container}>
        <Image source={logoImg} style={styles.logo_image} />
        <Text style={styles.logo_text}>{messages('backOffice')}</Text>
      </View>
    );
  }
}

// form View
export class FormView extends React.Component {
  static propTypes = {
    start: PropTypes.func.isRequired,
    copilotEvents: PropTypes.shape({
      on: PropTypes.func.isRequired,
    }).isRequired,
  };
  constructor(props) {
    super(props);
    this.state = {
      firebaseToken: '',
      isLoading: false,
      showPass: true,
      press: false,
      loginSuccess: false,
      loginId: 'kaliammanagncies@gmail.com',
      password: 'Test123',
      tenantCode: 'T', // skm 'T' //cleanmart 'T1'
      validateForm: true,
      mobileNoValidate: false,
      passwordValidate: true,
      tenantCodeValidate: false,
      //  tenantMappingIp: "",
      tenantMappingIp: 'http://192.168.43.63:8080',
      // tenantMappingIp: "http://52.89.157.97:8080" //testing server ip
      // tenantMappingIp: "http://14.102.13.211:8085", //kaliamman server ip
      // tenantMappingIp: "http://52.27.216.134:8080" //production server ip
      isForgotPasswordModalVisible: false,
      forgotPasswordDetails: {
        forgotPasswordUserId: '',
        domainName: '',
      },
    };
    this.showCreateAccountOption = false;
    this.showForgotPassowrdOption = true;
    this.showEditTenantMappingIpInput = true;

    this._showPass = this._showPass.bind(this);
    AsyncStorage.setItem(AppConstants.keyTenantCode, this.state.tenantCode);
    AsyncStorage.getItem(AppConstants.keyTenantMappingIp).then(
      tenantMappingIp => {
        if (tenantMappingIp) {
          this.setState({ tenantMappingIp });
        } else {
          AsyncStorage.setItem(AppConstants.keyTenantMappingIp, this.state.tenantMappingIp);
          this.setState({ tenantMappingIp: this.state.tenantMappingIp });
        }
      }
    );
  }

  componentDidMount() {
    firebase.messaging().getToken()
      .then(fcmToken => {
        if (fcmToken) {
          this.setState({ firebaseToken: fcmToken || '' });
        } else {
          // user doesn't have a device token yet
        }
      });
  }

  onPressForgotPassword() {
    AsyncStorage.setItem(AppConstants.keyTenantMappingIp, this.state.tenantMappingIp);
    this.props.navigation.navigate(Route.Register, { scrnType: 'forgotPassword', headerTitle: 'Forgot password' });
  }

  _showPass() {
    this.state.press === false ? this.setState({ showPass: false, press: true }) : this.setState({ showPass: true, press: false });
  }

  _callLoginService() {
    this.setState({ isLoading: true });
    new LoginController()
      .loginBackOffice(this.state.tenantMappingIp, {
        loginId: this.state.loginId,
        password: this.state.password,
        tenantCode: this.state.tenantCode,
      }).then(response => {
        if (response.status === 200) {
          AsyncStorage.setItem(AppConstants.keyLoggedInStatus, 'loggedIn');
          this.setState({
            tenantList: JSON.parse(response._bodyText).tenantMapping, isLoading: false,
          }, () => {
            const responseBody = JSON.parse(response._bodyText).responseTokenId;
            if (this.state.tenantList != 'undefined') {
              let parsedResponseData = JSON.parse(responseBody).response.content.bo_data;
              parsedResponseData.menus.forEach(element => {
                if (element.menuId == '1002') {
                  parsedResponseData.protectedViewEnabled = true;
                }
              });
              if (!parsedResponseData.protectedViewEnabled) {
                parsedResponseData.protectedViewEnabled = false;
              }
              AsyncStorage.setItem(AppConstants.keyLoginInfo,
                JSON.stringify({
                  [AppConstants.keyDatabaseName]: AppConstants.affordeBackOfficeDB,
                  [AppConstants.keyLoginToken]: JSON.parse(responseBody).response.header.taAuthToken,
                  [AppConstants.keyPublicIp]: this.state.tenantList.publicIp,
                  [AppConstants.keyTenantId]: this.state.tenantList.tenantId,
                  [AppConstants.keySessionInfo]: JSON.stringify(parsedResponseData),
                })
              );
              const sessionData = JSON.parse(responseBody).response.content.bo_data;
              sessionData.protectedViewEnabled = parsedResponseData.protectedViewEnabled;
              sessionData[AppConstants.keyCurrency] = Currency(sessionData);
              const publicIp = this.state.tenantList.publicIp.split(':');
              const workflowServerIp = publicIp[0] + ':' + publicIp[1] + ':' + MasterConstants.tenantConfig.tenantId.loopbackPort;
              const imageIp = publicIp[0] + ':' + publicIp[1] + ':' + MasterConstants.tenantConfig.tenantId.nginxPort;
              let sessiondetails = {
                databaseName: AppConstants.affordeBackOfficeDB,
                loginToken: JSON.parse(responseBody).response.header.taAuthToken,
                publicIp: this.state.tenantList.publicIp,
                tenantCode: this.state.tenantCode,
                workflowServerIp,
                info: sessionData,
                imageIp,
              };
              this.getFirebaseToken(sessiondetails);
              sessionInfo({ type: 'set', key: 'loginInfo', value: sessiondetails });
              getOrderParty(sessiondetails, sessiondetails.info.tenantCustomerId, '172129', (response) => {
                if (response.status === 200) {
                  const stockAccountId = JSON.parse(response._bodyText).response.content.bo_data.stockAccountId;
                  AsyncStorage.setItem(AppConstants.keyStockAccountId, stockAccountId);
                }
                if (sessiondetails.info.tenantCustomerId !== sessiondetails.info.loggedInCustomerId) {
                  getOrderParty(sessiondetails, sessiondetails.info.loggedInCustomerId, '172140', (responses) => {
                    if (responses.status === 200) {
                      var account = JSON.parse(responses._bodyText).response.content.bo_data;
                      if (typeof account.accounts !== 'undefined' && account.accounts.length > 0) {
                        const accountId = account.accounts[0].accountId;
                        AsyncStorage.setItem(AppConstants.accountId, accountId);
                      }
                    }
                  });
                }
              });

              InsertMenuDetail(AppConstants.affordeBackOfficeDB, sessionData.menus, () => {
                let loginDetail = {
                  loginId: this.state.loginId,
                  password: this.state.password,
                  tenantMappingIp: this.state.tenantMappingIp,
                  sessionInfo: JSON.stringify(sessiondetails),
                };
                // session details and login creditionals
                InsertLoginDetails(loginDetail, AppConstants.affordeBackOfficeDB, (isSuccess) => {
                  if (isSuccess) {
                    this.props.navigation.replace(Route.SplashScreen);
                  } else {
                    this.setState({ isLoading: false });
                    Toast.show('Try Agains!', Toast.SHORT);
                  }
                });
              });
            }
          });
        } else {
          this.setState({ isLoading: false });
          Toast.show('Login failure', Toast.SHORT);
        }
      });
  }

  getFirebaseToken(session) {
    NotificationUtil.getToken().then(token => {
      this.setState({ firebaseToken: token || '' }, () => {
      });
      this._registerGCMToken(session);
    });
  }

  _registerGCMToken(session) {
    new LoginController()
      .registerGCMToken(session.loginToken, session.publicIp + AppConstants.registerGCMToken, {
        tokenId: this.state.firebaseToken,
        userId: session.info.userId,
        appType: 'BackOffice',
        deviceId: '1234',
        osType: Platform.OS === 'android' ? 'android' : 'iOS',
        tenantId: session.info.tenantId,
      })
      .then(response => {
        if (response.status === 200) {
          this.setState({ isLoading: false });
          console.log('Firebase registration success : ', response.status);
        } else {
          console.log('Firebase registration failure: ', response.status);
        }
      });
  }

  _validateLoginId(text) {
    if (text.length > 0) {
      this.setState({
        validateForm: false,
        loginId: text,
        mobileNoValidate: true,
      });
    } else {
      this.setState({
        validateForm: false,
        loginId: text,
        mobileNoValidate: false,
      });
    }
  }

  _validatePassword(text) {
    if (text.length > 0) {
      this.setState({
        validateForm: false,
        password: text,
        passwordValidate: true,
      });
    } else {
      this.setState({
        validateForm: false,
        password: text,
        passwordValidate: false,
      });
    }
  }

  _setTenantCode(text) {
    if (text.length > 0) {
      this.setState({
        validateForm: false,
        tenantCode: text,
        tenantCodeValidate: true,
      });
    } else {
      this.setState({
        validateForm: false,
        tenantCode: text,
        tenantCodeValidate: false,
      });
    }
  }

  _renderSuccessIcon = (
    <nb.Icon name="checkmark-circle" style={{ color: '#085c87' }} />
  );

  _renderLoginUserNameErrorIcon = () => (
    this.state.loginId && this.state.loginId.length > 0 &&
    <nb.Icon onPress={() => { this.setState({ loginId: '' }); }} name="close" style={{ color: 'black' }} />
  );

  _renderLoginPasswordErrorIcon = () => (
    this.state.password && this.state.password.length > 0 &&
    <nb.Icon onPress={() => { this.setState({ password: '' }); }} name="close" style={{ color: 'black' }} />
  );

  renderInputBox = (value, placeholder, name) => (
    <nb.View
      style={{ marginTop: 15, flexDirection: 'row' }}>
      <TouchableOpacity
        style={[styles.rightContainer, { borderWidth: 1, borderRadius: 9, borderColor: 'lightgrey' }]}
        onPress={() => { }}>
        <nb.Textarea
          rowSpan={2}
          value={value}
          placeholder={placeholder}
          placeholderTextColor={'lightgrey'}
          style={[styles.rightContainer, { borderRadius: 3 }]}
          onChangeText={(text) => {
            this._updateStateValues(name, text);
          }} />
      </TouchableOpacity>
    </nb.View>
  )

  _updateStateValues(name, updateValues) {
    let view = this.state.forgotPasswordDetails;
    view[name] = updateValues;
    this.setState({ forgotPasswordDetails: view, isLoading: false });
  }

  sendUserIdForForgotPassword(IpAddress, url) {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        new Controller().getApi(url)
          .then((response) => {
            if (response.status === 200 && response !== 'undefined') {
              this.setState({ isLoading: false }, () => {
                if (response._bodyText === 100) {
                  this.setState({ isLoading: false });
                  Toast.show('Unable to connect with your provider', Toast.SHORT);
                } else {
                  let details = {
                    forgotPasswordUserId: '',
                    domainName: '',
                  };
                  this.setState({ forgotPasswordDetails: details });
                  this.SecurityQuestionsListener = DeviceEventEmitter.addListener('securityQuestionListener', e => {
                    Toast.show('Password Changed successfully!', Toast.SHORT);
                  });
                  this.props.navigation.navigate(Route.SecurityQuestions, {
                    questions: JSON.parse(response._bodyText),
                    headerTitle: 'Security Questions',
                    mappingIp: IpAddress,
                    scrnType: 'forgotPassword',
                  });
                }
              });
            } else if (response === 'undefined') {
              this.setState({ isLoading: false, isForgotPasswordModalVisible: false });
              Toast.show('Unable to connect with your provider', Toast.SHORT);
            }
            else {
              this.setState({ isLoading: false });
              Toast.show('Unable to connect with your provider', Toast.SHORT);
            }
          })
          .catch((error) => {
            this.setState({ isLoading: false });
            Toast.show('Unable to connect with your provider', Toast.SHORT);
          });
      }
    }).catch((error) => {
      this.setState({ isLoading: false, isForgotPasswordModalVisible: false });
      Toast.show(messages('makeInternetConnection'), Toast.SHORT);
    });
  }

  // getTenantMappingPublicIp() {
  //   AsyncStorage.setItem(AppConstants.keyTenantMappingIp, this.state.tenantMappingIp)
  //   new LoginController()
  //     .getPublicIp(this.state.tenantMappingIp, {
  //       tenantCode: this.state.tenantCode
  //     })
  //     .then(response => {
  //       if (response.status === 200) {
  //         let res = response;
  //   } else {
  //     //
  //   }
  // })
  // }

  renderForgotPasswordModal = () => (
    <Modal isVisible={this.state.isForgotPasswordModalVisible}>
      <nb.View style={{ backgroundColor: 'white', padding: 22, borderRadius: 4, borderColor: 'rgba(0, 0, 0, 0.1)' }}>
        <nb.View>
          <nb.Text style={styles.forgotPasswordLabel}>{'Login Id / Mobile Number'}</nb.Text>
          <nb.Icon
            onPress={() => { this.setState({ isForgotPasswordModalVisible: false }); }}
            name="md-close"
            style={{ color: 'darkgrey', position: 'absolute', right: 10 }} />
        </nb.View>
        {this.renderInputBox(
          this.state.forgotPasswordDetails.forgotPasswordUserId,
          'Enter login id (ex: abc@d.com, 9876543210)',
          'forgotPasswordUserId'
        )}
        <nb.Text style={{ marginTop: 7, ...styles.forgotPasswordLabel }}>{'Domain '}</nb.Text>
        {this.renderInputBox(
          this.state.forgotPasswordDetails.domainName,
          'Enter domain (ex: www.abc.com)',
          'domainName'
        )}
        <nb.View style={{ flexDirection: 'row', justifyContent: 'center' }}>
          <TouchableOpacity
            style={{ paddingLeft: 10 }} onPress={() => {
              this.setState({ isForgotPasswordModalVisible: false }, () => {
                // this.rejectReasonToServer()
              });
            }}>
            <nb.Text style={{ ...styles.buttonLabel, color: 'black', backgroundColor: 'lightgrey' }}>{'close'}</nb.Text>
            <nb.View />
          </TouchableOpacity>

          <TouchableOpacity style={{ paddingLeft: 10 }} onPress={() => {
            // this.getTenantMappingPublicIp();
            let publicIp = this.state.tenantMappingIp.split(':');
            let IpAddress = publicIp[1] + ':' + 9000;
            const url = `http:${IpAddress}/users/bo/getUserData?loginId=${this.state.forgotPasswordDetails.forgotPasswordUserId}&domain=${this.state.forgotPasswordDetails.domainName}`;
            if (this.state.forgotPasswordDetails.forgotPasswordUserId.length > 0 && this.state.forgotPasswordDetails.domainName.length > 0) {
              this.setState({ isForgotPasswordModalVisible: false, isLoading: true }, () => {
                this.sendUserIdForForgotPassword(IpAddress, url);
              });
            } else {
              this.setState({ isLoading: false }, () => {
                Toast.show('enter the details', Toast.SHORT);
              });
            }
          }}>
            <nb.Text style={styles.buttonLabel}>{' submit '}</nb.Text>
            <nb.View />
          </TouchableOpacity>
        </nb.View>
      </nb.View>
    </Modal>
  )

  render() {
    return (
      <View>
        <Loader
          loading={this.state.isLoading} />
        {this.renderForgotPasswordModal()}
        <CopilotStep text={messages('userIdMsg')} order={1} name="loginId">
          <WalkthroughableView style={{
            alignItems: 'center',
            justifyContent: 'center', borderRadius: 20,
          }}>
            {/* width: width * 87, marginTop: height * 5 */}
            <nb.Item
              rounded
              style={{
                backgroundColor: 'white',
                height: heightPercentageToDP('5%'),
                width: width * 87,
                marginLeft: 25,
                marginRight: 25,
                marginTop: heightPercentageToDP('5%'),
                shadowRadius: 5,
                shadowColor: 'red',
                // shadowOffset: { height: 130, width: 120 },
                shadowOpacity: 0.8,
                elevation: 6,
              }}
            >
              <nb.Icon
                active
                name="mobile"
                type="FontAwesome"
                style={{ color: '#085c87', fontSize: 26 }}
              />
              <nb.Input
                placeholder="Login Id / Mobile Number"
                style={{ color: 'black', marginTop: 8 }}
                value={this.state.loginId}
                onChangeText={(text) => {
                  this._validateLoginId(text);
                }}
              />
              {this.state.mobileNoValidate
                ? this._renderSuccessIcon
                : this.state.validateForm
                  ? this._renderLoginUserNameErrorIcon
                  : null}
            </nb.Item>
          </WalkthroughableView>
        </CopilotStep>
        <CopilotStep text={messages('userPaswordMsg')} order={2} name="password">
          <WalkthroughableView style={{
            alignItems: 'center', marginTop: 10,
            justifyContent: 'center', borderRadius: 20,
          }}>
            {/* width: width * 87,  marginTop: 10, */}
            <nb.Item
              rounded
              style={{
                backgroundColor: 'white',
                height: heightPercentageToDP('5%'),
                width: width * 87,
                marginLeft: 25,
                marginRight: 25,
                shadowRadius: 5,
                shadowColor: 'red',
                shadowOffset: { height: 130, width: 120 },
                shadowOpacity: 0.8,
                elevation: 6,
              }}
            >
              <nb.Icon
                active
                name="lock"
                type="FontAwesome"
                style={{ color: '#085c87', fontSize: 24 }}
              />
              <nb.Input
                placeholder="Password"
                style={{ color: 'black', marginTop: 8 }}
                value={this.state.password}
                onChangeText={(text) => {
                  this._validatePassword(text);
                }}
                secureTextEntry={this.state.showPass}
              />
              {!this.state.passwordValidate && this.state.validateForm
                ? this._renderLoginPasswordErrorIcon
                : null}
              {!this.state.passwordValidate && this.state.validateForm ? null :
                this.state.showPass ? (
                  <nb.Icon
                    name="eye-slash"
                    type="FontAwesome"
                    style={{ color: '#5586a0', fontSize: 22 }}
                    onPress={this._showPass}
                  />
                ) : (
                    <nb.Icon
                      name="eye"
                      type="FontAwesome"
                      style={{ color: '#5586a0', fontSize: 22 }}
                      onPress={this._showPass}
                    />
                  )}
            </nb.Item>
          </WalkthroughableView>
        </CopilotStep>
        {this.showEditTenantMappingIpInput && <CopilotStep text={messages('tenantCodeMsg')} order={3} name="tenantCode">
          <WalkthroughableView style={{
            alignItems: 'center', borderRadius: 20, marginTop: 10,
            justifyContent: 'center',
          }}>
            {/* width: width * 87, marginTop: 10 */}
            <nb.Item
              rounded
              style={{
                backgroundColor: 'white',
                height: heightPercentageToDP('5%'),
                width: width * 87,
                marginLeft: 25,
                marginRight: 25,
                shadowRadius: 5,
                shadowColor: 'red',
                shadowOffset: { height: 130, width: 120 },
                shadowOpacity: 0.8,
                elevation: 6,
              }}
            >
              <nb.Icon
                active
                name="building"
                type="FontAwesome"
                style={{ color: '#085c87', fontSize: 20 }}
              />
              <nb.Input
                placeholder="Tenant Code"
                value={this.state.tenantCode}
                onChangeText={text => {
                  this._setTenantCode(text);
                }}
                style={{ color: 'black', marginTop: 8 }}
              />
            </nb.Item>
          </WalkthroughableView>
        </CopilotStep>}
        {this.showEditTenantMappingIpInput &&
          <nb.Item
            rounded
            style={{
              backgroundColor: 'white',
              height: heightPercentageToDP('5%'),
              width: width * 87,
              marginTop: 10,
              marginLeft: 25,
              marginRight: 25,
              shadowRadius: 5,
              shadowColor: 'red',
              shadowOffset: { height: 130, width: 120 },
              shadowOpacity: 0.8,
              elevation: 6,
            }}
          >
            <nb.Icon
              active
              name="globe"
              type="FontAwesome"
              style={{ color: '#085c87', fontSize: 24 }}
            />
            <nb.Input
              placeholder="Enter Tenant Mapping IP"
              style={{ color: 'black', marginTop: 8 }}
              value={this.state.tenantMappingIp}
              onChangeText={text => {
                this.setState({ tenantMappingIp: text });
                AsyncStorage.setItem(AppConstants.keyTenantMappingIp, text);
              }}
            />
          </nb.Item>}
        <CopilotStep text={messages('submitMsg')} order={4} name="loginBtn">
          <WalkthroughableView style={{
            alignItems: 'center',
            justifyContent: 'center',
          }}>
            {/* width: width * 16, , marginTop: 25 */}
            <nb.Item
              rounded
              style={{
                backgroundColor: 'white',
                shadowRadius: 5,
                shadowColor: 'red',
                shadowOffset: { height: 130, width: 120 },
                marginTop: heightPercentageToDP('5%'),
                shadowOpacity: 1,
                elevation: 10,
                height: heightPercentageToDP('8%'),
                width: heightPercentageToDP('8%'),
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={this._callLoginService.bind(this)}
            >
              <TouchableOpacity onPress={this._callLoginService.bind(this)}>
                <nb.Icon
                  active
                  name="paper-plane"
                  type="FontAwesome"
                  style={{ marginTop: 5, marginLeft: 4, color: '#085c87', fontSize: 20 }}
                />
                <Text style={{ fontWeight: 'bold', fontSize: 14, padding: 5 }}>Login</Text>
              </TouchableOpacity>
            </nb.Item>
          </WalkthroughableView>
        </CopilotStep>
        <View style={{ flexDirection: 'row', justifyContent: this.showCreateAccountOption == false ? 'flex-end' : 'space-between', height: heightPercentageToDP('3%') }}>
          {this.showCreateAccountOption &&
            <TouchableOpacity onPress={() => { this.props.navigation.navigate(Route.Register, { scrnType: 'register', headerTitle: 'Registration' }); }}>
              <Text style={{ backgroundColor: 'transparent', fontWeight: 'bold', fontSize: 16, color: 'white', marginLeft: widthPercentageToDP('4%') }}>
                {'Create Account'}
              </Text>
            </TouchableOpacity>}
          {this.showForgotPassowrdOption &&
            <TouchableOpacity onPress={() => {
              this.setState({ isForgotPasswordModalVisible: true });
            }}>
              <Text style={{ backgroundColor: 'transparent', fontWeight: 'bold', fontSize: 16, color: 'white', marginRight: widthPercentageToDP('4%') }}>
                {'Forgot password?'}
              </Text>
            </TouchableOpacity>}
        </View>
      </View>
    );
  }
}

// login screen
class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };
  static propTypes = {
    start: PropTypes.func.isRequired,
    copilotEvents: PropTypes.shape({
      on: PropTypes.func.isRequired,
    }).isRequired,
  };
  constructor(props) {
    super(props);
    this.state = {
      token: null,
      otp: null,
    };
  }
  componentDidMount() {
    AsyncStorage.getItem(AppConstants.keyLoginTour).then((value) => {
      if (value !== 'loginTourCompleted' && AppConstants.enableTourOption) {
        this.props.start();
      }
    });

    this.props.copilotEvents.on('stop', () => {
      AsyncStorage.setItem(
        AppConstants.keyLoginTour,
        'loginTourCompleted'
      );
    });
  }

  render() {
    const navigation = this.props.navigation;
    return (
      <nb.Root>
        <StatusBar backgroundColor="#4C64FF" barStyle="light-content" />
        <BackgroundView>
          <KeyboardAvoidingView behavior="padding" >
            <View style={{ flex: 1, flexDirection: 'column' }}>
              <View style={styles.logo_container}>
                <Image source={logoImg} style={styles.logo_image} />
                <Text style={styles.logo_text}>{'Back Office'}</Text>
              </View>
              <FormView navigation={navigation} />
            </View>
          </KeyboardAvoidingView>
        </BackgroundView>
      </nb.Root>
    );
  }
}


LoginScreen = copilot({
  animated: true, // Can be true or false
  overlay: 'svg', // Can be either view or svg
  backdropColor: 'rgba(0, 0, 0, 0.4)',
  //  verticalOffset: height * 5.38
})(LoginScreen);
export default LoginScreen;

// styles
const styles = StyleSheet.create({
  buttonLabel: {
    textAlign: 'center',
    padding: 10,
    marginTop: 10,
    color: 'white',
    backgroundColor: Colors.headerBackground,
    borderRadius: 5,
  },
  logo_container: {
    flex: 0.9,
    alignItems: 'center',
    justifyContent: 'center',
    height: heightPercentageToDP('40%'),
  },
  logo_image: {
    width: heightPercentageToDP('8%'),
    height: heightPercentageToDP('8%'),
  },
  logo_text: {
    color: 'white',
    fontWeight: 'bold',
    backgroundColor: 'transparent',
    marginTop: 15,
    fontSize: 24,
  },
  form_container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    height: heightPercentageToDP('100%'),
  },
  form_btnEye: {
    position: 'absolute',
    top: 58,
    right: 28,
  },
  form_iconEye: {
    width: 25,
    height: 25,
    tintColor: 'rgba(0,0,0,0.2)',
  },
  signup_container: {
    flex: 1,
    width: widthPercentageToDP('100%'),
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  signup_text: {
    color: 'white',
    backgroundColor: 'transparent',
  },
  submit_container: {
    flex: 1,
    top: -95,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  submit_button: {
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#dae3f2',
    borderRadius: 20,
    borderColor: 'white',
    zIndex: 100,
  },
  submit_circle: {
    height: MARGIN,
    width: MARGIN,
    marginTop: -MARGIN,
    borderWidth: 1,
    borderColor: '#60647c',
    borderRadius: 100,
    alignSelf: 'center',
    zIndex: 99,
    backgroundColor: '#dae3f2',
  },
  submit_text: {
    color: '#0842a5',
    backgroundColor: 'transparent',
  },
  submit_image: {
    width: 24,
    height: 24,
  },
  submit_activityIndicatorWrapper: {
    backgroundColor: '#60647c',
    height: 24,
    width: 24,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  bgView_picture: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover',
    backgroundColor: '#115edb',
  },
  input: {
    backgroundColor: '#dae3f2',
    width: DEVICE_WIDTH - 40,
    height: 40,
    marginHorizontal: 20,
    paddingLeft: 45,
    borderRadius: 20,
    color: '#ffffff',
  },
  inputWrapper: {
    flex: 1,
  },
  inlineImg: {
    position: 'absolute',
    zIndex: 99,
    width: 22,
    height: 22,
    left: 35,
    top: 9,
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 25,
  },
  pdf: {
    flex: 1,
    width: Dimensions.get('window').width,
  },
  footer: {
    backgroundColor: 'transparent',
  },
  footerView: {
    flex: 1,
    flexDirection: 'row',
    left: 0,
    right: 0,
    bottom: 0,
    position: 'absolute',
    height: 50,
  },
  footerButtonContainer: {
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    marginRight: 1,
    height: 50,
    backgroundColor: Colors.headerBackground,
  },
  footerButtonText: {
    color: 'white',
    fontWeight: 'bold',
  },
  leftContainer: {
    paddingLeft: 10,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
  },
  rightContainer: {
    flex: 4,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  forgotPasswordLabel: {
    fontSize: 16,
    color: 'black',
    textAlign: 'left',
    fontWeight: 'bold',
  },
});
