/* eslint-disable no-undef */
import {openDatabase} from 'react-native-erp-mobile-library/react/database/DatabaseHandler';
import {TABLE} from 'react-native-erp-mobile-library/react/database/DBConstants';
import {QueryBuilder} from 'react-native-erp-mobile-library/react/database/DBUtilities';
import {
  DateFormat,
  DATE_TIME_SERVER,
} from 'react-native-erp-mobile-library/react/common/Common';
import AppConstants from '../../../constant/AppConstants';
import {sessionInfo} from 'react-native-erp-mobile-library';

export function GetLandingDashboardDetails(filter) {
  return new Promise((resolve, reject) => {
    let DBInstance = openDatabase(AppConstants.affordeBackOfficeDB);
    DBInstance.transaction(db => {
      var query = QueryBuilder(
        'SELECT dashboard_id, layout_id ,details FROM ' + TABLE.DashboardValues,
        filter,
        false,
      );
      db.executeSql(
        query,
        [],
        (tx, results) => {
          var dashboardValues = null;
          if (results.rows.length > 0) {
            const row = results.rows.item(0);
            dashboardValues = row.details;
          }
          resolve(dashboardValues);
        },
        error => {
          console.log('dashboard Error occured :' + JSON.stringify(error));
          reject('internalError');
        },
      );
    });
  });
}
export function GetDashboardDetails(filter) {
  return new Promise((resolve, reject) => {
    let DBInstance = openDatabase(AppConstants.affordeBackOfficeDB);
    DBInstance.transaction(db => {
      var query = QueryBuilder(
        'SELECT dashboard_id ,details FROM ' + TABLE.DashboardDetails,
        filter,
        false,
      );
      db.executeSql(
        query,
        [],
        (tx, results) => {
          var dashboardDetails = null;
          if (results.rows.length > 0) {
            const row = results.rows.item(0);
            dashboardDetails = row.details;
          }
          resolve(dashboardDetails);
        },
        error => {
          console.log('dashboard Error occured :' + JSON.stringify(error));
          reject('internalError');
        },
      );
    });
  });
}

export function InsertLandingDashboardDetails(datas, callback) {
  if (datas) {
    let DBInstance = openDatabase(AppConstants.affordeBackOfficeDB);

    DBInstance.transaction(db => {
      db.executeSql(
        'DELETE FROM ' +
          TABLE.DashboardValues +
          " where dashboard_id = '" +
          datas.dashboardId +
          "' and layout_id = '" +
          datas.layoutId +
          "'",
        [],
        () => {
          db.executeSql(
            'INSERT INTO ' +
              TABLE.DashboardValues +
              ' (' +
              'dashboard_id , layout_id , details ) ' +
              ' VALUES (?,?,?)',
            [datas.dashboardId, datas.layoutId, datas.details],

            () => {
              const session = sessionInfo({type: 'get', key: 'loginInfo'});
              const tenantId = session.info[AppConstants.keyTenantId];

              if (tenantId != null) {
                DBInstance.transaction(db => {
                  db.executeSql(
                    'INSERT OR REPLACE INTO ' + TABLE.Sync + ' VALUES (?,?,?)',
                    [
                      TABLE.DashboardValues,
                      DateFormat(new Date(), DATE_TIME_SERVER),
                      tenantId,
                    ],

                    () => {
                      console.log(
                        'Time updated dashboard values added Successfully',
                      );
                    },

                    error => {
                      console.log(
                        'Time update Error occured :' + JSON.stringify(error),
                      );
                    },
                  );
                });
              }

              callback(true);
            },
            error => {
              //
            },
          );
        },
        error => {
          console.log(
            'Insert dashboard details Error occured :' + JSON.stringify(error),
          );
          if (index == length - 1) {
            callback(false);
          }
        },
      );
    });
  }
}
