/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {FlatList, View, Text, TouchableOpacity} from 'react-native';
import {Card, CardItem, Body, Icon, Left} from 'native-base';
import {sessionInfo} from 'react-native-erp-mobile-library';
import AppConstants from 'react-native-erp-mobile-library/react/constant/AppConstants';
import {
  NumberFormat,
  RemoveSeparator,
} from 'react-native-erp-mobile-library/react/common/Common';

export default class LandingDashboardItemView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dashboard: {},
      filterDate: null,
      ...this.props,
    };
    this.init();
  }

  // Initialize the states
  init() {
    const widgets = this.state.dashboard.widgets;
    if (widgets && widgets.length % 2 === 1) {
      let remainder = widgets.length % 2;
      if (remainder === 1) {
        widgets.push({
          title: 'Empty',
        });
      }
    }
    this.state.dashboard.widgets = widgets;
    this.setState({...this.state});
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({...this.state, ...nextProps});
    this.init();
  }

  render() {
    const {navigation, dashboard} = this.props;
    return (
      <View>
        <CardItem
          style={{
            backgroundColor: 'white',
            marginLeft: 2,
            marginRight: 8,
            paddingLeft: 2,
            paddingRight: 2,
            paddingTop: 2,
            paddingBottom: 2,
          }}>
          <Left>
            <Body style={{backgroundColor: 'transparent'}}>
              <Text style={{fontWeight: 'bold', color: '#576772'}}>
                {dashboard.title}
              </Text>
              <FlatList
                extraData={this.state}
                data={dashboard.widgets}
                numColumns={2}
                renderItem={({item}) => (
                  <CategoryItemView
                    widget={item}
                    navigation={navigation}
                    onPressWidgetItem={this.props.onPressWidgetItem}
                  />
                )}
                keyExtractor={(item, index) => index.toString()}
              />
            </Body>
          </Left>
        </CardItem>
      </View>
    );
  }
}

class CategoryItemView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      widget: {},
      filterDate: null,
      session: sessionInfo({type: 'get', key: 'loginInfo'}),
      ...this.props,
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({...nextProps});
  }

  getSession() {
    return this.state.session;
  }
  getCurrency() {
    if (
      this.getSession() &&
      this.getSession().info &&
      this.getSession().info[AppConstants.keyCurrency]
    ) {
      return this.getSession().info[AppConstants.keyCurrency];
    }
    return null;
  }
  onPressWidget(widget) {
    this.props.onPressWidgetItem(widget);
  }

  renderOrderAmount(widget) {
    const amount = widget.orderAmount;
    const exponent = widget.exponent;
    switch (widget.widgetId) {
      case '1169':
      case '1170':
      case '1171':
        return (
          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                color: '#576772',
                // padding: 6,
                fontWeight: 'bold',
                fontSize: 11,
                textAlign: 'center',
                // flex: 1.7
              }}
              numberOfLines={2}>
              {Math.round(amount)}
            </Text>
            <Text
              style={{
                color: '#576772',
                // padding: 6,
                fontWeight: 'bold',
                fontSize: 11,
                textAlign: 'center',
                // flex: 1.7
              }}
              numberOfLines={2}>
              {typeof widget.salesOrderCount === 'undefined'
                ? ' '
                : widget.salesOrderCount + ' '}
              {typeof widget.salesOrderCount !== 'undefined' && (
                <Icon
                  style={{
                    color: '#576772',
                    // padding: 3,
                    fontWeight: 'bold',
                    fontSize: 11,
                    textAlign: 'center',
                  }}
                  name={'percent'}
                  type={'FontAwesome'}
                />
              )}
            </Text>
          </View>
        );
      // break;
      case '1172':
        return (
          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                color: '#576772',
                // padding: 3,
                fontWeight: 'bold',
                fontSize: 11,
                textAlign: 'center',
                // flex: 1.7
              }}
              numberOfLines={2}>
              {''}
            </Text>
          </View>
        );
      // break;
      default:
        return (
          <View style={{flexDirection: 'row'}}>
            {this.getCurrency() && (
              <Text
                style={{
                  color: '#576772',
                  // padding: 3,
                  fontWeight: 'bold',
                  fontSize: 11,
                  textAlign: 'center',
                  // flex: 1.7
                }}
                numberOfLines={2}>
                <Icon
                  style={{
                    color: '#576772',
                    // padding: 3,
                    fontWeight: 'bold',
                    fontSize: 11,
                    textAlign: 'center',
                  }}
                  name={this.getCurrency()}
                  type={'FontAwesome'}
                />
                {this.getCurrency() && ' '}
                {amount
                  ? NumberFormat(
                      RemoveSeparator(amount, exponent),
                      this.state.session.info,
                    )
                  : NumberFormat(
                      RemoveSeparator('0', exponent),
                      this.state.session.info,
                    )}
              </Text>
            )}
          </View>
        );
      // break;
    }
  }

  render() {
    const {widget} = this.state;
    if (widget.title === 'Empty') {
      return (
        <Card transparent style={{flex: 1, flexDirection: 'column'}}>
          <CardItem transparent style={{backgroundColor: 'transparent'}}>
            <Body
              style={{alignItems: 'center', backgroundColor: 'transparent'}}>
              <Text
                style={{
                  color: 'black',
                  fontWeight: 'bold',
                  padding: 3,
                  fontSize: 8,
                  textAlign: 'center',
                }}
                numberOfLines={3}
              />
            </Body>
          </CardItem>
        </Card>
      );
    }
    return (
      <Card style={{flex: 1, flexDirection: 'column', marginLeft: 4}}>
        <TouchableOpacity
          style={{flexDirection: 'row'}}
          onPress={() => {
            this.onPressWidget(widget);
          }}>
          <CardItem
            transparent
            style={{backgroundColor: 'transparent', flex: 0.7}}>
            <Body style={{backgroundColor: 'transparent'}}>
              <Text
                style={{
                  color: '#576772',
                  // padding: 5,
                  fontWeight: 'bold',
                  fontSize: 10,
                  marginBottom: 3.5,
                  textAlign: 'left',
                }}
                numberOfLines={3}>
                {widget.widgetName}
              </Text>
              <View style={{marginTop: 10}}>
                {this.renderOrderAmount(widget)}
              </View>
            </Body>
          </CardItem>
          <Icon
            name={widget.icon}
            type={widget.iconType}
            style={{
              alignSelf: 'center',
              flex: 0.3,
              // padding: 5,
              color: widget.color,
              fontSize: 35,
            }}
          />
        </TouchableOpacity>
      </Card>
    );
  }
}
