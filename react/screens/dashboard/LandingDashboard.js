/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  FlatList,
  View,
  Text,
  StatusBar,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {Icon, Container} from 'native-base';
import Toast from 'react-native-simple-toast';
import {
  DateFormat,
  DATE_TIME_UI,
} from 'react-native-erp-mobile-library/react/common/Common';
import Route from '../../router/Route';
import MasterConstants from '../../constant/MasterConstants';
import {sessionInfo} from 'react-native-erp-mobile-library';
import LandingDashboardItemView from './LandingDashboardItemView';
import {getLandingDashboardDetails} from '../../controller/SyncDataController';
import {GetLandingDashboardDetails} from './DBHelper/dashboardDBHelper';
import {messages} from 'react-native-erp-mobile-library/react/i18n/i18n';
import {GetAllProductDetails} from 'react-native-erp-mobile-library/react/screens/products/dbHelper/ProductDB';
// import DateTimePicker from 'react-native-modal-datetime-picker';
import NetInfo from '@react-native-community/netinfo';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import Colors from 'react-native-erp-mobile-library/react/style/Colors';
import {Styles} from 'react-native-erp-mobile-library/react/style/Styles';
import {renderHeaderView} from 'react-native-erp-mobile-library/react/util/HeaderUtil';
import DateTimePicker from 'react-native-modal-datetime-picker';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const widgetValues = {
  // menu id for orders = 209,210,211,212,213,214,215,216
  5001: {
    icon: 'shopping-cart',
    type: 'Entypo',
    color: 'rgba(141, 224, 144,0.5)',
    route: Route.OrderList,
    headerTitle: 'Purchase Order',
    routeProps: {},
    parentMenuId: 209,
  },
  5002: {
    icon: 'ios-paper',
    type: 'Ionicons',
    color: 'rgba(247, 205, 143,0.5)',
    route: Route.OrderList,
    headerTitle: 'Purchase invoice',
    routeProps: {},
    parentMenuId: 210,
  },
  5003: {
    icon: 'truck',
    type: 'FontAwesome',
    color: 'rgba(182, 202, 239, 0.5)',
    route: Route.OrderList,
    headerTitle: 'Goods Received',
    routeProps: {},
    parentMenuId: 211,
  },
  5004: {
    icon: 'reply',
    type: 'FontAwesome',
    color: 'rgba(252, 128, 128,0.5)',
    route: Route.OrderList,
    headerTitle: 'Purchase Return',
    routeProps: {},
    parentMenuId: 215,
  },
  5005: {
    icon: 'shopping-cart',
    type: 'Entypo',
    color: 'rgba(141, 224, 144,0.5)',
    route: Route.OrderList,
    headerTitle: 'Sales Order',
    routeProps: {},
    parentMenuId: 212,
  },
  5006: {
    icon: 'ios-paper',
    type: 'Ionicons',
    color: 'rgba(247, 205, 143,0.5)',
    route: Route.OrderList,
    headerTitle: 'Sales Invoice',
    routeProps: {},
    parentMenuId: 213,
  },
  5007: {
    icon: 'truck',
    type: 'MaterialCommunityIcons',
    color: 'rgba(182, 202, 239, 0.5)',
    route: Route.OrderList,
    headerTitle: 'Delivery Challan',
    routeProps: {},
    parentMenuId: 214,
  },
  5008: {
    icon: 'mail-forward',
    type: 'FontAwesome',
    color: 'rgba(252, 128, 128,0.5)',
    route: Route.OrderList,
    headerTitle: 'Sales Return',
    routeProps: {},
    parentMenuId: 216,
  },
  5009: {
    icon: 'money',
    type: 'FontAwesome',
    color: 'rgba(141, 224, 144,0.5)',
    // route: Route.ARAPFinanceList,
    headerTitle: 'Receipts',
    routeProps: {inOutFlag: MasterConstants.inOutFlagReceivable},
  },
  5010: {
    icon: 'money',
    type: 'FontAwesome',
    color: 'rgba(247, 205, 143,0.5)',
    // route: Route.ARAPFinanceList,
    headerTitle: 'Payouts',
    routeProps: {inOutFlag: MasterConstants.inOutFlagPayable},
  },
  5012: {
    icon: 'shoppingcart',
    type: 'AntDesign',
    color: 'rgba(141, 224, 144,0.5)',
  },
  5013: {
    icon: 'shoppingcart',
    type: 'AntDesign',
    color: 'rgba(247, 205, 143,0.5)',
  },
  5014: {
    icon: 'truck',
    type: 'FontAwesome',
    color: 'rgba(247, 205, 143,0.5)',
  },
  5015: {
    icon: 'truck',
    type: 'MaterialCommunityIcons',
    color: 'rgba(252, 128, 128,0.5)',
  },
  6000: {
    icon: 'folder-open',
    type: 'FontAwesome',
    color: 'rgba(141, 224, 144,0.5)',
  },
  6001: {
    icon: 'arrow-down',
    type: 'FontAwesome',
    color: 'rgba(247, 205, 143,0.5)',
  },
  6002: {
    icon: 'arrow-up',
    type: 'FontAwesome',
    color: 'rgba(182, 202, 239, 0.5)',
  },
  6003: {
    icon: 'folder',
    type: 'FontAwesome',
    color: 'rgba(252, 128, 128,0.5)',
  },
  1168: {
    icon: 'chart-bar-stacked',
    type: 'MaterialCommunityIcons',
    color: 'rgba(250, 99, 99,0.5)',
  },
  1169: {
    icon: 'line-chart',
    type: 'FontAwesome',
    color: 'rgba(141, 224, 144,0.5)',
  },
  1170: {
    icon: 'phone-paused',
    type: 'MaterialCommunityIcons',
    color: 'rgba(246, 106, 152,0.5)',
  },
  1171: {
    icon: 'volume-control-phone',
    type: 'FontAwesome',
    color: 'rgba(141, 224, 144,0.5)',
  },
  1172: {
    icon: 'product-hunt',
    type: 'FontAwesome',
    color: 'rgba(120, 122, 158,0.5)',
    route: Route.ProductList,
    headerTitle: 'Top 3 Ageing Products',
  },
  1173: {
    icon: 'user',
    type: 'FontAwesome',
    color: 'rgba(229, 139, 73,0.5)',
  },
  5030: {
    icon: 'shopping-cart',
    type: 'Entypo',
    color: 'rgba(141, 224, 144,0.5)',
    route: Route.OrderList,
    headerTitle: 'Cash Sale',
    routeProps: {},
    parentMenuId: 227,
  },
  5031: {
    icon: 'shopping-cart',
    type: 'Entypo',
    color: 'rgba(141, 224, 144,0.5)',
    route: Route.OrderList,
    headerTitle: 'Cash Sale Return',
    routeProps: {},
    parentMenuId: 900,
  },
  default: {
    icon: 'shopping-cart',
    type: 'Entypo',
    color: 'rgba(252, 128, 128,0.5)',
  },
};
// create a component
export default class LandingDashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      dashboardList: [],
      productExponent: null,
      session: sessionInfo({type: 'get', key: 'loginInfo'}),
      filterDate: DateFormat(new Date(), DATE_TIME_UI),
      conditions: [],
      isServiceCalled: false,
      isDateTimePickerVisible: false,
    };
  }

  componentDidMount() {
    this._initHeader();
    this._initDashboardType();
  }

  _initDashboardType() {
    this.onRefresh();
    this._getLandingDashboardDetails();
  }

  _initHeader() {
    this.props.navigation.setParams({
      headerTitle:
        this.props.route.params?.headerTitle ?? messages('dashboard'),
      headerType:
        this.props.route.params?.headerType ?? HeaderTypes.HeaderHomeMenu,
    });
    this.props.navigation.setOptions(renderHeaderView(this));
  }

  componentWillUnmount() {
    // this.backHandler.remove();
  }

  _getLandingDashboardDetails() {
    var filter = {
      conditions: [
        {
          column: 'dashboard_id',
          value: '490',
          operator: '=',
        },
        {
          column: 'layout_id',
          value: this.props.route.params.dashboardType,
          operator: '=',
        },
      ],
    };
    GetLandingDashboardDetails(filter)
      .then(dashboardValues => {
        if (dashboardValues) {
          this._parseDashboardValues(dashboardValues);
        } else {
          this._getAllDashboardDetailFromServer();
        }
      })
      .catch(error => {
        Toast.show(messages(error), Toast.SHORT);
        this.setState({isLoading: false});
      });
  }

  _getAllDashboardDetailFromServer() {
    const filteredDate = this.state.filterDate;
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        this.setState({isLoading: false});
        const filter = {
          selectedDate: filteredDate,
          dashboardType: this.props.route.params.dashboardType,
          menuId: '490',
        };
        getLandingDashboardDetails(this.state.session, filter, isSuccess => {
          if (isSuccess) {
            this.setState({isLoading: false});
            this._getLandingDashboardDetails();
          } else {
            Toast.show(messages('makeInternetConnection'), Toast.SHORT);
          }
        });
      } else {
        this.setState({isLoading: false, isServiceCalled: true});
      }
    });
  }

  _getFilterDate() {
    var date = new Date(this.state.filterDate);
    if (this.props.route.params.dashboardType === MasterConstants.daily) {
      return DateFormat(date, DATE_TIME_UI);
    } else if (
      this.props.route.params.dashboardType === MasterConstants.monthly
    ) {
      return DateFormat(date, 'MMM YYYY');
    } else if (
      this.props.route.params.dashboardType === MasterConstants.yearly
    ) {
      return DateFormat(date, 'YYYY');
    }
    this.setState({filterDate: DateFormat(date, DATE_TIME_UI)});
  }
  _parseDashboardValues(dashboardValues) {
    const values = JSON.parse(dashboardValues);
    const resultList = values.resultList;
    const widgets = values.widget;
    var filter = {
      limit: 1,
      offset: 0,
      orderBy: 'last_updated desc',
    };
    GetAllProductDetails(datas => {
      let productExponent = 2;
      if (datas[0] && datas[0].exponent) {
        productExponent = datas[0].exponent;
      }
      const orderTypes = {};
      widgets.forEach(widget => {
        orderTypes[widget.orderTypeId] = {
          widgetId: widget.operation.id,
          widgetName: widget.operation.name,
        };
      });
      var stockExecute = true;
      const resultCount = resultList.length;
      for (let index = 0; index < resultCount; index++) {
        const result = resultList[index];
        if (result.orderTypeId) {
          result.widgetName = orderTypes[result.orderTypeId].widgetName;
          result.widgetId = orderTypes[result.orderTypeId].widgetId;
          result.icon = widgetValues[result.widgetId]
            ? widgetValues[result.widgetId].icon
            : widgetValues.default.icon;
          result.iconType = widgetValues[result.widgetId]
            ? widgetValues[result.widgetId].type
            : widgetValues.default.type;
          result.color = widgetValues[result.widgetId]
            ? widgetValues[result.widgetId].color
            : widgetValues.default.color;
          result.exponent = this.state.session.info.exponent;
        } else if (stockExecute === true) {
          stockExecute = false;
          resultList.push({
            widgetId: '5011',
            widgetName: 'Opening',
            orderAmount: result.openingStock,
            widgetType: 'Stock',
            icon: widgetValues['6000'].icon,
            iconType: widgetValues['6000'].type,
            color: widgetValues['6000'].color,
            exponent: productExponent,
          });
          resultList.push({
            widgetId: '6001',
            widgetName: 'Stock In',
            orderAmount: result.stockIn,
            widgetType: 'Stock',
            icon: widgetValues['6001'].icon,
            iconType: widgetValues['6001'].type,
            color: widgetValues['6001'].color,
            exponent: productExponent,
          });
          resultList.push({
            widgetId: '6002',
            widgetName: 'Stock Out',
            orderAmount: result.stockOut,
            widgetType: 'Stock',
            icon: widgetValues['6002'].icon,
            iconType: widgetValues['6002'].type,
            color: widgetValues['6002'].color,
            exponent: productExponent,
          });
          resultList.push({
            widgetId: '6003',
            widgetName: 'Closing',
            orderAmount: result.closingStock,
            widgetType: 'Stock',
            icon: widgetValues['6003'].icon,
            iconType: widgetValues['6003'].type,
            color: widgetValues['6003'].color,
            exponent: productExponent,
          });
        }
      }

      const widgetList = resultList.sort((a, b) => {
        return a.widgetId > b.widgetId ? 1 : -1;
      });
      var ageingProduct = true;
      const widgetGroups = [];
      const widgetListCount = widgetList.length;
      for (let index = 0; index < widgetListCount; index++) {
        const widget = widgetList[index];
        if (widget.widgetType) {
          if (widget.orderTypeId === '005') {
            if (ageingProduct && widget.widgetId === '1172') {
              ageingProduct = false;
              let widgetGroupExist = false;
              for (let pos = 0; pos < widgetGroups.length; pos++) {
                const widgetGroup = widgetGroups[pos];
                if (widgetGroup.title === widget.widgetType) {
                  widgetGroup.widgets.push(widget);
                  widgetGroupExist = true;
                  if (!ageingProduct) {
                    continue;
                  }
                  break;
                }
              }
              if (!widgetGroupExist) {
                widgetGroups.push({
                  title: widget.widgetType,
                  widgets: [widget],
                });
              }
            }
          } else {
            let widgetGroupExist = false;
            for (let pos = 0; pos < widgetGroups.length; pos++) {
              const widgetGroup = widgetGroups[pos];
              if (widgetGroup.title === widget.widgetType) {
                widgetGroup.widgets.push(widget);
                widgetGroupExist = true;
                break;
              }
            }
            if (!widgetGroupExist) {
              widgetGroups.push({
                title: widget.widgetType,
                widgets: [widget],
              });
            }
          }
        }
      }
      this.setState({dashboardList: widgetGroups, isLoading: false}, () => {});
    }, filter);
  }

  _handleDatePicked = date => {
    this.setState({filterDate: DateFormat(date, DATE_TIME_UI)}, () => {
      this._getLandingDashboardDetails();
      this._hideDateTimePicker();
    });
  };

  _hideDateTimePicker = () => {
    this.onRefresh();
    this._getLandingDashboardDetails();
    this.setState({
      isDateTimePickerVisible: false,
    });
  };

  dashboardDateDetailsFilter() {
    if (
      DateFormat(this.state.filterDate) > DateFormat(new Date(), DATE_TIME_UI)
    ) {
      return false;
    } else {
      return true;
    }
  }

  nextMonthValidation() {
    if (
      DateFormat(this.state.filterDate, 'MMM YYYY') >
      DateFormat(new Date(), 'MMM YYYY')
    ) {
      return false;
    } else {
      return true;
    }
  }

  onRefresh() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        this.setState({isServiceCalled: false}, () => {
          this._getAllDashboardDetailFromServer();
        });
      } else {
        this._getLandingDashboardDetails();
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    });
  }

  onPressPreviousDateButton = () => {
    var date = new Date(this.state.filterDate);
    if (this.props.route.params.dashboardType === MasterConstants.daily) {
      date.setDate(date.getDate() - 1);
    } else if (
      this.props.route.params.dashboardType === MasterConstants.monthly
    ) {
      date.setMonth(date.getMonth() - 1);
    } else if (
      this.props.route.params.dashboardType === MasterConstants.yearly
    ) {
      date.setMonth(date.getMonth() - 12);
    }
    this.setState(
      {filterDate: DateFormat(date, DATE_TIME_UI), isLoading: true},
      () => {
        this.onRefresh();
      },
    );
  };

  onPressNextDateButton = () => {
    if (this.dashboardDateDetailsFilter()) {
      var date = new Date(this.state.filterDate);
      if (this.props.route.params.dashboardType === MasterConstants.daily) {
        date.setDate(date.getDate() + 1);
      } else if (
        this.props.route.params.dashboardType === MasterConstants.monthly
      ) {
        date.setMonth(date.getMonth() + 1);
      } else if (
        this.props.route.params.dashboardType === MasterConstants.yearly
      ) {
        date.setMonth(date.getMonth() + 12);
      }
      this.setState(
        {filterDate: DateFormat(date, DATE_TIME_UI), isLoading: true},
        () => {
          this.onRefresh();
        },
      );
    }
  };

  onPressWidgetItem = widget => {
    if (this.props.route.params.dashboardType !== MasterConstants.yearly) {
      if (
        widgetValues[widget.widgetId] &&
        widgetValues[widget.widgetId].route
      ) {
        this.props.navigation.navigate(widgetValues[widget.widgetId].route, {
          filterDate: this.state.filterDate,
          orderTypes: [widget.orderTypeId],
          headerType: HeaderTypes.HeaderBack,
          dashboardType: this.props.route.params.dashboardType,
          headerTitle: widgetValues[widget.widgetId].headerTitle,
          parentMenuId: widgetValues[widget.widgetId].parentMenuId,
          ...widgetValues[widget.widgetId].routeProps,
        });
      }
    } else {
      Toast.show(messages('cannotShowYearlyValues'), Toast.SHORT);
    }
  };

  ListEmptyView = () => {
    return (
      <View
        style={{
          width: DEVICE_WIDTH,
          height: (DEVICE_HEIGHT / 100) * 75,
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
        }}>
        <Text style={{textAlign: 'center', color: 'darkgrey', marginTop: 5}}>
          No data found.
        </Text>
      </View>
    );
  };

  render() {
    const {navigation} = this.props;

    return (
      <Container
        style={{
          backgroundColor: 'white',
        }}>
        <StatusBar
          backgroundColor={Styles.statusBar.backgroundColor}
          barStyle="light-content"
        />

        <DateTimePicker
          date={new Date(this.state.filterDate)}
          isVisible={this.state.isDateTimePickerVisible}
          mode="date"
          maximumDate={new Date()}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
        />
        <View
          style={{
            flexDirection: 'row',
            height: 30,
            backgroundColor: 'rgba(237, 239, 242, 0.5)',
          }}>
          <TouchableOpacity
            style={{flex: 1, justifyContent: 'center'}}
            onPress={this.onPressPreviousDateButton.bind(this)}>
            <Icon
              name="chevron-circle-left"
              type="FontAwesome"
              style={{
                fontSize: 27,
                color: Colors.headerBackground,
                marginLeft: 15,
                alignSelf: 'flex-start',
              }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.setState({
                isDateTimePickerVisible: true,
              })
            }
            style={{
              flex: 1,
              justifyContent: 'center',
              marginRight: 10,
              marginLeft: 10,
              fontSize: 16,
              marginTop: 5,
            }}>
            <Text style={{textAlign: 'center'}}>{this._getFilterDate()}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.onPressNextDateButton.bind(this)}
            style={{
              flex: 1,
              justifyContent: 'center',
            }}>
            <Icon
              name="chevron-circle-right"
              type="FontAwesome"
              style={{
                fontSize: 27,
                color: Colors.headerBackground,
                alignSelf: 'flex-end',
                marginRight: 15,
              }}
            />
          </TouchableOpacity>
        </View>
        <FlatList
          extraData={this.state}
          data={this.state.dashboardList}
          ItemSeparatorComponent={this.FlatListItemSeparator}
          ListEmptyComponent={this.ListEmptyView.bind(this)}
          onRefresh={() => this.onRefresh()}
          refreshing={this.state.isLoading}
          renderItem={({item}) => (
            <LandingDashboardItemView
              dashboard={item}
              navigation={navigation}
              onPressWidgetItem={this.onPressWidgetItem.bind(this)}
            />
          )}
          keyExtractor={(item, index) => index.toString()}
        />
      </Container>
    );
  }
}
