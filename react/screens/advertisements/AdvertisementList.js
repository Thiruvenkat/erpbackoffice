/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Image,
  StyleSheet,
  Dimensions,
  FlatList,
  StatusBar,
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import {
  Card,
  CardItem,
  Left,
  Body,
  Text,
  Icon,
  Button,
  Fab,
  Root,
} from 'native-base';
import Toast from 'react-native-simple-toast';
import Route from '../../router/Route';
import MasterConstants from '../../constant/MasterConstants';
import {GetAllAdvertisement} from './dbHelper/AdvertisementDB';
import {getAdvertisementDetails} from '../../controller/SyncDataController';
import PreparingProgress from 'react-native-erp-mobile-library/react/component/PreparingProgress';

import Loader from 'react-native-erp-mobile-library/react/component/Loader';
import {renderHeaderView} from 'react-native-erp-mobile-library/react/util/HeaderUtil';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import {sessionInfo} from 'react-native-erp-mobile-library';
import {messages} from 'react-native-erp-mobile-library/react/i18n/i18n';
import Colors from 'react-native-erp-mobile-library/react/style/Colors';
import {Styles} from 'react-native-erp-mobile-library/react/style/Styles';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

// create a component
class AdvertisementList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      advertisementList: [],
      isLoading: false,
      isConnected: true,
      loadMore: false,
      session: sessionInfo({type: 'get', key: 'loginInfo'}),
      offset: 0,
      isServiceCalled: false,
      limit: MasterConstants.screenFetchLimit,
      isRefresh: true,
    };
  }

  componentDidMount() {
    this._initHeader();
    this._getAdvertisementDetails();
  }

  _initHeader() {
    this.props.navigation.setParams({
      headerTitle:
        this.props.route.params?.headerTitle ?? messages('advertisement'),
      headerType:
        this.props.route.params?.headerType ?? HeaderTypes.HeaderHomeMenu,
    });
    this.props.navigation.setOptions(renderHeaderView(this));
  }

  _getAdvertisementDetails() {
    var filter = {
      limit: this.state.limit,
      offset: this.state.offset,
    };
    GetAllAdvertisement(datas => {
      if (datas.length > 0) {
        this.setState(
          {
            isServiceCalled: false,
            advertisementList:
              this.state.offset === 0
                ? datas
                : this.state.advertisementList.concat(datas),
            offset: this.state.offset + datas.length,
          },
          () => {
            this.setState({
              isLoading: false,
              loadMore: false,
              isRefresh: false,
            });
          },
        );
      } else if (!this.state.isServiceCalled) {
        this.setState({isRefresh: true}, () => {
          this._getAdvertisementDetailsFromServer();
        });
      }
    }, filter);
  }

  _getAdvertisementDetailsFromServer() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        getAdvertisementDetails(this.state.session, isSuccess => {
          this.setState({isServiceCalled: true, isRefresh: true});
          if (isSuccess) {
            this._getAdvertisementDetails();
          } else {
            this.setState({
              isLoading: false,
              loadMore: false,
              isRefresh: false,
            });
          }
        });
      } else {
        this.setState({isLoading: false, loadMore: false, isRefresh: false});
      }
    });
  }

  onRefresh() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        this.setState(
          {offset: 0, advertisementList: [], isServiceCalled: false},
          () => {
            this._getAdvertisementDetailsFromServer();
          },
        );
      } else {
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    });
  }

  _renderItem({item}) {
    return (
      <AdItemView
        navigation={this.props.navigation}
        advertisementId={item.advertisementId}
        averageRating={item.averageRating}
        description={item.description}
        imagePath={item.imagePath}
        tag={item.tag}
        totalLike={item.totalLike}
        validFrom={item.validFrom}
        validTill={item.validTill}
      />
    );
  }

  ListEmptyView = () => {
    return (
      <View
        style={{
          width: DEVICE_WIDTH,
          height: DEVICE_HEIGHT - 80,
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
        }}>
        <Icon
          name="picture-o"
          type="FontAwesome"
          style={{color: Colors.headerBackground, fontSize: 40}}
        />
        <Text
          style={{
            textAlign: 'center',
            color: 'darkgrey',
            justifyContent: 'center',
          }}>
          No Advertisement Found
        </Text>
      </View>
    );
  };

  render() {
    if (this.state.isLoading) {
      return <PreparingProgress />;
    } else {
      return (
        <Root>
          <View
            padder
            style={{
              backgroundColor: Styles.appViewBackground.backgroundColor,
            }}>
            <Loader loading={this.state.isLoading} />
            <StatusBar
              backgroundColor={Styles.statusBar.backgroundColor}
              barStyle="light-content"
            />
            <FlatList
              extraData={this.state}
              data={this.state.advertisementList}
              onEndReachedThreshold={0.1}
              onEndReached={({distanceFromEnd}) => {
                if (
                  !this.state.loadMore &&
                  this.state.offset >= this.state.limit
                ) {
                  this.setState({loadMore: true}, () => {
                    this._getAdvertisementDetails();
                  });
                }
              }}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              renderItem={item => this._renderItem(item)}
              keyExtractor={(item, index) => index.toString()}
              onRefresh={() => this.onRefresh()}
              refreshing={this.state.isRefresh}
              ListEmptyComponent={this.ListEmptyView}
            />
          </View>
          <Fab
            style={{backgroundColor: Colors.headerBackground, size: 5}}
            onPress={() => {
              this.props.navigation.navigate(Route.PostAd, {
                headerTitle: messages('postAdvertisement'),
                headerType: HeaderTypes.HeaderBack,
              });
            }}>
            <Icon
              type="FontAwesome"
              name="plus"
              style={{
                textAlign: 'center',
                alignItems: 'center',
                justifyContent: 'center',
                color: 'white',
                fontSize: 22,
              }}
            />
          </Fab>
        </Root>
      );
    }
  }
}

class AdItemView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      advertisementId: '',
      averageRating: 0,
      description: '',
      imagePath: '',
      tag: '',
      totalLike: 0,
      validFrom: '',
      validTill: '',
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({...nextProps});
  }

  render() {
    return (
      <View>
        <Card style={styles.card}>
          <CardItem>
            <Image
              resizeMode="stretch"
              style={{
                width: DEVICE_WIDTH,
                height: DEVICE_WIDTH / 2 - 20,
                flex: 1,
                alignSelf: 'stretch',
              }}
              source={{uri: this.state.imagePath}}
            />
          </CardItem>
          <Text style={{color: 'darkgrey', fontSize: 12, paddingLeft: 16}}>
            {'Valid:' + this.state.validFrom + '-' + this.state.validTill}
          </Text>
          <Text style={{color: 'darkgrey', fontSize: 12, paddingLeft: 16}}>
            {this.state.description}
          </Text>
          <CardItem>
            <Left>
              <Button
                iconLeft={true}
                style={{height: 30, backgroundColor: 'white'}}>
                <Icon
                  type="FontAwesome"
                  name="thumbs-up"
                  style={{color: Colors.headerBackground, fontSize: 18}}
                />
                <Text style={{color: 'darkgrey'}}>{this.state.totalLike}</Text>
              </Button>
              <Body>
                <Button
                  iconLeft={true}
                  style={{height: 30, backgroundColor: 'white'}}>
                  <Icon
                    type="FontAwesome"
                    name="star"
                    style={{color: Colors.headerBackground, fontSize: 18}}
                  />
                  <Text style={{color: 'darkgrey'}}>
                    {this.state.averageRating}
                  </Text>
                </Button>
              </Body>
            </Left>
          </CardItem>
        </Card>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  myStarStyle: {
    fontSize: 24,
    color: 'black',
    backgroundColor: 'transparent',
    textShadowColor: 'darkgrey',
    textShadowOffset: {width: 1, height: 1},
    textShadowRadius: 2,
  },
  myEmptyStarStyle: {
    fontSize: 24,
    color: 'darkgrey',
  },
  card: {
    backgroundColor: 'white',
    borderRadius: 3,
    elevation: 4,
  },
  circle: {
    width: 10,
    height: 10,
    borderRadius: 10 / 2,
    backgroundColor: 'red',
  },
  active_icon: {
    fontSize: 22,
    color: 'darkgrey',
    paddingLeft: 10,
    paddingRight: 10,
  },
  icon: {
    fontSize: 22,
    color: 'black',
    paddingLeft: 10,
    paddingRight: 10,
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  grid_contactItem: {
    marginLeft: 5,
    marginRight: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'transparent',
    color: 'black',
  },
  grid_leftContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
  },
  grid_rightContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    backgroundColor: 'transparent',
  },
});

//make this component available to the app
export default AdvertisementList;
