import {TABLE} from 'react-native-erp-mobile-library/react/database/DBConstants';
import {openDatabase} from 'react-native-erp-mobile-library/react/database/DatabaseHandler';
import {QueryBuilder} from 'react-native-erp-mobile-library/react/database/DBUtilities';
import AppConstants from '../../../constant/AppConstants';

export function GetAllAdvertisement(callback, filter) {
  let DBInstance = openDatabase(AppConstants.affordeBackOfficeDB);
  DBInstance.transaction(db => {
    var query = QueryBuilder(
      'SELECT advertisement_id,average_rating,description,image_path,tag,total_like,valid_from,valid_till,tenant_id,json FROM ' +
        TABLE.Advertisement,
      filter,
    );
    db.executeSql(
      query,
      [],
      (tx, results) => {
        var advertisement = [];
        for (let index = 0; index < results.rows.length; index++) {
          const row = results.rows.item(index);
          let ad = {
            advertisementId: row.advertisement_id,
            averageRating: row.average_rating,
            description: row.description,
            imagePath: row.image_path,
            tag: row.tag,
            totalLike: row.total_like,
            validFrom: row.valid_from,
            validTill: row.valid_till,
            tenantId: row.tenant_id,
            json: row.json,
          };
          advertisement[index] = ad;
        }
        callback(advertisement);
      },
      error => {
        console.log(
          'Select Advertisement Error occured :' + JSON.stringify(error),
        );
        callback([]);
      },
    );
  });
}

export function InsertAdvertisement(datas, callback) {
  if (datas.length > 0) {
    let DBInstance = openDatabase(AppConstants.affordeBackOfficeDB);
    DBInstance.transaction(db => {
      let length = datas.length;
      for (let index = 0; index < length; index++) {
        let values = datas[index];
        db.executeSql(
          'INSERT OR REPLACE INTO ' +
            TABLE.Advertisement +
            ' (' +
            'advertisement_id,' +
            'average_rating,' +
            'description,' +
            'image_path,' +
            'tag,' +
            'total_like,' +
            'valid_from,' +
            'valid_till,' +
            'tenant_id,' +
            'json) VALUES (?,?,?,?,?,?,?,?,?,?)',
          [
            values.advertisementId,
            values.averageRating,
            values.description.replace("'", '`'),
            values.imagePath,
            values.tag,
            values.totalLike,
            values.validFrom,
            values.validTill,
            values.tenantId,
            values.json.replace("'", '`'),
          ],
          () => {
            // insert success
            console.log(
              'Insert Advertisement Successfully :' + values.advertisementId,
            );
          },
          error => {
            // insert failure
            console.log(
              'Insert Advertisement Error occured :' + JSON.stringify(error),
            );
          },
        );
      }
      callback(true);
    });
  }
}
