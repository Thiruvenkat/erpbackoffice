/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  StatusBar,
  Dimensions,
} from 'react-native';
import AppConstants from '../../constant/AppConstants';
import {
  Input,
  Card,
  Textarea,
  Root,
  Container,
  Content,
  Footer,
  Toast,
} from 'native-base';
import Controller from 'react-native-erp-mobile-library/react/controller/Controller';
import Loader from 'react-native-erp-mobile-library/react/component/Loader';
import {
  DateFormat,
  DATE_TIME_SERVER,
  DATE_TIME_UI,
} from 'react-native-erp-mobile-library/react/common/Common';
import {sessionInfo} from 'react-native-erp-mobile-library';
import Colors from 'react-native-erp-mobile-library/react/style/Colors';
import ImagePicker from 'react-native-image-picker';
import {messages} from 'react-native-erp-mobile-library/react/i18n/i18n';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import {renderHeaderView} from 'react-native-erp-mobile-library/react/util/HeaderUtil';
import DateTimePicker from 'react-native-modal-datetime-picker';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const options = {
  title: 'Select a photo',
  takePhotoButtonTitle: 'Take a photo',
  chooseFromLibraryButtonTitle: 'Choose from gallery',
  quality: 1,
};

// create a component
class PostAdvertisement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      tags: '',
      session: sessionInfo({type: 'get', key: 'loginInfo'}),
      description: '',
      imageSource: null,
      imageResponse: null,
      isDateTimePickerVisible: false,
      validDateFrom: DateFormat(new Date(), DATE_TIME_SERVER),
      validDateTill: DateFormat(new Date(), DATE_TIME_SERVER),
    };
  }

  componentDidMount() {
    this._initHeader();
  }
  _initHeader() {
    this.props.navigation.setParams({
      headerTitle:
        this.props.route.params?.headerTitle ?? messages('postAdvertisement'),
      headerType:
        this.props.route.params?.headerType ?? HeaderTypes.HeaderHomeMenu,
    });
    this.props.navigation.setOptions(renderHeaderView(this));
  }

  componentWillUnmount() {
    // this.backHandler.remove();
  }

  _showFromDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: true, fromDatepicker: true});
  };

  _showToDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: true});
  };

  _hideDateTimePicker = () => {
    this.setState({
      isDateTimePickerVisible: false,
      fromDatepicker: false,
      toDatepicker: false,
    });
  };

  _handleDatePicked = date => {
    if (this.state.fromDatepicker) {
      this.setState({
        validDateFrom: DateFormat(date, DATE_TIME_SERVER),
      });
    } else {
      this.setState({
        validDateTill: DateFormat(date, DATE_TIME_SERVER),
      });
    }
    this._hideDateTimePicker();
  };

  selectPhoto() {
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        //response
      } else if (response.error) {
        //user cancelled
      } else {
        const source = {uri: response.uri};
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.setState({
          imageSource: source,
          imageResponse: response,
        });
      }
    });
  }
  _saveAdvertisement() {
    if (this.state.imageResponse == null) {
      Toast.show({
        text: "Can't able to submit without image",
        duration: 2000,
      });
    } else {
      this.setState({isLoading: true});
      const tenantId = this.state.session.info[AppConstants.keyTenantId];
      return new Controller()
        .playPostMultiPartApi(
          this.state.session.loginToken,
          this.state.session.publicIp + AppConstants.createAdvertisement,
          [
            {
              name: 'imageUpload0',
              filename: this.state.imageResponse.fileName,
              data: this.state.imageResponse.data,
              type: this.state.imageResponse.type,
            },
            {name: 'tenantId', data: tenantId},
            {name: 'tag', data: this.state.tags},
            {
              name: 'validFrom',
              data: this.state.validDateFrom.split('T')[0].concat(' 00:00:00'),
            },
            {
              name: 'validTill',
              data: this.state.validDateTill.split('T')[0].concat(' 23:59:59'),
            },
            {name: 'description', data: this.state.description},
          ],
        )
        .then(response => {
          this.setState({isLoading: false});
          if (response.respInfo.status === 200) {
            this.props.navigation.goBack();
          } else {
            Toast.show({
              text: 'Problem occurred!',
              duration: 3000,
            });
          }
        });
    }
  }

  render() {
    return (
      <Root>
        <Container>
          <Content padder style={{backgroundColor: '#f4f6f9'}}>
            <StatusBar backgroundColor="#047bd1" barStyle="light-content" />
            <Loader loading={this.state.isLoading} />
            <DateTimePicker
              isVisible={this.state.isDateTimePickerVisible}
              mode="date"
              is24Hour={false}
              onConfirm={this._handleDatePicked}
              onCancel={this._hideDateTimePicker}
            />
            <Card style={{padding: 5}}>
              <View style={{flexDirection: 'row', paddingVertical: 8}}>
                <Text style={[styles.leftContainer]}>Tags</Text>
                <Input
                  style={[
                    styles.rightContainer,
                    {color: 'black', fontSize: 14, backgroundColor: '#f2f3f4'},
                  ]}
                  placeholder="ex. Mega sale, Offer 25%"
                  onChangeText={text => {
                    this.setState({tags: text});
                  }}
                />
              </View>
              <View style={{flexDirection: 'row', paddingVertical: 8}}>
                <Text style={[styles.leftContainer]}>Description</Text>
                <Textarea
                  rowSpan={4}
                  style={[
                    styles.rightContainer,
                    {color: 'black', fontSize: 14, backgroundColor: '#f2f3f4'},
                  ]}
                  placeholder="About your advertisement"
                  onChangeText={text => {
                    this.setState({description: text});
                  }}
                />
              </View>
              <View style={{flexDirection: 'row', paddingVertical: 8}}>
                <Text style={[styles.leftContainer]}>Valid From</Text>
                <TouchableOpacity
                  style={[
                    styles.rightContainer,
                    {color: 'black', fontSize: 14},
                  ]}
                  onPress={() =>
                    this.setState({
                      isDateTimePickerVisible: true,
                      fromDatepicker: true,
                    })
                  }>
                  <Text
                    style={{
                      color: 'black',
                      fontSize: 14,
                      backgroundColor: '#f2f3f4',
                      padding: 3,
                    }}>
                    {DateFormat(this.state.validDateFrom, DATE_TIME_UI)}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{flexDirection: 'row', paddingVertical: 8}}>
                <Text style={[styles.leftContainer]}>Valid Till</Text>
                <TouchableOpacity
                  style={[styles.rightContainer]}
                  onPress={() =>
                    this.setState({
                      isDateTimePickerVisible: true,
                      toDatepicker: true,
                    })
                  }>
                  <Text
                    style={{
                      color: 'black',
                      fontSize: 14,
                      backgroundColor: '#f2f3f4',
                      padding: 3,
                    }}>
                    {DateFormat(this.state.validDateTill, DATE_TIME_UI)}
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  backgroundColor: 'trasparent',
                  marginRight: 5,
                  marginLeft: 15,
                  height: 1,
                }}
              />
              <View style={{justifyContent: 'center'}}>
                <TouchableOpacity onPress={this.selectPhoto.bind(this)}>
                  <Image
                    resizeMode="stretch"
                    source={
                      this.state.imageSource != null
                        ? this.state.imageSource
                        : require('../../../assets/no-image.png')
                    }
                    style={styles.imageStyle}
                  />
                </TouchableOpacity>
              </View>
            </Card>
          </Content>
          <Footer style={{backgroundColor: Colors.headerBackground}}>
            <TouchableOpacity onPress={this._saveAdvertisement.bind(this)}>
              <Text
                style={{
                  marginTop: 5,
                  fontSize: 20,
                  fontWeight: 'bold',
                  padding: 10,
                  width: DEVICE_WIDTH,
                  backgroundColor: 'transparent',
                  color: 'white',
                  textAlign: 'center',
                }}>
                Submit
              </Text>
            </TouchableOpacity>
          </Footer>
        </Container>
      </Root>
    );
  }
}

const styles = StyleSheet.create({
  imageStyle: {
    height: DEVICE_HEIGHT / 4,
    width: '90%',
    alignSelf: 'stretch',
  },
  leftContainer: {
    color: Colors.headerBackground,
    fontSize: 16,
    fontWeight: '900',
    paddingLeft: 10,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
  },
  rightContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
});

export default PostAdvertisement;
