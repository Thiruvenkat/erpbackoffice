/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  StyleSheet,
  StatusBar,
  ActivityIndicator,
  FlatList,
  View,
  Text,
  TouchableOpacity,
  TouchableNativeFeedback,
  DeviceEventEmitter,
} from 'react-native';
import {Icon, Left, Body, Fab, Card, CardItem, Root} from 'native-base';
import Toast from 'react-native-simple-toast';
import DateTimePicker from 'react-native-modal-datetime-picker';

import AppConstants from '../../constant/AppConstants';
import MasterConstants from '../../constant/MasterConstants';
import Route from '../../router/Route';
import {GetAllOrderService} from 'react-native-erp-mobile-library/react/screens/services/dbHelper/ServiceOrderDB';
import {getAllOrderService} from '../../controller/SyncDataController';
import {
  DateFormat,
  DATE_TIME_LAST_UPDATED,
  DATE_TIME_UI,
  DATE_UI,
} from 'react-native-erp-mobile-library/react/common/Common';
import Loader from 'react-native-erp-mobile-library/react/component/Loader';
import PreparingProgress from 'react-native-erp-mobile-library/react/component/PreparingProgress';
import {messages} from 'react-native-erp-mobile-library/react/i18n/i18n';
import {GetCustomersBasicDetails} from 'react-native-erp-mobile-library/react/screens/customers/dbHelper/CustomerDB';
import {ngetAllCustomerDetails} from 'react-native-erp-mobile-library/react/controller/SyncDataController';
import {sessionInfo} from 'react-native-erp-mobile-library';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import {renderHeaderView} from 'react-native-erp-mobile-library/react/util/HeaderUtil';
import Colors from 'react-native-erp-mobile-library/react/style/Colors';
import {
  Styles,
  IconColor,
  STATUS_COLOR,
} from 'react-native-erp-mobile-library/react/style/Styles';
import NetInfo from '@react-native-community/netinfo';

export default class TaskList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isPreparing: true,
      isLoading: true,
      isRefresh: false,
      loadMore: false,
      isConnected: true,
      session: sessionInfo({type: 'get', key: 'loginInfo'}),
      orderServiceList: [],
      filterDate: DateFormat(new Date(), DATE_TIME_UI),
      isDateTimePickerVisible: false,
      fetchAll: false,
      customerDetaiList: null,
      offset: 0,
      limit: 10,
      conditions: [
        {
          value: [
            MasterConstants.serviceRequestPending,
            MasterConstants.mobileResponseId,
            MasterConstants.waitingForApproval,
          ],
          column: 'status_id',
          operator: 'in',
          dataType: 'String',
        },
      ],
    };
  }

  componentDidMount() {
    this._initHeader();
    this._getAllOrderService(datas => {
      if (datas.length > 0) {
        this.setState({
          orderServiceList:
            this.state.offset === 0
              ? datas
              : this.state.orderServiceList.concat(datas),
          isLoading: false,
          loadMore: false,
          isPreparing: false,
          offset: this.state.offset + datas.length,
        });
      } else {
        this._getAllOrderServiceFromServer(null);
      }
    });
    this.onGoBackService = this.onGoBackService.bind(this);
  }

  _initHeader() {
    this.props.navigation.setParams({
      headerTitle:
        this.props.route.params?.headerTitle ?? 'Pending Allocations',
      headerType:
        this.props.route.params?.headerType ?? HeaderTypes.HeaderHomeMenu,
    });
    this.props.navigation.setOptions(renderHeaderView(this));
  }

  _getAllOrderService(callback) {
    var filter = {
      limit: this.state.limit,
      offset: this.state.offset,
      orderBy: 'last_updated desc',
      conditions: [...this.state.conditions],
    };
    if (!this.state.fetchAll) {
      filter.conditions.push({
        value: this.state.filterDate + 'T00:00:00.000',
        column: 'last_updated',
        operator: '>=',
        dataType: 'String',
      });
      filter.conditions.push({
        value: this.state.filterDate + 'T23:59:59.999',
        column: 'last_updated',
        operator: '<=',
        dataType: 'String',
      });
    }
    GetAllOrderService(
      AppConstants.affordeBackOfficeDB,
      datas => {
        callback(datas);
      },
      filter,
    );
  }

  _getAllOrderServiceFromServer(dataids) {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        let syncValue = {
          limit: this.state.limit,
          offset: this.state.offset,
          conditions: [],
        };
        if (dataids == null) {
          syncValue.conditions.push({
            value: MasterConstants.serviceRequestPending,
            column: 'status.statusId',
            operator: 'in',
            dataType: 'String',
          });
          if (!this.state.fetchAll) {
            syncValue.conditions.push({
              value:
                '' + new Date(this.state.filterDate + 'T00:00:00').getTime(),
              column: 'lastUpdatedDateTime',
              operator: '>=',
              dataType: 'Date',
            });
            syncValue.conditions.push({
              value:
                '' + new Date(this.state.filterDate + 'T23:59:59').getTime(),
              column: 'lastUpdatedDateTime',
              operator: '<=',
              dataType: 'Date',
            });
          }
        }
        getAllOrderService(this.state.session, syncValue, isSuccess => {
          if (isSuccess) {
            this._getAllOrderService(datas => {
              if (datas.length > 0) {
                this.setState({
                  orderServiceList:
                    this.state.offset === 0
                      ? datas
                      : this.state.orderServiceList.concat(datas),
                  isLoading: false,
                  loadMore: false,
                  isPreparing: false,
                  isRefresh: false,
                  offset: this.state.offset + datas.length,
                });
              } else {
                this.setState({
                  orderServiceList: datas,
                  isLoading: false,
                  loadMore: false,
                  isPreparing: false,
                  isRefresh: false,
                  offset: datas.length,
                });
              }
            });
          } else {
            this.setState({
              isLoading: false,
              loadMore: false,
              isPreparing: false,
              isRefresh: false,
            });
          }
        });
      } else {
        this.setState({
          isLoading: false,
          loadMore: false,
          isPreparing: false,
          isRefresh: false,
        });
        console.log('make internet connection..!');
      }
    });
  }

  onRefresh() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        this.setState({offset: 0}, () => {
          this._getAllOrderServiceFromServer('null');
        });
      } else {
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    });
  }

  onGoBackService() {
    this.setState({offset: 0}, () => {
      this._getAllOrderService(datas => {
        if (datas.length > 0) {
          this.setState({
            orderServiceList:
              this.state.offset === 0
                ? datas
                : this.state.orderServiceList.concat(datas),
            isLoading: false,
            isPreparing: false,
            loadMore: false,
            offset: this.state.offset + datas.length,
          });
        } else {
          this._getAllOrderServiceFromServer(null);
        }
      });
    });
  }

  _handleDatePicked = date => {
    this.setState(
      {
        orderServiceList: [],
        offset: 0,
        fetchAll: false,
        isLoading: true,
        filterDate: DateFormat(date, DATE_TIME_UI),
      },
      () => {
        this._getAllOrderService(datas => {
          if (datas.length > 0) {
            this.setState({
              orderServiceList: datas,
              isLoading: false,
              isPreparing: false,
              loadMore: false,
              offset: this.state.offset + datas.length,
            });
          } else {
            this._getAllOrderServiceFromServer(null);
          }
        });
        this._hideDateTimePicker();
      },
    );
  };

  _hideDateTimePicker = () => {
    this.setState({
      isDateTimePickerVisible: false,
    });
  };

  renderItem({item, index}) {
    return (
      <ServiceOrderItemView
        itemDetails={item}
        session={this.state.session}
        navigation={this.props.navigation}
        index={index}
        onGoBackService={this.onGoBackService}
      />
    );
  }

  ListEmptyView = () => {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
          marginTop: 200,
          backgroundColor: 'white',
        }}>
        <Icon
          name="tasks"
          type="FontAwesome"
          style={{color: Colors.headerBackground, fontSize: 40}}
        />
        <Text
          style={{
            textAlign: 'center',
            color: 'darkgrey',
            justifyContent: 'center',
          }}>
          No service(s) found!
        </Text>
      </View>
    );
  };

  render() {
    if (this.state.isPreparing) {
      return <PreparingProgress />;
    } else {
      return (
        <Root>
          <View
            padder
            style={{
              backgroundColor: Styles.appViewBackground.backgroundColor,
              ...StyleSheet.absoluteFillObject,
            }}>
            <StatusBar
              backgroundColor={Styles.statusBar.backgroundColor}
              barStyle="light-content"
            />
            <Loader loading={this.state.isLoading} />
            <DateTimePicker
              date={new Date(this.state.filterDate)}
              isVisible={this.state.isDateTimePickerVisible}
              mode="date"
              onConfirm={this._handleDatePicked}
              onCancel={this._hideDateTimePicker}
            />
            <Card>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                }}>
                <TouchableNativeFeedback
                  onPress={() => {
                    this.setState(
                      {
                        orderServiceList: [],
                        offset: 0,
                        fetchAll: true,
                        isLoading: true,
                        filterDate: DateFormat(new Date(), DATE_TIME_UI),
                      },
                      () => {
                        this._getAllOrderService(datas => {
                          if (datas.length > 0) {
                            this.setState({
                              orderServiceList: datas,
                              isLoading: false,
                              isPreparing: false,
                              loadMore: false,
                              offset: this.state.offset + datas.length,
                            });
                          } else {
                            this._getAllOrderServiceFromServer(null);
                          }
                        });
                      },
                    );
                  }}>
                  <Text
                    style={{
                      margin: 10,
                      padding: 5,
                      paddingLeft: 20,
                      paddingRight: 20,
                      color: '#fff',
                      backgroundColor: Colors.headerBackground,
                      fontSize: 18,
                      fontWeight: 'bold',
                    }}>
                    All
                  </Text>
                </TouchableNativeFeedback>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    padding: 10,
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      var date = new Date(this.state.filterDate);
                      date.setDate(date.getDate() - 1);
                      this.setState(
                        {
                          orderServiceList: [],
                          offset: 0,
                          fetchAll: false,
                          isLoading: true,
                          filterDate: DateFormat(date, DATE_TIME_UI),
                        },
                        () => {
                          this._getAllOrderService(datas => {
                            if (datas.length > 0) {
                              this.setState({
                                orderServiceList: datas,
                                isLoading: false,
                                isPreparing: false,
                                loadMore: false,
                                offset: this.state.offset + datas.length,
                              });
                            } else {
                              this._getAllOrderServiceFromServer(null);
                            }
                          });
                        },
                      );
                    }}>
                    <Icon
                      name="chevron-circle-left"
                      type="FontAwesome"
                      style={{fontSize: 27, color: Colors.headerBackground}}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      marginRight: 10,
                      marginLeft: 10,
                      fontSize: 16,
                      marginTop: 5,
                    }}
                    onPress={() =>
                      this.setState({
                        isDateTimePickerVisible: true,
                      })
                    }>
                    <Text>{DateFormat(this.state.filterDate, DATE_UI)}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      var date = new Date(this.state.filterDate);
                      date.setDate(date.getDate() + 1);
                      this.setState(
                        {
                          orderServiceList: [],
                          offset: 0,
                          fetchAll: false,
                          isLoading: true,
                          filterDate: DateFormat(date, DATE_TIME_UI),
                        },
                        () => {
                          this._getAllOrderService(datas => {
                            if (datas.length > 0) {
                              this.setState({
                                orderServiceList: datas,
                                isLoading: false,
                                isPreparing: false,
                                loadMore: false,
                                offset: this.state.offset + datas.length,
                              });
                            } else {
                              this._getAllOrderServiceFromServer(null);
                            }
                          });
                        },
                      );
                    }}>
                    <Icon
                      name="chevron-circle-right"
                      type="FontAwesome"
                      style={{
                        fontSize: 27,
                        color: Colors.headerBackground,
                        marginRight: 40,
                      }}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <View
                style={{
                  backgroundColor: 'lightgrey',
                  height: 0.5,
                }}
              />
            </Card>
            <FlatList
              extraData={this.state}
              data={this.state.orderServiceList}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              renderItem={item => this.renderItem(item)}
              onEndReachedThreshold={0.1}
              onEndReached={({distanceFromEnd}) => {
                if (
                  !this.state.loadMore &&
                  this.state.offset >= this.state.limit
                ) {
                  this.setState({loadMore: true}, () => {
                    this._getAllOrderService(datas => {
                      if (datas.length > 0) {
                        this.setState({
                          orderServiceList:
                            this.state.offset === 0
                              ? datas
                              : this.state.orderServiceList.concat(datas),
                          isLoading: false,
                          isPreparing: false,
                          loadMore: false,
                          offset: this.state.offset + datas.length,
                        });
                      } else {
                        this._getAllOrderServiceFromServer(null);
                      }
                    });
                  });
                }
              }}
              keyExtractor={(item, index) => index.toString()}
              onRefresh={() => this.onRefresh()}
              refreshing={this.state.isRefresh}
              ListEmptyComponent={this.ListEmptyView}
              ListFooterComponent={
                this.state.loadMore ? (
                  <View
                    style={{
                      padding: 7,
                      flex: 1,
                      flexDirection: 'row',
                      borderRadius: 2,
                      backgroundColor: Colors.headerBackground,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <ActivityIndicator size="small" color="white" />
                    <Text style={{color: 'white', fontWeight: 'bold'}}>
                      {' Loading..'}
                    </Text>
                  </View>
                ) : null
              }
            />
          </View>
          <Fab
            style={{backgroundColor: Colors.headerBackground, size: 5}}
            direction="up"
            position="bottomRight"
            onPress={() => {
              this.props.navigation.navigate(Route.Booking, {
                headerTitle: 'Booking',
                headerType: HeaderTypes.HeaderBack,
                getLogInCustomer: false,
              });
            }}>
            <Icon type="FontAwesome" name="plus" style={styles.fabicon} />
          </Fab>
        </Root>
      );
    }
  }
}

class ServiceOrderItemView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props,
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({...nextProps});
  }
  viewTask() {
    let customerId = this.state.itemDetails.order_to_id;
    this._getCustomerDetails(customerId);
  }
  _getCustomerDetails(customerId) {
    var filter = {
      conditions: [
        {
          value: customerId,
          column: 'customer_id',
          operator: '=',
        },
      ],
    };
    GetCustomersBasicDetails(
      this.state.session.databaseName,
      datas => {
        if (datas.length > 0) {
          this.setState({customerDetaiList: datas[0]}, () => {
            this.props.navigation.navigate(Route.ViewTask, {
              orderId: this.state.itemDetails.order_service_id,
              mobileNo: this.state.customerDetaiList.mobile_no,
              hideFieldwork: false,
            });
          });
        } else if (customerId) {
          this._getCustomerDetailsFromServer(customerId);
        }
      },
      filter,
    );
  }
  _getCustomerDetailsFromServer(customerId) {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        let syncValue = {
          limit: 500,
          offset: 0,
          conditions: [
            {
              column: 'cr.customer_id',
              value: customerId,
              dataType: 'String',
              operator: '=',
            },
          ],
          staticConditions: [
            {
              column: 'customerTypeId',
              value: [MasterConstants.CustomerTypeCustomer],
              dataType: 'String',
              operator: 'in',
            },
            {
              column: 'lastUpdated',
              value: '1483228800000',
              dataType: 'Date',
              operator: '>',
            },
          ],
        };
        ngetAllCustomerDetails(this.state.session, syncValue, isSuccess => {
          if (isSuccess) {
            this._getCustomerDetails();
          }
        });
      } else {
        console.log('make internet connection..!');
      }
    });
  }

  componentWillUnmount() {
    if (this.allocateTaskListener) {
      this.allocateTaskListener.remove();
    }
  }

  render() {
    const {navigation, index} = this.props;
    return (
      <Card style={styles.card}>
        <TouchableOpacity
          onPress={() => {
            this.viewTask();
          }}>
          <CardItem>
            <Left>
              <View
                style={{
                  width: 70,
                  height: 70,
                  borderRadius: 70 / 2,
                  backgroundColor: IconColor[index % IconColor.length],
                  justifyContent: 'center',
                }}>
                <Text
                  style={[styles.text, {color: 'white', textAlign: 'center'}]}>
                  {this.state.itemDetails.order_to_name
                    .substring(0, 2)
                    .toUpperCase()}{' '}
                </Text>
              </View>
              <Body>
                <Text style={styles.createDate}>
                  <Icon
                    type="FontAwesome"
                    name="history"
                    style={{fontSize: 15}}
                  />{' '}
                  {DateFormat(
                    this.state.itemDetails.last_updated,
                    DATE_TIME_LAST_UPDATED,
                  )}
                </Text>
                <Text style={styles.text}>
                  {' '}
                  {this.state.itemDetails.order_to_name}
                </Text>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.text}> Request No : </Text>
                  <Text> {this.state.itemDetails.order_code}</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.text}> Service Type : </Text>
                  <Text> {this.state.itemDetails.service_type_name}</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.text}> Service Date : </Text>
                  <Text>
                    {' '}
                    {DateFormat(
                      this.state.itemDetails.service_date,
                      DATE_TIME_LAST_UPDATED,
                    )}
                  </Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.text}> Classification Type : </Text>
                  <Text>
                    {' '}
                    {this.state.itemDetails.classification_type_name}
                  </Text>
                </View>
                {this.state.itemDetails.service_agent_name == null ? (
                  <Text style={[styles.notes, {color: 'red'}]}>
                    {' '}
                    {'Service Agent not Assigned'}
                  </Text>
                ) : (
                  <Text style={[styles.notes, {color: '#0169f1'}]}>
                    Service Agent is {this.state.itemDetails.service_agent_name}{' '}
                  </Text>
                )}
              </Body>
            </Left>
          </CardItem>
        </TouchableOpacity>
        <CardItem style={{backgroundColor: '#f5f6fa'}}>
          <Left>
            {this.state.itemDetails.service_agent_name == null &&
            this.state.itemDetails.status_id ===
              MasterConstants.serviceRequestPending ? (
              <TouchableOpacity
                style={{
                  paddingHorizontal: 15,
                  paddingVertical: 5,
                  borderRadius: 5,
                  backgroundColor: '#ff9f43',
                }}
                onPress={() => {
                  this.allocateTaskListener = DeviceEventEmitter.addListener(
                    'allocateTaskListener',
                    data => {
                      this.props.onGoBackService();
                    },
                  );
                  navigation.navigate(Route.AllocateTask, {
                    orderId: this.state.itemDetails,
                    headerTitle: 'Allocate Task',
                    reallocation: false,
                  });
                }}>
                <Text style={styles.icon}>
                  <Icon type="FontAwesome" name="user" style={styles.icon} />{' '}
                  Allocate Task
                </Text>
              </TouchableOpacity>
            ) : null}
          </Left>
          <View>
            <Text
              note
              style={[
                styles.statusText,
                {color: STATUS_COLOR[this.state.itemDetails.status_id]},
              ]}>
              {this.state.itemDetails.status_name}
            </Text>
          </View>
        </CardItem>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  card: {
    borderRadius: 3,
    elevation: 10,
  },
  icon: {
    fontSize: 15,
    color: '#fff',
    paddingLeft: 10,
    paddingRight: 10,
  },
  fabicon: {
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'white',
    fontSize: 22,
  },
  createDate: {
    fontSize: 13,
    textAlign: 'right',
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  statusContainer: {
    paddingBottom: 2,
    paddingLeft: 3,
    paddingRight: 3,
    borderRadius: 3,
  },
  statusText: {
    fontWeight: 'bold',
    borderRadius: 3,
    fontSize: 15,
  },
  notes: {
    fontWeight: 'bold',
    fontSize: 13,
  },
});
