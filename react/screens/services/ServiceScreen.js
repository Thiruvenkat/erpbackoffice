import React, { Component } from 'react';
import { View, ScrollView, Image, StyleSheet, FlatList, StatusBar, TouchableOpacity, Alert } from 'react-native';
import { Card, CardItem, Content, Left, Body, Text, Thumbnail, Icon, Button, Root, Container } from 'native-base';
import _ from 'lodash';
import ActionButton from 'react-native-action-button';
import Dimensions from 'Dimensions';
import { Styles } from '../../style/Styles';
import Colors from '../../style/Colors';
import Route from '../../router/Route';

const { width } = Dimensions.get('window');
const height = width * 0.6;

export default class ServiceScreen extends React.Component {

  static navigationOptions = {
    headerTitle: 'Services',
    headerStyle: {
      backgroundColor: Colors.headerBackground,
    },
    headerTitleStyle: {
      fontWeight: 'bold',
      color: Colors.headerTitleColor,
    },
  };

  constructor(props) {
    super(props);
    this.state = {
      serviceTypes: {
        "Home Appliances": {
          "title": "Home Appliances",
          "category": {
            "Refrigerator": {
              "title": "Refrigerator",
              "icon": require('../../../assets/refrigerator.png')
            },
            "Washing Machine": {
              "title": "Washing Machine",
              "icon": require('../../../assets/washing-machine.png')
            },
            "Television": {
              "title": "Television",
              "icon": require('../../../assets/computer.png')
            },
            "A/c": {
              "title": "A/c",
              "icon": require('../../../assets/air-conditioning.png')
            },
            "Fan": {
              "title": "Fan",
              "icon": require('../../../assets/fan.png')
            },
            "Microwave Oven": {
              "title": "Microwave Oven",
              "icon": require('../../../assets/microwave.png')
            }
          }
        },
        "Car": {
          "title": "Car",
          "category": {
            "Washing": {
              "title": "Washing",
              "icon": require('../../../assets/car_/car-wash.png')
            },
            "Wheel Alignment": {
              "title": "Wheel Alignment",
              "icon": require('../../../assets/car_/wheel.png')
            },
            "Tyre Change": {
              "title": "Tyre Change",
              "icon": require('../../../assets/car_/racing.png')
            },
            "Oil Change": {
              "title": "Oil Change",
              "icon": require('../../../assets/car_/car.png')
            },
            "Other Repairs": {
              "title": "Other Repairs",
              "icon": require('../../../assets/car_/car-repair.png')
            },
          }
        }
      }
    };
  }

  _renderItem({ item, index }) {
    const height = DEVICE_WIDTH - 150;
    return (
      <Card style={{ backgroundColor: 'white', width: DEVICE_WIDTH, height: height }}>
        <Image source={item.url}
          resizeMode="stretch"
          style={{ width: DEVICE_WIDTH, height: height, flex: 1, alignSelf: 'stretch', }} />
      </Card>
    );
  }

  render() {
    return (
      <Root>
        <Container>
          <Content style={{ backgroundColor: Styles.appViewBackground.backgroundColor }}>
            <StatusBar
              backgroundColor={Styles.statusBar.backgroundColor}
              barStyle="light-content" />
            <FlatList
              extraData={this.state}
              data={_.values(this.state.serviceTypes)}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              renderItem={({ item }) => (
                <ServieTypeItemView
                  title={item.title}
                  category={item.category}
                  navigate={this.props.navigation}
                />)
              }
              keyExtractor={(item, index) => index.toString()} />
          </Content>
          <ActionButton buttonColor="rgba(231,76,60,1)" onPress={() => { this.props.navigation.navigate('ServiceRequest') }}>
          </ActionButton>
        </Container>
      </Root>
    );
  }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  scrollContainer: {
    height,
  },
  image: {
    width,
    height,
  },
  card: {
    width,
    height,
    marginRight: 10,
    marginLeft: 10,
    marginTop: 10,
    marginBottom: 10
  },
});

class ServieTypeItemView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      category: {},
      ...props
    }
  }

  // Initialize the states
  componentDidMount() {
    const { title, category } = this.props
    this.setState({ ...this.props })
    console.log('entered into category');
    if (_.size(category) % 3 === 1 || _.size(category) % 3 === 2) {
      let remainder = _.size(category) % 3;
      if (remainder === 1) {
        category['Empty_1'] = {
          "title": "Empty",
        };
        category['Empty_2'] = {
          "title": "Empty",
        };
        this.setState({ category });
      } else {
        category['Empty_1'] = {
          "title": "Empty",
        };
        this.setState({ category });
      }
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { title, category } = nextProps
    this.setState({ ...nextProps })
  }

  render() {
    return (
      <Card >
        <CardItem style={{ backgroundColor: 'transparent' }}>
          <Left>
            <Body style={{ backgroundColor: 'transparent' }}>
              <Text style={{ fontWeight: 'bold', color: '#576772', }}>{this.state.title}</Text>
              <FlatList
                extraData={this.state}
                data={_.values(this.state.category)}
                numColumns={3}
                renderItem={({ item }) => (
                  <CategoryItemView
                    title={item.title}
                    icon={item.icon}
                    navigate={this.props.navigate}
                  />)}
                keyExtractor={(item, index) => index.toString()} />
            </Body>
          </Left>
        </CardItem>
      </Card>
    );
  }
}

class CategoryItemView extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      title: '',
      icon: '',
      ...props,
    }
  }

  // Initialize the states
  componentDidlMount() {
    const { title, icon } = this.props
    this.setState({ ...this.props })
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { title, icon } = nextProps
    this.setState({ ...nextProps })
  }

  render() {
    if (this.state.title === 'Empty') {
      return (
        <Card transparent style={{ flex: 1, flexDirection: 'column', }}>
          <CardItem transparent style={{ backgroundColor: 'transparent' }}>
            <Body style={{ alignItems: 'center', backgroundColor: 'transparent' }}>
              <Image
                style={{
                  justifyContent: 'center',
                  flex: 1,
                  alignItems: 'center',
                  height: 25,
                  width: 25,
                }} />
              <Text
                style={{
                  color: 'black',
                  fontWeight: 'bold',
                  padding: 3,
                  fontSize: 8,
                  textAlign: 'center',
                }}
                numberOfLines={2} >
              </Text>
            </Body>
          </CardItem>
        </Card>
      );
    }
    return (
      <Card transparent style={{ flex: 1, flexDirection: 'column', marginLeft: 4, }}>
        <TouchableOpacity
          onPress={() => {
            this.props.navigate.navigate(Route.SerialNoProductList);
          }}>
          <CardItem transparent style={{ alignItems: 'center', backgroundColor: 'transparent' }}>
            <Body style={{ alignItems: 'center', backgroundColor: 'transparent', }}>
              <Image
                source={this.state.icon}
                style={{
                  justifyContent: 'center',
                  flex: 1,
                  alignItems: 'center',
                  height: 45,
                  width: 45,
                }} />
              <Text
                style={{
                  color: '#576772',
                  padding: 3,
                  fontWeight: 'bold',
                  fontSize: 10,
                  textAlign: 'center',
                }}
                numberOfLines={2} >{this.state.title}
              </Text>
            </Body>
          </CardItem>
        </TouchableOpacity>
      </Card>
    );
  }
}