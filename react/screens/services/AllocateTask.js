/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-new-object */
import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  StatusBar,
  Alert,
  TouchableOpacity,
  DeviceEventEmitter,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {
  Card,
  Container,
  Content,
  Root,
  Footer,
  Input,
  FooterTab,
  Button,
  Picker,
  Icon,
} from 'native-base';
import Toast from 'react-native-simple-toast';
import AppConstants from '../../constant/AppConstants';
import MasterConstants from '../../constant/MasterConstants';
import ServiceController from 'react-native-erp-mobile-library/react/controller/ServiceController';
import Controller from 'react-native-erp-mobile-library/react/controller/Controller';
import Loader from 'react-native-erp-mobile-library/react/component/Loader';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { updateOrderServiceStatus } from 'react-native-erp-mobile-library/react/screens/services/dbHelper/ServiceOrderDB';
import {
  DATE_TIME_SERVER,
  DateFormat,
  DATE_TIME_LAST_UPDATED,
  DATE_TIME_UI,
  StringToDate,
  DATE_UI,
} from 'react-native-erp-mobile-library/react/common/Common';
import PreparingProgress from 'react-native-erp-mobile-library/react/component/PreparingProgress';
import CustomSearchView, {
  SearchType,
} from 'react-native-erp-mobile-library/react/utilities/CustomSearchView';
import { sessionInfo } from 'react-native-erp-mobile-library';
// import { approveServices } from 'react-native-erp-mobile-library/react/controller/WorkFlowController';
import NetInfo from '@react-native-community/netinfo';
import Colors from 'react-native-erp-mobile-library/react/style/Colors';
import { Styles } from 'react-native-erp-mobile-library/react/style/Styles';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import { renderHeaderView } from 'react-native-erp-mobile-library/react/util/HeaderUtil';
import { approveServices } from 'react-native-erp-mobile-library/react/controller/WorkFlowController';
import Route from 'react-native-erp-mobile-library/react/router/Route';

const DEVICE_HEIGHT = Dimensions.get('window').height;

export default class AllocateTask extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orderId: "",
      orderServiceId: props.route.params?.orderId ?? '',
      isConnected: true,
      loginToken: '',
      publicIp: '',
      orderNo: '',
      serviceAgentId: '',
      serviceAgentName: '',
      classificationType: '',
      serviceType: '',
      customerName: '',
      selectedEmployee: '',
      serviceId: '',
      isLoading: false,
      isPreparing: true,
      originalServiceDate: "" + DateFormat(new Date(), DATE_TIME_SERVER),
      serviceDate: "" + DateFormat(new Date(), DATE_TIME_LAST_UPDATED),
      followUpDate: "" + DateFormat(new Date(), DATE_TIME_SERVER),
      isDateTimePickerVisible: false,
      isDateTimePickerFolloup: false,
      appConfigType: "",
      isSelectTechnicianModal: false,
      doneButton: false,
      serialNoId: '',
      serviceLocation: [],
      serviceLocations: '',
      templateIds: null,
      templates: [],
      orderParams: [],
      taskId: "",
      expectedDeliveryDate: "",
      isDateTimePickerForDeliveryDate: false,
      condition: [{
          value: MasterConstants.CustomerTypeEmployee,
          column: 'customer_type_id',
          operator: '='
      }, {
          value: MasterConstants.communicationAddress,
          column: 'address_type_id',
          operator: '='
      }],
      session: sessionInfo({ type: 'get', key: 'loginInfo' }),
  }

  this._getServiceOrder = this._getServiceOrder.bind(this);
  this._getOrder = this._getOrder.bind(this);
  this._updateServiceRequestPro = this._updateServiceRequestPro.bind(this);
}


  componentDidMount() {
    this._initHeader();
    NetInfo.fetch().then(isConnected => {
        this.setState({ isConnected });
        if (isConnected) {
            AsyncStorage.getItem(AppConstants.keyLoginInfo).then(loginInfo => {
                const data = JSON.parse(loginInfo);
                this.setState({
                    loginToken: data[AppConstants.keyLoginToken],
                    publicIp: data[AppConstants.keyPublicIp],
                    isPreparing: true,
                },
                    () => this._getServiceOrder());
            });
        } else {
            this.setState({ isPreparing: false });
        }
    });
    this._getAllServiceLocation();
}

_initHeader() {
    this.props.navigation.setParams({
      headerTitle: this.props.route.params?.headerTitle ?? `Allocate Task`,
      headerType: this.props.route.params?.headerType ?? HeaderTypes.HeaderHomeMenu,
    });
    this.props.navigation.setOptions(renderHeaderView(this));
  }

_getAllServiceLocation() {
    let condition = {
        'filters': {
            'limit': 500,
        },
    };
    new ServiceController().getAllServiceLocation(this.state.session.loginToken,
        this.state.session.publicIp + AppConstants.getAllServiceLocation, condition
    ).then(res => {
        if (res.status === 200) {
            const responseBody = JSON.parse(res._bodyText);
            const serviceLoc = responseBody.response.content.dataList;
            this.setState({
                serviceLocation: [{ 'serviceLocationId': '', 'serviceLocationName': '--Select Location--' }, ...serviceLoc],
            });
        } else {
            Toast.show('Problem occurred!', Toast.SHORT);
        }
    });
}

_getOrder() {
    this.setState({ isPreparing: true });
    new Controller().playGetApi(
        this.state.session.loginToken, this.state.session.publicIp + AppConstants.getOrder + this.state.orderId)
        .then((response) => {
            this.setState({ isPreparing: false });
            if (response.status === 200) {
                const data = JSON.parse(response._bodyText).response.content.data;
                this.setState({
                    orderObj: data,
                    followUpDate: data.referenceDate3,
                    orderParams: data.orderParams,
                    expectedDeliveryDate: data.dueDate !== undefined ? data.dueDate : this.state.expectedDeliveryDate,
                }, () => {
                });
            } else {
                Toast.show('Problem occurred!', Toast.SHORT);
            }
        });
}

_handleDatePicked = date => {
    this.setState({
        serviceDate: DateFormat(date, DATE_TIME_LAST_UPDATED),
        originalServiceDate: DateFormat(date, DATE_TIME_SERVER),
    });
    this._hideDateTimePicker();
};

_handleDatePicke = date => {
    this.setState({
        followUpDate: DateFormat(date, DATE_TIME_SERVER),
    });
    this._hideDateTimePicker();
};

_handleDatePickedForDeliveryDate = date => {
    this.setState({
        expectedDeliveryDate: DateFormat(date, DATE_TIME_SERVER),
    });
    this._hideDateTimePicker();
}

_hideDateTimePicker = () => {
    this.setState({
        isDateTimePickerVisible: false,
        isDateTimePickerFolloup: false,
        isDateTimePickerForDeliveryDate: false,
    });
};


_getServiceOrder() {
    if (typeof this.state.orderServiceId === 'object') {
        var servicesId = this.state.orderServiceId.order_service_id;
        this.setState({ doneButton: true });
    } else {
        var servicesId = this.state.orderServiceId;
        let taskIds = this.props.route.params.taskDetail.taskId;
        this.setState({ taskId: taskIds });
    }
    this.setState({ isPreparing: true });
    new Controller().playGetApi(
        this.state.loginToken, this.state.publicIp + AppConstants.getOrderService + servicesId)
        .then((response) => {
            this.setState({ isPreparing: false });
            if (response.status === 200) {
                let serviceLocationId = '';
                const data = JSON.parse(response._bodyText).response.content.data;
                let objData = {
                    customer_id: typeof data.serviceAgent !== 'undefined' ? data.serviceAgent.id : null,
                    customer_name: typeof data.serviceAgent !== 'undefined' ? data.serviceAgent.name : null,
                };
                if (typeof data.serviceLocation !== 'undefined') {
                    serviceLocationId = data.serviceLocation.id;
                }
                let dueDatedate = new Date(DateFormat(data.serviceDate, DATE_TIME_UI).concat('T00:00:00'));
                dueDatedate.setDate(dueDatedate.getDate() + 2);
                this.setState({
                    serviceOrderData: data,
                    orderNo: data.order.code,
                    orderId: data.order.id,
                    serviceId: data.order.text10,
                    serviceDate: DateFormat(data.serviceDate, DATE_TIME_LAST_UPDATED),
                    originalServiceDate: DateFormat(data.serviceDate, DATE_TIME_SERVER),
                    classificationType: data.serviceClassificationType.name,
                    serviceType: data.serviceType.name,
                    customerName: data.order.text5,
                    orderServiceId: data.orderServiceId, /**/
                    orderTypeId: data.order.text3,
                    selectedEmployee: objData,
                    serialNoId: data.orderServiceDetails[0].serialNumber.id,
                    serviceLocations: serviceLocationId,
                    expectedDeliveryDate: StringToDate(dueDatedate),
                }, () => {
                    this._getOrder(), this._getProductDetailsUsingSerialNo(
                        this.state.serialNoId
                    );
                });
                // if (this.props.navigation.state.params.reallocation) {

                //     this.setState({ selectedEmployee: objData })
                // }
            } else {
                Toast.show('Problem occurred!', Toast.SHORT);
            }
        });
}

_getProductDetailsUsingSerialNo(serialNumberId) {
    this.setState({ isLoading: true });
    new ServiceController()
        .getProductDetailsUsingSerialNo(
            this.state.session.loginToken,
            this.state.session.publicIp + AppConstants.getProductDetailsUsingSerialNo,
            {
                request: {
                    envelope: {
                        sourceApplication: 'Afforde UI',
                        requestDate: DateFormat(new Date(), DATE_TIME_SERVER),
                    },
                    content: {
                        bo_data: {
                            type: 'getProductDetailsUsingSerialNo_BOO',
                            serialNo: serialNumberId,
                        },
                    },
                },
            }
        )
        .then(response => {
            if (response.status === 200) {
                const templateIds = [];
                const responseBody = JSON.parse(response._bodyText);
                for (
                    let index = 0;
                    index < responseBody.response.content.bo_data.templateIds.length;
                    index++
                ) {
                    const templateId =
                        responseBody.response.content.bo_data.templateIds[index];
                    templateIds[index] = templateId;
                }
                this.setState({ templateIds: templateIds });
                this._getAllTemplate();
            } else {
                this.setState({ isLoading: false });
                Toast.show('Problem occurred!', Toast.SHORT);
            }
        });
}

_getAllTemplate() {
    const templateIds = this.state.templateIds;
    if (templateIds != undefined && templateIds != null) {
        new ServiceController()
            .getAllTemplate(
                this.state.session.loginToken,
                this.state.session.publicIp + AppConstants.getAllTemplate,
                {
                    filters: {
                        offset: 0,
                        limit: 500,
                        conditions: [
                            {
                                column: 'templateId',
                                value: templateIds.join(),
                                dataType: 'String',
                                operator: 'in',
                            },
                        ],
                    },
                }
            )
            .then(response => {
                this.setState({ isLoading: false });
                if (response.status === 200) {
                    const templates = [];
                    const responseBody = JSON.parse(response._bodyText);
                    for (
                        let index = 0;
                        index < responseBody.response.content.dataList.length;
                        index++
                    ) {
                        const result = responseBody.response.content.dataList[index];
                        templates[index] = result;
                        templates[index].templateParam.validation = false;
                        for (let j = 0; j < templates[index].templateParam.length; j++) {
                            templates[index].templateParam[j].values = '';
                            const element = templates[index].templateParam[j];
                            element.param.values =
                                element.param.valueFormat.id == MasterConstants.checkBox
                                    ? false
                                    : element.param.valueFormat.id == MasterConstants.dropDown
                                        ? element.param.paramValues[0].valueId
                                        : '';
                        }
                    }
                    this.setState({ isLoading: false, templates: templates });
                } else {
                    this.setState({ isLoading: false });
                    Toast.show('Problem occurred!', Toast.SHORT);
                }
            });
    }
}

_createServiceRequestInBackOffice() {
    var serviceOrderData = this.state.serviceOrderData;
    var orderObj = this.state.orderObj;
    orderObj.salesAgent = {
        id: this.state.selectedEmployee.customer_id,
    };
    var obj = new Object();
    obj.id = this.state.selectedEmployee.customer_id;
    serviceOrderData.serviceAgent = obj;
    serviceOrderData.status.id = MasterConstants.serviceRequestForService;
    orderObj.orderStatus.id = MasterConstants.serviceRequestForService;
    orderObj.referenceNumber1 = this.state.serviceId;
    serviceOrderData.order = orderObj;
    serviceOrderData.serviceDate = this.state.originalServiceDate;
    const request = {
        'request': {
            'envelope': {
                'sourceApplication': 'Afforde UI',
                'requestDate': DateFormat(new Date(), DATE_TIME_SERVER),
            },
            'content': {
                'bo_data': {
                    'type': 'createServiceRequestByCustomer_BOO',
                    'order': orderObj,
                    'orderService': serviceOrderData,
                    'optValidationFlag': false,
                },
            },
        },
    };
    this.setState({ isLoading: true });
    new ServiceController().createServiceRequestByCustomer(
        this.state.loginToken, this.state.publicIp + AppConstants.createServiceRequestByCustomer, request)
        .then((response) => {
            if (response.status === 200) {
                const data = JSON.parse(response._bodyText);
                updateOrderServiceStatus(AppConstants.affordeBackOfficeDB, this.state.orderId, MasterConstants.mobileResponseId, MasterConstants.mobileResponseName, () => {
                    this.setState({ isLoading: false });
                    this.props.navigation.goBack();
                });
            } else {
                this.setState({ isLoading: false });
                console.log('service request update error');
            }
        });
    // }
}

_updateServiceRequestForReallocation() {
    var serviceOrderData = this.state.serviceOrderData;
    var orderObj = this.state.orderObj;
    var obj = new Object();
    obj.id = this.state.selectedEmployee.customer_id;
    serviceOrderData.serviceAgent = obj;
    serviceOrderData.status.id = MasterConstants.serviceRequestForService;
    orderObj.orderStatus.id = MasterConstants.serviceRequestForService;
    orderObj.referenceNumber1 = this.state.serviceId;
    serviceOrderData.order = orderObj;
    serviceOrderData.serviceDate = this.state.originalServiceDate;
    const request = {
        'request': {
            'envelope': {
                'sourceApplication': 'Afforde UI',
                'requestDate': DateFormat(new Date(), DATE_TIME_SERVER),
            },
            'content': {
                'bo_data': {
                    'type': 'updateServiceRequest_BOO',
                    'order': orderObj,
                    'orderService': serviceOrderData,
                },
            },
        },
    };
    this.setState({ isLoading: true });
    new ServiceController().updateServiceRequestForReallocation(
        this.state.loginToken, this.state.publicIp + AppConstants.updateServiceRequest, request)
        .then((response) => {
            if (response.status === 200) {
                const data = JSON.parse(response._bodyText);
                updateOrderServiceStatus(AppConstants.affordeBackOfficeDB, this.state.orderId, MasterConstants.mobileResponseId, MasterConstants.mobileResponseName, () => {
                    this.setState({ isLoading: false });
                    this.props.navigation.goBack();
                });
            } else {
                this.setState({ isLoading: false });
                console.log('service request update error');
            }
        });
}

_updateServiceRequestPro(cb) {
    var orderJsonData = this.state.appConfigType.createOrderJson;
    var serviceOrderData = this.state.serviceOrderData;
    var orderObj = this.state.orderObj;
    var obj = new Object();
    obj.id = this.state.selectedEmployee.customer_id;
    serviceOrderData.serviceAgent = obj;
    orderObj.referenceNumber1 = this.state.serviceId;
    orderObj.referenceNumber3 = this.state.serviceId;
    orderObj.referenceDate3 = this.state.followUpDate;
    if (this.state.expectedDeliveryDate != '') {
        orderObj.dueDate = this.state.expectedDeliveryDate;
    }
    if (this.state.serviceLocations != '') {
        let serviceLocation = {
            id: this.state.serviceLocations,
        };
        serviceOrderData.serviceLocation = serviceLocation;
    }
    if (
        this.state.templates.length > 0 &&
        this.state.templates[0].templateParam != undefined
    ) {
        for (
            let index = 0;
            index < this.state.templates[0].templateParam.length;
            index++
        ) {
            const element = this.state.templates[0].templateParam[index];
            const value = {
                lastUpdated: DateFormat(new Date(), DATE_TIME_SERVER),
                tenant: {
                    id: orderObj.tenant.id,
                },
                value: element.param.values,
                parameter: {
                    id: element.param.parameterId,
                },
                status: {
                    id: '104101',
                },
                template: {
                    id: this.state.templates[0].templateId,
                },
            };
            if (index == 0) {
                orderObj.orderParams = [];
            }
            orderObj.orderParams[index] = value;
        }
    }
    serviceOrderData.order = orderObj;
    serviceOrderData.serviceDate = this.state.originalServiceDate;
    const request = {
        'request': {
            'envelope': {
                'sourceApplication': 'Afforde UI',
                'requestDate': DateFormat(new Date(), DATE_TIME_SERVER),
            },
            'content': {
                'bo_data': {
                    'type': 'approveServices_BOO',
                    'order': orderObj,
                    'orderService': serviceOrderData,
                    'actionId': '208102',
                    'eventKey': 'order_' + MasterConstants.orderTypeServiceRequest,
                    'eventTypeId': '126109',
                    'isUpdateStock': false,
                    'buttonAction': 'approve',
                    'orderFinalStatusId': MasterConstants.waitingForApprovalServiceRequestPending,
                    'isUpdateProduction': false,
                    'canInitiateWorkflowInstance': false,
                    'autoCompleteTaskId': this.state.taskId,
                },
            },
        },
    };
    this.setState({ isLoading: true });
    // this.state.publicIp + AppConstants.createServiceRequestByCustomer,
    approveServices(this.state.session, request, (response) => {
        if (response.status === 200) {
            if (!this.state.doneButton) {
                Toast.show('Approved Sucesssfully', Toast.SHORT);
                cb(response);
            }
            this.setState({ isLoading: false, doneButton: false });
            const data = JSON.parse(response._bodyText);
            updateOrderServiceStatus(AppConstants.affordeBackOfficeDB, this.state.orderId, MasterConstants.mobileResponseId, MasterConstants.mobileResponseName, () => {
                console.log('Calling Go Back');
                this.setState({ isLoading: false, doneButton: false });
                this.props.route.params.onGoBack();
                this.props.navigation.goBack();
            });
        } else {
            this.setState({ isLoading: false });
            console.log('service request update error');
        }
    });

}

onSubmit(reasons) {
    this.state.templates[0].templateParam = reasons.templateParam;
}

_callPrepareBooking() {
    this.props.navigation.navigate(Route.PrepareBookingQuestion, {
        templateIds: this.state.templateIds,
        templateParam: this.state.templates[0],
        orderParams: this.state.orderParams,
        onSubmit: (reasons) => {
            if (reasons) {
                this.state.templates[0].templateParam = reasons.templateParam;
            }
        },
    });
}

_formValidation() {
    if (this.state.selectedEmployee.customer_id != undefined) {
        Alert.alert(
            'Confirmation',
            'Are you sure, do you want to allocate task?',
            [
                { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'Yes', onPress: () => this._createServiceRequestInBackOffice() },
            ],
            { cancelable: false }
        );
    } else {
        Toast.show('Please choose service agent', Toast.SHORT);
    }
}

render() {
    // const { navigation } = { ...this.props, ...this.state, ...this.props.screenProps };
    if (this.state.isPreparing) {
        return (
            <PreparingProgress />
        );
    } else if (this.state.orderNo == '') {
        return (
            <View style={{ alignItems: 'center', justifyContent: 'center', height: DEVICE_HEIGHT }}>
                <Text style={{ fontSize: 14, color: Colors.headerBackground }}>No data found</Text>
            </View>
        );
    } else {
        return (
            <Root>
                <Container>
                    <Content padder style={{ backgroundColor: '#f4f6f9' }}>
                        <StatusBar
                            backgroundColor={Styles.statusBar.backgroundColor}
                            barStyle="light-content" />
                        <Loader
                            loading={this.state.isLoading} />
                        <DateTimePicker
                            isVisible={this.state.isDateTimePickerVisible}
                            mode="datetime"
                            is24Hour={false}
                            minimumDate={new Date()}
                            onConfirm={this._handleDatePicked}
                            onCancel={this._hideDateTimePicker}
                        />
                        <DateTimePicker
                            isVisible={this.state.isDateTimePickerFolloup}
                            mode="datetime"
                            is24Hour={false}
                            minimumDate={new Date()}
                            onConfirm={this._handleDatePicke}
                            onCancel={this._hideDateTimePicker}
                        />
                        <DateTimePicker
                            isVisible={this.state.isDateTimePickerForDeliveryDate}
                            mode="datetime"
                            is24Hour={false}
                            minimumDate={new Date()}
                            onConfirm={this._handleDatePickedForDeliveryDate}
                            onCancel={this._hideDateTimePicker}
                        />
                        <CustomSearchView
                            modalVisible={this.state.isSelectTechnicianModal}
                            searchType={SearchType.customers}
                            conditions={this.state.condition}
                            session={this.state.session}
                            hideModel={() => {
                                this.setState({ isSelectTechnicianModal: false });
                            }}
                            onChooseSearchValue={(data) => {
                                let datas = {
                                    customer_id: data.id,
                                    customer_name: data.name,
                                };
                                this.setState({ selectedEmployee: datas, isSelectTechnicianModal: false });
                            }}
                        />
                        <Card style={{ paddingBottom: 5, paddingTop: 5 }}>
                            <View style={{ flexDirection: 'row', padding: 6, marginTop: 10 }}>
                                <Text style={[styles.leftContainer]}>
                                    Order no
                                </Text>
                                <Text
                                    style={[
                                        styles.rightContainer,
                                        { color: 'black', fontSize: 14 },
                                    ]}
                                >
                                    {this.state.orderNo}
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row', padding: 6, marginTop: 10 }}>
                                <Text style={[styles.leftContainer, { marginTop: 3, fontWeight: 'bold' }]}>
                                    Service date
                                </Text>
                                <TouchableOpacity
                                    style={[styles.rightContainer]}
                                    onPress={() =>
                                        this.setState({
                                            isDateTimePickerVisible: false,
                                        })
                                    }
                                >
                                    <Text
                                        style={[{
                                            borderRadius: 3,
                                            padding: 5,
                                            fontSize: 14,
                                            fontWeight: 'bold',
                                            backgroundColor: 'lightgrey',
                                        }]}
                                    >
                                        {'' + this.state.serviceDate}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flexDirection: 'row', padding: 6, marginTop: 10 }}>
                                <Text style={[styles.leftContainer]}>
                                    Customer name
                                </Text>
                                <Text
                                    style={[
                                        styles.rightContainer,
                                        { color: 'black', fontSize: 13 },
                                    ]}
                                >
                                    {this.state.customerName}
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row', padding: 6, marginTop: 10 }}>
                                <Text style={[styles.leftContainer]}>
                                    Product
                                </Text>
                                <Text
                                    style={[
                                        styles.rightContainer,
                                        { color: 'black', fontSize: 13 },
                                    ]}
                                >
                                    {(this.state.serviceOrderData.orderServiceDetails != null && this.state.serviceOrderData.orderServiceDetails.length > 0) ?
                                        this.state.serviceOrderData.orderServiceDetails[0].product.name : ''
                                    }
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row', padding: 6, marginTop: 10 }}>
                                <Text style={[styles.leftContainer]}>
                                    Serial number
                                </Text>
                                <Text
                                    style={[
                                        styles.rightContainer,
                                        { color: 'black', fontSize: 13 },
                                    ]}
                                >
                                    {(JSON.parse(this.state.serviceOrderData.jsonData).serialNoName != null) ?
                                        JSON.parse(this.state.serviceOrderData.jsonData).serialNoName : ''
                                    }
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row', padding: 6, marginTop: 10 }}>
                                <Text style={[styles.leftContainer]}>
                                    Classification type
                                </Text>
                                <Text
                                    style={[
                                        styles.rightContainer,
                                        { color: 'black', fontSize: 13 },
                                    ]}
                                >
                                    {this.state.classificationType}
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row', padding: 6, marginTop: 10 }}>
                                <Text style={[styles.leftContainer]}>
                                    Service type
                                </Text>
                                <Text
                                    style={[
                                        styles.rightContainer,
                                        { color: 'black', fontSize: 13 },
                                    ]}
                                >
                                    {this.state.serviceType}
                                </Text>
                            </View>
                            {this.state.session.databaseName == AppConstants.affordeBackOfficeDB && <View style={{ flexDirection: 'row', padding: 6, marginTop: 10 }}>
                                <Text style={[styles.leftContainer, { marginTop: 3, color: Colors.headerBackground }]}>
                                    Service Location :
              </Text>
                                <View style={[styles.rightContainer]}>
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                                        headerBackButtonText="Back"
                                        selectedValue={this.state.serviceLocations}
                                        onValueChange={value => {
                                            this.setState({ serviceLocations: value });
                                        }}
                                    >
                                        {this.state.serviceLocation.map((item, index) => {
                                            return (
                                                <Picker.Item key={index}
                                                    label={item.serviceLocationName}
                                                    value={item.serviceLocationId}
                                                />
                                            );
                                        })}
                                    </Picker>
                                </View>
                            </View>}
                            {this.state.templates.length > 0 && this.state.serviceLocations == AppConstants.Droping ||
                                this.state.session.databaseName == AppConstants.affordeCustomerDB &&
                                this.state.templates[0].templateParam.length > 0 ? (
                                    <View style={{ flexDirection: 'row', padding: 6, marginTop: 10 }}>
                                        <Text style={[styles.leftContainer, { marginTop: 3, fontWeight: 'bold' }]}>
                                            {'Service reason'}
                                        </Text>
                                        <TouchableOpacity style={[styles.rightContainer]} onPress={() => {
                                            this._callPrepareBooking();
                                        }}>
                                            <Text
                                                style={[{
                                                    borderRadius: 3,
                                                    padding: 2,
                                                    fontSize: 14,
                                                    fontWeight: 'bold',
                                                    backgroundColor: 'lightgrey',
                                                }]}
                                            >{`Choose ${this.state.templates[0].templateParam.length}+ reasons`}</Text>
                                        </TouchableOpacity>
                                    </View>
                                ) : null}
                            <View style={{ flexDirection: 'row', padding: 6, marginTop: 10 }}>
                                <Text style={[styles.leftContainer, { marginTop: 3, fontWeight: 'bold' }]}>
                                    Follow-up date
                                </Text>
                                <TouchableOpacity
                                    style={[styles.rightContainer]}
                                    onPress={() =>
                                        this.setState({
                                            isDateTimePickerFolloup: true,
                                        })
                                    }
                                >
                                    <Text
                                        style={[{
                                            borderRadius: 3,
                                            padding: 5,
                                            fontSize: 14,
                                            fontWeight: 'bold',
                                            backgroundColor: 'lightgrey',
                                        }]}
                                    >
                                        {'' + DateFormat(this.state.followUpDate, DATE_UI)}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flexDirection: 'row', padding: 6, marginTop: 10 }}>
                                <Text style={[styles.leftContainer, { marginTop: 3, fontWeight: 'bold' }]}>
                                    Expected delivery date
                                </Text>
                                <TouchableOpacity
                                    style={[styles.rightContainer]}
                                    onPress={() =>
                                        this.setState({
                                            isDateTimePickerForDeliveryDate: true,
                                        })
                                    }
                                >
                                    <Text
                                        style={[{
                                            borderRadius: 3,
                                            padding: 5,
                                            fontSize: 14,
                                            fontWeight: 'bold',
                                            backgroundColor: 'lightgrey',
                                        }]}
                                    >
                                        {'' + DateFormat(this.state.expectedDeliveryDate, DATE_UI)}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flexDirection: 'row', padding: 6, marginTop: 10 }}>
                                <Text style={[styles.leftContainer]}>
                                    Service reference.no
                                </Text>
                                <Input
                                    autoFocus={true}
                                    value={this.state.serviceId}
                                    style={[styles.rightContainer, { height: 40, borderRadius: 1, backgroundColor: '#f2f3f4' }]}
                                    onChangeText={(text) => {
                                        this.setState({ serviceId: text });
                                    }} />
                            </View>
                            <View style={{ flexDirection: 'row', padding: 6, marginTop: 10 }}>
                                <Text style={[styles.leftContainer, { marginTop: 3, fontWeight: 'bold' }]}>
                                    Service Group
                                    <Text style={{ color: 'red' }}>
                                        {' *'}
                                    </Text>
                                </Text>
                                <TouchableOpacity
                                    style={[styles.rightContainer]}
                                    onPress={() => {
                                        // navigation.navigate(Route.CustomerList, {
                                        //     title: 'Select Technician',
                                        //     screenType: 'select',
                                        //     serviceDate: this.state.originalServiceDate,
                                        //     customerTypeId: [MasterConstants.CustomerTypeEmployee],
                                        //     reallocation: this.props.navigation.state.params.reallocation,
                                        //     onChooseCustomer: (data) => {
                                        //         this.setState({ selectedEmployee: data.customerData })
                                        //     },
                                        // });
                                        this.setState({ isSelectTechnicianModal: true });
                                    }}
                                >
                                    {this.state.selectedEmployee.customer_id != undefined || this.state.selectedEmployee.customer_id != null ?
                                        <Text
                                            style={[{
                                                borderRadius: 3,
                                                padding: 5,
                                                fontSize: 14,
                                                fontWeight: 'bold',
                                                backgroundColor: 'lightgrey',
                                            }]}
                                        >
                                            {'' + this.state.selectedEmployee.customer_name}
                                        </Text>
                                        :
                                        <Text
                                            style={[{
                                                borderRadius: 3,
                                                padding: 5,
                                                fontSize: 14,
                                                fontWeight: 'bold',
                                                backgroundColor: 'lightgrey',
                                            }]}
                                        >
                                            Select technician
                                        </Text>}
                                </TouchableOpacity>
                            </View>
                        </Card>
                    </Content>
                    {this.state.doneButton && <Footer style={{ backgroundColor: Styles.statusBar.backgroundColor }}>
                        <TouchableOpacity
                            onPress={this._formValidation.bind(this)}>
                            <Text style={{ marginTop: 5, fontSize: 20, fontWeight: 'bold', padding: 10, width: '100%', backgroundColor: 'transparent', color: 'white', textAlign: 'center' }}>
                                Done
                            </Text>
                        </TouchableOpacity>
                    </Footer>}
                </Container>
            </Root>
        );
    }
}
}

const styles = StyleSheet.create({
container: {
    flex: 1,
    justifyContent: 'center',
},
horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
},
leftContainer: {
    color: Colors.headerBackground,
    fontSize: 16,
    paddingLeft: 10,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
    fontWeight: 'bold',
},
rightContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'transparent',
},

});

