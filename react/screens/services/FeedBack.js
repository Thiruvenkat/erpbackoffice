/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, StyleSheet, TouchableHighlight} from 'react-native';
import {Textarea} from 'native-base';
import Stars from 'react-native-stars';
import MyIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../../style/Colors';
import Route from '../../router/Route';
import AppConstants from '../../constant/AppConstants';
import Loader from 'react-native-erp-mobile-library/react/component/Loader';
import {
  DATE_TIME_SERVER,
  DateFormat,
} from 'react-native-erp-mobile-library/react/common/Common';
import Controller from 'react-native-erp-mobile-library/react/controller/Controller';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

// create a component
class FeedBack extends React.Component {
  static navigationOptions = {
    headerTitle: 'Feed Back',
    headerTintColor: 'white',
    headerLeft: null,
    headerStyle: {
      backgroundColor: Colors.headerBackground,
    },
    headerTitleStyle: {
      fontWeight: 'bold',
      color: Colors.headerTitleColor,
    },
  };

  constructor(props) {
    super(props);
    this.state = {
      followUpComments: '',
      rating: 0,
      isLoading: false,
    };
    console.log(this.props);
  }

  componentDidMount() {
    AsyncStorage.getItem(AppConstants.keyLoginToken).then(loginToken => {
      AsyncStorage.getItem(AppConstants.keyPublicIp).then(publicIp => {
        AsyncStorage.getItem(AppConstants.keySessionInfo).then(sessionInfo => {
          this.setState({
            loginToken: loginToken,
            publicIp: publicIp,
            userId: JSON.parse(sessionInfo).userId,
            tenantId: JSON.parse(sessionInfo).tenantId,
          });
        });
      });
    });
  }

  _createFollowUp() {
    this.setState({isLoading: true});
    new Controller()
      .playPostApi(
        this.state.loginToken,
        this.state.publicIp + AppConstants.createFollowUp,
        {
          request: {
            envelope: {
              sourceApplication: 'sourceApplication',
              requestDate: DateFormat(new Date(), DATE_TIME_SERVER),
            },
            content: {
              data: {
                type: 'followUp',
                followUpComments: this.state.followUpComments,
                objectId: this.props.navigation.state.params.appointmentId,
                objectRefNo: this.props.navigation.state.params.referenceNo,
                followUpType: {
                  id: '223005',
                },
                status: {
                  id: '104184',
                },
                rating: this.state.rating,
                nextFollowUpDate: DateFormat(new Date(), DATE_TIME_SERVER),
                followUpCount: 1,
                tenant: {
                  id: this.state.tenantId,
                },
              },
            },
          },
        },
      )
      .then(response => {
        this.setState({isLoading: false});
        if (response.status === 200) {
          this.props.navigation.replace(Route.DrawerCommon);
        } else {
          Toast.show('GetAll Advertisement Failed', Toast.SHORT);
        }
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <Loader loading={this.state.isLoading} />
        <Stars
          default={0}
          count={5}
          half={false}
          update={star => {
            this.setState({rating: star});
          }}
          fullStar={<MyIcon name={'star'} style={[styles.myStarStyle]} />}
          emptyStar={
            <MyIcon
              name={'star-outline'}
              style={[styles.myStarStyle, styles.myEmptyStarStyle]}
            />
          }
          halfStar={<MyIcon name={'star-half'} style={[styles.myStarStyle]} />}
        />
        <Textarea
          rowSpan={5}
          bordered
          placeholder="Enter you comments"
          style={{width: '80%'}}
          onChangeText={text => {
            this.setState({followUpComments: text});
          }}
        />
        <TouchableHighlight
          style={styles.submit}
          onPress={() => {
            this._createFollowUp();
          }}
          underlayColor="#68a0cf">
          <Text style={[styles.submitText]}>Submit</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  submit: {
    paddingRight: 20,
    paddingLeft: 20,
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: 20,
    backgroundColor: Colors.headerBackground,
    borderColor: '#fff',
    borderRadius: 10,
    borderWidth: 1,
  },
  submitText: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 25,
  },
  myStarStyle: {
    fontSize: 70,
    color: Colors.headerBackground,
    backgroundColor: 'transparent',
    textShadowColor: 'darkgrey',
    textShadowOffset: {width: 1, height: 1},
    textShadowRadius: 2,
  },
  myEmptyStarStyle: {
    fontSize: 70,
    color: 'darkgrey',
  },
});

//make this component available to the app
export default FeedBack;
