/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  StyleSheet,
  StatusBar,
  ActivityIndicator,
  FlatList,
  View,
  Text,
  TouchableOpacity,
  TouchableNativeFeedback,
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import {Icon, Left, Body, Card, CardItem, Root} from 'native-base';
import {Styles, STATUS_COLOR, IconColor} from '../../style/Styles';
import Colors from '../../style/Colors';
import MyIcon from 'react-native-vector-icons/MaterialIcons';
import Toast from 'react-native-simple-toast';
import AppConstants from '../../constant/AppConstants';
import MasterConstants from '../../constant/MasterConstants';
import Route from '../../router/Route';
import {GetAllOrderService} from 'react-native-erp-mobile-library/react/screens/services/dbHelper/ServiceOrderDB';
import {getAllOrderService} from '../../controller/SyncDataController';
import {
  DateFormat,
  DATE_TIME_LAST_UPDATED,
  DATE_TIME_UI,
  DATE_UI,
} from 'react-native-erp-mobile-library/react/common/Common';
import Loader from 'react-native-erp-mobile-library/react/component/Loader';
import DateTimePicker from 'react-native-modal-datetime-picker';
import PreparingProgress from 'react-native-erp-mobile-library/react/component/PreparingProgress';
import {sessionInfo} from 'react-native-erp-mobile-library';
import {messages} from 'react-native-erp-mobile-library/react/i18n/i18n';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import {renderHeaderView} from 'react-native-erp-mobile-library/react/util/HeaderUtil';

export default class TaskList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isPreparing: true,
      isLoading: true,
      isRefresh: false,
      loadMore: false,
      isConnected: true,
      session: sessionInfo({type: 'get', key: AppConstants.keyLoginInfo}),
      orderServiceList: [],
      filterDate: DateFormat(new Date(), DATE_TIME_UI),
      isDateTimePickerVisible: false,
      fetchAll: false,
      offset: 0,
      limit: 10,
      conditions: [
        {
          value: [
            MasterConstants.serviceRequestPending,
            MasterConstants.requestForService,
            MasterConstants.servicePaused,
            MasterConstants.serviceStarted,
          ],
          column: 'status_id',
          operator: 'not in',
          dataType: 'String',
        },
      ],
    };
  }

  componentDidMount() {
    this._initHeader();
    this._getAllOrderService(datas => {
      if (datas.length > 0) {
        this.setState({
          orderServiceList:
            this.state.offset === 0
              ? datas
              : this.state.orderServiceList.concat(datas),
          isLoading: false,
          loadMore: false,
          isPreparing: false,
          offset: this.state.offset + datas.length,
        });
      } else {
        this._getAllOrderServiceFromServer(null);
      }
    });
    this.onGoBackService = this.onGoBackService.bind(this);
  }

  _initHeader() {
    this.props.navigation.setParams({
      headerTitle: this.props.route.params?.headerTitle ?? 'Services',
      headerType:
        this.props.route.params?.headerType ?? HeaderTypes.HeaderHomeMenu,
    });
    this.props.navigation.setOptions(renderHeaderView(this));
  }

  componentWillUnmount() {
    // this.backHandler.remove();
  }

  _getAllOrderService(callback) {
    var filter = {
      limit: this.state.limit,
      offset: this.state.offset,
      orderBy: 'last_updated desc',
      conditions: [...this.state.conditions],
    };
    if (!this.state.fetchAll) {
      filter.conditions.push({
        value: this.state.filterDate + 'T00:00:00.000',
        column: 'last_updated',
        operator: '>=',
        dataType: 'String',
      });
      filter.conditions.push({
        value: this.state.filterDate + 'T23:59:59.999',
        column: 'last_updated',
        operator: '<=',
        dataType: 'String',
      });
    }
    GetAllOrderService(
      AppConstants.affordeBackOfficeDB,
      datas => {
        callback(datas);
      },
      filter,
    );
  }

  _getAllOrderServiceFromServer(dataids) {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        let syncValue = {
          limit: this.state.limit,
          offset: this.state.offset,
          conditions: [],
        };
        if (dataids == null) {
          syncValue.conditions.push({
            value:
              MasterConstants.serviceRequestPending +
              ',' +
              MasterConstants.requestForService +
              ',' +
              MasterConstants.servicePaused +
              ',' +
              MasterConstants.serviceStarted,
            column: 'status.statusId',
            operator: 'not in',
            dataType: 'String',
          });
          if (!this.state.fetchAll) {
            syncValue.conditions.push({
              value:
                '' + new Date(this.state.filterDate + 'T00:00:00').getTime(),
              column: 'lastUpdatedDateTime',
              operator: '>=',
              dataType: 'Date',
            });
            syncValue.conditions.push({
              value:
                '' + new Date(this.state.filterDate + 'T23:59:59').getTime(),
              column: 'lastUpdatedDateTime',
              operator: '<=',
              dataType: 'Date',
            });
          }
        }
        getAllOrderService(this.state.session, syncValue, isSuccess => {
          if (isSuccess) {
            this._getAllOrderService(datas => {
              if (datas.length > 0) {
                this.setState({
                  orderServiceList:
                    this.state.offset === 0
                      ? datas
                      : this.state.orderServiceList.concat(datas),
                  isLoading: false,
                  loadMore: false,
                  isPreparing: false,
                  isRefresh: false,
                  offset: this.state.offset + datas.length,
                });
              }
            });
          } else {
            this.setState({
              isLoading: false,
              loadMore: false,
              isPreparing: false,
              isRefresh: false,
            });
          }
        });
      } else {
        this.setState({
          isLoading: false,
          loadMore: false,
          isPreparing: false,
          isRefresh: false,
        });
        console.log('make internet connection..!');
      }
    });
  }

  onRefresh() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        this.setState({offset: 0}, () => {
          this._getAllOrderServiceFromServer('null');
        });
      } else {
        Toast.show(messages('makeInternetConnection'), Toast.SHORT);
      }
    });
  }

  onGoBackService() {
    this.setState({offset: 0}, () => {
      this._getAllOrderService(datas => {
        if (datas.length > 0) {
          this.setState({
            orderServiceList:
              this.state.offset === 0
                ? datas
                : this.state.orderServiceList.concat(datas),
            isLoading: false,
            isPreparing: false,
            loadMore: false,
            offset: this.state.offset + datas.length,
          });
        } else {
          this._getAllOrderServiceFromServer(null);
        }
      });
    });
  }

  _handleDatePicked = date => {
    this.setState(
      {
        orderServiceList: [],
        offset: 0,
        fetchAll: false,
        isLoading: true,
        filterDate: DateFormat(date, DATE_TIME_UI),
      },
      () => {
        this._getAllOrderService(datas => {
          if (datas.length > 0) {
            this.setState({
              orderServiceList: datas,
              isLoading: false,
              isPreparing: false,
              loadMore: false,
              offset: this.state.offset + datas.length,
            });
          } else {
            this._getAllOrderServiceFromServer(null);
          }
        });
        this._hideDateTimePicker();
      },
    );
  };

  _hideDateTimePicker = () => {
    this.setState({
      isDateTimePickerVisible: false,
    });
  };

  renderItem({item, index}) {
    return (
      <ServiceOrderItemView
        itemDetails={item}
        navigation={this.props.navigation}
        index={index}
        onGoBackService={this.onGoBackService}
      />
    );
  }

  ListEmptyView = () => {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
          marginTop: 200,
          backgroundColor: 'white',
        }}>
        <Icon
          name="bars"
          type="FontAwesome"
          style={{color: Colors.headerBackground, fontSize: 40}}
        />
        <Text style={{textAlign: 'center', color: 'darkgrey'}}>
          No Pending Task's Found
        </Text>
      </View>
    );
  };

  render() {
    if (this.state.isPreparing) {
      return <PreparingProgress />;
    } else {
      return (
        <Root>
          <View
            padder
            style={{
              backgroundColor: Styles.appViewBackground.backgroundColor,
              ...StyleSheet.absoluteFillObject,
            }}>
            <StatusBar
              backgroundColor={Styles.statusBar.backgroundColor}
              barStyle="light-content"
            />
            <Loader loading={this.state.isLoading} />
            <DateTimePicker
              date={new Date(this.state.filterDate)}
              isVisible={this.state.isDateTimePickerVisible}
              mode="date"
              onConfirm={this._handleDatePicked}
              onCancel={this._hideDateTimePicker}
            />
            <Card>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                }}>
                <TouchableNativeFeedback
                  onPress={() => {
                    this.setState(
                      {
                        orderServiceList: [],
                        offset: 0,
                        fetchAll: true,
                        isLoading: true,
                        filterDate: DateFormat(new Date(), DATE_TIME_UI),
                      },
                      () => {
                        this._getAllOrderService(datas => {
                          if (datas.length > 0) {
                            this.setState({
                              orderServiceList: datas,
                              isLoading: false,
                              isPreparing: false,
                              loadMore: false,
                              offset: this.state.offset + datas.length,
                            });
                          } else {
                            this._getAllOrderServiceFromServer(null);
                          }
                        });
                      },
                    );
                  }}>
                  <Text
                    style={{
                      margin: 10,
                      padding: 5,
                      paddingLeft: 20,
                      paddingRight: 20,
                      color: '#fff',
                      backgroundColor: Colors.headerBackground,
                      fontSize: 18,
                      fontWeight: 'bold',
                    }}>
                    All
                  </Text>
                </TouchableNativeFeedback>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    padding: 10,
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      var date = new Date(this.state.filterDate);
                      date.setDate(date.getDate() - 1);
                      this.setState(
                        {
                          orderServiceList: [],
                          offset: 0,
                          fetchAll: false,
                          isLoading: true,
                          filterDate: DateFormat(date, DATE_TIME_UI),
                        },
                        () => {
                          this._getAllOrderService(datas => {
                            if (datas.length > 0) {
                              this.setState({
                                orderServiceList: datas,
                                isLoading: false,
                                isPreparing: false,
                                loadMore: false,
                                offset: this.state.offset + datas.length,
                              });
                            } else {
                              this._getAllOrderServiceFromServer(null);
                            }
                          });
                        },
                      );
                    }}>
                    <Icon
                      name="chevron-circle-left"
                      type="FontAwesome"
                      style={{fontSize: 27, color: Colors.headerBackground}}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      marginRight: 10,
                      marginLeft: 10,
                      fontSize: 16,
                      marginTop: 5,
                    }}
                    onPress={() =>
                      this.setState({
                        isDateTimePickerVisible: true,
                      })
                    }>
                    <Text>{DateFormat(this.state.filterDate, DATE_UI)}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      var date = new Date(this.state.filterDate);
                      date.setDate(date.getDate() + 1);
                      this.setState(
                        {
                          orderServiceList: [],
                          offset: 0,
                          fetchAll: false,
                          isLoading: true,
                          filterDate: DateFormat(date, DATE_TIME_UI),
                        },
                        () => {
                          this._getAllOrderService(datas => {
                            if (datas.length > 0) {
                              this.setState({
                                orderServiceList: datas,
                                isLoading: false,
                                isPreparing: false,
                                loadMore: false,
                                offset: this.state.offset + datas.length,
                              });
                            } else {
                              this._getAllOrderServiceFromServer(null);
                            }
                          });
                        },
                      );
                    }}>
                    <Icon
                      name="chevron-circle-right"
                      type="FontAwesome"
                      style={{
                        fontSize: 27,
                        color: Colors.headerBackground,
                        marginRight: 40,
                      }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => {}}>
                    <MyIcon
                      style={{
                        fontSize: 27,
                        color: Colors.headerBackground,
                        marginRight: 10,
                      }}
                      name="filter-list"
                      size={24}
                      color={'white'}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <View
                style={{
                  backgroundColor: 'lightgrey',
                  height: 0.5,
                }}
              />
            </Card>
            <FlatList
              extraData={this.state}
              data={this.state.orderServiceList}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              renderItem={item => this.renderItem(item)}
              onEndReachedThreshold={0.1}
              onEndReached={({distanceFromEnd}) => {
                if (
                  !this.state.loadMore &&
                  this.state.offset >= this.state.limit
                ) {
                  this.setState({loadMore: true}, () => {
                    this._getAllOrderService(datas => {
                      if (datas.length > 0) {
                        this.setState({
                          orderServiceList:
                            this.state.offset === 0
                              ? datas
                              : this.state.orderServiceList.concat(datas),
                          isLoading: false,
                          isPreparing: false,
                          loadMore: false,
                          offset: this.state.offset + datas.length,
                        });
                      } else {
                        this._getAllOrderServiceFromServer(null);
                      }
                    });
                  });
                }
              }}
              keyExtractor={(item, index) => index.toString()}
              onRefresh={() => this.onRefresh()}
              refreshing={this.state.isRefresh}
              ListEmptyComponent={this.ListEmptyView}
              ListFooterComponent={
                this.state.loadMore ? (
                  <View
                    style={{
                      padding: 7,
                      flex: 1,
                      flexDirection: 'row',
                      borderRadius: 2,
                      backgroundColor: Colors.headerBackground,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <ActivityIndicator size="small" color="white" />
                    <Text style={{color: 'white', fontWeight: 'bold'}}>
                      {' Loading..'}
                    </Text>
                  </View>
                ) : null
              }
            />
          </View>
        </Root>
      );
    }
  }
}

class ServiceOrderItemView extends React.Component {
  constructor(props) {
    super(props);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({...nextProps});
  }

  render() {
    const {navigation, index} = this.props;
    return (
      <Card style={styles.card}>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate(Route.ViewTask, {
              orderId: this.state.itemDetails.order_service_id,
              hideFieldwork: true,
              onGoBack: data => {
                this.props.onGoBackService();
              },
            });
          }}>
          <CardItem>
            <Left>
              <View
                style={{
                  width: 70,
                  height: 70,
                  borderRadius: 70 / 2,
                  backgroundColor: IconColor[index % IconColor.length],
                  justifyContent: 'center',
                }}>
                <Text
                  style={[styles.text, {color: 'white', textAlign: 'center'}]}>
                  {this.state.itemDetails.order_to_name
                    .substring(0, 2)
                    .toUpperCase()}{' '}
                </Text>
              </View>
              <Body>
                <Text style={styles.createDate}>
                  <Icon
                    type="FontAwesome"
                    name="history"
                    style={{fontSize: 15}}
                  />{' '}
                  {DateFormat(
                    this.state.itemDetails.last_updated,
                    DATE_TIME_LAST_UPDATED,
                  )}
                </Text>
                <Text style={styles.text}>
                  {' '}
                  {this.state.itemDetails.order_to_name}
                </Text>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.text}> Request No : </Text>
                  <Text> {this.state.itemDetails.order_code}</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.text}> Service Type : </Text>
                  <Text> {this.state.itemDetails.service_type_name}</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.text}> Service Date : </Text>
                  <Text>
                    {' '}
                    {DateFormat(
                      this.state.itemDetails.service_date,
                      DATE_TIME_LAST_UPDATED,
                    )}
                  </Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.text}> Classification Type : </Text>
                  <Text>
                    {' '}
                    {this.state.itemDetails.classification_type_name}
                  </Text>
                </View>
                {this.state.itemDetails.service_agent_name == null ? (
                  <Text style={[styles.notes, {color: 'red'}]}>
                    {' '}
                    {'Service Agent not Assigned'}
                  </Text>
                ) : (
                  <Text style={[styles.notes, {color: '#0169f1'}]}>
                    Service Agent is {this.state.itemDetails.service_agent_name}{' '}
                  </Text>
                )}
              </Body>
            </Left>
          </CardItem>
        </TouchableOpacity>
        <CardItem style={{backgroundColor: '#f5f6fa'}}>
          <Left />
          <View>
            <Text
              note
              style={[
                styles.statusText,
                {color: STATUS_COLOR[this.state.itemDetails.status_id]},
              ]}>
              {this.state.itemDetails.status_name}
            </Text>
          </View>
        </CardItem>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  card: {
    borderRadius: 3,
    elevation: 10,
  },
  icon: {
    fontSize: 15,
    color: '#fff',
    paddingLeft: 10,
    paddingRight: 10,
  },
  fabicon: {
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'white',
    fontSize: 22,
  },
  createDate: {
    fontSize: 13,
    textAlign: 'right',
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  statusContainer: {
    paddingBottom: 2,
    paddingLeft: 3,
    paddingRight: 3,
    borderRadius: 3,
  },
  statusText: {
    borderRadius: 3,
    fontWeight: '900',
    fontSize: 15,
  },
  notes: {
    fontWeight: 'bold',
    fontSize: 13,
  },
});
