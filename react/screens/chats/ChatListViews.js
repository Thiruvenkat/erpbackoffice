/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  StatusBar,
  StyleSheet,
  View,
  Text,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Colors from '../../style/Colors';
import {Icon, Card, CardItem, Left, Body, Fab, Root} from 'native-base';
import AppConstants from '../../constant/AppConstants';

import {Styles, IconColor} from '../../style/Styles';
import {
  DateFormat,
  DATE_TIME_UI,
} from 'react-native-erp-mobile-library/react/common/Common';
import PreparingProgress from 'react-native-erp-mobile-library/react/component/PreparingProgress';
import {
  backHandler,
  renderHeaderView,
} from 'react-native-erp-mobile-library/react/util/HeaderUtil';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';

export default class ChatListView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      followupList: [
        {personName: 'Thiru'},
        {personName: 'Rajesh Kumar'},
        {personName: 'Lingesh'},
      ],
      isPreparing: false,
      filterDate: DateFormat(new Date(), DATE_TIME_UI),
      expired: true,
    };
    this.renderItem = this.renderItem.bind(this);
    this._getAllFollowUp = this._getAllFollowUp.bind(this);
  }
  componentDidMount() {
    this._initHeader();
    this._initLoginSessionInfo();
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  _initHeader() {
    this.props.navigation.setParams({
      headerTitle: this.props.route.params?.headerTitle ?? 'Chat List',
      headerType:
        this.props.route.params?.headerType ?? HeaderTypes.HeaderHomeMenu,
    });
    this.props.navigation.setOptions(renderHeaderView(this));
  }

  _initLoginSessionInfo() {
    AsyncStorage.getItem(AppConstants.keyLoginToken).then(loginToken => {
      this.setState({loginToken: loginToken});
      AsyncStorage.getItem(AppConstants.keyPublicIp).then(publicIp => {
        this.setState({publicIp: publicIp});
        AsyncStorage.getItem(AppConstants.keySessionInfo).then(sessionInfo => {
          this.setState({
            customerId: JSON.parse(sessionInfo).loggedInCustomerId,
          });
          this._getAllFollowUp();
        });
      });
    });
  }
  onRefresh() {
    this._getAllFollowUp();
  }

  _getAllFollowUp() {}

  renderItem({item, index}) {
    return (
      <ItemView
        navigation={this.props.navigation}
        itemDetails={item}
        index={index}
      />
    );
  }

  render() {
    if (this.state.isPreparing) {
      return <PreparingProgress />;
    } else {
      return (
        <Root>
          <View
            padder
            style={{backgroundColor: Styles.appViewBackground.backgroundColor}}>
            <StatusBar
              backgroundColor={Styles.statusBar.backgroundColor}
              barStyle="light-content"
            />
            <View>
              <FlatList
                extraData={this.state}
                data={this.state.followupList}
                ItemSeparatorComponent={this.FlatListItemSeparator}
                renderItem={item => this.renderItem(item)}
                keyExtractor={(item, index) => index.toString()}
                onRefresh={() => this.onRefresh()}
                refreshing={this.state.isPreparing}
              />
            </View>
          </View>
          <Fab
            style={{backgroundColor: Colors.headerBackground, size: 5}}
            direction="up"
            position="bottomRight"
            // onPress={() => { this.props.navigation.navigate(Route.ChatScreen) }}
          >
            <Icon type="FontAwesome" name="plus" style={styles.fabicon} />
          </Fab>
        </Root>
      );
    }
  }
}
class ItemView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props,
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({...nextProps});
  }

  render() {
    const {index} = this.props;
    return (
      <Card style={styles.card}>
        <TouchableOpacity
        // onPress={() => { navigation.navigate(Route.ChatScreen); }}
        >
          <CardItem>
            <Left>
              <View
                style={{
                  width: 70,
                  height: 70,
                  borderRadius: 70 / 2,
                  backgroundColor: IconColor[index % IconColor.length],
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={[styles.text, {color: 'white'}]}>
                  {this.state.itemDetails.personName
                    .substring(0, 2)
                    .toUpperCase()}{' '}
                </Text>
              </View>
              <Body>
                <View style={{flexDirection: 'row'}}>
                  <Text> {this.state.itemDetails.personName}</Text>
                </View>
              </Body>
            </Left>
          </CardItem>
        </TouchableOpacity>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  card: {
    borderRadius: 3,
    elevation: 10,
  },
  icon: {
    fontSize: 25,
    color: 'black',
    paddingLeft: 10,
    paddingRight: 10,
  },
  fabicon: {
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'white',
    fontSize: 22,
  },
  createDate: {
    fontSize: 13,
    textAlign: 'right',
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  statusContainer: {
    paddingBottom: 2,
    paddingLeft: 3,
    paddingRight: 3,
    borderRadius: 3,
  },
  statusText: {
    fontWeight: 'bold',
    color: 'white',
  },
});
