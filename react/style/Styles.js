import { StyleSheet, Platform, } from 'react-native';
import { IconColor as Iconcolor, STATUS_COLOR as StatusColor, Styles as style } from 'react-native-erp-mobile-library/react/style/Styles';


export const STATUS_COLOR = StatusColor;
export const IconColor = Iconcolor;
export const Styles = StyleSheet.create({
  ...style,
});
