import AppConstants from 'react-native-erp-mobile-library/react/constant/AppConstants';

export default {
  ...AppConstants,
};
