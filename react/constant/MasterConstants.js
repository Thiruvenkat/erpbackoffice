import MasterConstants from 'react-native-erp-mobile-library/react/constant/MasterConstants';

//Master constant values
export default {
  ...MasterConstants,
};

