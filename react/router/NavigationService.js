import { StackActions, NavigationActions } from 'react-navigation';
let navigator;

export function setTopLevelNavigator(ref) {
    navigator = ref;
}

export function navigate(routeName, params) {
    navigator.dispatch(
        NavigationActions.navigate({
            routeName,
            params
        })
    );
}

export function goBack() {
    navigator.dispatch(
        NavigationActions.goBack()
    );
}

export function navigateAndRemoveCurrentScrn(newScreenRouteName, params = {}, replaceIndex = 0) {
    const resetAction = StackActions.reset({
        index: replaceIndex,
        actions: [
            NavigationActions.navigate({ routeName: newScreenRouteName, params})
        ]
    });
    navigator.dispatch(resetAction);
}

export default {
    navigate,
    goBack
}
