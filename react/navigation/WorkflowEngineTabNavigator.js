import React, { Component } from "react";
import { Dimensions } from "react-native";
import Colors from "../style/Colors";
import { Icon, Text } from "native-base";
import { Styles } from "../style/Styles";
import { messages } from "react-native-erp-mobile-library/react/i18n/i18n";

/*workflow engine*/
import MyTask from "../screens/workflowengine/MyTask";
import GroupTask from "../screens/workflowengine/GroupTask";
import TaskHistory from "../screens/workflowengine/TaskHistory";

export const WorkflowEngineTab = createMaterialTopTabNavigator({
  Tab1: MyTask,
  Tab2: GroupTask,
  // Tab3: TaskHistory,
}, {
  initialRouteName: "Tab1",
  order: ["Tab1", "Tab2"],
  tabBarOptions: {
    scrollEnabled: true,
    labelStyle: {
      fontSize: 12,
      fontWeight: 'bold'
    },
    tabStyle: {
      width: Dimensions.get('window').width / 2,
    },
    style: {
      backgroundColor: Colors.headerBackground,
    },
    indicatorStyle: {
      backgroundColor: Colors.tabIndicator
    }
  },
});

WorkflowEngineTab.navigationOptions = ({ navigation }) => ({
  headerTitle: messages('inbox'),
  headerTintColor: "white",
  headerStyle: {
    backgroundColor: Colors.headerBackground
  },
  headerTitleStyle: {
    fontWeight: "bold",
    color: Colors.headerTitleColor
  },
  headerLeft:
    <Icon style={Styles.homeBurgerMenu} name="bars" type="FontAwesome" onPress={() => {
      navigation.dispatch(DrawerActions.openDrawer())
    }} />
});


