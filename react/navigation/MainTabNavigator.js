/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Dimensions} from 'react-native';
import {Icon} from 'native-base';

import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import Colors from 'react-native-erp-mobile-library/react/style/Colors';
import {Styles} from 'react-native-erp-mobile-library/react/style/Styles';
import Route from '../router/Route';
import {messages} from 'react-native-erp-mobile-library/react/i18n/i18n';
import {DrawerActions} from '@react-navigation/native';

import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import AdvertisementList from '../screens/advertisements/AdvertisementList';
import PostAdvertisement from '../screens/advertisements/PostAdvertisement';
import MasterConstants from 'react-native-erp-mobile-library/react/constant/MasterConstants';
import LandingDashboard from '../screens/dashboard/LandingDashboard';
import OrderList from 'react-native-erp-mobile-library/react/screens/orders/OrderList';
import NonAllocatedTaskList from '../screens/services/NonAllocatedTaskList';
import NonAppointmentTaskList from '../screens/services/NonAppointmentTaskList';
import SalesFollowUp from '../screens/followup/SalesFollowUpList';
import ServiceFollowUp from '../screens/followup/ServiceFollowUpList';
import CustomerList from 'react-native-erp-mobile-library/react/screens/customers/CustomerList';
import AllocateTask from '../screens/services/AllocateTask';
import PrepareBookingQuestion from 'react-native-erp-mobile-library/react/screens/services/PrepareBookingQuestion';
import ViewTask from 'react-native-erp-mobile-library/react/screens/services/ViewTask';
import SerialNoProductList from 'react-native-erp-mobile-library/react/screens/products/SerialNoProductList';
import BookingScreen from '../screens/services/BookingScreen';
import SelectLocationScreen from 'react-native-erp-mobile-library/react/screens/services/SelectLocationScreen';
import SelectGeoLevel from 'react-native-erp-mobile-library/react/screens/services/SelectGeoLevel';
import SearchProduct from 'react-native-erp-mobile-library/react/screens/products/SearchProduct';
import AddProductScreen from 'react-native-erp-mobile-library/react/screens/products/AddProductScreen';
import OtpVerification from 'react-native-erp-mobile-library/react/screens/services/OTPVerification';
import FeedBack from 'react-native-erp-mobile-library/react/screens/services/FeedBack';
import ViewFollowUp from '../screens/followup/ViewFollowUp';
import ViewOrderDetailTabNavigator from 'react-native-erp-mobile-library/react/screens/orders/ViewOrderDetailTabNavigator';
import CreateOrder from 'react-native-erp-mobile-library/react/screens/orders/CreateOrder';
import ProductList from 'react-native-erp-mobile-library/react/screens/products/ProductList';
import ScanSerialNo from 'react-native-erp-mobile-library/react/screens/orders/ScanSerialNo';

const DashboardTopTab = createMaterialTopTabNavigator();
const TaskListTopTab = createMaterialTopTabNavigator();
const FollowUpTopTab = createMaterialTopTabNavigator();

const CommonBottomTab = createBottomTabNavigator();
const RetailerBottomTab = createBottomTabNavigator();

const DashboardTabStack = createStackNavigator();
const TaskListTabStack = createStackNavigator();
const FollowUpTabStack = createStackNavigator();
const AdvertisementStack = createStackNavigator();
const MyOrdersTabStack = createStackNavigator();

const icons = {
  Dashboard: {name: 'view-dashboard', type: 'MaterialCommunityIcons'},
  ServiceTasks: {name: 'tasks', type: 'FontAwesome'},
  FollowUp: {name: 'user', type: 'FontAwesome'},
  Advertisement: {name: 'picture-o', type: 'FontAwesome'},
  MyOrders: {name: 'shopping-cart', type: 'FontAwesome'},
};

const tabOptions = {
  labelStyle: {
    fontSize: 12,
    fontWeight: 'bold',
  },
  lazy: false,
  showIcon: true,
  tabStyle: {
    width: Dimensions.get('window').width / 2,
  },
  indicatorStyle: {
    backgroundColor: Colors.tabIndicator,
  },
};

function tabBarIcon(name, type, focused) {
  return (
    <Icon
      type={type}
      name={name}
      style={{
        marginBottom: -3,
        color: focused ? Colors.tabIconSelected : Colors.tabIconDefault,
        fontSize: focused ? 26 : 18,
      }}
    />
  );
}

/************** DASHBOARD ***************/

const DashboardTabListNavigator = () => (
  <DashboardTopTab.Navigator headerMode="none">
    <DashboardTopTab.Screen
      options={{
        headerStyle: {
          backgroundColor: Colors.headerBackground,
        },
      }}
      name={Route.Daily}
      initialParams={{
        dashboardType: MasterConstants.daily,
      }}
      component={LandingDashboard}
    />
    <DashboardTopTab.Screen
      options={{
        headerStyle: {
          backgroundColor: Colors.headerBackground,
        },
      }}
      name={Route.Monthly}
      initialParams={{dashboardType: MasterConstants.monthly}}
      component={LandingDashboard}
    />
  </DashboardTopTab.Navigator>
);

const DashboardStacks = () => (
  <DashboardTabStack.Navigator>
    <DashboardTabStack.Screen
      options={props => {
        return {
          title: messages('dashboard'),
          headerStyle: {
            backgroundColor: Colors.headerBackground,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: () => (
            <Icon
              style={Styles.homeBurgerMenu}
              name="bars"
              type="FontAwesome"
              onPress={() => {
                props.navigation.dispatch(DrawerActions.openDrawer());
              }}
            />
          ),
        };
      }}
      name={Route.Dashboard}
      component={DashboardTabListNavigator}
    />
    <DashboardTabStack.Screen name={Route.OrderList} component={OrderList} />
    <DashboardTabStack.Screen
      name={Route.ViewOrderTabNavigator}
      component={ViewOrderDetailTabNavigator}
    />
    <DashboardTabStack.Screen
      initialParams={{
        headerTitle: 'Create order',
        headerType: HeaderTypes.HeaderBack,
      }}
      name={Route.CreateOrder}
      component={CreateOrder}
    />
    <DashboardTabStack.Screen
      initialParams={{
        headerTitle: 'Scan Serial.no',
        headerType: HeaderTypes.HeaderBack,
      }}
      name={Route.ProductList}
      component={ProductList}
    />
    <DashboardTabStack.Screen
      name={Route.ScanSerialNo}
      component={ScanSerialNo}
    />
  </DashboardTabStack.Navigator>
);

/************** SERVICES ****************/

const taskListTopTabNavigator = () => (
  <TaskListTopTab.Navigator
    headerMode="none"
    initialRouteName={'NonAllocatedTaskList'}>
    <TaskListTopTab.Screen
      options={{
        headerStyle: {
          backgroundColor: Colors.headerBackground,
        },
      }}
      initialParams={{
        headerTitle: messages('pendingAllocations'),
        headerType: HeaderTypes.HeaderHomeMenu,
      }}
      name={'NonAllocatedTaskList'}
      component={NonAllocatedTaskList}
    />
    <TaskListTopTab.Screen
      options={{
        headerStyle: {
          backgroundColor: Colors.headerBackground,
        },
      }}
      initialParams={{
        headerTitle: messages('pendingAppointments'),
        headerType: HeaderTypes.HeaderHomeMenu,
      }}
      name={'NonAppointmentTaskList'}
      component={NonAppointmentTaskList}
    />
  </TaskListTopTab.Navigator>
);

const taskListStacks = () => (
  <TaskListTabStack.Navigator>
    <TaskListTabStack.Screen
      options={props => {
        return {
          title: messages('Services'),
          headerStyle: {
            backgroundColor: Colors.headerBackground,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: () => (
            <Icon
              style={Styles.homeBurgerMenu}
              name="bars"
              type="FontAwesome"
              onPress={() => {
                props.navigation.dispatch(DrawerActions.openDrawer());
              }}
            />
          ),
        };
      }}
      name={'ServiceTasks'}
      component={taskListTopTabNavigator}
    />
    <TaskListTabStack.Screen
      name={Route.CustomerList}
      component={CustomerList}
    />
    <TaskListTabStack.Screen name={Route.Booking} component={BookingScreen} />
    <TaskListTabStack.Screen
      name={Route.AllocateTask}
      component={AllocateTask}
    />
    <TaskListTabStack.Screen
      name={Route.PrepareBookingQuestion}
      component={PrepareBookingQuestion}
    />
    <TaskListTabStack.Screen name={Route.ViewTask} component={ViewTask} />
    <TaskListTabStack.Screen
      name={'NonAllocatedTaskList'}
      component={NonAllocatedTaskList}
    />
    <TaskListTabStack.Screen
      name={'NonAppointmentTaskList'}
      component={NonAppointmentTaskList}
    />
    <TaskListTabStack.Screen
      initialParams={{
        headerTitle: 'Select product',
        headerType: HeaderTypes.HeaderBack,
      }}
      name={Route.SerialNoProductList}
      component={SerialNoProductList}
    />
    <TaskListTabStack.Screen
      initialParams={{
        headerTitle: 'Select product',
        headerType: HeaderTypes.HeaderBack,
      }}
      name={Route.SelectLocationScreen}
      component={SelectLocationScreen}
    />
    <TaskListTabStack.Screen
      initialParams={{
        headerTitle: 'Select product',
        headerType: HeaderTypes.HeaderBack,
      }}
      name={Route.SelectGeoLevel}
      component={SelectGeoLevel}
    />
    <TaskListTabStack.Screen
      initialParams={{
        headerTitle: 'Search product',
        headerType: HeaderTypes.HeaderBack,
      }}
      name={Route.SearchProduct}
      component={SearchProduct}
    />
    <TaskListTabStack.Screen
      initialParams={{
        headerTitle: 'Product details',
        headerType: HeaderTypes.HeaderBack,
      }}
      name={Route.AddProductScreen}
      component={AddProductScreen}
    />
    <TaskListTabStack.Screen
      initialParams={{
        headerTitle: 'OTP Verify',
        headerType: HeaderTypes.HeaderBack,
      }}
      name={Route.OTPVerification}
      component={OtpVerification}
    />
    <TaskListTabStack.Screen name={Route.FeedBack} component={FeedBack} />
  </TaskListTabStack.Navigator>
);

/************** FOLLOWUP ****************/

const followUpTopTabNavigator = () => (
  <FollowUpTopTab.Navigator
    headerMode="none"
    initialRouteName={'SalesFollowUpList'}>
    <FollowUpTopTab.Screen
      options={{
        headerStyle: {
          backgroundColor: Colors.headerBackground,
        },
      }}
      initialParams={{
        headerTitle: messages('sales/followUp'),
        headerType: HeaderTypes.HeaderHomeMenu,
      }}
      name={'SalesFollowUpList'}
      component={SalesFollowUp}
    />
    <FollowUpTopTab.Screen
      options={{
        headerStyle: {
          backgroundColor: Colors.headerBackground,
        },
      }}
      initialParams={{
        headerTitle: messages('service/followUp'),
        headerType: HeaderTypes.HeaderHomeMenu,
      }}
      name={'ServiceFollowUpList'}
      component={ServiceFollowUp}
    />
  </FollowUpTopTab.Navigator>
);

const followUpListStacks = () => (
  <FollowUpTabStack.Navigator>
    <FollowUpTabStack.Screen
      options={props => {
        return {
          title: messages('Follow-up'),
          headerStyle: {
            backgroundColor: Colors.headerBackground,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: () => (
            <Icon
              style={Styles.homeBurgerMenu}
              name="bars"
              type="FontAwesome"
              onPress={() => {
                props.navigation.dispatch(DrawerActions.openDrawer());
              }}
            />
          ),
        };
      }}
      name={'FollowUp'}
      component={followUpTopTabNavigator}
    />
    <FollowUpTabStack.Screen
      name={Route.ViewFollowUp}
      component={ViewFollowUp}
    />
    <FollowUpTabStack.Screen
      name={Route.ViewOrderTabNavigator}
      component={ViewOrderDetailTabNavigator}
    />
  </FollowUpTabStack.Navigator>
);

/************** ADVERTISMENT ***********/

const AdvertisementStacks = () => (
  <AdvertisementStack.Navigator>
    <AdvertisementStack.Screen
      initialParams={{
        headerTitle: messages('advertisement'),
        headerType: HeaderTypes.HeaderHomeMenu,
      }}
      name={Route.Advertisement}
      component={AdvertisementList}
    />
    <AdvertisementStack.Screen
      name={Route.PostAd}
      component={PostAdvertisement}
    />
  </AdvertisementStack.Navigator>
);

/************ MYORDERS *************/

const MyOrdersStacks = () => (
  <MyOrdersTabStack.Navigator>
    <MyOrdersTabStack.Screen name={Route.MyOrders} component={OrderList} />
    <MyOrdersTabStack />
  </MyOrdersTabStack.Navigator>
);

//
export const CommonTabNavigator = () => (
  <CommonBottomTab.Navigator
    tabBarOptions={tabOptions}
    screenOptions={props => {
      return {
        tabBarIcon: ({focused}) =>
          tabBarIcon(
            icons[props.route.name].name,
            icons[props.route.name].type,
            focused,
          ),
      };
    }}
    initialRouteName={Route.Dashboard}>
    <CommonBottomTab.Screen
      name={Route.Dashboard}
      options={{
        title: messages('dashboard'),
        headerStyle: {
          backgroundColor: '#f4511e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}
      component={DashboardStacks}
    />
    <CommonBottomTab.Screen
      name={'ServiceTasks'}
      options={{
        title: messages('Services'),
        headerStyle: {
          backgroundColor: '#f4511e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}
      component={taskListStacks}
    />
    <CommonBottomTab.Screen
      name={'FollowUp'}
      options={{
        title: messages('Follow-up'),
        headerStyle: {
          backgroundColor: '#f4511e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}
      component={followUpListStacks}
    />
    <CommonBottomTab.Screen
      name={Route.Advertisement}
      component={AdvertisementStacks}
    />
  </CommonBottomTab.Navigator>
);

export const RetailerTabNavigator = () => (
  <RetailerBottomTab.Navigator
    tabBarOptions={tabOptions}
    screenOptions={props => {
      return {
        tabBarIcon: ({focused}) =>
          tabBarIcon(
            icons[props.route.name].name,
            icons[props.route.name].type,
            focused,
          ),
      };
    }}
    initialRouteName={Route.Dashboard}>
    <RetailerBottomTab.Screen
      name={Route.Dashboard}
      options={{
        title: messages('dashboard'),
        headerStyle: {
          backgroundColor: '#f4511e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}
      component={DashboardStacks}
    />
    <RetailerBottomTab.Screen
      name={Route.MyOrders}
      component={MyOrdersStacks}
    />
  </RetailerBottomTab.Navigator>
);
