import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import DrawerScreen from './DrawerScreen';
import {CommonTabNavigator, RetailerTabNavigator} from './MainTabNavigator';
import OrderTypeList from 'react-native-erp-mobile-library/react/screens/orders/OrderTypeList';
import OrderList from 'react-native-erp-mobile-library/react/screens/orders/OrderList';
import Route from '../router/Route';
import MasterConstants from 'react-native-erp-mobile-library/react/constant/MasterConstants';
import {messages} from 'react-native-erp-mobile-library/react/i18n/i18n';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import AboutScreen from '../screens/main/AboutScreen';
import SyncData from 'react-native-erp-mobile-library/react/screens/masters/SyncData';
import ProductList from 'react-native-erp-mobile-library/react/screens/products/ProductList';
import ProductDetailScreen from 'react-native-erp-mobile-library/react/screens/products/ProductDetailScreen';
import CustomerList from 'react-native-erp-mobile-library/react/screens/customers/CustomerList';
import ViewCustomerScreen from 'react-native-erp-mobile-library/react/screens/customers/ViewCustomerScreen';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import CustomerProfileDetails from 'react-native-erp-mobile-library/react/screens/customers/CustomerProfileDetails';
import CustomerFinanceDashboard from 'react-native-erp-mobile-library/react/screens/customers/CustomerFinanceDashboard';
import CreateOrder from 'react-native-erp-mobile-library/react/screens/orders/CreateOrder';
import ViewOrderDetailTabNavigator from 'react-native-erp-mobile-library/react/screens/orders/ViewOrderDetailTabNavigator';
import ViewOrderSummary from 'react-native-erp-mobile-library/react/screens/orders/ViewOrderSummary';
import ViewOrderProducts from 'react-native-erp-mobile-library/react/screens/orders/ViewOrderProducts';
import FieldworkActivityList from '../screens/fieldwork/FieldworkActivityList';
import FieldworkCustomerList from '../screens/fieldwork/FieldworkCustomerList';
import FieldworkEventList from '../screens/fieldwork/FieldworkEventList';
import Colors from 'react-native-erp-mobile-library/react/style/Colors';
import ScanSerialNo from 'react-native-erp-mobile-library/react/screens/orders/ScanSerialNo';
import TravelLocationMapView from '../screens/fieldwork/TravelLocationMapView';
import FieldworkEmployeeList from '../screens/fieldwork/FieldworkEmployeeList';
import ARAPFinanceList from '../screens/finance/ARAPFinanceList';
import UnClearedList from '../screens/finance/UnClearedList';
import CreateReceiptPayout from '../screens/finance/CreateReceiptPayout';
import PlanComponentDetail from '../screens/finance/PlanComponentDetail';
import PlanSummaryDetail from '../screens/finance/PlanComponentDetail';
import {Styles} from 'react-native-erp-mobile-library/react/style/Styles';
import {Icon} from 'native-base';
import {DrawerActions} from '@react-navigation/native';
import {Dimensions} from 'react-native';
import StockList from '../screens/stock/StockList';
import ProfileScreen from 'react-native-erp-mobile-library/react/screens/customers/ProfileScreen';
import SelectLocationScreen from 'react-native-erp-mobile-library/react/screens/services/SelectLocationScreen';
import SelectGeoLevel from 'react-native-erp-mobile-library/react/screens/services/SelectGeoLevel';
import MyTask from '../screens/workflowengine/MyTask';
import GroupTask from '../screens/workflowengine/GroupTask';
import AllocateTask from '../screens/services/AllocateTask';
import TenantDetails from '../screens/master/tenantDetails';
import FinanceListTabs from './FinanceTabNaviator';
import VerifyOTP from 'react-native-erp-mobile-library/react/screens/customers/VerifyOTP';
import ViewTask from 'react-native-erp-mobile-library/react/screens/services/ViewTask';
import BookingListScreen from 'react-native-erp-mobile-library/react/screens/services/BookingListScreen';

const DrawerCommon = createDrawerNavigator();
const DrawerRetailer = createDrawerNavigator();

const viewOrderTab = createMaterialTopTabNavigator();
const financeTabNavigator = createMaterialTopTabNavigator();
const customerDetailTabNavigator = createMaterialTopTabNavigator();
const WorkflowTabNavigator = createMaterialTopTabNavigator();
const ServiceTabNavigator = createMaterialTopTabNavigator();

const ProfileStack = createStackNavigator();
const OrderStack = createStackNavigator();
const FinanceStack = createStackNavigator();
const ProductStack = createStackNavigator();
const StockStack = createStackNavigator();
const CustomerStack = createStackNavigator();
const CustomerFinanceStack = createStackNavigator();
const ServiceStack = createStackNavigator();
const FieldworkStack = createStackNavigator();
const SyncStack = createStackNavigator();
const AboutStack = createStackNavigator();
const WorkflowStack = createStackNavigator();
const TenantDetailStack = createStackNavigator();

const drawerContent = props => <DrawerScreen {...props} />;

export const DrawerNavigator = () => (
  <DrawerCommon.Navigator headerMode="none" drawerContent={drawerContent}>
    <DrawerCommon.Screen
      name={Route.DrawerCommon}
      component={CommonTabNavigator}
    />
    <DrawerCommon.Screen
      initialParams={{
        headerTitle: 'Profile',
        headerType: HeaderTypes.HeaderBack,
      }}
      name={Route.ProfileScreen}
      component={profileStack}
    />
    <DrawerCommon.Screen name={Route.OrderStack} component={orderStack} />
    <DrawerCommon.Screen
      name={Route.FinanceListTabs}
      component={financeStack}
    />
    <DrawerCommon.Screen name={'ProductStack'} component={productStack} />
    <DrawerCommon.Screen name={'StockStack'} component={stockStack} />
    <DrawerCommon.Screen name={'ServiceHistory'} component={serviceStack} />
    <DrawerCommon.Screen name={Route.CustomerList} component={customerStack} />
    <DrawerCommon.Screen name={'WorkflowStack'} component={workflowStack} />
    <DrawerCommon.Screen name={'AttendenceStack'} component={fieldworkStack} />
    <DrawerCommon.Screen
      name={'DrawerTenantDetails'}
      component={tenantListStack}
    />
    <DrawerCommon.Screen
      name={'DrawerTechnicianList'}
      component={customerStack}
    />
    <DrawerCommon.Screen
      name={Route.FieldworkActivityList}
      component={fieldworkStack}
    />
    <DrawerCommon.Screen
      name={Route.FieldworkEmployeeList}
      component={fieldworkStack}
    />
    <DrawerCommon.Screen name={Route.syncData} component={syncStack} />
    <DrawerCommon.Screen name={Route.AboutStack} component={aboutStack} />
  </DrawerCommon.Navigator>
);

export const DrawerNavigatorRetailer = () => (
  <DrawerRetailer.Navigator headerMode="none" drawerContent={drawerContent}>
    <DrawerRetailer.Screen
      name={Route.DrawerRetailer}
      component={RetailerTabNavigator}
    />
    <DrawerRetailer.Screen name={Route.OrderStack} component={orderStack} />
  </DrawerRetailer.Navigator>
);

/* ------------------------ order -----------------*/

export const ViewOrderTabStack = navigation => (
  <viewOrderTab.Navigator>
    <viewOrderTab.Screen
      options={{
        headerStyle: {
          backgroundColor: Colors.headerBackground,
        },
      }}
      initialParams={{
        parentProps: navigation.screenProps.propsValue,
        parentState: navigation.screenProps.state,
      }}
      name={Route.Summary}
      component={ViewOrderSummary}
    />
    <viewOrderTab.Screen
      options={{
        headerStyle: {
          backgroundColor: Colors.headerBackground,
        },
      }}
      initialParams={{
        parentProps: navigation.screenProps.propsValue,
        parentState: navigation.screenProps.state,
      }}
      name={Route.Produsts}
      component={ViewOrderProducts}
    />
  </viewOrderTab.Navigator>
);

export const orderStack = () => (
  <OrderStack.Navigator>
    <OrderStack.Screen
      initialParams={{
        headerTitle: messages('orderTypeList'),
        headerType: HeaderTypes.HeaderHomeMenu,
        orderTypes: [
          MasterConstants.orderTypeQuotation,
          MasterConstants.orderTypeSalesOrder,
          MasterConstants.orderTypeGrn,
          MasterConstants.orderTypeCashSales,
        ],
      }}
      name={Route.OrderTypeList}
      component={OrderTypeList}
    />
    <OrderStack.Screen name={Route.OrderList} component={OrderList} />
    <OrderStack.Screen
      initialParams={{
        headerTitle: 'Order Details',
        headerType: HeaderTypes.HeaderBack,
      }}
      name={Route.ViewOrderTabNavigator}
      component={ViewOrderDetailTabNavigator}
    />
    <OrderStack.Screen
      initialParams={{
        headerTitle: 'Verification',
        headerType: HeaderTypes.HeaderBack,
      }}
      name={Route.VerifyOTP}
      component={VerifyOTP}
    />
    <OrderStack.Screen
      initialParams={{
        headerTitle: 'Create order',
        headerType: HeaderTypes.HeaderBack,
      }}
      name={Route.CreateOrder}
      component={CreateOrder}
    />
    <OrderStack.Screen
      initialParams={{
        headerTitle: 'Select product',
        headerType: HeaderTypes.HeaderBack,
      }}
      name={Route.ProductList}
      component={ProductList}
    />
    <OrderStack.Screen
      initialParams={{
        headerTitle: 'Scan Serial.no',
        headerType: HeaderTypes.HeaderBack,
      }}
      name={Route.ScanSerialNo}
      component={ScanSerialNo}
    />
  </OrderStack.Navigator>
);

/* ----------------- profile -------------------*/

export const profileStack = () => (
  <ProfileStack.Navigator>
    <ProfileStack.Screen
      initialParams={{
        headerTitle: 'Profile',
        headerType: HeaderTypes.HeaderBack,
      }}
      name={Route.ProfileScreen}
      component={ProfileScreen}
    />
    <ProfileStack.Screen
      name={Route.SelectLocationScreen}
      component={SelectLocationScreen}
    />
    <ProfileStack.Screen
      name={Route.SelectGeoLevel}
      component={SelectGeoLevel}
    />
  </ProfileStack.Navigator>
);

/*------------------- tenant list ----------------*/

export const tenantListStack = () => (
  <TenantDetailStack.Navigator>
    <TenantDetailStack.Screen
      initialParams={{
        headerTitle: 'Tenant List',
        headerType: HeaderTypes.HeaderHomeMenu,
      }}
      name={Route.TenantDetails}
      component={TenantDetails}
    />
  </TenantDetailStack.Navigator>
);

/* -------------------- finance -----------------*/

const tabOptions = {
  labelStyle: {
    fontSize: 12,
    fontWeight: 'bold',
  },
  lazy: false,
  showIcon: true,
  tabStyle: {
    width: Dimensions.get('window').width / 3,
  },
  indicatorStyle: {
    backgroundColor: Colors.headerBackground,
  },
};

export const FinanceTabNavigator = props => (
  <financeTabNavigator.Navigator tabBarOptions={tabOptions}>
    <financeTabNavigator.Screen
      options={{
        headerStyle: {
          backgroundColor: Colors.headerBackground,
        },
      }}
      initialParams={{
        parentProps: props.screenProps
          ? props.screenProps.parentNavigation.route.params
          : props,
        inOutFlag: MasterConstants.inOutFlagReceivable,
        initialFilter: '104112',
      }}
      name={'Receivable'}
      component={ARAPFinanceList}
    />
    <financeTabNavigator.Screen
      options={{
        headerStyle: {
          backgroundColor: Colors.headerBackground,
        },
      }}
      initialParams={{
        parentProps: props.screenProps
          ? props.screenProps.parentNavigation.route.params
          : props,
        inOutFlag: MasterConstants.inOutFlagPayable,
      }}
      name={'Payable'}
      component={ARAPFinanceList}
    />
    {!props.screenProps.info.protectedViewEnabled && (
      <financeTabNavigator.Screen
        options={{
          headerStyle: {
            backgroundColor: Colors.headerBackground,
          },
        }}
        initialParams={{
          parentProps: props.screenProps
            ? props.screenProps.parentNavigation.route.params
            : props,
          headerTitle: 'Uncleared',
          headerType: HeaderTypes.HeaderHomeMenu,
        }}
        name={'UnCleared'}
        component={UnClearedList}
      />
    )}
  </financeTabNavigator.Navigator>
);

export const financeStack = props => (
  <FinanceStack.Navigator>
    <FinanceStack.Screen
      initialParams={{
        customerData: props.route.params?.customerData ?? '',
        state:
          props.route.state !== undefined
            ? props.route.state.routes[0].state
            : '',
        headerTitle: props.route.params?.headerTitle ?? 'Finance',
        headerType:
          props.route.params?.headerType ?? HeaderTypes.HeaderHomeMenu,
      }}
      name={Route.FinanceListTabs}
      component={FinanceListTabs}
    />
    <FinanceStack.Screen
      name={Route.CreateReceiptPayout}
      component={CreateReceiptPayout}
    />
    <FinanceStack.Screen
      name={Route.PlanSummaryDetail}
      component={PlanSummaryDetail}
    />
    <FinanceStack.Screen
      name={Route.PlanComponentDetail}
      component={PlanComponentDetail}
    />
  </FinanceStack.Navigator>
);

/* -------------------- customer ----------------------*/

export const customerStack = prop => (
  <CustomerStack.Navigator>
    <CustomerStack.Screen
      initialParams={{
        headerTitle:
          prop.route.name === 'DrawerTechnicianList'
            ? 'Technicians'
            : messages('Customers'),
        headerType: HeaderTypes.HeaderHomeMenu,
        customerTypeId:
          prop.route.name === 'DrawerTechnicianList'
            ? MasterConstants.CustomerTypeEmployee
            : MasterConstants.CustomerTypeCustomerAndSupplier,
      }}
      name={Route.CustomerList}
      component={CustomerList}
    />
    <CustomerStack.Screen
      initialParams={{
        headerTitle: messages('customerDetails'),
        headerType: HeaderTypes.HeaderBack,
      }}
      name={Route.ViewCustomerScreen}
      component={ViewCustomerScreen}
    />
    <CustomerStack.Screen
      initialParams={{
        headerTitle: messages('customerDetails'),
        headerType: HeaderTypes.HeaderBack,
      }}
      name={Route.CustomerDetailsTabnavigator}
      component={CustomerDetailTabNavigator}
    />
    <CustomerStack.Screen
      options={{
        headerShown: false,
      }}
      initialParams={{
        headerTitle: messages('Customer Finance'),
        headerType: HeaderTypes.HeaderBack,
      }}
      name={'customerFinance'}
      component={customerFinanceStack}
    />
  </CustomerStack.Navigator>
);

export const CustomerDetailTabNavigator = navigation => (
  <customerDetailTabNavigator.Navigator>
    <customerDetailTabNavigator.Screen
      options={{
        headerStyle: {
          backgroundColor: Colors.headerBackground,
        },
      }}
      initialParams={{
        customerId: navigation.customerId,
        parentNavigation: navigation.parentNavigation,
        headerTitle: messages('profile'),
        headerType: HeaderTypes.HeaderHomeMenu,
      }}
      name={Route.CustomerProfileDetails}
      component={CustomerProfileDetails}
    />
    <customerDetailTabNavigator.Screen
      options={{
        headerStyle: {
          backgroundColor: Colors.headerBackground,
        },
      }}
      initialParams={{
        customerId: navigation.customerId,
        parentNavigation: navigation.parentNavigation,
        headerTitle: messages('finance'),
        headerType: HeaderTypes.HeaderHomeMenu,
      }}
      name={Route.CustomerFinanceDashboard}
      component={CustomerFinanceDashboard}
    />
  </customerDetailTabNavigator.Navigator>
);

export const customerFinanceStack = props => (
  <CustomerFinanceStack.Navigator>
    <CustomerFinanceStack.Screen
      initialParams={{
        customerData: props.route.params?.customerData ?? '',
        customerId: props.route.params?.customerData.customer_id ?? '',
        state:
          props.route.state !== undefined
            ? props.route.state.routes[0].state
            : '',
        headerTitle: props.route.params?.headerTitle ?? 'Finance',
        headerType:
          props.route.params?.headerType ?? HeaderTypes.HeaderHomeMenu,
      }}
      name={Route.FinanceListTabs}
      component={FinanceListTabs}
    />
    <CustomerFinanceStack.Screen
      name={Route.CreateReceiptPayout}
      component={CreateReceiptPayout}
    />
  </CustomerFinanceStack.Navigator>
);

/* ------------------- product ------------------*/

export const productStack = () => (
  <ProductStack.Navigator>
    <ProductStack.Screen
      initialParams={{
        headerTitle: messages('products'),
        headerType: HeaderTypes.HeaderHomeMenu,
        screenType: 'view',
      }}
      name={'ProductStack'}
      component={ProductList}
    />
    <ProductStack.Screen
      initialParams={{
        headerTitle: messages('productDetails'),
        headerType: HeaderTypes.HeaderBack,
        screenType: 'view',
      }}
      name={Route.ProductDetailScreen}
      component={ProductDetailScreen}
    />
  </ProductStack.Navigator>
);

/* --------------- stock ---------------- */

export const stockStack = () => (
  <StockStack.Navigator>
    <StockStack.Screen
      initialParams={{
        headerTitle: messages('Stock'),
        headerType: HeaderTypes.HeaderHomeMenu,
        screenType: 'view',
      }}
      name={'StockStack'}
      component={StockList}
    />
  </StockStack.Navigator>
);

/* --------------- workflow -----------------*/

const workflowTabOptions = {
  labelStyle: {
    fontSize: 12,
    fontWeight: 'bold',
  },
  lazy: false,
  showIcon: true,
  tabStyle: {
    width: Dimensions.get('window').width / 2,
  },
  indicatorStyle: {
    backgroundColor: Colors.headerBackground,
  },
};

export const workflowStack = props => (
  <WorkflowStack.Navigator>
    <WorkflowStack.Screen
      options={prop => {
        return {
          title: messages('Inbox'),
          headerStyle: {
            backgroundColor: Colors.headerBackground,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: () => (
            <Icon
              style={Styles.homeBurgerMenu}
              name="bars"
              type="FontAwesome"
              onPress={() => {
                prop.navigation.dispatch(DrawerActions.openDrawer());
              }}
            />
          ),
        };
      }}
      name={'WorkflowStack'}
      component={workflowTopTab}
    />
    <WorkflowStack.Screen
      initialParams={{
        headerTitle: 'Order Details',
        headerType: HeaderTypes.HeaderBack,
      }}
      name={Route.ViewOrderTabNavigator}
      component={ViewOrderDetailTabNavigator}
    />
    <WorkflowStack.Screen
      options={{
        headerStyle: {
          backgroundColor: Colors.headerBackground,
        },
      }}
      initialParams={{
        screenProps: props.navigation.screenProps,
        headerTitle: 'AllocateTask',
        headerType: 'home',
      }}
      name={Route.AllocateTask}
      component={AllocateTask}
    />
    <WorkflowStack.Screen
      initialParams={{
        headerTitle: 'Scan Serial.no',
        headerType: HeaderTypes.HeaderBack,
      }}
      name={'ScanSerialNo'}
      component={ScanSerialNo}
    />
  </WorkflowStack.Navigator>
);

export const workflowTopTab = () => (
  <WorkflowTabNavigator.Navigator tabBarOptions={workflowTabOptions}>
    <WorkflowTabNavigator.Screen name={'MyTask'} component={MyTask} />
    <WorkflowTabNavigator.Screen name={'GroupTask'} component={GroupTask} />
  </WorkflowTabNavigator.Navigator>
);

/* ------------ services ----------------------*/

export const bookingTabNavigator = prop => (
  <ServiceTabNavigator.Navigator tabBarOptions={tabOptions}>
    <ServiceTabNavigator.Screen
      name={'Pending'}
      component={props => (
        <BookingListScreen
          screenProps={{parentNavigation: prop}}
          {...props}
          bookingStatus={[MasterConstants.serviceRequestPending]}
        />
      )}
    />
    <ServiceTabNavigator.Screen
      name={'Completed'}
      component={props => (
        <BookingListScreen
          screenProps={{parentNavigation: prop}}
          {...props}
          bookingStatus={[
            MasterConstants.serviceCompleted,
            MasterConstants.serviceClosed,
            MasterConstants.statusInvoiced,
          ]}
        />
      )}
    />
    <ServiceTabNavigator.Screen
      name={'Requests'}
      component={props => (
        <BookingListScreen
          screenProps={{parentNavigation: prop}}
          {...props}
          bookingStatus={[
            MasterConstants.serviceRequestForService,
            MasterConstants.serviceRequestCancel,
            MasterConstants.servicePaused,
            MasterConstants.serviceStarted,
            MasterConstants.workInProgress,
            MasterConstants.workNotStarted,
          ]}
        />
      )}
    />
  </ServiceTabNavigator.Navigator>
);

export const serviceStack = () => (
  <ServiceStack.Navigator>
    <ServiceStack.Screen
      options={props => {
        return {
          title: messages('Services'),
          headerStyle: {
            backgroundColor: Colors.headerBackground,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: () => (
            <Icon
              style={Styles.homeBurgerMenu}
              name="bars"
              type="FontAwesome"
              onPress={() => {
                props.navigation.dispatch(DrawerActions.openDrawer());
              }}
            />
          ),
        };
      }}
      name={Route.BookingListScreen}
      component={bookingTabNavigator}
    />
    <ServiceStack.Screen
      initialParams={{
        headerTitle: 'View Task',
        headerType: HeaderTypes.HeaderBack,
      }}
      name={Route.ViewTask}
      component={ViewTask}
    />
  </ServiceStack.Navigator>
);

/* -------------- Attendence & fieldwork -------------- */

export const fieldworkStack = props => {
  return (
    <FieldworkStack.Navigator initialRouteName={props.route.name}>
      <FieldworkStack.Screen
        initialParams={{
          screenType:
            props.route.name === 'AttendenceStack' ? 'attendence' : 'editable',
          headerTitle:
            props.route.name === 'AttendenceStack'
              ? 'Attendence'
              : "Field Activity's",
          trackingType:
            props.route.name === 'AttendenceStack'
              ? MasterConstants.attendence
              : MasterConstants.fieldWork,
        }}
        name={Route.FieldworkActivityList}
        component={FieldworkActivityList}
      />
      <FieldworkStack.Screen
        name={Route.TravelLocationMapView}
        component={TravelLocationMapView}
      />
      <FieldworkStack.Screen
        name={Route.FieldworkCustomerList}
        component={FieldworkCustomerList}
      />
      <FieldworkStack.Screen
        name={Route.FieldworkEventList}
        component={FieldworkEventList}
      />
      <FieldworkStack.Screen
        initialParams={{
          headerTitle: 'Sales Person',
          customerTypeId: [MasterConstants.CustomerTypeEmployee],
          screenType: 'view',
        }}
        name={Route.FieldworkEmployeeList}
        component={FieldworkEmployeeList}
      />
      <FieldworkStack.Screen name={Route.CreateOrder} component={CreateOrder} />
      <FieldworkStack.Screen name={Route.ProductList} component={ProductList} />
    </FieldworkStack.Navigator>
  );
};

/* ----------- Sync -------------------*/

export const syncStack = () => (
  <SyncStack.Navigator>
    <SyncStack.Screen
      initialParams={{
        headerTitle: messages('sync'),
        headerType: HeaderTypes.HeaderHomeMenu,
        headerRight: HeaderTypes.HeaderRightSyncAll,
      }}
      name={Route.syncData}
      component={SyncData}
    />
  </SyncStack.Navigator>
);

/* ------------ About -------------------------*/

export const aboutStack = () => (
  <AboutStack.Navigator>
    <AboutStack.Screen
      initialParams={{
        headerTitle: messages('about'),
        headerType: HeaderTypes.HeaderHomeMenu,
      }}
      name={Route.About}
      component={AboutScreen}
    />
  </AboutStack.Navigator>
);
