/* eslint-disable prettier/prettier */
import React from 'react';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import { renderHeaderView } from 'react-native-erp-mobile-library/react/util/HeaderUtil';
import { sessionInfo } from 'react-native-erp-mobile-library';
import { FinanceTabNavigator } from './DrawerNavigator';
import * as nb from 'native-base';

export default class FinanceListTabs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            session: sessionInfo({ type: 'get', key: 'loginInfo' }),
            initialFilter: '104112',
        };
        this.childRefs = [];
        this.index = 0;
    }

    addRefs = (ref) => {
        this.childRefs.push(ref);
    }

    addRefsUnclear = (refer) => {
        this.childRefs.push(refer);
    }
    componentDidMount() {
        let customer = this.props.route.params?.customerData ?? '';
        if (customer && customer.customer_id) {
            this.props.navigation.setParams({
                headerTitle: this.props.route.params?.headerTitle ?? 'Customer Finance',
                headerType: this.props.route.params?.headerType ?? HeaderTypes.HeaderBack,
            });
            this.props.navigation.setOptions(renderHeaderView(this));
        } else {
            this.props.navigation.setParams({
                headerTitle: this.props.route.params?.headerTitle ?? 'Customer Finance',
                headerType: this.props.route.params?.headerType ?? HeaderTypes.HeaderHomeMenu,
                headerRight: HeaderTypes.headerRightFinanceList,
                onPressFilter: () => {
                    this.childRefs[this.index].params.parentProps.applyFilter();
                },
            });
            this.props.navigation.setOptions(renderHeaderView(this));

        }
    }

    // componentWillUnmount() {
    // }

    // componentDidUpdate(){
    //     console.log(this.state);
    //     console.log(this.state);
    // }

    render() {
        let { state } = this.props.route;
        if (state !== undefined) { this.childRefs = state.routes; this.index = state.index; }
        return (
            <nb.Root >
                <nb.Container>
                    <FinanceTabNavigator screenProps={{ parentNavigation: this.props, ...this.state.session }} />
                </nb.Container>
            </nb.Root>
        );
    }
}
