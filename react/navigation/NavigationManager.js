import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import SplashScreen from '../screens/main/SplashScreen';
import LoginScreen from '../screens/main/LoginScreen';
import {DrawerNavigator, DrawerNavigatorRetailer} from './DrawerNavigator';
import Route from '../router/Route';
import ChangePassword from 'react-native-erp-mobile-library/react/screens/register/ChangePassword';
import SecurityQuestions from 'react-native-erp-mobile-library/react/screens/register/SecurityQuestions';
import {messages} from 'react-native-erp-mobile-library/react/i18n/i18n';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import UserRegistration from 'react-native-erp-mobile-library/react/screens/register/UserRegistration';
import UserRegistrationVerification from 'react-native-erp-mobile-library/react/screens/register/UserRegistrationVerification';

const Stack = createStackNavigator();

function StackNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={{
          headerShown: false,
        }}
        name={Route.SplashScreen}
        component={SplashScreen}
      />
      <Stack.Screen
        options={{
          headerShown: false,
        }}
        name={Route.Login}
        component={LoginScreen}
      />
      <Stack.Screen
        options={{
          headerShown: false,
        }}
        name={Route.DrawerCommon}
        component={DrawerNavigator}
      />
      <Stack.Screen
        options={{
          headerShown: false,
        }}
        name={Route.Register}
        component={UserRegistration}
      />
      <Stack.Screen
        options={{
          headerShown: false,
        }}
        name={Route.Verification}
        component={UserRegistrationVerification}
      />
      <Stack.Screen
        options={{
          headerShown: false,
        }}
        name={Route.DrawerRetailer}
        component={DrawerNavigatorRetailer}
      />
      <Stack.Screen
        initialParams={{
          headerTitle: messages('changePassword'),
          headerType: HeaderTypes.HeaderBack,
        }}
        name={Route.ChangePassword}
        component={ChangePassword}
      />
      <Stack.Screen
        initialParams={{
          headerTitle: messages('changePassword'),
          headerType: HeaderTypes.HeaderBack,
        }}
        name={Route.SecurityQuestions}
        component={SecurityQuestions}
      />
    </Stack.Navigator>
  );
}

export default function NavigationManager() {
  return (
    <NavigationContainer>
      <StackNavigator />
    </NavigationContainer>
  );
}
