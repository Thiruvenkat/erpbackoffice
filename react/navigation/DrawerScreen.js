/* eslint-disable no-shadow */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import PropTypes from 'prop-types';
import {DrawerActions} from '@react-navigation/native';
import {
  ScrollView,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import {Icon, Button, Text, View} from 'native-base';
import AppConstants from '../constant/AppConstants';
import Toast from 'react-native-simple-toast';
import Route from '../router/Route';
import profile from '../../assets/profile.png';
import MasterConstants from '../constant/MasterConstants';
import {
  deleteData,
  deleteMenuData,
} from 'react-native-erp-mobile-library/react/database/DatabaseHandler';
import {GetAllMenu} from 'react-native-erp-mobile-library/react/navigation/dbHelper/MenuDBHelper';
import {sessionInfo} from 'react-native-erp-mobile-library';
import Modal from 'react-native-modal';
import HeaderTypes from 'react-native-erp-mobile-library/react/util/HeaderTypes';
import {heightPercentageToDP} from 'react-native-erp-mobile-library/react/utilities/PixelRatioConverter';
import {GetAllCustomers} from 'react-native-erp-mobile-library/react/screens/customers/dbHelper/CustomerDB';
import {GetAllTasks} from 'react-native-erp-mobile-library/react/screens/workflow/dbHelper/WorkflowDBHelper';
import {getAllTasks} from 'react-native-erp-mobile-library/react/controller/WorkFlowController';
import Colors from 'react-native-erp-mobile-library/react/style/Colors';

const defaultMenus = [
  {
    icon: 'home',
    menuName: 'Home',
    menuId: 'home',
  },
  {
    icon: 'cloud-download',
    menuName: 'Sync Data',
    menuId: 'syncData',
  },
  {
    icon: 'question-circle-o',
    menuName: 'About',
    menuId: 'AboutStack',
  },
  {
    icon: 'lock',
    menuName: 'Logout',
    menuId: 'logout',
  },
];

class DrawerScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      taskListCount: 0,
      showLogoutModal: false,
      session: sessionInfo({type: 'get', key: 'loginInfo'}),
      deleteSyncData: false,
      userName: '',
      tenantName: '',
      imageSource: null,
      limit: 10,
      offset: 0,
      taskList: [],
      activeScreen: 'Home',
      menuList: [...defaultMenus],
      menuConfig: {
        '201': {
          // Orders
          route: 'OrderStack',
          props: {
            orderTypes: [
              MasterConstants.orderTypeQuotation,
              MasterConstants.orderTypeSalesOrder,
              MasterConstants.orderTypeGrn,
              MasterConstants.orderTypeCashSales,
            ],
          },
        },
        '202': {
          // Products
          route: 'ProductStack',
          props: {
            title: 'Products',
            screenType: 'view',
          },
        },
        '203': {
          // Finance
          route: Route.FinanceListTabs,
          props: {
            tabBarVisibility: true,
          },
        },
        '204': {
          // Stock
          route: 'StockStack',
          props: {
            title: 'Stock',
            headerType: HeaderTypes.HeaderHomeMenu,
          },
        },
        '206': {
          // Customers
          route: Route.CustomerList,
          props: {
            screenType: 'view',
            customerTypeId: [MasterConstants.CustomerTypeCustomerAndSupplier],
          },
        },
        '3051': {
          // Service
          route: 'ServiceHistory',
        },
        '217': {
          // Inbox
          route: 'WorkflowStack',
        },
        '1071': {
          // Tenant Details
          route: 'DrawerTenantDetails',
        },
        '250': {
          // Dashboard
          route: 'DrawerOrderList',
        },
        '228': {
          // Field Work
          route: Route.FieldworkActivityList,
          props: {
            screenType: 'editable',
            trackingType: MasterConstants.fieldWork,
          },
        },
        '249': {
          // Field Work by Rep
          route: Route.FieldworkEmployeeList,
          props: {
            customerTypeId: [MasterConstants.CustomerTypeEmployee],
          },
        },
        '1082': {
          // Technician
          route: 'DrawerTechnicianList',
          props: {
            title: 'Technician',
            screenType: 'view',
            customerTypeId: [MasterConstants.CustomerTypeEmployee],
          },
        },
        Bookings: {
          // Bookings
          route: 'DrawerCompletedTaskList',
        },
        '2050': {
          // attendence
          route: 'AttendenceStack',
          props: {
            title: 'Attendence',
            screenType: 'view',
            trackingType: MasterConstants.attendence,
          },
        },
      },
    };
    this.syncCustomerDetails = this.syncCustomerDetails.bind(this);
    this.getTaskCount = this.getTaskCount.bind(this);
    sessionInfo({type: 'set', key: 'DrawerScreen', value: this});
  }

  componentDidMount() {
    AsyncStorage.getItem(AppConstants.keyLoginInfo).then(loginInfo => {
      const data = JSON.parse(loginInfo);
      const sessionInfo = JSON.parse(data[AppConstants.keySessionInfo]);
      this.setState(
        {
          databaseName: data[AppConstants.keyDatabaseName],
          userName: sessionInfo.userName,
          tenantName: sessionInfo.tenantName,
        },
        () => {
          var filter = {
            orderBy: 'sequence asc',
            conditions: [
              {
                column: 'depth',
                value: 0,
                operator: '=',
              },
              {
                column: 'tenant_id',
                value: [sessionInfo.tenantId, MasterConstants.globalTenantId],
                operator: 'in',
              },
            ],
          };
          GetAllMenu(
            this.state.databaseName,
            menus => {
              this.setState({
                menuList: [
                  defaultMenus[0],
                  ...menus,
                  defaultMenus[1],
                  defaultMenus[2],
                  defaultMenus[3],
                ],
              });
            },
            filter,
          );
        },
      );
    });
    this.getTaskCount();
    this.syncCustomerDetails();
  }

  //   navigateToScreen = route => () => {
  //     const navigateAction = NavigationActions.navigate({
  //       routeName: route,
  //     });
  //     this.props.navigation.dispatch(navigateAction);
  //     this.props.navigation.dispatch(DrawerActions.closeDrawer());
  //   };

  renderCheckBoxItem() {
    return (
      <View
        style={{
          flexDirection: 'row',
          paddingTop: 10,
          color: 'red',
        }}>
        <TouchableOpacity
          onPress={() => {
            this.setState({deleteSyncData: !this.state.deleteSyncData});
          }}>
          <View style={{flexDirection: 'row'}}>
            <Icon
              style={{color: 'red'}}
              name={
                this.state.deleteSyncData
                  ? 'checkbox-marked'
                  : 'checkbox-blank-outline'
              }
              type="MaterialCommunityIcons"
            />
            <Text style={{paddingTop: 5, color: 'red'}}>
              {'Delete synced data'}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  getTaskCount() {
    var filter = {
      orderBy: 'last_updated desc',
      limit: this.state.limit,
      offset: this.state.offset,
      conditions: [
        {
          column: 'tenant_id',
          value: [this.state.session.info[AppConstants.keyTenantId]],
          operator: 'in',
        },
        {
          column: 'status_id',
          value: MasterConstants.StatusUnAssigned,
          operator: '=',
        },
      ],
    };
    GetAllTasks(datas => {
      console.log(this.state.taskList);
      this.setState({
        taskListCount:
          this.state.offset === 0
            ? datas.length
            : Number(this.state.taskListCount) + Number(datas.length),
      });
      if (datas.length > 0) {
        this.setState({loadMore: false, isLoading: false, isPreparing: false});
      } else if (!this.state.isServiceCalled) {
        this._getAllTaskListFromServer();
      }
    }, filter);
  }

  _getAllTaskListFromServer() {
    NetInfo.fetch().then(isConnected => {
      if (isConnected) {
        let syncValue = {
          url: AppConstants.ngetAllGroupTasks,
          filter: {
            filters: {
              orderBy: 't.last_updated',
              limit: this.state.limit,
              offset: this.state.offset,
              conditions: [
                {
                  column: 't.status_id',
                  value: MasterConstants.StatusUnAssigned,
                  dataType: 'String',
                  operator: 'in',
                },
                {
                  column: 'ug.user_id',
                  value: this.state.session.info.userId,
                  dataType: 'String',
                  operator: 'in',
                },
              ],
            },
          },
        };
        getAllTasks(this.state.session, syncValue, isSuccess => {
          if (isSuccess) {
            this.getTaskCount();
          }
          this.setState({
            taskListCount: 0,
            isServiceCalled: true,
            isLoading: false,
            isPreparing: false,
          });
        });
      } else {
        this.setState({isLoading: false, loadMore: false});
      }
    });
  }

  syncCustomerDetails() {
    let filter = {
      conditions: [
        {
          column: 'customer_id',
          value: [this.state.session.info.loggedInCustomerId],
          operator: 'in',
        },
      ],
    };
    GetAllCustomers(
      this.state.session.databaseName,
      datas => {
        if (datas.length > 0) {
          const data = datas[datas.length - 1];
          this.setState({
            imageSource:
              !data.document_location ||
              data.document_location === 'null' ||
              data.document_location === 'undefined'
                ? null
                : this.state.session.imageIp + data.document_location,
          });
        }
      },
      filter,
    );
  }

  render() {
    return (
      <View
        style={{
          backgroundColor: Colors.headerBackground,
          height: heightPercentageToDP('100%'),
        }}>
        <ScrollView style={{flexGrow: 0}}>
          <View>
            {this.renderLogoutOptions()}
            <View
              style={{
                backgroundColor: Colors.headerBackground,
                padding: 2,
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-start',
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate(Route.ProfileScreen, {
                    headerTitle: 'Profile',
                    headerType: HeaderTypes.HeaderBack,
                  });
                }}>
                <Image
                  source={
                    this.state.imageSource
                      ? {uri: this.state.imageSource}
                      : profile
                  }
                  style={{
                    height: 50,
                    width: 50,
                    marginTop: 30,
                    marginLeft: 10,
                    marginBottom: 20,
                    borderRadius: 50 / 2,
                  }}
                />
              </TouchableOpacity>
              <View style={{marginLeft: 15, marginTop: 35}}>
                <Text
                  style={{
                    fontSize: 16,
                    color: 'white',
                    fontWeight: 'bold',
                  }}>
                  {this.state.userName}
                </Text>
                <Text style={{fontSize: 14, marginTop: 2, color: 'white'}}>
                  {this.state.tenantName}
                </Text>
              </View>
            </View>
            <View
              style={{
                backgroundColor: 'lightgrey',
                height: 0.5,
              }}
            />

            {this.state.menuList.map((menu, index) => {
              if (menu.menuId === '250' || menu.menuId === '116') {
                return null;
              }
              return (
                <TouchableOpacity
                  key={index}
                  onPress={() => {
                    if (menu.menuId === 'logout') {
                      this.props.navigation.dispatch(
                        DrawerActions.closeDrawer(),
                      );
                      this.setState({showLogoutModal: true});
                    } else if (menu.menuId === 'AboutStack') {
                      this.setState({
                        activeScreen: menu.menuName,
                      });
                      this.props.navigation.dispatch(
                        DrawerActions.closeDrawer(),
                      );
                      this.props.navigation.navigate(Route.AboutStack);
                    } else if (menu.menuId === 'syncData') {
                      this.setState({
                        activeScreen: menu.menuName,
                      });
                      this.props.navigation.dispatch(
                        DrawerActions.closeDrawer(),
                      );
                      this.props.navigation.navigate(Route.syncData);
                    } else if (menu.menuId === 'home') {
                      this.setState({
                        activeScreen: menu.menuName,
                      });
                      this.props.navigation.dispatch(
                        DrawerActions.closeDrawer(),
                      );
                      if (
                        this.state.session.loggedInUserType ===
                        AppConstants.loggedInUserTypeRetailer
                      ) {
                        this.props.navigation.navigate('LandingPage2', {
                          headerType: HeaderTypes.HeaderHomeMenu,
                        });
                      } else {
                        this.props.navigation.navigate(Route.Dashboard, {
                          headerType: HeaderTypes.HeaderHomeMenu,
                        });
                      }
                    } else {
                      if (this.state.menuConfig[menu.menuId] !== undefined) {
                        this.setState({
                          activeScreen: menu.menuName,
                        });
                        var config = this.state.menuConfig[menu.menuId];
                        if (config.props !== undefined) {
                          this.props.navigation.closeDrawer();
                          // this.props.navigation.navigate(config.route);
                          this.props.navigation.dispatch(
                            DrawerActions.jumpTo(config.route, config.props),
                          );
                          this.getTaskCount();
                        } else {
                          this.props.navigation.closeDrawer();
                          this.props.navigation.dispatch(
                            DrawerActions.jumpTo(config.route),
                          );
                        }
                      } else {
                        Toast.show('Menu config not found', Toast.SHORT);
                      }
                    }
                  }}>
                  <View
                    style={
                      this.state.activeScreen === menu.menuName
                        ? styles.SelectItem
                        : styles.item
                    }>
                    {menu.menuName === 'Inbox' ? (
                      <View>
                        <Icon
                          name="envelope"
                          type="FontAwesome"
                          style={styles.itemIcon}
                        />
                        <View style={styles.IconBadge}>
                          <Text
                            style={{
                              fontSize: 10,
                              color: 'white',
                              textAlign: 'center',
                              fontWeight: 'bold',
                            }}>
                            {this.state.taskListCount}
                          </Text>
                        </View>
                      </View>
                    ) : (
                      <Icon
                        name={menu.icon
                          .replace('fa_', '')
                          .replace('fa-', '')
                          .replace('_', '-')
                          .replace('chart', 'line-chart')}
                        type="FontAwesome"
                        style={styles.itemIcon}
                      />
                    )}
                    <Text style={styles.itemText}>{menu.menuName}</Text>
                  </View>
                </TouchableOpacity>
              );
            })}
            <View style={{height: 40, backgroundColor: 'transparent'}} />
          </View>
        </ScrollView>
      </View>
    );
  }

  renderLogoutOptions() {
    return (
      <Modal isVisible={this.state.showLogoutModal}>
        <View style={styles.showLogoutModalView}>
          <Text
            style={{
              alignitems: 'flex-start',
              fontWeight: 'bold',
              fontSize: 20,
            }}>
            {'LOGOUT'}
          </Text>
          <Text style={{paddingTop: 20}}>{'Do you want to logout ? '}</Text>
          {this.renderCheckBoxItem()}
          <View style={{flexDirection: 'row', paddingTop: 25}}>
            <View style={{width: (Dimensions.get('screen').width / 100) * 2}} />
            <Button
              style={styles.buttonNo}
              onPress={() => {
                this.setState({showLogoutModal: false}, () => {
                  AsyncStorage.setItem(
                    AppConstants.keyLoggedInStatus,
                    'loggedOut',
                  );
                  if (this.state.deleteSyncData) {
                    deleteData(AppConstants.affordeBackOfficeDB);
                  } else {
                    deleteMenuData(AppConstants.affordeBackOfficeDB);
                  }
                  this.props.navigation.reset({
                    index: 0,
                    routes: [{name: Route.Login}],
                  });
                });
              }}>
              <Text style={{color: 'white', fontWeight: 'bold'}}>Yes</Text>
            </Button>
            <View
              style={{width: (Dimensions.get('screen').width / 100) * 10}}
            />
            <Button
              style={styles.buttonNo}
              onPress={() => {
                this.props.navigation.dispatch(DrawerActions.closeDrawer());
                this.setState({showLogoutModal: false});
              }}>
              <Text style={{color: 'white', fontWeight: 'bold'}}>No</Text>
            </Button>
            <View style={{width: (Dimensions.get('screen').width / 100) * 2}} />
          </View>
        </View>
      </Modal>
    );
  }
}

DrawerScreen.propTypes = {
  navigation: PropTypes.object,
};

const styles = StyleSheet.create({
  item: {
    padding: 15,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  SelectItem: {
    padding: 15,
    backgroundColor: '#4fbeff',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  itemText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: 15,
  },
  itemIcon: {
    color: 'white',
    fontSize: 22,
    justifyContent: 'center',
    textAlign: 'center',
  },
  IconBadge: {
    position: 'absolute',
    top: -3,
    right: -5,
    minWidth: 15,
    height: 15,
    borderRadius: 15 / 2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'orange',
  },
  showLogoutModalView: {
    flexDirection: 'column',
    borderRadius: 4,
    backgroundColor: 'white',
    padding: 25,
  },
  logoutModalText: {
    fontSize: 20,
    padding: 4,
    textAlign: 'center',
  },
  logoutModalIcon: {
    color: 'green',
    fontSize: 55,
  },
  buttonNo: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.headerBackground,
  },
});
export default DrawerScreen;
