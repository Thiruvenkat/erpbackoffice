import React, { Component } from "react";
import { Dimensions } from "react-native";
import { createMaterialTopTabNavigator, createStackNavigator } from "react-navigation";
import GroupTaskList from "../screens/workflow/GroupTaskList";
import MyTaskList from "../screens/workflow/MyTaskList";
import Colors from "../style/Colors";
import { Icon, Text } from "native-base";
import { DrawerActions } from 'react-navigation';
import { Styles } from "../style/Styles";
import { renderHeaderView } from "react-native-erp-mobile-library/react/util/HeaderUtil";
import { messages } from "react-native-erp-mobile-library/react/i18n/i18n";

export const WorkflowTab = createMaterialTopTabNavigator({
  Tab1: MyTaskList,
  Tab2: GroupTaskList
}, {
    initialRouteName: "Tab1",
    order: ["Tab1", "Tab2"],
    tabBarOptions: {
      scrollEnabled: true,
      labelStyle: {
        fontSize: 12,
        fontWeight: 'bold'
      },
      tabStyle: {
        width: Dimensions.get('window').width / 2,
      },
      style: {
        backgroundColor: Colors.headerBackground,
      },
      indicatorStyle: {
        backgroundColor: Colors.tabIndicator
      }
    },
  });

WorkflowTab.navigationOptions = ({ navigation }) => ({
  headerTitle: messages('inbox'),
  headerTintColor: "white",
  headerStyle: {
    backgroundColor: Colors.headerBackground
  },
  headerTitleStyle: {
    fontWeight: "bold",
    color: Colors.headerTitleColor
  },
  headerLeft:
    <Icon style={Styles.homeBurgerMenu} name="bars" type="FontAwesome" onPress={() => {
      navigation.dispatch(DrawerActions.openDrawer())
    }} />
});


