import AppConstants from '../constant/AppConstants';
import Controller from 'react-native-erp-mobile-library/react/controller/Controller';
import {Alert} from 'react-native';

export default class LoginController {
  loginApi(IP, content) {
    return new Controller().postApi(IP + AppConstants.loginUrl, content);
  }

  loginBackOffice(IP, content) {
    return new Controller().postApi(
      IP + AppConstants.loginBackOfficeUrl,
      content,
    );
  }

  getPublicIp(IP, content) {
    return new Controller().postApi(IP + AppConstants.getPublicIp, content);
  }

  registerApi(IP, content) {
    return new Controller().postApi(IP + AppConstants.registerUserUrl, content);
  }

  otpValidationApi(IP, content) {
    return new Controller().postApi(IP + AppConstants.validateOtpUrl, content);
  }

  getTenantList(IP) {
    return new Controller().getApi(IP + AppConstants.getAllTenants);
  }

  validateUserByLogin(IP, content) {
    return new Controller().postApi(
      IP + AppConstants.validateUserByLogin,
      content,
    );
  }

  getLoginTokenApi(IP, content) {
    return new Controller().postApi(IP + AppConstants.getLoginToken, content);
  }

  registerGCMToken(loginToken, url, content) {
    console.log('Request-token', loginToken);
    console.log('Request-url', url);
    console.log('Request-content', JSON.stringify(content));
    return fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        ta_authentication_token: loginToken,
      },
      body: JSON.stringify(content),
    })
      .then(response => {
        console.log('Response-status: ', response.status);
        console.log('Response-body', response._bodyText);
        return response;
      })
      .catch(error => {
        console.error(error);
        Alert.alert('Alert Title failure' + JSON.stringify(error));
      });
  }
}
