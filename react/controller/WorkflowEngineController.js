import Controller from 'react-native-erp-mobile-library/react/controller/Controller';
import Toast from 'react-native-simple-toast';

export function getUserTask(url, serviceCallbackListener) {
  new Controller().getTaskApi(url).then(response => {
    if (response.status === 200) {
      const responseBody = JSON.parse(response._bodyText);
      if (responseBody.length > 0) {
        serviceCallbackListener(responseBody);
      } else {
        serviceCallbackListener([]);
      }
    } else {
      serviceCallbackListener([]);
      Toast.show('Try re-login to continue!', Toast.SHORT);
    }
  });
}

export function getTaskHistory(callback) {
  const url = 'http://192.168.0.101:3000/api/userTaskHistories';
  new Controller().getTaskApi(url).then(task => {
    const taskDetail = JSON.parse(task._bodyText).results;
    if (taskDetail) {
      callback(taskDetail);
    }
    callback([]);
  });
}

export function getGroupTask(url, serviceCallbackListener) {
  new Controller().getTaskApi(url).then(response => {
    if (response.status === 200) {
      const responseBody = JSON.parse(response._bodyText);
      if (responseBody.length > 0) {
        serviceCallbackListener(responseBody);
      } else {
        serviceCallbackListener([]);
      }
    } else {
      serviceCallbackListener([]);
      Toast.show('Try re-login to continue!', Toast.SHORT);
    }
  });
}

export function claimTaskToServer(url, content, serviceCallbackListener) {
  new Controller().taskPostApi(url, content).then(response => {
    if (response.status === 200) {
      serviceCallbackListener(response);
    } else {
      serviceCallbackListener([]);
      Toast.show('Try re-login to continue!', Toast.SHORT);
    }
  });
}

export function rejectTask(url, content, serviceCallbackListener) {
  new Controller().taskPostApi(url, content).then(response => {
    if (response.status === 200) {
      serviceCallbackListener(response);
    } else {
      serviceCallbackListener([]);
      Toast.show('Try re-login to continue!', Toast.SHORT);
    }
  });
}
