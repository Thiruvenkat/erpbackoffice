import AppConstants from '../constant/AppConstants';
import MasterConstants from '../constant/MasterConstants';
import {InsertFollowup} from '../screens/followup/dbHelper/FollowUpDB';
import Toast from 'react-native-simple-toast';
import {InsertAdvertisement} from '../screens/advertisements/dbHelper/AdvertisementDB';
import {InsertCustomers} from 'react-native-erp-mobile-library/react/screens/customers/dbHelper/CustomerDB';
import Controller from 'react-native-erp-mobile-library/react/controller/Controller';
import {
  DateFormat,
  DATE_TIME_SERVER,
} from 'react-native-erp-mobile-library/react/common/Common';
import {InsertLandingDashboardDetails} from '../screens/dashboard/DBHelper/dashboardDBHelper';
import {InsertOrderService} from 'react-native-erp-mobile-library/react/screens/services/dbHelper/ServiceOrderDB';

export function getReportTemplate(session, request, serviceCallbackListener) {
  new Controller()
    .playPostApi(
      session.loginToken,
      session.publicIp + AppConstants.getTemplateName,
      request,
    )
    .then(response => {
      serviceCallbackListener(response);
    });
}
export function getSendmailJson(session, request, serviceCallbackListener) {
  new Controller()
    .playPostApi(
      session.loginToken,
      session.publicIp + AppConstants.getMailOrderJson,
      request,
    )
    .then(response => {
      serviceCallbackListener(response);
    });
}

function prepareFilter(syncValue, isNGetAll) {
  var request = {
    filters: {
      orderBy:
        syncValue.orderBy !== undefined
          ? syncValue.orderBy
          : isNGetAll
          ? 'last_updated'
          : 'lastUpdatedDateTime',
    },
  };
  request.filters.limit =
    syncValue.limit !== undefined && syncValue.limit != null
      ? syncValue.limit
      : MasterConstants.fetchLimit;
  request.filters.offset =
    syncValue.offset !== undefined && syncValue.offset != null
      ? syncValue.offset
      : 0;
  if (syncValue.conditions !== undefined && syncValue.conditions != null) {
    request.filters.conditions = syncValue.conditions;
  }
  if (
    syncValue.staticConditions !== undefined &&
    syncValue.staticConditions != null
  ) {
    request.filters.staticConditions = syncValue.staticConditions;
  }
  return request;
}

export function getAdvertisementDetails(session, serviceCallbackListener) {
  var request = {
    request: {
      envelope: {
        sourceApplication: 'Afforde UI',
        requestDate: DateFormat(new Date(), DATE_TIME_SERVER),
      },
      content: {
        bo_data: {
          type: 'getAdvertisementDetails_BOO',
          filters: {
            orderBy: 'lastUpdatedDateTime',
          },
        },
      },
    },
  };
  new Controller()
    .playPostApi(
      session.loginToken,
      session.publicIp + AppConstants.getAdvertisementDetails,
      request,
    )
    .then(response => {
      if (response.status === 200) {
        const responseBody = JSON.parse(response._bodyText);
        var advertisements = [];
        for (
          let index = 0;
          index <
          responseBody.response.content.bo_data.advertisementDetails.length;
          index++
        ) {
          const result =
            responseBody.response.content.bo_data.advertisementDetails[index];
          let ad = {
            advertisementId: result.advertisementId,
            averageRating: Number(result.averageRating).toFixed(1),
            description: result.description,
            imagePath: session.imageIp + result.imagePath,
            tag: result.tag,
            totalLike: Math.floor(result.totalLike),
            validFrom: result.validFrom,
            validTill: result.validTill,
            tenantId:
              responseBody.response.content.bo_data.sessionInfo.tenantId,
            json: JSON.stringify(result),
          };
          advertisements[index] = ad;
        }
        if (advertisements.length > 0) {
          InsertAdvertisement(advertisements, isSuccess => {
            if (isSuccess) {
              console.log('insert seccessfullt');
            } else {
              console.log('insert Error');
            }
            serviceCallbackListener(isSuccess);
          });
        } else {
          serviceCallbackListener(false);
        }
      } else {
        serviceCallbackListener(false);
        Toast.show('Try re-login to continue!', Toast.SHORT);
      }
    });
}

export function getAllCustomerDetails(
  session,
  syncValue,
  serviceCallbackListener,
) {
  new Controller()
    .playPostApi(
      session.loginToken,
      session.publicIp + AppConstants.getAllCustomerDetails,
      prepareFilter(syncValue, false),
    )
    .then(response => {
      if (response.status === 200) {
        const customerList = [];
        const responseBody = JSON.parse(response._bodyText);
        let i = 0;
        for (
          let index = 0;
          index < responseBody.response.content.dataList.length;
          index++
        ) {
          const result = responseBody.response.content.dataList[index];
          const customer = {
            customer_code: result.customerCode,
            customer_name: result.customerName,
            customer_id: result.customerId,
            customer_parent_id:
              result.parentCustomer !== undefined
                ? result.parentCustomer.id
                : '',
            customer_parent_name:
              result.parentCustomer !== undefined
                ? result.parentCustomer.name
                : '',
            last_updated: result.lastUpdated,
            mobile_no: result.mobileNo,
            status_id: result.status.id,
            status_name: result.status.name,
            customer_type_id: result.customerType.id,
            json: JSON.stringify(result),
            tenant_id: result.tenant.id,
          };
          customerList[i++] = customer;
        }
        if (customerList.length > 0) {
          InsertCustomers(customerList, isSuccess => {
            if (isSuccess) {
            } else {
              console.log('getAllCustomers Error');
            }
            serviceCallbackListener(isSuccess);
          });
        } else {
          serviceCallbackListener(false);
        }
      } else {
        serviceCallbackListener(false);
        Toast.show('Problem occurred!', Toast.SHORT);
      }
    });
}

export function getAllOrderService(
  session,
  syncValue,
  serviceCallbackListener,
) {
  new Controller()
    .playPostApi(
      session.loginToken,
      session.publicIp + AppConstants.getAllOrderService,
      prepareFilter(syncValue, false),
    )
    .then(response => {
      if (response.status === 200) {
        const responseBody = JSON.parse(response._bodyText);
        var taskdetails = [];
        for (
          let index = 0;
          index < responseBody.response.content.dataList.length;
          index++
        ) {
          const result = responseBody.response.content.dataList[index];
          let ad = {
            order_service_id: result.orderServiceId,
            order_to_id: result.order.text4,
            order_to_name: result.order.text5,
            order_code: result.order.code,
            service_date: result.serviceDate,
            status_id: result.status.id,
            status_name: result.status.name,
            service_type_id: result.serviceType.id,
            service_type_name: result.serviceType.name,
            classification_type_id: result.serviceClassificationType.id,
            classification_type_name: result.serviceClassificationType.name,
            tenantId: result.tenant.id,
            orderId: result.order.id,
            productId: JSON.parse(result.jsonData).productId,
            last_updated: result.lastUpdated,
            json: JSON.stringify(result),
          };
          if (result.serviceAgent != null) {
            ad.service_agent_id = result.serviceAgent.id;
            ad.service_agent_name = result.serviceAgent.name;
          }
          taskdetails[index] = ad;
        }
        if (taskdetails.length > 0) {
          InsertOrderService(
            AppConstants.affordeBackOfficeDB,
            taskdetails,
            isSuccess => {
              if (isSuccess) {
                serviceCallbackListener(isSuccess);
              } else {
                console.log('GetAllOrderService Error');
                serviceCallbackListener(false);
              }
            },
          );
        } else {
          serviceCallbackListener(false);
        }
      } else {
        serviceCallbackListener(false);
        Toast.show('Problem occurred!', Toast.SHORT);
      }
    });
}

export function getAllFollowUpDetails(
  session,
  syncValue,
  serviceCallbackListener,
) {
  new Controller()
    .playPostApi(
      session.loginToken,
      session.publicIp + AppConstants.getAllFollowUp,
      prepareFilter(syncValue, false),
    )
    .then(response => {
      if (response.status === 200) {
        const responseBody = JSON.parse(response._bodyText);
        var taskdetails = [];
        for (
          let index = 0;
          index < responseBody.response.content.dataList.length;
          index++
        ) {
          const result = responseBody.response.content.dataList[index];
          let ad = {
            followup_id: result.followUpId,
            object_ref_no: result.objectRefNo,
            followup_count: result.followUpCount,
            next_followup_date: result.nextFollowUpDate,
            status_id: result.status.id,
            status_name: result.status.name,
            last_updated: result.lastUpdated,
            followup_type_id: result.followUpType.id,
            rating: result.rating,
            reason_id: result.reason != null ? result.reason.id : null,
            reason_name: result.reason != null ? result.reason.name : null,
            tenant_id: result.tenant.id,
            json: JSON.stringify(result),
            followUpUserId: result.followUpUserId,
            object_id: result.objectId,
          };
          taskdetails[index] = ad;
        }
        if (taskdetails.length > 0) {
          InsertFollowup(taskdetails, isSuccess => {
            if (isSuccess) {
            } else {
              console.log('GetAllFollowup Error');
            }
            serviceCallbackListener(isSuccess);
          });
        } else {
          serviceCallbackListener(false);
        }
      } else {
        serviceCallbackListener(false);
        Toast.show('Problem occurred!', Toast.SHORT);
      }
    });
}

export function getLandingDashboardDetails(
  session,
  syncValue,
  serviceCallbackListener,
) {
  const request = {
    request: {
      envelope: {
        sourceApplication: 'Afforde UI',
        requestDate: 'Sun Sep 17 2017 17:44:56 GMT+0530 (IST)',
      },
      content: {
        bo_data: {
          type: 'getLandingDashboardDetails_BOO',
          selectedDate: syncValue.selectedDate,
          selectionType: Number(syncValue.dashboardType) + 1,
        },
      },
    },
  };
  new Controller()
    .playPostApi(
      session.loginToken,
      session.publicIp + AppConstants.getLandingDashboardDetails,
      request,
    )
    .then(response => {
      if (response.status === 200) {
        let dashboardDetails = null;
        const responseBody = JSON.parse(response._bodyText);
        dashboardDetails = responseBody.response.content.bo_data;
        dashboardDetails.sessionInfo = null;
        const values = {
          dashboardId: syncValue.menuId,
          layoutId: syncValue.dashboardType,
          details: JSON.stringify(dashboardDetails),
        };
        if (dashboardDetails) {
          InsertLandingDashboardDetails(values, isSuccess => {
            if (isSuccess) {
            } else {
              console.log('getLandingDasboardDetails Error');
            }
            serviceCallbackListener(isSuccess);
          });
        } else {
          serviceCallbackListener(false);
        }
      } else {
        serviceCallbackListener(false);
        Toast.show('Try re-login to continue!', Toast.SHORT);
      }
    });
}

export function getDashboardDetails(
  session,
  syncValue,
  serviceCallbackListener,
) {
  const request = {
    request: {
      envelope: {
        sourceApplication: 'Afforde UI',
        requestDate: 'Sun Sep 17 2017 17:44:56 GMT+0530 (IST)',
      },
      content: {
        bo_data: {
          type: 'getDashboardDetails_BOO',
          layoutId: syncValue.dashboardType,
          JsonData: '',
          resultJson: 'resultJson',
        },
      },
    },
  };
  new Controller()
    .playPostApi(
      session.loginToken,
      session.publicIp + AppConstants.getDashboardDetails,
      request,
    )
    .then(response => {
      if (response.status === 200) {
        let dashboardDetails = null;
        const responseBody = JSON.parse(response._bodyText);
        dashboardDetails = responseBody.response.content.bo_data;
        dashboardDetails.sessionInfo = null;
        const values = {
          dashboardId: syncValue.menuId,
          layoutId: syncValue.dashboardType,
          details: JSON.stringify(dashboardDetails),
        };
        if (dashboardDetails) {
          InsertLandingDashboardDetails(values, isSuccess => {
            if (isSuccess) {
              console.log('Get Dasboard Details Success');
            } else {
              console.log('Get Dasboard Details Error');
            }
            serviceCallbackListener(isSuccess);
          });
        } else {
          serviceCallbackListener(false);
        }
      } else {
        serviceCallbackListener(false);
        Toast.show('Try re-login to continue!', Toast.SHORT);
      }
    });
}
