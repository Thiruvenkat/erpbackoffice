/* eslint-disable eqeqeq */
import AppConstants from '../constant/AppConstants';
import {
  InsertFieldActivity,
  InsertFieldActivityVisits,
  InsertFieldworkEvents,
} from '../screens/fieldwork/dbHelper/FieldWorkDBHelper';
import MasterConstants from '../constant/MasterConstants';
import Controller from 'react-native-erp-mobile-library/react/controller/Controller';
import Toast from 'react-native-simple-toast';

function prepareFilter(syncValue, isNGetAll) {
  var request = {
    filters: {
      orderBy: isNGetAll ? 'last_updated' : 'lastUpdatedDateTime',
    },
  };
  request.filters.limit =
    syncValue.limit != undefined && syncValue.limit != null
      ? syncValue.limit
      : MasterConstants.fetchLimit;
  request.filters.offset =
    syncValue.offset != undefined && syncValue.offset != null
      ? syncValue.offset
      : 0;
  if (syncValue.conditions != undefined && syncValue.conditions != null) {
    request.filters.conditions = syncValue.conditions;
  }
  return request;
}

export function createFieldActivity(session, data, serviceCallbackListener) {
  data.request.content.bo_data.type = 'createFieldActivityWithPlans_BOO';
  new Controller()
    .playPostApi(
      session.loginToken,
      session.publicIp + AppConstants.createFieldActivityWithPlans,
      data,
    )
    .then(response => {
      if (response.status === 200) {
        serviceCallbackListener(true);
      } else {
        serviceCallbackListener(false);
        Toast.show('Problem occurred!', Toast.SHORT);
      }
    });
}

export function updateFieldActivity(session, data, serviceCallbackListener) {
  data.request.content.bo_data.type = 'updateFieldActivityStatus_BOO';
  new Controller()
    .playPostApi(
      session.loginToken,
      session.publicIp + AppConstants.updateFieldActivity,
      data,
    )
    .then(response => {
      if (response.status === 200) {
        serviceCallbackListener(true);
      } else {
        serviceCallbackListener(false);
        Toast.show('Problem occurred!', Toast.SHORT);
      }
    });
}

export function getAllFieldActivity(
  session,
  syncValue,
  serviceCallbackListener,
) {
  new Controller()
    .playPostApi(
      session.loginToken,
      session.publicIp + AppConstants.ngetAllFieldActivity,
      prepareFilter(syncValue, true),
    )
    .then(response => {
      if (response.status === 200) {
        const responseBody = JSON.parse(response._bodyText);
        if (responseBody.response.content.dataList[0].data.length > 0) {
          var data = [];
          for (
            let index = 0;
            index < responseBody.response.content.dataList[0].data.length;
            index++
          ) {
            const object = responseBody.response.content.dataList[0].data[
              index
            ].split("','");
            var fieldActivity = {
              fieldActivityId: object[0].substring(1, object[0].length),
              date: object[1].replace(' ', 'T'),
              userId: object[2],
              startTime: object[3] != 'null' ? object[3] : '',
              endTime: object[4] != 'null' ? object[4] : '',
              statusId: object[5],
              statusName: object[6],
              startLocation: object[7],
              endLocation: object[8],
              reasonId: object[9],
              reasonName: object[10],
              notes: object[11],
              lastUpdated: object[12],
              tenantId: object[13],
              optLock: object[14],
              visitCount: object[15],
              fieldPlanId: object[16] != 'null' ? object[16] : '',
              seqNo:
                object[17] != 'null'
                  ? object[17].substring(0, object[17].length - 1)
                  : '',
              syncStatus: 'Updated',
            };
            data[index] = fieldActivity;
          }
          InsertFieldActivity(data, isSuccess => {
            if (isSuccess) {
            } else {
              console.log('getAllFieldActivity Error');
            }
            serviceCallbackListener(isSuccess);
          });
        } else {
          serviceCallbackListener(false);
        }
      } else {
        serviceCallbackListener(false);
        Toast.show('Problem occurred!', Toast.SHORT);
      }
    });
}

export function createFieldActivityVisit(
  session,
  request,
  serviceCallbackListener,
) {
  new Controller()
    .playPostApi(
      session.loginToken,
      session.publicIp + AppConstants.createFieldActivityCustomerVisit,
      request,
    )
    .then(response => {
      if (response.status === 200) {
        serviceCallbackListener(true);
      } else {
        serviceCallbackListener(false);
        Toast.show('Problem occurred!', Toast.SHORT);
      }
    });
}

export function updateFieldActivityVisit(
  session,
  request,
  serviceCallbackListener,
) {
  new Controller()
    .playPostApi(
      session.loginToken,
      session.publicIp + AppConstants.updateFieldActivityCustomerVisit,
      request,
    )
    .then(response => {
      if (response.status === 200) {
        serviceCallbackListener(true);
      } else {
        serviceCallbackListener(false);
        Toast.show('Problem occurred!', Toast.SHORT);
      }
    });
}

export function getAllFieldActivityVisit(
  session,
  syncValue,
  serviceCallbackListener,
) {
  new Controller()
    .playPostApi(
      session.loginToken,
      session.publicIp + AppConstants.ngetAllFieldActivityCustomerVisit,
      prepareFilter(syncValue, true),
    )
    .then(response => {
      if (response.status === 200) {
        const responseBody = JSON.parse(response._bodyText);
        if (responseBody.response.content.dataList[0].data.length > 0) {
          var data = [];
          for (
            let index = 0;
            index < responseBody.response.content.dataList[0].data.length;
            index++
          ) {
            const object = responseBody.response.content.dataList[0].data[
              index
            ].split("','");
            var fieldActivity = {
              fieldActivityCustomerVisitId: object[0].substring(
                1,
                object[0].length,
              ),
              fieldActivityId: object[1],
              customerId: object[2],
              customerName: object[3],
              startTime: object[4] != 'null' ? object[4] : '',
              endTime: object[5] != 'null' ? object[5] : '',
              statusId: object[6],
              statusName: object[7],
              startLocation: object[8],
              endLocation: object[9],
              reasonId: object[10],
              reasonName: object[11],
              notes: object[12],
              lastUpdated: object[13],
              tenantId: object[14],
              optLock: object[15],
              eventCount: object[16],
              seqNo: object[17],
              orderMandatory: object[18].substring(0, object[18].length - 1),
              syncStatus: 'Updated',
              startEndLocationMatched: '0',
            };
            data[index] = fieldActivity;
          }
          InsertFieldActivityVisits(data, isSuccess => {
            if (isSuccess) {
            } else {
              console.log('getAllFieldActivityVisit Error');
            }
            serviceCallbackListener(isSuccess);
          });
        } else {
          serviceCallbackListener(false);
        }
      } else {
        serviceCallbackListener(false);
        Toast.show('Problem occurred!', Toast.SHORT);
      }
    });
}

export function getAllFieldActivityEvents(
  session,
  syncValue,
  serviceCallbackListener,
) {
  new Controller()
    .playPostApi(
      session.loginToken,
      session.publicIp + AppConstants.getAllFieldActivityEvents,
      prepareFilter(syncValue, false),
    )
    .then(response => {
      if (response.status === 200) {
        const responseBody = JSON.parse(response._bodyText);
        if (responseBody.response.content.dataList.length > 0) {
          var data = [];
          for (
            let index = 0;
            index < responseBody.response.content.dataList.length;
            index++
          ) {
            const object = responseBody.response.content.dataList[index];
            var fieldActivity = {
              fieldActivityEventsId: object.fieldActivityEventsId,
              fieldActivityCustomerVisitId:
                object.fieldActivityCustomerVisit != undefined
                  ? object.fieldActivityCustomerVisit.id
                  : '',
              eventDate: object.eventDateTime,
              eventReferenceId: object.eventReferenceId,
              eventLocation: object.eventLocation,
              eventTypeId: object.eventTypeId,
              lastUpdated: object.lastUpdated,
              tenantId: object.tenant.id,
              optLock: object.optLock,
              syncStatus: 'Updated',
            };
            data[index] = fieldActivity;
          }
          InsertFieldworkEvents(data, isSuccess => {
            if (isSuccess) {
            } else {
              console.log('getAllFieldActivityEvents Error');
            }
            serviceCallbackListener(isSuccess);
          });
        } else {
          serviceCallbackListener(false);
        }
      } else {
        serviceCallbackListener(false);
        Toast.show('Problem occurred!', Toast.SHORT);
      }
    });
}

export function getAllFieldActivityAgentTracking(session, syncValue) {
  return new Promise((resolve, reject) => {
    new Controller()
      .playPostApi(
        session.loginToken,
        session.publicIp + AppConstants.getAllFieldActivityAgentTracking,
        prepareFilter(syncValue, false),
      )
      .then(response => {
        if (response.status === 200) {
          const responseBody = JSON.parse(response._bodyText);
          const dataList = responseBody.response.content.dataList;
          var data = [];
          if (dataList.length > 0) {
            for (let index = 0; index < dataList.length; index++) {
              const object = dataList[index];
              var location = {
                date: object.date,
                location: JSON.parse(object.location),
              };
              data.push(location);
            }
          }
          resolve(data);
        } else {
          reject();
          Toast.show('Problem occurred!', Toast.SHORT);
        }
      });
  });
}

export function createFieldActivityAgentTracking(session, request) {
  return new Promise((resolve, reject) => {
    new Controller()
      .playPostApi(
        session.loginToken,
        session.publicIp + AppConstants.createFieldActivityAgentTracking,
        request,
      )
      .then(response => {
        if (response.status === 200) {
          resolve(request);
        } else {
          reject();
          Toast.show('Problem occurred!', Toast.SHORT);
        }
      });
  });
}
