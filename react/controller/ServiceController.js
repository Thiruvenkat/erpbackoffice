import AppConstants from '../constant/AppConstants';
import Controller from 'react-native-erp-mobile-library/react/controller/Controller';

export default class ServiceController {
  technicianOtpVerificationApi(content) {
    return new Controller().postApi(
      AppConstants.verifyTechnicianOtpUrl,
      content,
    );
  }

  searchProductCodeName(loginToken, url, content) {
    return new Controller().playPostApi(loginToken, url, content);
  }

  searchStockSerialNo(loginToken, url, content) {
    return new Controller().playPostApi(loginToken, url, content);
  }

  getAllStockSerialNumber(loginToken, url, content) {
    return new Controller().playPostApi(loginToken, url, content);
  }

  getAllCustomers(loginToken, url, content) {
    return new Controller().playPostApi(loginToken, url, content);
  }

  getProduct(loginToken, url) {
    return new Controller().playGetApi(loginToken, url);
  }

  createStockSerialNoInService(loginToken, url, content) {
    return new Controller().playPostApi(loginToken, url, content);
  }

  getAllServiceType(loginToken, url, content) {
    return new Controller().playPostApi(loginToken, url, content);
  }

  saveServiceRequest(loginToken, url, content) {
    return new Controller().playPostApi(loginToken, url, content);
  }

  getProductDetailsUsingSerialNo(loginToken, url, content) {
    return new Controller().playPostApi(loginToken, url, content);
  }

  getAllTemplate(loginToken, url, content) {
    return new Controller().playPostApi(loginToken, url, content);
  }

  ngetAllViewOrdersService(loginToken, url, content) {
    return new Controller().playPostApi(loginToken, url, content);
  }

  createServiceRequestByCustomer(loginToken, url, content) {
    return new Controller().playPostApi(loginToken, url, content);
  }

  updateServiceRequestForReallocation(loginToken, url, content) {
    return new Controller().playPostApi(loginToken, url, content);
  }
  getOrderParty(loginToken, url, content) {
    return new Controller().playPostApi(loginToken, url, content);
  }

  getAllGeoLevels(loginToken, url, content) {
    return new Controller().playPostApi(loginToken, url, content);
  }

  updateCustomerAddress(loginToken, url, content) {
    return new Controller().playPostApi(loginToken, url, content);
  }
}
