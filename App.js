/* eslint-disable eqeqeq */
/* eslint-disable dot-notation */
/* eslint-disable prettier/prettier */
import React from 'react';
import {MenuProvider} from 'react-native-popup-menu';
import { View, AppRegistry, StyleSheet, BackHandler, Alert, Platform } from 'react-native';
import Toast from 'react-native-simple-toast';
import haversine from 'haversine';
import firebase from 'react-native-firebase';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';

import NavigationManager from './react/navigation/NavigationManager';
import CustomModel from 'react-native-erp-mobile-library/react/component/CustomModel';
import { sessionInfo } from 'react-native-erp-mobile-library/index';
import { createListeners, clearListeners } from 'react-native-erp-mobile-library/react/notification/Listeners';
import BaseComponent from 'react-native-erp-mobile-library/react/screens/masters/BaseComponent';
import { createFieldActivityAgentTracking } from './react/controller/FieldworkController';
import { prepareFieldActivityTrackingRequest } from './react/screens/fieldwork/dbHelper/FieldWorkDBHelper';
import AppConstants from './react/constant/AppConstants';
import MasterConstants from './react/constant/MasterConstants';
import { GetAllCustomers } from 'react-native-erp-mobile-library/react/screens/customers/dbHelper/CustomerDB';
import { DateFormat, DATE_TIME_UI } from 'react-native-erp-mobile-library/react/common/Common';
import { updateActivity } from './react/screens/fieldwork/FieldworkActivityItemView';

export default class App extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: false,
      percentage: 0,
      title: '',
      session: sessionInfo({ type: 'get', key: 'loginInfo' }),
      tenantAddress: null,
    };
    this.isTrackingEnabled = false;
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    sessionInfo({ type: 'set', key: 'appState', value: this });
    sessionInfo({ type: 'set', key: AppConstants.keyDatabaseName, value: AppConstants.affordeBackOfficeDB });

    if (Platform.OS === 'android') {
      // // for show update alert dialog modal
      // ApkManager.getAppInformation('com.afforde.germisol').then((data) => {
      //   this.setState({ appVersion: data.versionName }, () => {
      //     sessionInfo({ type: 'set', key: 'appState', value: this });
      //   });
      // }).catch((error) => {
      //   console.log(error);
      // });
    }
  }

  async componentDidMount() {
    createListeners();
    this.checkPermission();
    this.startTracking();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  async checkPermission() {
    const enabled = firebase.messaging().hasPermission();
    enabled ? this.getToken() : this.requestPermission();
  }

  async getToken() {
    // openDatabase(AppConstants.affordeBackOfficeDB);
    firebase.messaging().getToken()
      .then(fcmToken => {
        if (fcmToken) {
          fcmToken = fcmToken || '';
        } else {
          // user doesn't have a device token yet
        }
      });
  }

  requestPermission() {
    try {
      firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }

  getTenantLocation() {
    return new Promise((resolve, reject) => {
      if (!this.state.tenantAddress) {
        const session = sessionInfo({ type: 'get', key: 'loginInfo' });
        if (session) {
          var filterTenantAddress = {
            conditions: [{
              column: 'customer_id',
              value: [session.info.tenantCustomerId],
              operator: 'in',
            },
            {
              column: 'address_type_id',
              value: [MasterConstants.billingAddress],
              operator: '=',
            }],
          };
          GetAllCustomers(session.databaseName, (datas) => {
            if (datas.length > 0) {
              const data = datas[0];
              this.state.tenantAddress = JSON.stringify({
                latitude: data.latitude,
                longitude: data.longitude,
              });
              resolve();
            } else { reject(); }
          }, filterTenantAddress);
        } else {
          reject();
        }
      } else {
        resolve();
      }
    });
  }

  componentWillUnmount() {
    clearListeners();
    this.stopTracking();
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    // Linking.removeEventListener('url', this._handleOpenURL);
  }

  handleBackButtonClick() {
    Alert.alert(
      'Exit App',
      'Exiting the application?',
      [
        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'OK', onPress: () => BackHandler.exitApp() },
      ],
      { cancelable: false }
    );
    return true;
  }

  startTracking() {
    if (this._interval) {
      this.stopTracking();
    }
    AsyncStorage.getItem(AppConstants.scheduler).then(data => {
      if (data) {
        const scheduler = JSON.parse(data);
        if (scheduler.isTrackingEnabled && scheduler.extraData) {
          this.trackLocation();
          if (scheduler.trackingType != MasterConstants.attendence) {
            const timing = scheduler.appConfig.tracking.fieldActivity.timing;
            this._interval = setInterval(() => {
              this.trackLocation();
              // every half an hour location fetch for fieldwork
            }, timing);

          } else {
            const timing = scheduler.appConfig.tracking.attendence.timing;
            this._interval = setInterval(() => {
              this.trackLocation();
              // every one minute location fetch for attendence
            }, timing);

          }
        }
      }
    });
  }

  trackLocation() {
    AsyncStorage.getItem(AppConstants.scheduler).then(data => {
      if (data) {
        const scheduler = JSON.parse(data);
        const session = sessionInfo({ type: 'get', key: 'loginInfo' });
        const distanceInMeters = scheduler.appConfig.tracking.attendence.meters;
        if (scheduler.isTrackingEnabled && scheduler.extraData) {
          this._fetchCurrentLocation()
            .then(location => {
              NetInfo.fetch()
                .then(isConnected => {
                  if (isConnected) {
                    if (scheduler.trackingType != MasterConstants.attendence) {
                      scheduler.extraData.activity['trackingStatus'] = MasterConstants.StatusInFieldWork;
                      this.callService(session, scheduler, location);
                    } else {
                      this.getTenantLocation().then(() => {
                        if (this.state.tenantAddress) {
                          let distanceFromSavedLocation = this.calcDistance(JSON.parse(this.state.tenantAddress), location);
                          if (distanceFromSavedLocation < distanceInMeters) {
                            if (scheduler.extraData.activity.statusId == MasterConstants.statusWorkClosedId) {
                              //nothing for work closed
                            } else {
                              if (!scheduler.status || scheduler.status == MasterConstants.StatusOutId) {
                                if (this.validatePastDateActivity(scheduler.extraData.activity.startTime)) {
                                  let endTime = scheduler.appConfig.tracking.fieldActivity.endTime;
                                  let concatDateTime = `${DateFormat(new Date(scheduler.extraData.activity.startTime), DATE_TIME_UI)}T${endTime}`;
                                  let activity = scheduler.extraData.activity;
                                  activity['endTime'] = concatDateTime;
                                  activity['statusId'] = MasterConstants.statusWorkClosedId;
                                  activity['statusName'] = MasterConstants.statusWorkClosedName;
                                  activity['endLocation'] = {
                                    longitude: location.longitude,
                                    latitude: location.latitude,
                                    altitude: 0,
                                    accuracy: 0,
                                    bearing: 0,
                                    verticalAccuracyMeters: 0,
                                    provider: 0,
                                  };
                                  updateActivity(activity, session).then(() => {
                                    clearInterval(this._interval);
                                    this._interval = null;
                                    AsyncStorage.setItem(AppConstants.scheduler, null);
                                  });
                                } else {
                                  scheduler.extraData.activity['trackingStatus'] = MasterConstants.StatusInId;
                                  scheduler['status'] = MasterConstants.StatusInId;
                                  this.callService(session, scheduler, location);
                                  // AsyncStorage.setItem(AppConstants.scheduler, JSON.stringify(scheduler));
                                }
                              }
                            }
                          } else {
                            if (!scheduler.status || scheduler.status == MasterConstants.StatusInId) {
                              scheduler.extraData.activity['trackingStatus'] = MasterConstants.StatusOutId;
                              scheduler.status = MasterConstants.StatusOutId;
                              this.callService(session, scheduler, location);
                              // AsyncStorage.setItem(AppConstants.scheduler, JSON.stringify(scheduler));
                            }
                          }
                        }
                      });
                    }
                  } else {
                    Toast.show('make internet connection!', Toast.SHORT);
                  }
                });
            });
        }
      } else {
        this.setState({ isPreparing: false, isLoading: false, loadMore: false });
      }
    }).catch(error => {
      console.log('error code :' + error);
    });
  }

  callService(session, scheduler, location) {
    createFieldActivityAgentTracking(session,
      JSON.parse(prepareFieldActivityTrackingRequest(scheduler.extraData.activity, location)))
      .then((res) => {
        if (res) {
          if (scheduler.extraData.activity.statusId == MasterConstants.statusWorkClosedId) {
            AsyncStorage.setItem(AppConstants.scheduler, null);
          } else {
            AsyncStorage.setItem(AppConstants.scheduler, JSON.stringify(scheduler));
          }
        }
      });
  }

  calcDistance = (prevLatLng, newLatLng) => {
    return haversine(prevLatLng, newLatLng) || 0;
  };

  validatePastDateActivity(startTime) {
    const activityDate = DateFormat(new Date(startTime), DATE_TIME_UI);
    const currentDate = DateFormat(new Date(), DATE_TIME_UI);
    if (activityDate !== currentDate) {
      return true;
    }
    return false;
  }

  stopTracking(scheduler) {
    clearInterval(this._interval);
    this._interval = null;
    if (scheduler && scheduler.isTrackingEnabled) {
      this._fetchCurrentLocation()
        .then(location => {
          NetInfo.fetch()
            .then(isConnected => {
              if (isConnected) {
                if (scheduler.trackingType == MasterConstants.attendence) {
                  const session = sessionInfo({ type: 'get', key: 'loginInfo' });
                  scheduler.extraData.activity['trackingStatus'] = MasterConstants.StatusOutId;
                  scheduler.status = MasterConstants.StatusOutId;
                  this.callService(session, scheduler, location);
                } else {
                  //
                }
              } else {
                Toast.show('make internet connection!', Toast.SHORT);
              }
            });
        });
    }
  }

  render() {
    const prefix = 'backoffice://';
    return (
      <MenuProvider>
        <View style={styles.container}>
          <NavigationManager uriPrefix={prefix} />
          <CustomModel
            visibleModal={sessionInfo({ type: 'get', key: 'appState' }).state.visibleModal}
            title={sessionInfo({ type: 'get', key: 'appState' }).state.modelTitle}
            percentage={sessionInfo({ type: 'get', key: 'appState' }).state.percentage} />
        </View>
      </MenuProvider >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
});

AppRegistry.registerComponent('erp_backoffice', () => App);
